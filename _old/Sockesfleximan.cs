﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Net.Sockets;

//using System.Globalization.NumberStyles;

public class Sockesfleximan : MonoBehaviour
{
	internal Boolean socketReady = false;

	TcpClient mySocket;

	NetworkStream theStream;

	StreamWriter theWriter;

	StreamReader theReader;

	String Host = "localhost";

	Int32 Port = 5333;



	void Update ()
	{
		string receivedText = readSocket ();
	}

	void OnGUI ()
	{
		if (!socketReady) { 
			if (GUILayout.Button ("Connect")) {
				setupSocket ();
				writeSocket ("serverStatus:");
			}
		}

		// if (GUILayout.Button("Send"))
		// {
		//     writeSocket("test string");
		// }

		// if (GUILayout.Button("Close"))
		// {
		//     closeSocket();
		// }
		//GUI.Label(new Rect(0, 40, 1000, 400), tekst);
		if (socketReady) {  
				}
	}



	void OnApplicationQuit ()
	{

		closeSocket ();

	}



	public void setupSocket ()
	{

		try {

			mySocket = new TcpClient (Host, Port);

			theStream = mySocket.GetStream ();

			theWriter = new StreamWriter (theStream);

			theReader = new StreamReader (theStream);

			socketReady = true;

		} catch (Exception e) {

			Debug.Log ("Socket error: " + e);

		}

	}



	public void writeSocket (string theLine)
	{

		if (!socketReady)
			return;

		String foo = theLine + "\r\n";
		char[] buffer = new char[]{(char)0,(char)0,(char)0,(char)0,(char)0,(char)6,(char)0,(char)4,
			(char)128,(char)0,(char)0,(char)6};
		theWriter.Write (buffer);

		theWriter.Flush ();
		//Debug.Log();
	}



	public String readSocket ()
	{

		if (!socketReady)
			return "";

		if (theStream.DataAvailable)
			return theReader.ReadLine ();

		//return theReader.ReadToEnd();

		return "";

	}



	public void closeSocket ()
	{

		if (!socketReady)
			return;

		theWriter.Close ();

		theReader.Close ();

		mySocket.Close ();

		socketReady = false;

	}



	private byte HexToByte (string hexVal)
	{

		if (hexVal == "0")
			return (0);
		else if (hexVal == "1")
			return (1);
		else if (hexVal == "2")
			return (2);
		else if (hexVal == "3")
			return (3);
		else if (hexVal == "4")
			return (4);
		else if (hexVal == "5")
			return (5);
		else if (hexVal == "6")
			return (6);
		else if (hexVal == "7")
			return (7);
		else if (hexVal == "8")
			return (8);
		else if (hexVal == "9")
			return (9);
		else if (hexVal == "A")
			return (10);
		else if (hexVal == "B")
			return (11);
		else if (hexVal == "C")
			return (12);
		else if (hexVal == "D")
			return (13);
		else if (hexVal == "E")
			return (14);
		else if (hexVal == "F")
			return (15);
		else
			return (0);

	}

}

