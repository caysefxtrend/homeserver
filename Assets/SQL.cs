﻿using UnityEngine;
using UnityEngine.UI;
using System.Data.SqlClient;

public class SQL : MonoBehaviour
{	
	public Text textUI;
	//public Text textUI_DB;
	public InputField inputField;
	public InputField inputInterval;
	public SqlConnection conn;
	public string connString;
	public float connect_interval = 60;
	private float lastConnectTime = 0;
	public string log;
	private string dbIP = "127.0.0.1,1433";
	private bool errorLastTime = false;
	private int currentRowIndex = 0;
	// Use this for initialization
	void Start () {
		if ( inputField != null ) 
		{
			inputField.text = dbIP;
		}
		if ( inputInterval != null ) 
		{
			inputInterval.text = connect_interval.ToString();
		}
		//StartConnection();
	}

	public void SetInterval()
	{
		if ( inputInterval != null ) float.TryParse( inputInterval.text, out connect_interval );
	}
	
	// Update is called once per frame
	void Update () {
		if ( Time.time - lastConnectTime > connect_interval ) // every second
		{
			int last = GetLastRowIndex();
			if ( currentRowIndex == 0 ) currentRowIndex = last;
			else if ( last <= currentRowIndex ) {}
			else if ( ReadRows(currentRowIndex+1,last) ) currentRowIndex = last;
			//StartConnection();
			lastConnectTime = Time.time;
		}
	}

	private bool ReadRows(int begin, int end)
	{
		
		if ( !Connect() ) return false;

		string queryString = 
			"SELECT idAutoEvents,tDesc,tFullName FROM dbo.tblEvents WHERE idAutoEvents BETWEEN "+begin+" AND "+end+";";

		SqlCommand command = new SqlCommand(queryString, conn);
		SqlDataReader reader = command.ExecuteReader();
		bool ok = true;
		try
		{
			while (reader.Read())
			{
				string result = "";
				for ( int i = 0; i < reader.FieldCount; i++ )
					result += "["+i+"]["+reader[i].ToString().Length+"]="+reader[i].ToString()+", ";
				Debug.Log("SQL.Read["+begin+","+end+"]: "+result);
				if ( reader[2].ToString() == "PIN Code: 3434" )
					SwitchLight();
				//Debug.Log(string.Format("{0}, {1}",reader.FieldCount, reader[0]));
				//Debug.Log(string.Format("{0}, {1}",reader[0], reader[1]));
			}
		}
		catch { ok = false; }
		finally
		{
			// Always call Close when done reading.
			reader.Close();
		}

		Disconnect("read rows "+begin+"-"+end);	
		return ok;
	}

	public void SwitchLight()
	{
		if ( Master.GetDeviceByName("Подсветка малого дома").
			GetValueByName("Включено") == "0" )
		{
			Debug.Log("SQL.SwitchLight: Подсветка малого дома"+"  Включено=1");
			Master.OnClientSetParameter( "Подсветка малого дома", "Включено=1", null );
			Debug.Log("SQL.SwitchLight: Подсветка ворот"+"  "+"Включено=1");
			Master.OnClientSetParameter( "Подсветка ворот", "Включено=1", null );
			Debug.Log("SQL.SwitchLight: Подсветка калитки"+"  "+"Включено=1");
			Master.OnClientSetParameter( "Подсветка калитки", "Включено=1", null );
		}
		else
		{
			Debug.Log("SQL.SwitchLight: Подсветка малого дома"+"  Включено=0");
			Master.OnClientSetParameter( "Подсветка малого дома", "Включено=0", null );
			Debug.Log("SQL.SwitchLight: Подсветка ворот"+"  "+"Включено=0");
			Master.OnClientSetParameter( "Подсветка ворот", "Включено=0", null );
			Debug.Log("SQL.SwitchLight: Подсветка калитки"+"  "+"Включено=0");
			Master.OnClientSetParameter( "Подсветка калитки", "Включено=0", null );
		}
	}

	private int GetLastRowIndex()
	{
		int readedIndex = 0;
		if ( !Connect() ) return 0;
		string queryString = 
			"SELECT TOP 1 idAutoEvents FROM dbo.tblEvents ORDER BY idAutoEvents DESC;";

		//			"SELECT idAutoEvents, CustomerID FROM dbo.tblEvents;";
		SqlCommand command = new SqlCommand(queryString, conn);
		SqlDataReader reader = command.ExecuteReader();

		try
		{
			if ( reader.Read() )
			{
				if ( int.TryParse(reader[0].ToString(), out readedIndex) )
				{
			//		Debug.Log("SQL.Read: last index = "+readedIndex);
				}
			}
		}
		finally
		{
			// Always call Close when done reading.
			reader.Close();
		}
		conn.Close();
		errorLastTime = false;
//		Disconnect("readIndex="+readedIndex);
		return readedIndex;
	}

	private void Disconnect(string status)
	{
		conn.Close();
		SetStatus(status);
		errorLastTime = false;
	}

	private bool Connect()
	{
		connString = "";
		if ( inputField != null ) connString += "Server="+inputField.text+";";
		else connString += "Server="+dbIP + ";";
		connString += 
			"Database=AxTrax1;" +
			"User ID=sa;" +
			"Password=sa;";
		try {
			conn = new SqlConnection(connString);
			conn.Open();
		} catch(System.Exception exc) 
		{ 
			if ( !errorLastTime ) Debug.LogWarning(connString+" _ "+exc.Message);
			log = "("+Time.time+") "+inputField.text +" : "+exc.Message; 
			SetStatus( log );
			errorLastTime = true;
			return false; 
		}
		return true;
	}

	public void StartConnection()
	{
		connString = "";
		if ( inputField != null ) connString += "Server="+inputField.text+";";
		else connString += "Server="+dbIP + ";";
		connString += 
			"Database=AxTrax1;" +
			"User ID=sa;" +
			"Password=sa;";
		try {
			conn = new SqlConnection(connString);
			conn.Open();
		} catch(System.Exception exc) 
		{ 
			if ( !errorLastTime ) Debug.LogWarning(connString+" _ "+exc);
			log = inputField.text +" : "+exc.Message; 
			SetStatus( log );
			errorLastTime = true;
			return; 
		}

		/// dbo.tblEvents rows:
		/// idAutoEvents: 1,2,3, ... X
		/// idEmpSlot: 0,1
		/// tDesc: 1\Panel 1\Reader 1
		/// dtEventReal: 01.12.2016 8:12:30
		/// IdReader: 1
		/// IdDoor: 0
		/// IdPanel: 1
		/// iEventType: 19,97
		/// iEventSubType: 1,5
		/// iEventSource: 33,34,35
		/// IdCardSlot: 0
		/// eAlarmHandler: 0
		/// bCameraPopup: false
		/// dtEventUpload: 01.12.2016 8:12:31
		/// IdEmpNum: 0,1
		/// iEventNum: 	1...14410,
		/// IdInput: 0
		/// IdOutput: 0
		/// iSiteCode: null
		/// iCardCode: 0
		/// tFullName: Test Test, PIN Code:, PIN Code: 97878
		/// CameraId: 0
		/// wCarParkingSubGroup: 0
		/// iSiteCodeInt: 0


		//IDbCommand dbcmd = CreateCommand();// = new IDbCommand();// IDbCommand. .CreateCommand();
		//IDbCommand..cre

		string queryString = 
			"SELECT TOP 1 idAutoEvents FROM dbo.tblEvents ORDER BY idAutoEvents DESC;";

//			"SELECT idAutoEvents, CustomerID FROM dbo.tblEvents;";
		SqlCommand command = new SqlCommand(queryString, conn);
		SqlDataReader reader = command.ExecuteReader();
		//reader.
		try
		{
			while (reader.Read())
			{
				string result = "";
				for ( int i = 0; i < reader.FieldCount; i++ )
					result += "["+i+"]="+reader[i].ToString()+", ";
				Debug.Log("SQL.Read: "+result);
				//Debug.Log(string.Format("{0}, {1}",reader.FieldCount, reader[0]));
				//Debug.Log(string.Format("{0}, {1}",reader[0], reader[1]));
			}
		}
		finally
		{
			// Always call Close when done reading.
			reader.Close();
		}

		conn.Close();
		SetStatus("connected");
		errorLastTime = false;
	}

	private void SetStatus( string status )
	{
		if ( textUI != null ) 
		{
			textUI.text = "AxTrax1: "+status;
		}
	}

}
