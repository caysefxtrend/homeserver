﻿using UnityEngine;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Collections.Generic;

public class ClientServerSimple : MonoBehaviour
{
    public static ClientServerSimple instance;
    Socket listener;
    public int listenPort = 24;
    public int maximumConnections = 4;
    public List<StateObject> connections = new List<StateObject>();
    public int stateIdCounter = 0;
    public bool setShowLog = false;
    public Encoding encoding = Encoding.UTF8;
    public bool skipFrameAfterSend = false;
    public bool waitToEndSend = false;
    [System.NonSerialized] public bool encodeToASCII = true;
    public bool useProtocolSimple = true;
    public int sendTimeoutMS = 10000;
    public bool isStateAdded = false;
    // Use this for initialization
    Thread acceptThread;

    void Awake()
    {
        instance = this;
    }
    void Start()
    {
    }


    void OnEnable()
    {
    }
    void OnDisable()
    {
        for (int i = connections.Count - 1; i >= 0; i--) connections[i].Close();
        connections.Clear();
        listener.Listen(0);
        listener.Close();
        listener = null;
    }

    void Update()
    //void FixedUpdate()
    {
        if (connections == null) { gameObject.SetActive(false); return; }

        if (listener == null) StartListening();
        if (isStateAdded)
        {
            isStateAdded = false;
            for (int i = connections.Count - 1; i >= 0; i--)
            {
                StateObject state = connections[i];
                if (!state.isNew) continue;
                state.isNew = false;
                for (int j = connections.Count - 1; j >= 0; j--)
                {
                    StateObject state2 = connections[j];
                    if (state != state2 && state.remoteIP == state2.remoteIP)
                    {
                        state2.isMustClosed = true;
                        state2.status = " state doubled ";
                    }
                }
            }
        }

        for (int i = connections.Count - 1; i >= 0; i--)
        {
            if (connections[i] == null)
            {
                Debug.LogError("connection["+i+"] is null !?, connections="+connections.Count);
                connections.RemoveAt(i);
            }
            else if (connections[i].isMustClosed) CloseConnetion(connections[i]);
        }

        for (int i = connections.Count - 1; i >= 0; i--)
        {
            ConcurrentList<Device.Message> list = connections[i].device.messagesSimple;
            if (list.Count == 0) continue;
            int max = list.Count;
            for (int j = max - 1; j >= 0; j--)
                if (list[j] == null) Debug.LogWarning("AddCmd: client=" + connections[i].device.GetValueByName("Название") + " have message index=" + j + "(max=" + max + ") is null, total messages=" + list.Count);
        }

        foreach (StateObject state in connections)
        {
            state.IsConnected();
            //if ( state.isClosed ) continue;
            if (state.isMustClosed) continue;
            if (state.workSocket == null) continue;
            if (state.workSocket.Connected == false) continue;
            if (state.device == null) continue;
            if (state.device.messagesSimple == null) continue;
            int count = state.device.messagesSimple.Count;
            state.messages = count;
            if (count > 0)
            {
                if (skipFrameAfterSend) count = 1;
                for (int i = 0; i < count; i++)
                {
                    if (waitToEndSend && state.onSend) break;
                    bool toSend = true;

                    //					if ( state.isClosed || state.device.messages.Count == 0 )
                    //					{
                    //						Debug.LogWarning(""+state+" isClosed="+state.isClosed+", i="+i+", state.device.messages="+state.device.messages);
                    //						break;
                    //					}
                    Device.Parameter par = null;
                    try {
                        ConcurrentList<Device.Message> _msgs = state.device.messagesSimple;
                        Device.Message _msg = _msgs[0];
                        if (_msg == null)
                        {
                            Debug.LogWarning("" + state + ", device=" + state.device.GetValueByName("Название") + ", message[0](" + i + "<" + count + ")=null" + ", state.device.messagesSimple.Count=" + state.device.messagesSimple.Count);
                            state.device.messagesSimple.RemoveAt(0);
                            continue;
                        }
                        par = _msg.par;
                    } catch (Exception exc)
                    {
                        Debug.LogWarning("" + state + " isMustClosed=" + state.isMustClosed + ", i=" + i + ", state.device.messagesSimple.Count=" + state.device.messagesSimple.Count + ", exc=" + exc);
                        break;
                    }
                    if (par != null)
                        for (int j = 1; j < state.device.messagesSimple.Count; j++)
                        {
                            if (state.device.messagesSimple[j].par == par) { toSend = false; break; }
                        }
                    
                    if (toSend)
                    {
                        if (state.device.messagesSimple[0].simpleCmd.Count > 0)
                            SendSimple(state, state.device.messagesSimple[0].simpleCmd);
                        state.messageId++;
                    }
                    //Send(state, state.device.messages[0]);
                    state.device.messagesSimple.RemoveAt(0);
                }
            }
        }
    }

    public static void CloseConnetion(StateObject state)
    {
        instance.connections.Remove(state);
        state.Close();
    }

    public void StartListening() {
        // Data buffer for incoming data.
        //byte[] bytes = new Byte[1024];
        try {
            // Establish the local endpoint for the socket.
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            // The DNS name of the computer
            // running the listener is "host.contoso.com".
            //IPHostEntry ipHostInfo = Dns.Resolve(Dns.GetHostName());
            //		IPAddress ipAddress = ipHostInfo.AddressList[0];
            string total = "";
            foreach (IPAddress ip in ipHostInfo.AddressList) total += ip + ", ";
            Debug.Log("ipHostInfo" + ipHostInfo + ", ipAddress[" + ipHostInfo.AddressList.Length + "]=[" + total + "], IPAddress.Loopback=" + IPAddress.Loopback + ", IPAddress.Broadcast=" + IPAddress.Broadcast);
        } catch (Exception ex) { Debug.LogWarning("Possibly not connected to network err=" + ex); }

        IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, listenPort);
        // Create a TCP/IP socket.
        listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        //listener
        // Bind the socket to the local endpoint and listen for incoming connections.
        try {
            listener.Bind(localEndPoint);
            //listener.Listen(maximumConnections*2); // непомогло - // возможно когда мы удаляем сокет и создаём новый - новый не успевает получить разрешение на слушание
            listener.Listen((int)SocketOptionName.MaxConnections);
        } catch (Exception e) { Debug.LogError(e.ToString()); }

        acceptThread = new Thread(new ThreadStart(ExecuteAccept));
        acceptThread.Start();
    }

    private void ExecuteAccept()
    {
        while (true)
        {
            if (listener == null) { Debug.LogWarning("listener is null"); break; }
            Socket socket = null;
            try
            {
                socket = listener.Accept();
                if (connections.Count >= maximumConnections)
                {
                    if (socket != null) socket.Close();
                    continue;
                }
            }
            catch (SocketException exc)
            {
                if (socket != null) socket.Close();
                Debug.LogWarning("ExecuteAccept: code=" + exc.ErrorCode + ", err=" + exc.SocketErrorCode + ", exc=" + exc);
                continue;
            }
            catch (Exception exc)
            {
                if (socket != null) socket.Close();
                Debug.LogWarning("ExecuteAccept: exc=" + exc);
                continue;
            }
            if (socket == null) continue;
            StateObject state = null;
            try
            {
                state = StateCreate(socket);
            }
            catch (Exception exc)
            {
                Debug.LogWarning("ExecuteAccept.StateCreate: exc=" + exc);
            }
            if (state == null) continue;
            StateBeginReceive(state);
        }
    }

    private StateObject StateCreate(Socket socket)
    {
        StateObject state = new StateObject(stateIdCounter); stateIdCounter++;
        state.listenerSocket = listener;
        state.workSocket = socket;
        state.status = "success accept";
        IPEndPoint remoteEndPoint = (IPEndPoint)state.workSocket.RemoteEndPoint;
        state.remoteIP = remoteEndPoint.Address.ToString();

        state.device = Master.GetOrCreateClientByIP(state.remoteIP);
        //ClientControl.AddCmd(state.device, null, null, Device.Command.YourIP, state.remoteIP);
        /// тут проблема
        /// если клиент зарегистрирован и был офлайн то он накопит кучу
        /// сообщений от других клиентов
        /// и тогда когда клиент подключиться с желанием скачать базу
        /// то он сначала получит кучу ненужных теперь ему сообщений
        /// а только потом базу - это не правильно ! TODO
        connections.Add(state);
        isStateAdded = true;
        Debug.Log(state + " is new");
        return state;
    }

    private void StateBeginReceive(StateObject state)
    {
        try {
            if (state.isMustClosed) throw new UnityException("state is close before BeginReceive");
            state.status = "begin receive";
            state.workSocket.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                new AsyncCallback(ReadCallback), state);
        }
        catch (SocketException exc)
        {
            state.isMustClosed = true;
            Debug.LogWarning("" + state + ", StateBeginReceive: code=" + exc.ErrorCode + ", err=" + exc.SocketErrorCode + ", exc=" + exc);
        }
        catch (Exception exc)
        {
            state.isMustClosed = true;
            Debug.LogWarning("" + state + ", StateBeginReceive: exc=" + exc);
        }
    }

    private void ReadCallback(IAsyncResult ar) {
        StateObject state = (StateObject)ar.AsyncState;
        try {
            // Retrieve the state object and the handler socket from the asynchronous state object.
            if (state.isMustClosed) return;
            Socket handler = state.workSocket;
            if (handler == null) throw new Exception("handler is null");
            // Read data from the client socket. 
            state.status = "receive fail ?";
            int bytesRead = handler.EndReceive(ar);
            state.status = "receive " + bytesRead + " bytes";
            if (bytesRead <= 0)
            {
                // thats mean stream from client is stopped
                state.isMustClosed = true;
                state.status = " stream stopped ";
                return;
            }

            var list = new List<byte>(state.buffer);
            list.RemoveRange(bytesRead,state.buffer.Length-bytesRead);

            var log = ClientControl.SimpleFromClient(state.device, state.remoteIP, list);
            if (!string.IsNullOrEmpty(log)) Debug.LogError("SimpleFromClient: "+log);

            if (instance.setShowLog)
            {
                string res = ""; for (int i = 0; i < bytesRead; i++) res += state.buffer[i].ToString() + ", ";
                Debug.Log(state + ".ReadCallback: read [bytes=" + bytesRead + "]. Continue receiving. Msg=" + res);
            }

            StateBeginReceive(state);
        }
        catch (SocketException exc)
        {
            state.isMustClosed = true;
            Debug.LogWarning("" + state + ", ReadCallback: code=" + exc.ErrorCode + ", err=" + exc.SocketErrorCode + ", exc=" + exc);
        }
        catch (Exception exc)
        {
            state.isMustClosed = true;
            Debug.LogWarning("" + state + ", ReadCallback: exc=" + exc);
        }
    }

    private static void SendSimple(StateObject state, List<byte> data)
    {
        try
        {
            if (state.isMustClosed) return;
            state.onSend = true;
            state.sendSaved = "simple data";

            if (instance.setShowLog)
                Debug.Log("" + state + ".Send(" + Time.time + "): data=" + data);
            byte[] byteData = data.ToArray();
            state.status = "begin send simple";
            state.workSocket.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(instance.SendCallback), state);
        }
        catch (SocketException exc)
        {
            state.isMustClosed = true;
            Debug.LogWarning("" + state + ", Send: code=" + exc.ErrorCode + ", err=" + exc.SocketErrorCode + ", exc=" + exc);
        }
        catch (Exception exc)
        {
            state.isMustClosed = true;
            Debug.LogWarning("" + state + ", Send: exc=" + exc);
        }
    }    

	private void SendCallback(IAsyncResult ar) {
		StateObject state = (StateObject) ar.AsyncState;
		try {
			if ( state.isMustClosed ) return;
			Socket handler = state.workSocket;
			if ( handler == null ) throw new Exception("handler is null");
			// Complete sending the data to the remote device.
			state.status = "send fail ?";
			int bytesSent = handler.EndSend(ar);	
			state.status = "send "+bytesSent+" bytes";

			if ( bytesSent <= 0 ) Debug.LogWarning(state+".Sent "+bytesSent+" bytes to client." );
			if (instance.setShowLog)
				Debug.Log(state+".SendCallback "+bytesSent+" bytes to client." );
			state.onSend = false;
		} 		
		catch (SocketException exc)
		{
			state.isMustClosed = true;
			Debug.LogWarning(""+state+", SendCallback: code="+exc.ErrorCode+", err="+exc.SocketErrorCode+", exc="+exc);
		}
		catch (Exception exc)
		{
			state.isMustClosed = true;
			Debug.LogWarning(""+state+", SendCallback: exc="+exc);
		}
	}

	public static bool IsClientIpOnline(string remoteIP)
	{
		foreach(StateObject state in instance.connections)
		{
			if ( !state.isMustClosed && state.isConnected && state.remoteIP == remoteIP ) return true;
		}
		return false;
	}

	public static void RemoveConnectionByIP(string ip)
	{
		foreach(StateObject state in instance.connections)
		{
			if ( state.remoteIP == ip ) state.isMustClosed = true;
		}
	}

	public static string GetConnectionInfo()
	{
		string result = "";
		List<StateObject> conns = instance.connections;
		for(int i = conns.Count-1; i>=0; i--)
		{			
			result += "\n     "+conns[i].ToString();
		}
		return result;
	}

}