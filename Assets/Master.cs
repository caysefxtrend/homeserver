﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;


public class Master : MonoBehaviour
{
	public static string clientVersion = "1.37";
	public static bool isOnInit = false;
	public float inetResetOneAfterMinutes = 15;
	private bool resetOneCompleted = false;

	//public float inetReset1AfterMinutes = 15;
	public static Master instance;
	//public bool loadOnlyFirstFloor = true;
	bool loadDefault = true;
	public bool isChanged = false;
//	public int addTestDevicesCount = 1000;
	public const string deviceTypes = "CLIENT,SERVER,APPLETV,SAMSUNGTV,ROOM,SCENARIO,CURTAIN,DIMMER,DMX,DENON_AVR,MEDIA,TRIGGER,SENSOR,BERKER,DOOR,LOCK,HVAC";
	private string dataPath = "";
	public static string Datapath { get { return Master.instance.dataPath; } }

	public static bool IsChanged
	{ 
		get { 
			if ( Master.instance == null ) return false; 
			else return Master.instance.isChanged; 
		} 
		set {
			if ( Master.instance != null ) Master.instance.isChanged = value; 
		}
	}

	public static List<Device> Devices 
	{ get 
		{ 
			if ( Master.instance == null ) return new List<Device>(); 
		 	else return Master.instance.devices; 
		} 
	}
	public List<Device> devices = new List<Device>();

	//public List<string> deviceTypeNames = new List<string> {"Invalid", "DIMMER", "Light Switch", 
	//	"UBIQ", "Sensor", "SB", "Curtain", "Zone",
	//	"DMX", "DENON_AVR", "Wintek", "HVAC","LOCK","BTrigger","LG TV"};

	//public List<Connection> connections = new List<Connection>();
	//public static List<Connection> Connections { get { 
	//		if ( Master.instance == null ) throw new UnityException("Master is not loaded!");
	//		else return Master.instance.connections; 
	//} }

	void Awake ()
	{
		Master.instance = this;
		dataPath = Application.dataPath;
		Utility2.SelfTest();
//		string testSymbols = "абеёийуфхцчшщьъэюяАБЕЁИЙФХЦЧШЩЬЪЭЮЯ";
		// а=1072, б=1073, е=1077, ё=1105, и=1080, й=1081, у=1091, ф=1092, х=1093, ц=1094, 
		// ч=1095, ш=1096, щ=1097, ь=1100, ъ=1098, э=1101, ю=1102, я=1103, А=1040, Б=1041, 
		// Е=1045, Ё=1025, И=1048, Й=1049, Ф=1060, Х=1061, Ц=1062, Ч=1063, Ш=1064, Щ=1065, 
		// Ь=1068, Ъ=1066, Э=1069, Ю=1070, Я=1071, 
		// 1025(Ё)-1105(ё) -> ascii 128-208
//		char[] charArray = testSymbols.ToCharArray();
//		string result = "";
//		for(int i=0;i<charArray.Length;i++)
//			result += ""+charArray[i]+"="+(int)charArray[i]+", ";
//		Debug.Log(result);

		if (loadDefault)
		{
			List<string> stringsList = new List<string>();
			// default server
			stringsList = SERVER.GetDefaultDevices(); // Id: [1]
			foreach(string element in stringsList) AddDevice( element );
			// default rooms
			stringsList = ROOM.GetDefaultDevices(); // Id: [10000-10030]
			foreach(string element in stringsList) AddDevice( element );
			// default clients
			stringsList = CLIENT.GetDefaultDevices(); // todo empty
			//foreach(string element in stringsList) AddDevice( element );
			
			// default certains
			stringsList = CURTAIN.GetDefaultDevices(); // id: [900-918] [400-408]
			foreach(string element in stringsList) AddDevice( element );
			// default dmx
			stringsList = DMX.GetDefaultDevices(); // id: [800-856],[301-314]
			foreach(string element in stringsList) AddDevice( element );
			// default dimmers
			stringsList = DIMMER.GetDefaultDevices(); // id: [1000-1015]
			foreach(string element in stringsList) AddDevice( element );
			// default triggers
			stringsList = TRIGGER.GetDefaultDevices(); // id: [101-119], [130-174], [200-229], [500-678]
			foreach(string element in stringsList) AddDevice( element );
			// default denons
			stringsList = DENON_AVR.GetDefaultDevices(); // todo no id
			//foreach(string element in stringsList) AddDevice( element );
			// default sensors
			stringsList = SENSOR.GetDefaultDevices(); // todo no id
			//foreach(string element in stringsList) AddDevice( element );
			// default locks
			stringsList = LOCK.GetDefaultDevices(); // todo no id
			//foreach(string element in stringsList) AddDevice( element );
			// default berkers
			stringsList = BERKER.GetDefaultDevices(); // todo no id
			//foreach(string element in stringsList) AddDevice( element );
			// default media
			stringsList = MEDIA.GetDefaultDevices(); // todo no id
	//		foreach(string element in stringsList) AddDevice( element );
			// default samsungtv
			stringsList = SAMSUNGTV.GetDefaultDevices(); // todo no id
			//foreach(string element in stringsList) AddDevice( element );
			// default appletv
			stringsList = APPLETV.GetDefaultDevices(); // todo no id
			//foreach(string element in stringsList) AddDevice( element );
			// default hvac
			stringsList = HVAC.GetDefaultDevices(); // todo no id
			//foreach(string element in stringsList) AddDevice( element );
			// default doors
			stringsList = DOOR.GetDefaultDevices(); // todo no id
			//foreach(string element in stringsList) AddDevice( element );
			stringsList = SCENARIO.GetDefaultDevices(); // id: [1100-1100]
			foreach(string element in stringsList) AddDevice( element );

			// string scenario = "Id=1010,Тип=SCENARIO,Комната=Детская"+
			// 	",Название=некий странный сценарий,Включено=0"+
			// 	",Команда 01=некий триггер.Включено=0";
			//AddDevice(scenario);
			
			// string scenario2 = "Тип=SCENARIO,Комната=Детская"+
			// 	",Название=некий тяжёлый сценарий,Включено=0";
			// for(int i=1; i <80;i++) 
			// 	scenario2+=",Команда "+string.Format ("{0:00}",i)+"=некий триггер.Включено=0";
			// string homeScenario = "Тип=SCENARIO,Комната=Детская"+
			// 	",Название=сценарий для экрана,Включено=0"+
			// 	",экранная кнопка(percent)=2"+
			// 	",Команда 01=некий триггер.Включено=0";
			// string homeScenario2 = "Тип=SCENARIO,Комната=Детская"+
			// 	",Название=сценарий для экрана 2,Включено=0"+
			// 	",экранная кнопка(percent)=0"+
			// 	",Команда 01=некий триггер.Включено=0";
			// rooms
	
			//AddDevice(dimmer);
		//	AddDevice(trigger);
		//	AddDevice(denon);
		//	AddDevice(sensor);
			
			//AddDevice(scenario2);			
			//AddDevice(homeScenario);
			//AddDevice(homeScenario2);

			//string deviceTypes = "DIMMER,DMX,DENON_AVR,TRIGGER,SENSOR,BERKER6,BERKER8";
			//string userNames = "Витя,Семён Семёныч,Дядя Толя,Админ,кошка";
		//	string ipadNames = "Айпад 1, Айпад 2, Айпад 3, Айпад 4";
		
//			string berker6 = "Тип=BERKER6,Комната=Детская"+
//				",Название=беркер 1,Включено=0"+",Адрес(percent)=1"+
//				",Команда 1=некий триггер.Включено)=0"+
//				",Команда 2="+	",Команда 3="+	",Команда 4="+
//				",Команда 5="+	",Команда 6=";
//			AddDevice(berker6);
//			string berker8 = "Тип=BERKER8,Комната=Детская"+
//				",Название=беркер 2,Включено=0"+",Адрес(percent)=1"+
//				",Команда 1=некий триггер.Включено)=0"+
//				",Команда 2="+	",Команда 3="+	",Команда 4="+
//				",Команда 5="+	",Команда 6="+
//				",Команда 7="+	",Команда 8=";
//			AddDevice(berker8);
	//		string lock1 = "Тип=LOCK,Комната=Детская"+
	//			",Название=замок 1,Включено=0,Адрес(percent)=1"+
	//			",Команда=Открыть-Закрыть";
	//		AddDevice(lock1);
	//		string samsung_tv = "Тип=SAMSUNG_TV,Комната=Детская"+
	//			",Название=некий телевизор 1,Включено=1"+
	//			",громкость(percent)=0,адрес(percent)=1";
	//		AddDevice(samsung_tv);
			
			string hvac = "Тип=HVAC,Комната=Детская"+
				",Название=некий кондиционер 1,Включено=1"+
				",адрес(percent)=1";
			//AddDevice(hvac);

			/*/ connections
			// 117-button[1], 3445-switch[0-1], 1233-slider[0-100], 1231-color[0-255,0-255,0-255]
            // триггеры гостиная
            connections.Add(new Connection() { Id = 101, Device = "Шина прожекторов - над камином 1", Component = "Включено" });
            connections.Add(new Connection() { Id = 102, Device = "Шина прожекторов - над камином 2", Component = "Включено" });
            connections.Add(new Connection() { Id = 103, Device = "Шина прожекторов - над камином 3", Component = "Включено" });
            connections.Add(new Connection() { Id = 104, Device = "Шина прожекторов - над диваном 2", Component = "Включено" });
            connections.Add(new Connection() { Id = 105, Device = "Шина прожекторов - над диваном 2", Component = "Включено" });
            connections.Add(new Connection() { Id = 106, Device = "* Шина прожекторов - над диваном 3", Component = "Включено" });
            connections.Add(new Connection() { Id = 107, Device = "Шина прожекторов - над столом 1", Component = "Включено" });
            connections.Add(new Connection() { Id = 108, Device = "Шина прожекторов - над столом 2", Component = "Включено" });
            connections.Add(new Connection() { Id = 109, Device = "Шина прожекторов - над столом 3", Component = "Включено" });
            connections.Add(new Connection() { Id = 110, Device = "Подсветка окон сверху 4 окна у кухни и стола", Component = "Включено" });
            connections.Add(new Connection() { Id = 111, Device = "Подсветка окон сверху 3 окна и дверь у камина и дивана", Component = "Включено" });
            connections.Add(new Connection() { Id = 112, Device = "Подсветка окон сверху 3 окна и дверь напротив входа", Component = "Включено" });
            connections.Add(new Connection() { Id = 113, Device = "Подсветка над кухней 6 круглых карданов", Component = "Включено" });
            connections.Add(new Connection() { Id = 114, Device = "подсветка полки кухни точечные светильники", Component = "Включено" });
            connections.Add(new Connection() { Id = 115, Device = "Подсветка колонны точечные светильники", Component = "Включено" });
            connections.Add(new Connection() { Id = 116, Device = "Бра кухни", Component = "Включено" });
            connections.Add(new Connection() { Id = 117, Device = "Дверь у дивана", Component = "Включено" });
            connections.Add(new Connection() { Id = 118, Device = "Дверь беседка", Component = "Включено" });
            connections.Add(new Connection() { Id = 119, Device = "Дверь стол обеденный", Component = "Включено" });
            // улица
		    connections.Add(new Connection() { Id = 130, Device = "Уличная подсветка фронтального балкона у двери", Component = "Включено" });
		    connections.Add(new Connection() { Id = 131, Device = "Уличная подсветка фронтального балкона на закрытой части", Component = "Включено" });
		    connections.Add(new Connection() { Id = 132, Device = "Подсветка балкона кабинета", Component = "Включено" });
		    connections.Add(new Connection() { Id = 133, Device = "Уличная подсветка фронтального балкона центральный", Component = "Включено" });
		    connections.Add(new Connection() { Id = 134, Device = "Уличная подсветка главный вход 1 фонарь 1", Component = "Включено" });
		    connections.Add(new Connection() { Id = 135, Device = "Уличная подсветка главный вход 2 фонаря 2", Component = "Включено" });
		    connections.Add(new Connection() { Id = 136, Device = "Уличная подсветка главный вход 1 фонарь 3", Component = "Включено" });
		    connections.Add(new Connection() { Id = 137, Device = "Подсветка бокового балкона диван стол", Component = "Включено" });
		    connections.Add(new Connection() { Id = 138, Device = "Уличная подсветка заднего балкона 3 фонаря", Component = "Включено" });
            // беседка
		    connections.Add(new Connection() { Id = 139, Device = "Уличная подсветка 4 фонаря", Component = "Включено" });
		    connections.Add(new Connection() { Id = 140, Device = "Свет - 3 внутренние группы", Component = "Включено" });
            // улица
		    connections.Add(new Connection() { Id = 141, Device = "1-й высокий фонарь", Component = "Включено" });
		    connections.Add(new Connection() { Id = 142, Device = "Высокий фонарь дорожки 1", Component = "Включено" });
		    connections.Add(new Connection() { Id = 143, Device = "Подсветка скамейки", Component = "Включено" });
		    connections.Add(new Connection() { Id = 144, Device = "Высокий фонарь дорожки 2", Component = "Включено" });
		    connections.Add(new Connection() { Id = 145, Device = "Низкие фонари дорожки", Component = "Включено" });
		    connections.Add(new Connection() { Id = 146, Device = "Фонари вокруг дома", Component = "Включено" });
		    connections.Add(new Connection() { Id = 147, Device = "Фонари при въезде", Component = "Включено" });
		    connections.Add(new Connection() { Id = 148, Device = "Подсветка малого дома", Component = "Включено" });
		    connections.Add(new Connection() { Id = 149, Device = "Подсветка ворот", Component = "Включено" });
		    connections.Add(new Connection() { Id = 150, Device = "Подсветка калитки", Component = "Включено" });
		    connections.Add(new Connection() { Id = 151, Device = "Подсветка входа в щитовую", Component = "Включено" });
		    connections.Add(new Connection() { Id = 152, Device = "Подсветка входа в кулерную", Component = "Включено" });
		    connections.Add(new Connection() { Id = 153, Device = "Подсветка гостевого дома", Component = "Включено" });
		    connections.Add(new Connection() { Id = 154, Device = "Подсветка 14", Component = "Включено" });
		    connections.Add(new Connection() { Id = 155, Device = "Подсветка 15", Component = "Включено" });
		    connections.Add(new Connection() { Id = 156, Device = "Подсветка 16", Component = "Включено" });
		    connections.Add(new Connection() { Id = 157, Device = "Подсветка 17", Component = "Включено" });
		    connections.Add(new Connection() { Id = 158, Device = "Подсветка 18", Component = "Включено" });
		    connections.Add(new Connection() { Id = 159, Device = "Подсветка 19", Component = "Включено" });
		    connections.Add(new Connection() { Id = 160, Device = "Подсветка 20", Component = "Включено" });
            // улица бекоф2
		    connections.Add(new Connection() { Id = 161, Device = "* Ворота отк-останов импульс", Component = "Включено" });
		    connections.Add(new Connection() { Id = 162, Device = "* Ворота закр-останов импульс", Component = "Включено" });
		    connections.Add(new Connection() { Id = 163, Device = "* Гараж отк-останов импульс", Component = "Включено" });
		    connections.Add(new Connection() { Id = 164, Device = "* Гараж закр-останов импульс", Component = "Включено" });
            // генераторная
		    connections.Add(new Connection() { Id = 165, Device = "* Жалюзи генераторная открыть", Component = "Включено" });
		    connections.Add(new Connection() { Id = 166, Device = "* Жалюзи генераторная закрыть", Component = "Включено" });
            // кулерная
            connections.Add(new Connection() { Id = 167, Device = "* Жалюзи кулерная открыть", Component = "Включено" });
		    connections.Add(new Connection() { Id = 168, Device = "* Жалюзи кулерная закрыть", Component = "Включено" });
            // улица
		    connections.Add(new Connection() { Id = 169, Device = "* Диоды ворота красный", Component = "Включено" });
		    connections.Add(new Connection() { Id = 170, Device = "* Диоды ворота зеленый", Component = "Включено" });
		    connections.Add(new Connection() { Id = 171, Device = "* Диоды ворота синий", Component = "Включено" });
		    connections.Add(new Connection() { Id = 172, Device = "* Ворота въезд работа привода - импульсы", Component = "Включено" });
		    connections.Add(new Connection() { Id = 173, Device = "* Гараж работа привода - импульсы", Component = "Включено" });
		    connections.Add(new Connection() { Id = 174, Device = "Запуск питания насоса полива", Component = "Включено" });
		    for (int i = 1; i < 30; i++)
		        connections.Add(new Connection() { Id = 199+i, Device = "* Полив " + i, Component = "Включено" });


            //connections.Add(new Connection() { Id = 200, Device = "* Подсветка стены", Component = "яркость" });
            //connections.Add(new Connection() { Id = 201, Device = "Потолочные карданы", Component = "яркость" });
            // dmx
            connections.Add(new Connection() { Id = 301, Device = "DMX подсветка прожекторных ниш у камина", Component = "Цвет" });
            connections.Add(new Connection() { Id = 302, Device = "DMX подсветка прожекторных ниш у дивана", Component = "Цвет" });
            connections.Add(new Connection() { Id = 303, Device = "DMX подсветка прожекторных ниш у стола", Component = "Цвет" });
            connections.Add(new Connection() { Id = 304, Device = "Подсветка над кухней среднее стекло", Component = "Цвет" });
            connections.Add(new Connection() { Id = 305, Device = "Подсветка над кухней верхнее стекло", Component = "Цвет" });
            connections.Add(new Connection() { Id = 306, Device = "Подсветка над кухней нижнее стекло", Component = "Цвет" });
            connections.Add(new Connection() { Id = 307, Device = "Подсветка колонны диоды", Component = "Цвет" });
            connections.Add(new Connection() { Id = 308, Device = "Подсветка ниши 3 сегмента - камин и кухня", Component = "Цвет" });
            connections.Add(new Connection() { Id = 309, Device = "Подсветка ниш для штор кухня стол -1", Component = "Цвет" });
            connections.Add(new Connection() { Id = 310, Device = "Подсветка ниш для штор кухня стол -2", Component = "Цвет" });
            connections.Add(new Connection() { Id = 311, Device = "Подсветка ниш для штор камин диван -1", Component = "Цвет" });
            connections.Add(new Connection() { Id = 312, Device = "Подсветка ниш для штор камин диван -2", Component = "Цвет" });
            connections.Add(new Connection() { Id = 313, Device = "DMX подсветка полки кухни диоды", Component = "Цвет" });
            connections.Add(new Connection() { Id = 314, Device = "Подсветка барной стойки стекло", Component = "Цвет" });
            // шторы
		    connections.Add(new Connection() { Id = 400, Device = "Дверь у стола -1", Component = "уровень" });
		    connections.Add(new Connection() { Id = 401, Device = "Два окна у кухни", Component = "уровень" });
		    connections.Add(new Connection() { Id = 402, Device = "Одно окна за диваном - две шторы", Component = "уровень" });
		    connections.Add(new Connection() { Id = 403, Device = "Одно окно у стола", Component = "уровень" });
		    connections.Add(new Connection() { Id = 404, Device = "Дверь у стола -2", Component = "уровень" });
		    connections.Add(new Connection() { Id = 405, Device = "Дверь у камина", Component = "уровень" });
		    connections.Add(new Connection() { Id = 406, Device = "Два окна у стола", Component = "уровень" });
		    connections.Add(new Connection() { Id = 407, Device = "Окно у камина", Component = "уровень" });
		    connections.Add(new Connection() { Id = 408, Device = "Два окна у дивана", Component = "уровень" });

		    //connections.Add( new Connection(){Id=1117, Device="Шина прожекторов - над камином 1", Component="Включить"} );
		    //connections.Add(new Connection(){Id=3445, Device="Шина прожекторов - над камином 2", Component="Включено"} );
		    //connections.Add(new Connection(){Id=1233, Device="* Подсветка стены", Component="яркость"} );
		    //connections.Add(new Connection(){Id=1231, Device="DMX подсветка прожекторных ниш у камина", Component="Цвет"} );
            //*/
			Master.IsChanged = true;
		}
	//	ModbusClient mbClient = gameObject.GetComponent<ModbusClient>();
	//	mbClient.enabled = true;
	//	ClientServer clientServer = gameObject.GetComponent<ClientServer>();
	//	clientServer.enabled = true;
	}
		
	void Start ()	
	{ 
		Master.InitializeDevices();
	}

	public static void SetPar(string devName, string parName, string newValue)
	{
		if ( Master.instance == null ) 
		{ Debug.LogWarning("Master.instance == null"); return; }
		var device = Master.instance.GetDevice(devName);
		if ( device == null ) 
		{ Debug.LogWarning("device("+devName+") == null"); return; }
		var par = device.GetParameterByName(parName);
		if ( par == null ) 
		{ Debug.LogWarning("device("+devName+") par("+parName+") == null"); return; }
			
//		Debug.Log("Master.SetPar: devName="+devName+", parName="+parName+", newValue="+newValue );
		par.value = newValue;
		//Debug.Log("Master.SetPar: devName="+devName+", parName="+parName+", newValue="+newValue+", now value="+Master.instance.GetDevice(devName).GetParameterByName(parName).value );
		Master.IsChanged = true;
	}
		

	private float LastTime = 0;
	private float LastTime60 = 0;
	void Update()
	{
        if (Master.instance == null) { gameObject.SetActive(false); return; }
		if ( Time.time - LastTime > 1f ) // every second
		{
			SetPar("Сервер","Время работы сервера", string.Format( "{0:F2} минут", Time.time/60f ) );
			LastTime = Time.time;
			// надо проверить онлайн клиентов
			List<Device> devices = Master.GetDevicesByType("CLIENT");
			foreach(Device device in devices)
			{
				bool isOnline = ClientServer.IsClientIpOnline(device.GetValueByName("IP"));
				device.SetValueByName(isOnline?"1":"0", "Подключён");
			}
		}
		if ( Time.time - LastTime60 > 60f ) // every 60 second
		{
//			Device server = GetDevice("Сервер");
//			Device.Parameter par;
//			par = server.GetParameterByName("Время работы сервера");
//			ClientControl.SendDeviceParameter(server, par);	
//			par = server.GetParameterByName("Время работы бекофа");
//			ClientControl.SendDeviceParameter(server, par);	
//			par = server.GetParameterByName("Сообщений от бекофа");
//			ClientControl.SendDeviceParameter(server, par);	
//			par = server.GetParameterByName("Сообщений от клиентов");
//			ClientControl.SendDeviceParameter(server, par);
//			par = server.GetParameterByName("Бекоф подключён");
//			ClientControl.SendDeviceParameter(server, par);
			LastTime60 = Time.fixedTime;

			CheckInternet();
		}
	}

	private void CheckInternet()
	{
		//inetResetOneAfterMinutes
		if (InetLook.DisconnectedTime > inetResetOneAfterMinutes)
		{	
			if ( !resetOneCompleted )
			{
				ResetInternet();
				resetOneCompleted = true;
			}
		}
		else resetOneCompleted = false;		
	}

//	public static void ResetInternetStatic()
//	{
//		Master.instance.StartCoroutine(Master.instance.ResetInternetCoroutine(15));
//	}

	public void ResetInternet()
	{
		Master.instance.StartCoroutine(Master.instance.ResetInternetCoroutine(15));
	}

	IEnumerator ResetInternetCoroutine (float secondsToWait)
	{
		Debug.LogWarning("Internet reset initiated with wait time = "+secondsToWait+" seconds.");
		ModbusClient.SendSet(BeckhoffItems.TRIGGER, (8<<8) + 7, 1);
		yield return new WaitForSeconds(secondsToWait);
		ModbusClient.SendSet(BeckhoffItems.TRIGGER, (8<<8) + 7, 0);
		yield break;
	}

	public Device AddDeviceAsClientByIP(string clientIP)
	{
		string element = CLIENT.GetDefaultDevice(clientIP);
		Device device = AddDevice( element );
		Master.IsChanged = true;
		List<Device> clients = Master.GetDevicesByType("CLIENT");
		foreach(Device client in clients)
		{
		//	if ( client.messages == null ) continue;
			if ( client.GetValueByName("IP") == clientIP ) continue;
			ClientControl.AddCmd(client,device,null,Device.Command.AddDevice,"");
			//client.messages.Add(new Device.Message(client,null,"AddDevice;"+device.ToString()));
		}
		//ClientControl.SendDevice(device, clientIP);
		return device;
	}

	public static Device GetOrCreateClientByIP(string ip)
	{
		Device device = Master.instance.GetDevice("CLIENT","IP",ip);
		if ( device != null ) return device;
		return Master.instance.AddDeviceAsClientByIP(ip);
	}
	
	// Update is called once per frame
	//void Update ()	{	}
	public bool IsGoodName(string newName)
	{
		for ( int i = 0; i < devices.Count; i++ )
		{
			if ( devices[i].GetValueByName("Название") == newName ) return false;
			if ( devices[i].GetValueByName("Комната") == newName ) return false;
		}
		return true;
	}

	//private int maxLength = 0; // 56->64 detected
	public Device AddDevice(string deviceString)
	{
		if ( deviceString.Length == 0 )
		{
			Debug.LogWarning("Master.AddDevice: wrong deviceString="+deviceString);
			return null;
		}
		Device device = new Device();
		if ( !device.Parse( deviceString ) ) return null;

		if (false == int.TryParse(device.GetValueByName("Id"), out device.id))
		{
			Debug.LogWarning("Master.AddDevice: cannot parse id=" + device.GetValueByName("Id") + ", device=" + device);
		}

		string roomName = device.GetValueByName("Комната");
		bool roomFound = false;
		if ( roomName == "" ) roomFound = true;
		// test name for unique
		for ( int i = 0; i < devices.Count; i++ )
		{
			if ( devices[i].GetValueByName("Название") == device.GetValueByName("Название") )
			{
				Debug.LogWarning("Master.AddDevice: names equal: new device("+device.ToString()+")"+
					" vs ["+i+"]("+devices[i].ToString()+")");
			}
			if ( !roomFound && roomName != "" && devices[i].GetValueByName("Тип")=="ROOM" )
			{
				if ( roomName == devices[i].GetValueByName("Название") ) roomFound = true;
			}
		}
		if ( roomFound == false )
			Debug.LogWarning("Master.AddDevice: no room for new device: "+device.ToString() );
//		if ( device.GetValueByName("Название").Length > maxLength )
//		{ maxLength = device.GetValueByName("Название").Length; Debug.Log("maxLen="+maxLength+" for device "+device.ToString()); }
		devices.Add( device );
		DEFAULT.InitializeDevice( device, true );
		//InitializeValuesOfDevice( device );
		return device;
	}

	public static void InitializeDevices()
	{
		Master.isOnInit = true;
		Debug.Log("Master.InitializeDevices: BEGIN devices.Count="+Master.Devices.Count);
		foreach(Device device in Master.Devices)
			DEFAULT.InitializeDevice( device, false );
		//Debug.Log("Master.InitializeDevices: END devices.Count="+Master.Devices.Count);
		//Master.isOnInit = false;
	}

	public static void OnClientSetParameter( string devNameValue, string parString, Device client )
	{
//		Debug.Log("Master.OnClientSetParameter: devNameValue='"+devNameValue+"', parString='"+parString+"'");
		Device device = Master.instance.GetDevice(devNameValue);
		if ( device == null ) { Debug.Log("Master.OnClientSetParameter: UNKNOWN devNameValue='"+devNameValue+"', parString='"+parString+"'"); return; }
//		string type = device.GetValueByName("Тип");
		Device.Parameter newPar = device.NewParameterFromString( parString );
	//	Device.Parameter oldPar = device.GetParameterByDesc( newPar.desc );
		//bool isUnknown = false;
		//bool isChanged = false;
		DEFAULT.OnClientSetParameter(device, newPar, client);
		//if ( DEFAULT.OnClientSetParameter(device, newPar, client) ) {}
		//else {}

		//else isUnknown = true;
		//if ( isUnknown) Debug.LogWarning("Master.OnClientSetParameter: unknown devNameValue='"+devNameValue+"', parString='"+parString+"', newPar="+newPar+", type="+type);
	}

/*	public static void OnReceiveMessageFromModbus( int addressType, int addressIndex, int value)
	{
		Device.Parameter par = Master.instance.GetDevice("Сервер").GetParameterByName("Сообщений от бекофа");
		par.value = (int.Parse(par.value) + 1).ToString();
		Master.IsChanged = true;

		int value1 = value&0xFF; int value2 = (value>>8)&0xFF;
		int index2 = addressIndex&0xFF; int index1 = (addressIndex>>8)&0xFF;
		//Device device = null;
		//Debug.Log("OnReceiveMessageFromModbus: "+addressType+"["+addressIndex+"="+index1+", "+index2+"]="+value );
		switch((BeckhoffItems)addressType)
		{
		case BeckhoffItems.DIMMER:
			HandleLog.LogBeckhoff("OnReceiveMessageFromModbus: DIMMER["+addressIndex+"]="+value );
			DIMMER.OnReceiveMessageFromModbus(addressIndex,value);
			break;
		case BeckhoffItems.TRIGGER:
			HandleLog.LogBeckhoff("OnReceiveMessageFromModbus: TRIGGER["+index1+", "+index2+"]="+value );
			TRIGGER.OnReceiveMessageFromModbus(BeckhoffItems.TRIGGER, addressIndex, value);
			break;
		case BeckhoffItems.UBIQ:
			HandleLog.LogBeckhoff("OnReceiveMessageFromModbus: UBIQ["+addressIndex+"]="+value );
			break;
		case BeckhoffItems.MS:
			HandleLog.LogBeckhoff("OnReceiveMessageFromModbus: MS["+addressIndex+"]=(выкл:"+value1+", двж:"+value2+")" );
			SENSOR.OnReceiveMessageFromModbus(addressIndex,value);
			break;
		case BeckhoffItems.SB:
			HandleLog.LogBeckhoff("OnReceiveMessageFromModbus: SB["+addressIndex+"="+index1+", "+index2+"]="+value );
			break;
		case BeckhoffItems.CURTAINS:
			HandleLog.LogBeckhoff("OnReceiveMessageFromModbus: CURTAINS["+addressIndex+"]="+value );
			CURTAIN.OnReceiveMessageFromModbus(addressIndex, value);
			break;
		case BeckhoffItems.ZONE:
			HandleLog.LogBeckhoff("OnReceiveMessageFromModbus: ZONE["+addressIndex+"]="+value );
			break;
		case BeckhoffItems.DMX:
			HandleLog.LogBeckhoff("OnReceiveMessageFromModbus: DMX["+addressIndex+"]="+value+", color="+Utility2.IntToRgbString(value));
			DMX.OnReceiveMessageFromModbus(addressIndex, value);
			break;
		case BeckhoffItems.WINTEK:
			HandleLog.LogBeckhoff("OnReceiveMessageFromModbus: WINTEK["+addressIndex+"]="+value );
			break;
		case BeckhoffItems.HVAC:
			HandleLog.LogBeckhoff("OnReceiveMessageFromModbus: HVAC["+addressIndex+"]="+value );
			break;
		case BeckhoffItems.LOCK:
			HandleLog.LogBeckhoff("OnReceiveMessageFromModbus: LOCK["+addressIndex+"]="+value );
			LOCK.OnReceiveMessageFromModbus(addressIndex, value);
			break;
		case BeckhoffItems.BTRIGGER:
			HandleLog.LogBeckhoff("OnReceiveMessageFromModbus: BTRIGGER["+index1+", "+index2+"]="+value );
			//HandleLog.LogBeckhoff("OnReceiveMessageFromModbus: BTRIGGER["+addressIndex+"]="+value );
			TRIGGER.OnReceiveMessageFromModbus(BeckhoffItems.BTRIGGER, addressIndex, value);
			break;
		default:
			HandleLog.LogBeckhoff("OnReceiveMessageFromModbus: unknown "+addressType+"["+addressIndex+"]="+value );
			break;
		}
		//Master.IsChanged = true;
	}
	*/

	public static void NewDeviceByType(Device clientDevice, string devType, string devRoom)
	{
		Debug.LogWarning("TODO: AddDevice: devType="+devType);
		string devString = DEFAULT.GetDefaultDeviceString(devType);
		if ( devString.Length == 0 ) 
		{ 
			Debug.LogWarning("Не могу создать устройство типа:"+devType); 
			ClientControl.AddCmd( clientDevice, null, null, Device.Command.Message, "Не могу создать устройство типа:"+devType);
			return; 
		}
		Device device = Master.instance.AddDevice( devString );
		if ( device == null )
		{ 
			Debug.LogWarning("Не могу создать устройство типа:"+devType); 
			ClientControl.AddCmd( clientDevice, null, null, Device.Command.Message, "Не могу создать устройство типа:"+devType);
			return; 
		}
		device.SetValueByName(devRoom,"Комната");
		List<Device> clients = Master.GetDevicesByType("CLIENT");
		foreach(Device client in clients)
		{
			ClientControl.AddCmd(client,device,null,Device.Command.AddDevice,"");
		}
	}

	public static void CopyDevice(Device clientDevice, string devName)
	{
		Debug.LogWarning("TODO: CopyDevice: devName="+devName);
		Device deviceSource = Master.instance.GetDevice(devName);
		if ( deviceSource == null )
		{ 
			Debug.LogWarning("Не могу скопировать устройство:"+devName); 
			ClientControl.AddCmd( clientDevice, null, null, Device.Command.Message, "Не могу скопировать устройство:"+devName);
			return; 
		}
		string savedName = deviceSource.GetValueByName("Название");
		deviceSource.SetValueByName(savedName+" "+new System.Random().Next(11111,99999).ToString(),"Название");
		string devString = deviceSource.ToString();
		deviceSource.SetValueByName(savedName,"Название");
		Device device = Master.instance.AddDevice( devString );
		if ( device == null )
		{ 
			Debug.LogWarning("Не могу скопировать устройство:"+devName); 
			ClientControl.AddCmd( clientDevice, null, null, Device.Command.Message, "Не могу скопировать устройство:"+devName);
			return; 
		}
		List<Device> clients = Master.GetDevicesByType("CLIENT");
		foreach(Device client in clients)
		{
			ClientControl.AddCmd(client,device,null,Device.Command.AddDevice,"");
		}

	}

	public static void DeleteDevice(Device clientDevice, string devName)
	{
		Debug.LogWarning("TODO: DeleteDevice: devName="+devName);
		Device device = Master.instance.GetDevice(devName);
		if ( device == null )
		{ 
			Debug.LogWarning("Не могу удалить устройство:"+devName); 
			ClientControl.AddCmd( clientDevice, null, null, Device.Command.Message, "Не могу удалить устройство:"+devName);
			return; 
		}
		//ClientControl.AddCmd( clientDevice, null, null, Device.Command.Message, "Не хочу удалять устройство:"+devName);
		List<Device> clients = Master.GetDevicesByType("CLIENT");
		foreach(Device client in clients)
		{
			ClientControl.AddCmd(client,device,null,Device.Command.RemoveDevice,"");
		}
		Master.instance.devices.Remove( device );
	}

	public static void DeviceCommand(Device clientDevice, string devName, string parName)
	{
		//Debug.LogWarning("Master.DeviceCommand: device="+devName+", parName="+parName);
		Device device = Master.instance.GetDevice( devName );
		if ( device == null ) 
		{ 
			Debug.LogWarning("Нет в базе данных такого устройства="+devName); 
			ClientControl.AddCmd( clientDevice, null, null, Device.Command.Message, "Нет в базе данных такого устройства \""+devName+"\"");
			return; 
		}
		Device.Parameter par = device.GetParameterByName( parName );
		if ( par == null )
		{ 
			Debug.LogWarning("Устройство="+devName+" не содержит параметр="+parName); 
			ClientControl.AddCmd( clientDevice, null, null, Device.Command.Message, "Устройство \""+devName+"\" не содержит параметр \""+parName+"\"");
			return; 
		}
		DEFAULT.OnClientCommand(device,par,clientDevice);
	}

	public static void SetBeckhoffStatus( bool status )
	{		
		Master.SetPar("Сервер","Бекоф подключён", status?"1":"0" );	
		if ( status )
		{
			Debug.Log("ModbusClient: Beckhoff подключён к unity-server!");
			Master.SetPar("Сервер","Сообщений от бекофа", "0" );
			Master.InitializeDevices();
		}
		Device device = Master.instance.GetDevice("Сервер");
		Device.Parameter par = device.GetParameterByName("Бекоф подключён");
		ClientControl.SendDeviceParameter(device, par);	
	}

	public static void SetInternetStatus( int status )
	{		
		Master.SetPar("Сервер","Интернет", status.ToString() );	
		Device device = Master.instance.GetDevice("Сервер");
		Device.Parameter par = device.GetParameterByName("Интернет");
		ClientControl.SendDeviceParameter(device, par);	
	}

	public static List<Device> GetDevicesByType(string devType)
	{
		List<Device> list = new List<Device>();
		foreach(Device device in instance.devices)
		{
			if ( device.GetValueByName("Тип") == devType ) list.Add(device);
		}
		return list;
	}

	public static Device GetDeviceByName(string devNameValue )
	{ return Master.instance.GetDevice(devNameValue); }

    public static Device GetDeviceById(int id)
    {
        return Master.instance.devices.FirstOrDefault(t => t.id == id);
    }

    public Device GetDevice(int index )
	{
		if ( index < 0 || index >= devices.Count ) return null;
		return devices[index];
	}

	public Device GetDevice(string devNameValue )
	{
		foreach(Device device in devices)
		{
			if ( devNameValue != device.GetValueByName("Название") ) continue;
			return device;
		}
		return null;
	}

	public Device GetDevice(string typeValue, string parName, string parValue )
	{
		foreach(Device device in devices)
		{
			if ( typeValue != device.GetValueByName("Тип") ) continue;
			if ( parValue != device.GetValueByName(parName) ) continue;
			return device;
		}
		return null;
	}

	public Device GetDevice(string typeValue, string par1Name, string par1Value, string par2Name, string par2Value )
	{
		foreach(Device device in devices)
		{
			if ( typeValue != device.GetValueByName("Тип") ) continue;
			if ( par1Value != device.GetValueByName(par1Name) ) continue;
			if ( par2Value != device.GetValueByName(par2Name) ) continue;
			return device;
		}
		return null;
	}

	public Device GetDevice(string typeValue, string par1Name, string par1Value, string par2Name, string par2Value, string par3Name, string par3Value )
	{
		foreach(Device device in devices)
		{
			if ( typeValue != device.GetValueByName("Тип") ) continue;
			if ( par1Value != device.GetValueByName(par1Name) ) continue;
			if ( par2Value != device.GetValueByName(par2Name) ) continue;
			if ( par3Value != device.GetValueByName(par3Name) ) continue;
			return device;
		}
		return null;
	}

	public static string SaveConfiguration()
	{
		System.DateTime now = System.DateTime.Now;
		string name = "Config "+string.Format("{0:D4}-{1:D2}-{2:D2}",now.Year,now.Month,now.Day);
		name += " "+string.Format("{0:D2}-{1:D2}-{2:D2}",now.Hour,now.Minute,now.Second);
		name += " ";
		System.Random random = new System.Random();
		string vowels = "aeiouy";
		string consonants = "bcdfghjklmnpqrstvwxz";
		name += consonants.Substring(random.Next(0,consonants.Length),1);
		name += vowels.Substring(random.Next(0,vowels.Length),1);
		name += consonants.Substring(random.Next(0,consonants.Length),1);
		name += vowels.Substring(random.Next(0,vowels.Length),1);
		name += consonants.Substring(random.Next(0,consonants.Length),1);
		string filename = Master.Datapath + "/"+name+".txt";
		Debug.LogWarning("Save configuration to "+filename);
		if (File.Exists(filename))
		{
			Debug.LogWarning(filename+" already exists.");
			return " уже есть такое имя ";
		}
		try 
		{
			var sw = File.CreateText(filename);
			//System.IO.StreamWriter sw = new System.IO.StreamWriter(filename);
			foreach(Device device in Master.Devices)
			{
				//sw.Write(device.ToString());
				sw.WriteLine (device.ToString());
			}
			sw.Close(); 
		}
		catch(System.Exception exc)
		{
			Debug.LogError("При записи конфигурации в "+filename+" возникла ошибка "+exc);
			return " возникла ошибка ";
		}

		return name;
	}

	public static List<string> GetConfigurations()
	{
		List<string> names = new List<string>();
		Debug.LogWarning("Looking in "+Master.Datapath+" by mask *.txt");
		//foreach (string file in System.IO.Directory.GetFiles(Master.Datapath,"*.txt"))
		//{ 
		//	names.Add( file );
		//	Debug.LogWarning("found file "+file);
		//}

		DirectoryInfo dir = new DirectoryInfo(Master.Datapath);
		FileInfo[] info = dir.GetFiles("*.txt");
		foreach (FileInfo f in info) 
		{
			names.Add( f.Name );
			Debug.LogWarning("found file "+f.Name+", time="+f.LastWriteTime);
		}

		return names;
	}

	public static void LoadConfig(Device client, string configName)
	{
		Debug.Log("Загружаем конфигурацию \""+configName+"\"");

		string filename = Master.Datapath + "/"+configName;
		if (!File.Exists(filename))
		{
			Debug.LogWarning(filename+" not exist!");
			ClientControl.AddCmd( client, null, null, Device.Command.Message, "На сервере нет конфигурации с таким названием \""+configName+"\"");
			return;
		}

		Master master = Master.instance;
		for(int i = master.devices.Count-1; i>=0; i--)
			master.devices[i].Clear();
		master.devices.Clear();

		var file = File.OpenText(filename);
		string line = "";
		//System.IO.StreamWriter sw = new System.IO.StreamWriter(filename);
		do
		{
			//sw.Write(device.ToString());
			line = file.ReadLine();
			master.AddDevice( line );

		} while(line.Length != 0);
		file.Close(); 

	}


}

