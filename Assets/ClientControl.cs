﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


public class ClientControl : MonoBehaviour
{
    public static ClientControl instance;

    //public List<string> clients = new List<string>();
    // Use this for initialization
    void Start()
    {
        ClientControl.instance = this;
    }

    // Update is called once per frame
    void Update()
    {
    }

//	public void AddClient(string clientIP)
//{
    //	if ( clients.Contains( clientIP ) ) return;
    //	clients.Add( clientIP );
    //Master.instance.AddDeviceAsClientByIP(clientIP);
    //}

    public static void IncomeCommand(Device clientDevice, string clientIP, int deviceIndex, int deviceParamIndex, int deviceParamValue)
    {
        Device.Parameter serverPar = Master.instance.GetDevice("Сервер").GetParameterByName("Сообщений от клиентов");
        serverPar.value = (int.Parse(serverPar.value) + 1).ToString();
        Master.IsChanged = true;

        Device device = Master.instance.GetDevice(deviceIndex);
        if (device == null)
        {
            string result = "устройство с индексом " + deviceIndex + " не найдено";
            Debug.LogWarning("ClientControl.IncomeCommand: " + result);
            ClientControl.AddCmd(clientDevice, null, null, Device.Command.Message, result);
        }
        else if (deviceParamIndex == 1)
        {
            // клиент включает триггер, сервер включает триггер но возможно
            // бекофф не включит триггер(бекофф например выключен)
            // тогда и в клиенте и в сервере будут неверные значения
            // однако когда бекофф включится то сервер переинициализируется
            Device.Parameter par = device.GetParameterByName("Включено");
            if (par == null)
            {
                string result = "устройство " + device.GetValueByName("Название") + " с индексом " + deviceIndex + " не содержит параметр с индексом" + deviceParamIndex + " - Включено";
                Debug.LogWarning("ClientControl.IncomeCommand: " + result);
                ClientControl.AddCmd(clientDevice, null, null, Device.Command.Message, result);
            }
            else
            {
                Debug.Log("ClientControl.IncomeCommand: " + "устройство [" + deviceIndex + "] " + device.GetValueByName("Название") + ", параметр [" + deviceParamIndex + "] " + par.name + " меняет значение с " + par.value + " на " + deviceParamValue);
                //par.value = deviceParamValue == 0 ? "0" : "1";
                Device.Parameter parNew = new Device.Parameter(par.name, par.type, par.value);
                parNew.value = deviceParamValue == 0 ? "0" : "1";
                if (DEFAULT.OnClientSetParameter(device, par, clientDevice))
                    par.value = deviceParamValue == 0 ? "0" : "1";
            }
        }
        else
        {
            string result = "устройство " + device.GetValueByName("Название") + " с индексом " + deviceIndex + " не содержит параметр с индексом " + deviceParamIndex;
            Debug.LogWarning("ClientControl.IncomeCommand: " + result);
            ClientControl.AddCmd(clientDevice, null, null, Device.Command.Message, result);
        }

    }

    public static void IncomeCommand(Device clientDevice, string clientIP, string command)
    {
        Device.Parameter serverPar = Master.instance.GetDevice("Сервер").GetParameterByName("Сообщений от клиентов");
        serverPar.value = (int.Parse(serverPar.value) + 1).ToString();
        Master.IsChanged = true;

        string cmd = command.ToString();
        if (cmd.IndexOf(':') >= 0) cmd = cmd.Substring(0, cmd.IndexOf(':'));
        if (cmd == "RequestDevices")
        {
            string clientVersion = cmd.Length + 1 >= command.Length ? "" : command.Substring(cmd.Length + 1, command.Length - cmd.Length - 1);
            Debug.Log("ClientControl.IncomeCommand: [" + clientIP + "]->" + cmd + ": версия='" + clientVersion + "' (нужна '" + Master.clientVersion + "'), " + Master.Devices.Count + " devices in system");
            if (clientVersion != Master.clientVersion)
                ClientControl.AddCmd(clientDevice, null, null, Device.Command.Message,
                    "Версия '" + clientVersion + "' программы устарела. Новую версию '" + Master.clientVersion + "' скачайте из облака иридиума, проведя слева направо от левого края экрана.");

            clientDevice.isClientStopSetMessages = true;
            AddCmd(clientDevice, null, null, Device.Command.BeginLoadDB, "");
            foreach (Device device2 in Master.Devices)
            {
                AddCmd(clientDevice, device2, null, Device.Command.AddDevice, "");
                //device.messages.Add( new Device.Message(device2,null,"AddDevice;"+device2.ToString()) );
                //ClientServer.Send(clientIP, "AddDevice:"+device2.ToString());
            }
            AddCmd(clientDevice, null, null, Device.Command.EndLoadDB, "");
            clientDevice.isClientStopSetMessages = false;
        }
        else if (cmd == "SetParameter")
        {
            string body = command.Substring(cmd.Length + 1, command.Length - cmd.Length - 1);
            string devNameValue = body.Substring(0, body.IndexOf(','));
            string par = body.Substring(body.IndexOf(',') + 1);
            //cmd = cmd.Substring(0, cmd.IndexOf(':'));
            Debug.Log("ClientControl.IncomeCommand: [" + clientIP + "]->'" + cmd + "=" + devNameValue + "," + par + "'");
            //Debug.Log("Master.OnClientSetParameter: devNameValue='"+devNameValue+"', parString='"+parString+"'");

            Master.OnClientSetParameter(devNameValue, par, clientDevice);
        }
        else if (cmd == "AddDevice")
        {
            string body = command.Substring(cmd.Length + 1, command.Length - cmd.Length - 1);
            string[] split = body.Split(',');
            if (split.Length != 2)
            {
                Debug.LogWarning("Wrong command=" + command);
                return;
            }
            Debug.Log("ClientControl.IncomeCommand: [" + clientIP + "]->'" + cmd + "=" + split[0] + "," + split[1] + "'");
            Master.NewDeviceByType(clientDevice, split[0], split[1]);
            //Debug.LogWarning("TODO: AddDevice: devType="+devType);
        }
        else if (cmd == "DeleteDevice")
        {
            string devName = command.Substring(cmd.Length + 1, command.Length - cmd.Length - 1);
            Debug.Log("ClientControl.IncomeCommand: [" + clientIP + "]->'" + cmd + "=" + devName + "'");
            Master.DeleteDevice(clientDevice, devName);
            //Debug.LogWarning("TODO: DeleteDevice: devType="+devName);
        }
        else if (cmd == "CopyDevice")
        {
            string devName = command.Substring(cmd.Length + 1, command.Length - cmd.Length - 1);
            //Debug.LogWarning("TODO: CopyDevice: devType="+devName);
            Debug.Log("ClientControl.IncomeCommand: [" + clientIP + "]->'" + cmd + "=" + devName + "'");
            Master.CopyDevice(clientDevice, devName);
        }
        else if (cmd == "Command")
        {
            string body = command.Substring(cmd.Length + 1, command.Length - cmd.Length - 1);
            string[] split = body.Split(',');
            if (split.Length != 2 || split[0].Length == 0 || split[1].Length == 0)
            {
                Debug.LogWarning("Wrong command=" + command);
                return;
            }
            Debug.Log("ClientControl.IncomeCommand: [" + clientIP + "]->'" + cmd + "=" + split[0] + "," + split[1] + "'");
            Master.DeviceCommand(clientDevice, split[0], split[1]);
        }
        else if (cmd == "LoadConfig")
        {
            string configName = command.Substring(cmd.Length + 1, command.Length - cmd.Length - 1);
            //Debug.LogWarning("TODO: CopyDevice: devType="+devName);
            Master.LoadConfig(clientDevice, configName);
        }
        else if (cmd == "PingReturn")
        {
            string id = command.Substring(cmd.Length + 1, command.Length - cmd.Length - 1);
            //Debug.LogWarning("TODO: CopyDevice: devType="+devName);
            //Master.LoadConfig(clientDevice, configName);
            PingServer.PingReturn(clientDevice, id);
        }
        else if (cmd == "Ping")
        {
            ClientControl.AddCmd(clientDevice, null, null, Device.Command.PingReturn, "");
        }
        //else if ( cmd == "PingEnable" )
        //{
        //string id = command.Substring( cmd.Length+1, command.Length - cmd.Length -1 );
        //Debug.LogWarning("TODO: CopyDevice: devType="+devName);
        //Master.LoadConfig(clientDevice, configName);
        //	ClientControl.AddCmd(clientDevice, null, null, Device.Command.Ping, id);
        //}
        else Debug.Log("ClientControl.IncomeCommand: unknown command[" + clientIP + "]='" + command + "', length=" + command.Length);
    }

//	public static void SendDevice(Device device, string butNotHim)
//	{
//		string cmd = "AddDevice:"+device.ToString();
//		foreach( string client in ClientControl.instance.clients )
//		{
//			if ( client == butNotHim ) continue;
//			ClientServer.Send(client, cmd);
//		}
//	}

    public static void SendDeviceParameter(Device device, Device.Parameter par)
    {
        SimpleSendDeviceParameter(device, par);
        //Device.Parameter par = device.GetParameterByDesc(parDesc);
        string devName = device.GetValueByName("Название");
        string devType = device.GetValueByName("Тип");
        string devRoom = device.GetValueByName("Комната");
        // SetParameterIndex deviceIndex parameterIndex parameterValue
        string cmd = "s;" + devName + ";" + devType + ";" + devRoom + ";" + par.name + ";" + par.value;
//		Debug.Log("ClientControl.SetParameterByDesc: cmd="+cmd);
        List<Device> clients = Master.GetDevicesByType("CLIENT");
        foreach (Device client in clients)
        {
            // TODO possible flood by spam messages:
            // sample "SetParameter:DEVICE_NAME,SOME_DMX(rgb)=113.233.11"
            if (!client.isClientStopSetMessages)
                AddCmd(client, device, par, Device.Command.SetParameter, cmd);
        }
    }

    public static void AddCmd(Device client, Device device, Device.Parameter par, Device.Command cmd, string message)
    {
        if (client == null)
        {
            Debug.LogWarning("AddCmd: client is null, device=" + device.GetValueByName("Название") + ", par=" + par + ", cmd=" + cmd + ", msg=" + message);
            return;
        }
        Device.Message devMsg = null;
        switch (cmd)
        {
            case Device.Command.AddDevice:
                devMsg = new Device.Message(device, null, "a;" + Master.Devices.IndexOf(device) + ";" + device.ToString());
                break;
            case Device.Command.RemoveDevice:
                devMsg = new Device.Message(device, null, "r;" + Master.Devices.IndexOf(device));
                break;
            case Device.Command.Message:
                devMsg = new Device.Message(device, null, "m;" + message);
                break;
            case Device.Command.SetParameter:
                // 
                devMsg = new Device.Message(device, par, message);
                if (client.GetValueByName("Подключён") == "1")
                    Debug.Log("AddCmd '" + cmd.ToString() + "'(" + par + ")(" + message + ") to client '" + client.GetValueByName("Название") + "'");
                break;
            case Device.Command.YourIP:
                devMsg = new Device.Message(null, null, "i;" + message);
                break;
            case Device.Command.BeginLoadDB:
                devMsg = new Device.Message(device, null, "b");
                Debug.Log("AddCmd '" + cmd.ToString() + "' to client '" + client.GetValueByName("Название") + "'");
                break;
            case Device.Command.EndLoadDB:
                devMsg = new Device.Message(device, null, "e");
                break;
            case Device.Command.ConfigsList:
                devMsg = new Device.Message(device, null, "l;" + message);
                break;
            case Device.Command.Ping:
                devMsg = new Device.Message(null, null, "p;" + message);
                break;
            case Device.Command.PingReturn:
                devMsg = new Device.Message(null, null, "p;" + message);
                break;
            default:
                Debug.LogWarning("AddCmd=" + cmd.ToString());
                break;
        }
        if (devMsg == null) return;
        client.messages.Add(devMsg);
//		int max = client.messages.Count;
//		for( int i = client.messages.Count-1;i>=0;i--)
//			if ( client.messages[i]==null ) Debug.LogWarning("AddCmd: client="+client.GetValueByName("Название")+" have message index="+i+"(max="+max+") is null, total messages="+client.messages.Count);

//		if ( client.messages.Count > 0 && client.messages[client.messages.Count-1] == null ) Debug.LogError("странная фигня="+client.messages.Count);
    }

	public static string SimpleFromClient(Device clientDevice, string clientIP, List<byte> buffer)
    {
        int length = buffer.Count;
		string bufferText = "buffer["+length+"]="; for( int i = 0; i < length; i++) bufferText += buffer[i].ToString()+", ";
        string client = "client: " + clientDevice.GetValueByName("Название") + " ip=" + clientIP + " buffer=["+bufferText+"]";
        if (length == 0) return "Receive zero buffer. "+client;
        int type = buffer[0];
        string logicValueString = "";
        List<byte> newBuffer = new List<byte>(buffer);
        if (length < 3) return "Type " + type + " message wrong length=" + length + ", " + client;
        int logicId = (buffer[1] << 8) + buffer[2];
        string typeString = "";
        // получаем команду от клиента и отправляем её всем другим клиентам
        switch (type)
        {
            case 1: // on off [0,1]
                if (length < 4) return "Type 1 message wrong length=" + length + ", " + client;
                typeString = "Включено";
                logicValueString = buffer[3].ToString();
                newBuffer.RemoveRange(0, 4);
                SimpleSendToAll(new Device.Message(new List<byte>() { 3, buffer[1], buffer[2], buffer[3] }), clientDevice);
                break;
            case 2: // color
                if (length < 6) return "Type 2 message wrong length=" + length + ", " + client;
                typeString = "Цвет";
                logicValueString = buffer[3]+"."+ buffer[4]+"."+ buffer[5];
                newBuffer.RemoveRange(0, 6);
                // не отправляем цвет клиентам так как они его не хранят!
                //SimpleSendToAll(new Device.Message(new List<byte>() { 3, buffer[1], buffer[2], buffer[3], buffer[4], buffer[5] }));
                break;
            case 3: // request status
                if (length < 3) return "Type 3 message wrong length=" + length + ", " + client;
                logicValueString = "";
                newBuffer.RemoveRange(0, 3);
                string r = SimpleRequestStatus(logicId);
                if (!string.IsNullOrEmpty(r)) Debug.Log("Request Cancel: " + client + ", logicId=" + logicId + ", result=" + r + ", " + bufferText);
                break;
            case 4: // яркость
                if (length < 4) return "Type 4 message wrong length=" + length + ", " + client;
                typeString = "яркость";
                logicValueString = buffer[3].ToString();
                newBuffer.RemoveRange(0, 4);
                // не отправляем яркость клиентам так как они её не хранят
                //SimpleSendToAll(new Device.Message(new List<byte>() { 3, buffer[1], buffer[2], buffer[3] }));
                break;

            default:
                return "Wrong type="+type+", "+client;
        }
        
        // отправляем команду от клиента на физическое устройство
        if (!string.IsNullOrEmpty(logicValueString))
        {
            var device = Master.GetDeviceById(logicId);
            // get connection
            //Connection connection = Master.Connections.FirstOrDefault(conn => conn.Id == logicId);
            if (device == null) return client + " not found device with id=" + logicId + ", " + bufferText;
            // send to physical device
            var result = DEFAULT.OnClientSimpleCommand(device, typeString, logicValueString);
            if (result != "true")
                Debug.Log("Device Cancel: " + client + ", " + device + ", value=" + logicValueString + ", result=" +
                          result + ", " + bufferText);
        }
        //else Debug.LogWarning("SimpleFromClient: unknown logic");

        if (newBuffer.Count > 0) return SimpleFromClient(clientDevice, clientIP, newBuffer);
        return null;
    }

    private static void SimpleSendToAll(Device.Message message, Device excludeDevice=null)
    {
        List<Device> clients = Master.GetDevicesByType("CLIENT");
        Debug.Log("SimpleSendToAll: clients["+clients.Count+"] msg="+message.ToString());
        foreach (Device clientItem in clients)
        {
            if (clientItem == excludeDevice) continue;
            // TODO possible flood by spam messages:
            //if (!clientItem.isClientStopSetMessages)
                clientItem.messagesSimple.Add(message);
        }
    }

    private static void SimpleSendDeviceParameter(Device device, Device.Parameter par)
    {
        //var devName = device.GetValueByName("Название");
        //Connection connection = Master.Connections.FirstOrDefault(conn => conn.Device == devName && conn.Component == par.name);
        //if (connection == null) return;
        //SimpleRequestStatus(connection.Id, connection);
        SimpleRequestStatus(device.id);
    }
    
    private static string SimpleRequestStatus(int deviceId)//int logicId, Connection connection = null)
    {
        var device = Master.GetDeviceById(deviceId);
        if (device == null) return "device no found with id="+deviceId;//if (connection != null) logicId = connection.Id;
        // todo
        //Debug.LogWarning("SimpleRequestStatus todo");
        //
        var logicId = device.id;
        byte logicId0 = (byte)((logicId >> 8) & 0xff);
        byte logicId1 = (byte)(logicId & 0xff);
        // get connection
        //if (connection == null) connection = Master.Connections.FirstOrDefault(conn => conn.Id == logicId);
        //if (connection == null) return "Not found connection id=" + logicId;
        // get device
        //var device = Master.GetDeviceById(logicId);
        
        /*/ get parametr
        var par = device.GetParameterByName(connection.Component);
        if (par == null) return "connection.Device=" + connection.Device + " component=" + connection.Component + " not found";

        //return "todo";
        // parse parametr
        /*
        if (par.type == "Команда") return "Команда: пропустим";
        if (par.type == "Цвет")
        {
            string[] colors = par.value.Split('.');
            int red = int.Parse(colors[0]);
            int green = int.Parse(colors[1]);
            int blue = int.Parse(colors[2]);
            SimpleSendToAll(new Device.Message(new List<byte>() { 3, logicId0, logicId1, (byte)red, (byte)green, (byte)blue }));
        }
        else 
        {
            int value = -1;
            if (!int.TryParse(par.value, out value)) return "cant parse to int value=" + par.value;
            SimpleSendToAll(new Device.Message(new List<byte>() { 3, logicId0, logicId1, (byte)value }));
        }
        */
        var valueString = device.GetValueByName("Включено");
        if (!string.IsNullOrEmpty(valueString))
        {
            int value = valueString == "0" ? 0 : 1;
            SimpleSendToAll(new Device.Message(new List<byte>() { 3, logicId0, logicId1, (byte)value }));
        }

        return null;
    }

}

