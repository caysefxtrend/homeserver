﻿using UnityEngine;
using System.Collections;
using System.Net.Sockets;
using System.Collections.Generic;
using System;

public class TcpClientDriver2 : MonoBehaviour
{
	public static TcpClientDriver2 instance;
	void Awake () { TcpClientDriver2.instance = this; }

	public List<TcpClientState> states = new List<TcpClientState>();

	public static IDriver Create(Device device, string ip, int port)
	{
		return SendCommand(ip,port,"",device);
	}

	public static IDriver SendCommand(string ip, int port, string cmd, Device device)
	{
		//Debug.Log("Ip2irClient.SendCommand: irip="+irip+", irport="+irport+", cmd="+cmd);
		return instance.AddCommand(ip,port,cmd,device);
	}

	public IDriver AddCommand(string ip, int port, string cmd, Device device)
	{
	//	Debug.Log("Ip2irClient.SendCommand: ip="+ip+", port="+port+", cmd="+cmd);
		foreach(TcpClientState state in states)
			if ( state.ip == ip ) { state.AddCommand(port, cmd, device); return state; }
		TcpClientState newState = new TcpClientState(ip,port);
		Debug.Log("TcpClientDriver2.Create driver: ip="+ip+", port="+port+", device="+device.GetValueByName("Название"));
		states.Add(newState);
		newState.AddCommand(port, cmd, device);
		return newState;
	}

	// Use this for initialization
	void Start ()	{	}
	
	// Update is called once per frame
	void Update ()	
	{
		foreach(TcpClientState state in states)
			state.Update();	
	}

}

[System.Serializable]
public class TcpClientState : IDriver {
	bool isSocketOpened = false;
	bool isInConnect = false;
	public string state;
	byte[] dataBuffer = new byte[1024];
	TcpClient mySocket;
	NetworkStream theStream;
	public string ip;
	int port;
	public Device masterDevice = null;
	public List<string> commands = new List<string>();
	public List<Device> devices = new List<Device>();
	public override string ToString ()
	{
		return state;// string.Format ("[TcpClientDriver]");
	}
	public TcpClientState(string ip, int port)
	{
		this.ip = ip;
		this.port = port;
	}
	public void SendCommand(string cmd, Device device)
	{
		if ( cmd.Length == 0 ) return;
		commands.Add(cmd);
		devices.Add(device);
		masterDevice = device;
	}
	public void Destroy() {}
	public void AddCommand(int port, string cmd, Device device)
	{
		if ( cmd.Length == 0 ) return;
		if ( port != this.port )
		{
			Debug.LogWarning("State ip="+ip+", port="+this.port+" != "+port);
			return;
		}
		commands.Add(cmd);
		devices.Add(device);
		masterDevice = device;
	}

	private float everySecond = 0;
	private float oneTenSecond = 0;
	public void Update()
	{
		//if ( isSelfDestroy ) { CloseSocket (); }
		if ( Time.time - everySecond > 1f ) // every second
		{
			if ( !isSocketOpened && !isInConnect) OpenSocket();
			everySecond = Time.time;
		}
		if ( Time.time - oneTenSecond > 0.1f ) // 1/10 second
		{
			if ( isSocketOpened && !isInConnect && commands.Count > 0 ) 
			{
				WriteSocket( commands[0] );
				//if ( isShowLog )
				//	Debug.Log("Device "+device.GetValueByName("Название")+"."+device.GetValueByName("Комната")+" - driver connected to "+host+":"+port+", Write: "+commands[0]);
				if ( commands.Count > 0 ) 
				{
					commands.RemoveAt(0); // socket can be close and commands cleared
					devices.RemoveAt(0);
				}
			}				
			oneTenSecond = Time.time;
		}

		string msg = ReadSocketString ();
		if ( msg.Length > 0 )
		{
			//if ( isShowLog )
			//	Debug.Log("Device "+device.GetValueByName("Название")+"."+device.GetValueByName("Комната")+" - driver connected to "+host+":"+port+", Receive: "+msg);
			//Debug.Log(Time.time.ToString()+" : Receive: "+msg);
			//masterDevice
			if ( masterDevice != null )
				DEFAULT.OnDriverReceiveString( masterDevice, msg );
			//ParseDataFromModbus( bytesReaded );
		}
	}

	public void OpenSocket ()
	{
		isSocketOpened = false;
		isInConnect = true;
		try {
			if ( masterDevice != null )
				masterDevice.SetValueByName("0","подключено");
			if ( mySocket == null )
				mySocket = new System.Net.Sockets.TcpClient(AddressFamily.InterNetwork);
			//transactionId ++;
			isInConnect = true;
			state = "in connect";
			mySocket.BeginConnect(ip, port, 
				new AsyncCallback(ConnectCallback), mySocket);
		} catch (Exception e) {	Debug.LogError ("TcpClientState.OpenSocket: "+ip+":"+port+" error: " + e); }
	}

	public void ConnectCallback(IAsyncResult ar)
	{		
		TcpClient socket = (TcpClient) ar.AsyncState;
		isInConnect = false;
		state = "connect fail";
		socket.EndConnect(ar); // thread break if connection fail !
		theStream = socket.GetStream ();
		//transactionId = 0;
		isSocketOpened = true;	
		state = "connect success";
		if ( masterDevice != null ) masterDevice.SetValueByName("1","подключено");
		//Debug.Log("Device "+device.GetValueByName("Название")+"."+device.GetValueByName("Комната")+" - driver connected to "+host+":"+port+", подключено="+device.GetValueByName("подключено"));
		//commands.Clear();
		if ( masterDevice != null ) DEFAULT.OnDriverConnect( masterDevice );
	}

	public void WriteSocket (string cmd)
	{		
		if (!isSocketOpened) return;
		if (!mySocket.Connected ) { Debug.Log("Socket is disconnected"); return; }
		try {	
			state = "send: "+cmd;
			//	Debug.Log(Time.time.ToString()+" : "+state);
			byte[] byteBuffer = System.Text.Encoding.UTF8.GetBytes (cmd);
			theStream.Write(byteBuffer,0,byteBuffer.Length);
			theStream.Flush();
		} catch (Exception e) 
		{	
			//Debug.Log ("Socket error(device="+device.GetValueByName("Название")+"): " + e); 
			Debug.LogError ("TcpClientState.OpenSocket: "+ip+":"+port+" error: " + e);
			CloseSocket();
		}
	}

	public void CloseSocket ()
	{
		state = "socket closed";
		commands.Clear();
		devices.Clear();
		//lastWrite = "TvDriver.CloseSocket: ["+transactionId+"] "+host+":"+port;
		//if (setReadWriteLog) Debug.Log(lastWrite);
		if (!isSocketOpened) return;
		//theWriter.Close ();
		//theReader.Close ();
		if ( mySocket != null ) { mySocket.Close (); mySocket = null; }
		isSocketOpened = false;
		//transactionId = 0;
		//device.SetValueByName("0","подключено");
		//DEFAULT.OnDriverDisconnect( device );
		//isInited = false;
	}

	public string ReadSocketString ()
	{
		if (!isSocketOpened || !mySocket.Connected) return "";
		if(theStream.CanRead && theStream.DataAvailable)
		{
			//	int numberOfBytesRead = 0;
			//	string receiveMsg = "";
			//byte[] data = new byte[1024];
			//return theStream.ReadByte().ToString();
			int numberOfBytesRead = theStream.Read(dataBuffer, 0, dataBuffer.Length);  
			if ( numberOfBytesRead == 0 ) { Debug.LogWarning("zero bytes received"); return ""; }
			return System.Text.Encoding.UTF8.GetString(dataBuffer, 0, numberOfBytesRead);
			//if ( numberOfBytesRead == 0 ) socketReady = false;
			//for(int i = 0; i < numberOfBytesRead; i++)
			//	receiveMsg += dataBuffer[i].ToString()+", ";
			//		Debug.Log("ReadSocket: ["+numberOfBytesRead+"] '" + receiveMsg+"', data.available="+theStream.DataAvailable.ToString());
			//lastRead = receiveMsg;
			//return numberOfBytesRead;
		}
		return "";
	}
}

