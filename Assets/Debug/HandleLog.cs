﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class HandleLog : MonoBehaviour
{
	public bool showBeckhoffLog = false;
	public static HandleLog instance = null;
	private int maxLogSize = 200;
	public List<string> stringLog = new List<string>();
	public static List<string> GetStringLog() { return HandleLog.instance.stringLog; }

	public List<string> beckhoffLog = new List<string>();
	public static List<string> GetBeckhoffLog() { return HandleLog.instance.beckhoffLog; }

	void Awake () 
	{
		HandleLog.instance = this;
		Application.logMessageReceivedThreaded += HandleLogCallback;
	}
	// Use this for initialization
	void Start ()	{	}

	public static string today 
	{ 
		get 
		{
			string now = "";
			System.DateTime time = System.DateTime.Now;
			now += time.Year+"."+time.Month+"."+time.Day;
			now += " "+time.Hour+":"+time.Minute+":"+time.Second;
			return now;
		}
	}
	
	// Update is called once per frame
	//void Update ()	{		}

	public static void LogBeckhoff(string message)
	{
		if (HandleLog.instance.showBeckhoffLog) Debug.LogWarning("beckhoffLog+="+message);
	//	if ( HandleLog.GetBeckhoffLog().Count > 100 )
	//		Debug.LogWarning("1_HandleLog.instance.beckhoffLog.Count="+HandleLog.GetBeckhoffLog().Count+", HandleLog.instance.maxLogSize="+HandleLog.instance.maxLogSize);
		HandleLog.GetBeckhoffLog().Add( today+" "+message );
		if ( HandleLog.GetBeckhoffLog().Count > HandleLog.instance.maxLogSize ) 
			HandleLog.GetBeckhoffLog().RemoveAt(0);
		if ( HandleLog.GetBeckhoffLog().Count > HandleLog.instance.maxLogSize ) 
			HandleLog.GetBeckhoffLog().RemoveAt(0);
	//	if ( HandleLog.GetBeckhoffLog().Count > 100 )
	//		Debug.LogWarning("2_HandleLog.instance.beckhoffLog.Count="+HandleLog.GetBeckhoffLog().Count+", HandleLog.instance.maxLogSize="+HandleLog.instance.maxLogSize);
	}

	void HandleLogCallback (string logString, string stackTrace, LogType type) {
		string item = today+" "+type.ToString()+", "+logString;
		if ( type != LogType.Log && type != LogType.Warning )
			item += ", "+stackTrace;
		FileAddString("log.log",item+System.Environment.NewLine);
		stringLog.Add(item);
		if ( stringLog.Count > maxLogSize ) stringLog.RemoveAt(0);
		if ( stringLog.Count > maxLogSize ) stringLog.RemoveAt(0);
	}

	void HandleLogCallback2 (string logString, string stackTrace, LogType type) {
		//		if UNITY_EDITOR			
		//		if (Application.isEditor) {
		//			EditorApplication.isPlaying = false;
		//		}		
		//		endif

		//System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace();

		//output = logString;
		//stack = stackTrace;
		//string stack = stackTrace.Split(;
		string stack = stackTrace;
		if (type == LogType.Log || type == LogType.Warning) {
			string[] splittedText = stackTrace.Split (new char[] { '\n' });
			stack = splittedText [1];
			int index = stack.LastIndexOf ('/');
			stack = stack.Substring (index + 1, stack.Length - index - 1 - 1);
			return;
		}
		else stack = "\n"+stack;
		if ( type == LogType.Exception ) {
			logString = Utility.SetColor (logString, Color.red);
		}
		//ShowDebug.AddToLog (logString + "  {" + type.ToString () + "}", stack);
	}

	public void FileAddString(string filename, string text)
	{
//		string filepath = Master.Datapath + "/"+filename;
		//Debug.LogWarning("Save configuration to "+filename);
		try 
		{
			File.AppendAllText(Master.Datapath + "/"+filename,text,System.Text.Encoding.UTF8);
		}
		catch//(System.Exception exc)
		{
		//	Debug.LogError("При записи в "+filename+" возникла ошибка "+exc);
		}	
	}
}

