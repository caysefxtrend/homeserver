﻿using UnityEngine;
using System.Collections;

//It is common to create a class to contain all of your
//extension methods. This class must be static.
using System.Collections.Generic;


public static class ExtensionMethods
{
	//Even though they are used like normal methods, extension
	//methods must be declared static. Notice that the first
	//parameter has the 'this' keyword followed by a Transform
	//variable. This variable denotes which class the extension
	//method becomes a part of.
	public static void AddWord(this List<byte> list, int value)
	{
		list.Add((byte)((value>>8)&0x00ff));
		list.Add((byte)(value&0x00ff));
	}
	public static void AddByte(this List<byte> list, int value)
	{
		list.Add((byte)(value&0x00ff));
	}
}