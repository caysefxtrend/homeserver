﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class LOCK 
{
	public static string CreateDefault()
	{
		string randomName = new System.Random().Next(11111,99999).ToString();
		return Make( "", "Замок "+randomName, "0" );		
	}

	private static string Make(string room, string name, string address)
	{
		return "Тип=LOCK,Комната="+room+",Название="+name+
			",Включено=0,адрес(percent)="+address
			+",Открыть-Закрыть(Команда)=";
	}

	public static List<string> GetDefaultDevices()
	{
		List<string> list = new List<string>();	
		list.Add( Make( "Зал 1 этаж","Замок Зал 1 этаж","1" ) );		
		list.Add( Make( "Гостиная","Замок Гостиная У камина","2" ) );		
		list.Add( Make( "Гостиная","Замок Гостиная У дивана","3" ) );		
		list.Add( Make( "Гостиная","Замок Гостиная У стола","4" ) );		
		list.Add( Make( "Бассейн","Замок Бассейн","5" ) );		
		list.Add( Make( "Зал 2 этаж","Замок Зал 2 этаж","6" ) );		
		list.Add( Make( "Хозяйский СУ","Замок Хозяйский СУ","7" ) );		
		list.Add( Make( "Кабинет","Замок Кабинет","8" ) );		
		list.Add( Make( "СПА","Клапан быстрого слива воды в душе SPA","9" ) );		

		return list;
	}

	public static string OnClientSetParameter( Device device, Device.Parameter newPar )
	{
		string result = "true";
		if ( newPar.name == "Включено" )
		{
			int position = int.Parse( device.GetValueByName("адрес") );
			int value = newPar.value == "0" ? 0 : 1;
			ModbusClient.SendSet(BeckhoffItems.LOCK, position, value);
		}
		else {
			Debug.LogWarning("LOCK.OnClientSetParameter: unknown par: "+newPar.ToString()+", newPar.name='"+newPar.name+"'");
			result = "false";
		}

		return result;
	}

	public static void Initialize(Device device, bool onlySelf)
	{
		if ( onlySelf ) return;
		int position = int.Parse( device.GetValueByName("адрес") );
		ModbusClient.SendGet(BeckhoffItems.LOCK, position);
	}

	public static void OnReceiveMessageFromModbus(int addressIndex, int value)
	{
		Device device = Master.instance.GetDevice("LOCK","адрес",addressIndex.ToString() );
		if ( device == null ) return;
		device.SetValueByName( value == 0 ? "0" : "1", "Включено" );				
		ClientControl.SendDeviceParameter(device, device.GetParameterByName("Включено"));
		if ( !Master.isOnInit )
			Debug.Log("LOCK: "+device.GetValueByName("Комната")+" "+device.GetValueByName("Название")+" Включено="+device.GetValueByName("Включено"));				
		if ( Master.isOnInit && device.GetValueByName("адрес") == "9" ) 
		{
			Debug.Log("Master.InitializeDevices: END devices.Count="+Master.Devices.Count);
			Master.isOnInit = false;
		}

	}
}

