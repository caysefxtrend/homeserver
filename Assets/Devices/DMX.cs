﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class DMX 
{
	public static string CreateDefault()
	{
		string randomName = new System.Random().Next(11111,99999).ToString();
		return Make( -1,"", "DMX "+randomName, "0" );		
	}

	private static string Make(int id, string room, string name, string address)
	{
		return "Тип=DMX,Комната="+room+",Название="+name+
			",Включено=0,Цвет=0.0.0,адрес(percent)="+address+
			",Включить(Команда)="+",Выключить(Команда)="+",Id="+id+
			"";
	}

	public static List<string> GetDefaultDevices() // id: [800-856],[301-314]
	{
		List<string> list = new List<string>();
	//	string dmx;
		list.Add( Make( 800,"Зал 0 этаж","Подсветка боковой конструкции с паркетом","1" ) ); // 7 17-15
		list.Add( Make( 801,"Зал 0 этаж","Подсветка боковой конструкции с зеркалами","2" ) ); // 11
		//		list.Add( Make( -1,"СПА","DMX подсветка боковых светильников","3" ) ); // 4
		list.Add( Make( 802,"СПА","Вход в душевую и потолок в центре","4" ) ); // 17
		list.Add( Make( 803,"СПА","Периметр на потолке","5" ) ); // 14
		list.Add( Make( 804,"СПА","* Вход в парилку и потолок в центре","6" ) ); // 20
		list.Add( Make( 301,"Гостиная","DMX подсветка прожекторных ниш у камина","7" ) ); // 47
		list.Add( Make( 302,"Гостиная","DMX подсветка прожекторных ниш у дивана","8" ) ); // 44
		list.Add( Make( 303,"Гостиная","DMX подсветка прожекторных ниш у стола","9" ) ); // 41
		list.Add( Make( 304,"Гостиная","Подсветка над кухней среднее стекло","10" ) ); // 32
		list.Add( Make( 305,"Гостиная","Подсветка над кухней верхнее стекло","11" ) ); // 35
		list.Add( Make( 306,"Гостиная","Подсветка над кухней нижнее стекло","12" ) ); // 29
		list.Add( Make( 307,"Гостиная","Подсветка колонны диоды","13" ) ); // 38
		list.Add( Make( 308,"Гостиная","Подсветка ниши 3 сегмента - камин и кухня","14" ) ); // 62
		list.Add( Make( 309,"Гостиная","Подсветка ниш для штор кухня стол -1","15" ) ); // 50
		list.Add( Make( 310,"Гостиная","Подсветка ниш для штор кухня стол -2","16" ) ); // 53
		list.Add( Make( 311,"Гостиная","Подсветка ниш для штор камин диван -1","17" ) ); // 56
		list.Add( Make( 312,"Гостиная","Подсветка ниш для штор камин диван -2","18" ) ); // 59
		list.Add( Make( 313,"Гостиная","DMX подсветка полки кухни диоды","19" ) ); // 23
		list.Add( Make( 314,"Гостиная","Подсветка барной стойки стекло","20" ) ); // 26
		list.Add( Make( 805,"Гостевой СУ","подсветка зеркала","21" ) ); // 77
		list.Add( Make( 806,"Гостевой СУ","Подсветка подоконника ","22" ) ); // 74
		list.Add( Make( 807,"Гостевой СУ","Подсветка потолка периметр","23" ) ); // 71
		list.Add( Make( 808,"Гостевой СУ","Подсветка раковины 8","24" ) ); // 68
		list.Add( Make( 809,"Зал 1 этаж","Подсветка по периметру торца стены","25" ) ); // 65
		list.Add( Make( 810,"Бассейн","Потолочные светильники центральный 1 часть 8","26" ) ); // 80
		list.Add( Make( 811,"Бассейн","Потолочные светильники центральный 1 часть -2","27" ) ); // 83
		list.Add( Make( 812,"Бассейн","Потолочные светильники центральный 2 часть 8","28" ) ); // 86
		list.Add( Make( 813,"Бассейн","Потолочные светильники центральный 2 часть -2","29" ) ); // 89
		list.Add( Make( 814,"Бассейн","Потолочные светильники центральный 3 часть","30" ) ); // 92
		list.Add( Make( 815,"Бассейн","* Потолочные светильники периметр весь","31" ) ); // 95
		list.Add( Make( 816,"Детская","Подсветка ниш для штор над столом","32" ) ); // 116
		list.Add( Make( 817,"Детская","Подсветка ниш для штор у кровати","33" ) ); // 119
		list.Add( Make( 818,"Детская","Потолочная подсветка над кроватью центр 1 полоса","34" ) ); // 122
		list.Add( Make( 819,"Детская","Потолочная подсветка над кроватью 3 полосы","35" ) ); // 125
		list.Add( Make( 820,"Детская","Потолочная подсветка над ТВ 3 полосы","36" ) ); // 233
		list.Add( Make( 821,"Детский СУ","Подсветка столешницы и зеркала","37" ) ); // 104
		list.Add( Make( 822,"Хозяйская спальня","Подсветка ниши для штор","38" ) ); // 128
		list.Add( Make( 823,"Хозяйская спальня","Светильник в нише на потолке - ближе к изголовью","39" ) ); // 134
		list.Add( Make( 824,"Хозяйская спальня","Светильник в нише на потолке - дальше от изголовья","40" ) ); // 131
		list.Add( Make( 825,"Хозяйский СУ","Подсветка ниши штор","41" ) ); // 236
		list.Add( Make( 826,"Хозяйский СУ","Подсветка столешницы с раковинами","42" ) ); // 239
		list.Add( Make( 827,"Хозяйский СУ","Подсветка ниши для штор - ванна 2 окна и дверь","43" ) ); // 146
		list.Add( Make( 828,"Хозяйский СУ","Подсветка ванны","44" ) ); // 149
		list.Add( Make( 829,"Кабинет","Подсветка стена из шкур","45" ) ); // 137
		list.Add( Make( 830,"Кабинет","Подсветка потолка квадрат из шкур","46" ) ); // 140
		list.Add( Make( 831,"Кабинет","Подсветка ниш для штор","47" ) ); // 143
		list.Add( Make( 832,"Гостевая спальня","Подсветка ниш для штор и периметр комнаты","48" ) ); // 110
		list.Add( Make( 833,"Гостевая спальня СУ","Подсветка зеркала и раковины","49" ) ); // 113
		list.Add( Make( 834,"Театр","Подсветка окон - 1","50" ) ); // 163
		list.Add( Make( 835,"Театр","Подсветка окон - 2","51" ) ); // 166
		list.Add( Make( 836,"Театр","Подсветка окон - 3","52" ) ); // 169
		list.Add( Make( 837,"Театр","Подсветка окон - 4","53" ) ); // 172
		list.Add( Make( 838,"Театр","Подсветка колонн низ и верх экрана","54" ) ); // 197
		list.Add( Make( 839,"Театр","Подсветка стены и полок за барной стойкой","55" ) ); // 160
		list.Add( Make( 840,"Театр","Подсветка левая ниша по периметру","56" ) ); // 157
		list.Add( Make( 841,"Театр","Подсветка правой нишы по периметру","57" ) ); // 154
		list.Add( Make( 842,"Театр","Подсветка ниши на потолке над входным порталом","58" ) ); // 212
		list.Add( Make( 843,"Театр","Натяжной потолок 6 линий левая сторона","59" ) ); // 175
		list.Add( Make( 844,"Театр","Натяжной потолок 2 правая сторона и 2 центр","60" ) ); // 178
		list.Add( Make( 845,"Театр","Натяжной потолок 4 правая сторона","61" ) ); // 181
		list.Add( Make( 846,"Театр","Подсветка столешницы барной стойки и колонн","62" ) ); // 200
		list.Add( Make( 847,"Театр","Подсветка рабочей зоны кухни ","63" ) ); // 215
		list.Add( Make( 848,"Зал 3 этаж","Подсветка периметра над лестн вер ур ","64" ) ); // 191
		list.Add( Make( 849,"Зал 3 этаж","Подсветка периметра над лестн нижний ур","65" ) ); // 194
		list.Add( Make( 850,"Кальянная","* Подсветка подоконников","66" ) ); // 206
		list.Add( Make( 851,"СУ 3 этаж","Подсветка зеркала в нише - стена и потолок","67" ) ); // 188
		list.Add( Make( 852,"СУ 3 этаж","Подсветка раковины -2","68" ) ); // 209
		list.Add( Make( 853,"Гостевая спальня","DMX подсветка ниш для штор и периметр комнаты -2","69" ) ); // 107
		list.Add( Make( 854,"СПА","Подсветка душа","70" ) ); // 1
		list.Add( Make( 855,"Театр","* Натяжной потолок 4 правая сторона -2","71" ) ); // 184
		list.Add( Make( 856,"Зал 3 этаж","Подсветка верхнего окна","72" ) ); // 203

	//	list.Add( Make( -1,"Бассейн","* Потолочные светильники периметр правая сторона","" ) ); // 98->95
	//	list.Add( Make( -1,"Бассейн","* Потолочные светильники периметр центр","" ) ); // 101->95
	//	list.Add( Make( -1,"СПА","* Подсветка душа","" ) ); // 24->1

		return list;
	}	

	public static string OnClientSetParameter( Device device, Device.Parameter newPar )
	{
		string result = "true";
		if ( newPar.name == "Включено" )
		{
			int value = 0;
			if ( newPar.value == "1" )
			{
				value = Utility2.RgbStringToInt( device.GetValueByName("Цвет") );
				if ( value == 0 ) value = 0xFFFF;
			}
			int position = int.Parse( device.GetValueByName("адрес") );	
			ModbusClient.SendSet(BeckhoffItems.DMX, position, value);
		}
		else if ( newPar.name == "Цвет" )
		{
		    device.SetValueByName(newPar.value == "0.0.0" ? "0" : "1", "Включено");
		    device.SetValueByName(newPar.value, "Цвет");

            int position = int.Parse( device.GetValueByName("адрес") );
				string color = newPar.value;
				var value = Utility2.RgbStringToInt( color );
				ModbusClient.SendSet(BeckhoffItems.DMX, position, value);
		}		
		else {
			Debug.LogWarning("DMX.OnClientSetParameter: unknown par: "+newPar.ToString()+", newPar.name='"+newPar.name+"'");
			result = "false";
		}

		return result;
	}

	public static void Initialize(Device device, bool onlySelf)
	{
		if ( onlySelf ) return;
		int position = int.Parse( device.GetValueByName("адрес") );
		ModbusClient.SendGet(BeckhoffItems.DMX, position);
	}

	public static void OnReceiveMessageFromModbus(int addressIndex, int value)
	{
		Device device = Master.instance.GetDevice("DMX","адрес",addressIndex.ToString() );
		if ( device == null ) return;
		if ( value > 0 ) 
		{
			device.SetValueByName( Utility2.IntToRgbString(value), "Цвет" );			
			device.SetValueByName( "1", "Включено" );		
			ClientControl.SendDeviceParameter(device, device.GetParameterByName("Цвет") );				
		}
		else {
			device.SetValueByName( "0", "Включено" );		
			ClientControl.SendDeviceParameter(device, device.GetParameterByName("Включено") );		
		}
		//ClientControl.SendDeviceParameter(device, device.GetParameterByName("Цвет") );				
		//ClientControl.SendDeviceParameter(device, device.GetParameterByName("Включено") );		

		if ( !Master.isOnInit )
			Debug.Log("DMX: "+ device.id + " " + device.GetValueByName("Комната")+" "+device.GetValueByName("Название")+" Включено="+device.GetValueByName("Включено")+", Цвет="+device.GetValueByName("Цвет"));				
	}

	public static void OnClientCommand( Device device, Device.Parameter par, Device client )
	{
		if ( par.name == "Включить" )
		{
			OnClientSetParameter( device, new Device.Parameter("Включено","","1") );
		}
		else if ( par.name == "Выключить" )
		{
			OnClientSetParameter( device, new Device.Parameter("Включено","","0") );
		}
		else 
		{
			ClientControl.AddCmd( client, null, null, Device.Command.Message, 
				"Устройство \""+ device.id + " " + device.GetValueByName("Название")+
				"\" не готово выполнить команду \""+par.name+"\"");
		}
	}
}

