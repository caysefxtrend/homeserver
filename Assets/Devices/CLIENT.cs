﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class CLIENT 
{
	public static List<string> GetDefaultDevices()
	{
		List<string> list = new List<string>();
//		list.Add( "Тип=CLIENT,Комната=Гостиная,Название=Клиент 127.0.0.1"+
//			",Включено=1"+",IP(string)=127.0.0.1"+
//			",Редактирование(bool)=0,Все комнаты(bool)=0" );
		return list;
	}

	public static string GetDefaultDevice( string ip )
	{
		return "Тип=CLIENT,Комната=Гостиная,Название=Клиент "+ip+
			//",Включено=0"+
			",IP(string)="+ip+
			",Редактирование(bool)=1,Все комнаты(bool)=1,Подключён(bool)=0" +
		//	",Сообщений(string)="+
			//",Требует сброса(string)=1"+
			",Пинг(bool)=0,Пингов в мс(string)=100"+
			"";		
	}

	public static void Initialize(Device device, bool onlySelf)
	{
        if (onlySelf)
        {
            if (device.messages == null)
                device.messages = new ConcurrentList<Device.Message>();
            if (device.messagesSimple == null)
                device.messagesSimple = new ConcurrentList<Device.Message>();
        }
    }

	public static string OnClientSetParameter( Device device, Device.Parameter newPar )
	{
		string result = "true";
		if ( newPar.name == "Редактирование" )
		{
			// сначала рассылаем новое имя
			ClientControl.SendDeviceParameter(device, newPar);	
			// а потом пишем в базу
			device.SetValueByName( newPar.value, newPar.name );		
			Master.IsChanged = true;
		}
		else if ( newPar.name == "Все комнаты" )
		{
			// сначала рассылаем новое имя
			ClientControl.SendDeviceParameter(device, newPar);	
			// а потом пишем в базу
			device.SetValueByName( newPar.value, newPar.name );		
			Master.IsChanged = true;
		}	
		else if ( newPar.name == "Комната" )
		{
			ClientControl.SendDeviceParameter(device, newPar);	
			device.SetValueByName( newPar.value, newPar.name );		
			Master.IsChanged = true;
		}
		else if ( newPar.name == "Пинг" )
		{
			ClientControl.SendDeviceParameter(device, newPar);	
			device.SetValueByName( newPar.value, newPar.name );		
			Master.IsChanged = true;
			PingServer.OnChangePingOfDevice(device);
		}
		else if ( newPar.name == "Пингов в мс" )
		{
			ClientControl.SendDeviceParameter(device, newPar);	
			device.SetValueByName( newPar.value, newPar.name );		
			Master.IsChanged = true;
			PingServer.OnChangePingOfDevice(device);
		}
		else {
			Debug.LogWarning("CLIENT.OnClientSetParameter: unknown par: "+newPar.ToString()+", newPar.name='"+newPar.name+"'");
			result = "false";
		}

		return result;
	}


}

