﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class HVAC 
{
	private static string Make(string name, string room)
	{
		return "Тип=HVAC,Название="+name+
			",Комната="+room+
			",Включено=0"+
			",Температура(string)=+23"+",Вентилятор(string)=1"+
			",Включить(Команда)="+"Выключить(Команда)="+
			"";
	}

	public static List<string> GetDefaultDevices()
	{
		List<string> list = new List<string>();	
		list.Add( Make( "HVAC 0 этаж","Зал 0 этаж" ) );		
		list.Add( Make( "HVAC гостиная","Гостиная" ) );		
		list.Add( Make( "HVAC детская","Детская" ) );		
		list.Add( Make( "HVAC гостевая","Гостевая спальня" ) );		
		list.Add( Make( "HVAC хозяйская","Хозяйская спальня" ) );		
		list.Add( Make( "HVAC кабинет","Кабинет" ) );		
		list.Add( Make( "HVAC кальянная","Кальянная" ) );		
		list.Add( Make( "HVAC театр","Театр" ) );		
		return list;
	}

	public static void Initialize(Device device, bool onlySelf)
	{
	}

	public static void OnReceiveMessageFromModbus(int addressIndex, int value)
	{
	}
}

