﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class ROOM 
{
//	public static string CreateDefault()
//	{		
//		string randomName = new System.Random().Next(11111,99999).ToString();
//		return "Тип=ROOM,Название="+"Комната"+" "+randomName+",МедиаЗона(string)=";
//	}

	public static List<string> GetDefaultDevices() // Id: [10000-10030]
	{
		List<string> list = new List<string>();
		string[] roomNames = {
			// 0 этаж
			"СПА","Зал 0 этаж","Зал 0 этаж СУ","Щитовая 0 этаж","Водоподготовочная",
			"Джакузи","Спортзал",
			// 1 этаж
			"Гостиная","Гостевой СУ","Зал 1 этаж","Бассейн",
			// 2 этаж
			"Зал 2 этаж",
			"Детская","Детский СУ","Хозяйская спальня","Хозяйский СУ",
			"Кабинет","Гостевая спальня","Гостевая спальня СУ",
			"Хозяйская гардеробная","Серверная",
			// 3 этаж
			"Кальянная","Театр","Зал 3 этаж","СУ 3 этаж",
			// улица
			"Беседка","Улица","Гараж","Кулерная","Бойлерная","Генераторная"
			// 
		};
		int roomStartId = 10000;
		for(int i=0; i<roomNames.Length;i++) // 31 rooms
		{
			if ( roomNames[i] == "Гостиная" )
				list.Add( "Тип=ROOM,Название="+roomNames[i]+",МедиаЗона=Гостиная"+",Id="+(roomStartId+i));
			else 
				list.Add( "Тип=ROOM,Название="+roomNames[i]+",МедиаЗона="+",Id="+(roomStartId+i));
		}
		return list;
	}

	public static void Initialize(Device device, bool onlySelf) { }
}

