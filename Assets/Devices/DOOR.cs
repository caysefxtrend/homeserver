﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class DOOR
{
	public static string CreateDefault()
	{
		string randomName = new System.Random().Next(11111,99999).ToString();
		return Make( "", "Дверь "+randomName, "0" );		
	}

	private static string Make(string room, string name, string address)
	{
		return "Тип=DOOR,Комната="+room+",Название="+name+
			",Включено=0,датчик(bool)=0,код(string)=,адрес(percent)="+address
			+",Открыть-Закрыть(Команда)=";
	}

	public static List<string> GetDefaultDevices()
	{
		List<string> list = new List<string>();	
		list.Add( Make( "Зал 1 этаж","Дверь Зал 1 этаж","1" ) );		
		list.Add( Make( "Гостиная","Дверь Гостиная У камина","2" ) );		
		list.Add( Make( "Гостиная","Дверь Гостиная У дивана","3" ) );		
		list.Add( Make( "Гостиная","Дверь Гостиная У стола","4" ) );		
		list.Add( Make( "Бассейн","Дверь Бассейн","5" ) );		
		//list.Add( Make( "Зал 2 этаж","Дверь Зал 2 этаж","6" ) );		
		list.Add( Make( "Хозяйский СУ","Дверь Хозяйский СУ","7" ) );		
		list.Add( Make( "Кабинет","Дверь Кабинет","8" ) );		
		//list.Add( Make( "СПА","Клапан быстрого слива воды в душе SPA","9" ) );		

		return list;
	}

	public static void Initialize(Device device, bool onlySelf)
	{
	}

	public static void OnReceiveMessageFromModbus(int addressIndex, int value)
	{
	}

	public static string OnClientSetParameter( Device device, Device.Parameter newPar )
	{
		string result = "true";
		if ( newPar.name == "Включено" )
		{
			// сначала рассылаем новое имя
//			ClientControl.SendDeviceParameter(device, newPar);	
			// а потом пишем в базу
			device.SetValueByName( newPar.value, newPar.name );		
			Master.IsChanged = true;
		}
		else {
			Debug.LogWarning("DOOR.OnClientSetParameter: unknown par: "+newPar.ToString()+", newPar.name='"+newPar.name+"'");
			result = "false";
		}

		return result;
	}

}

