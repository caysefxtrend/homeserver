﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class CURTAIN 
{
	public static string CreateDefault()
	{
		string randomName = new System.Random().Next(11111,99999).ToString();
		return Make( -1, "", "Штора "+randomName, "0", "0-0", "0-0" );		
	}

	private static string Make(int id, string room, string name, string address, string trigOpen, string trigClose)
	{
		return "Тип=CURTAIN,Комната="+room+",Название="+name+
			",Включено=0,уровень(percent)=100,адрес(percent)="+address+
			",триггер открытия(percent2)="+trigOpen+",триггер закрытия(percent2)="+trigClose+
			",время открытия(float)=30.0"+",время закрытия(float)=30.0"+
			",Открыть-Закрыть(Команда)="+",Id="+id;
	}

	public static List<string> GetDefaultDevices() // [900-918] [400-408]
	{
		List<string> list = new List<string>();	
		list.Add( Make( 900,"Детская","Окно у стола -1","1","9-14","9-15" ) );		
		list.Add( Make( 901,"Хозяйский СУ","Рулонка у двери","2","9-16","10-1" ) );		
		list.Add( Make( 902,"Кабинет","Дверь римка - до пола","3","10-2","10-3" ) );		
		list.Add( Make( 903,"Кабинет","Окно у стола -2","4","10-4","10-5" ) );		
		list.Add( Make( 904,"Кабинет","Окно у ТВ -1","5","10-6","10-7" ) );		
		list.Add( Make( 905,"Детская","Окно у ТВ -2","6","10-8","10-9" ) );		
		list.Add( Make( 906,"Кабинет","Окно у двери","7","10-10","10-11" ) );		
		list.Add( Make( 907,"Хозяйская спальня","Римская штора у кровати","8","10-16","11-1" ) );		
		list.Add( Make( 400,"Гостиная","Дверь у стола -1","9","15-2","15-3" ) );		
		list.Add( Make( 401,"Гостиная","Два окна у кухни","10","15-4","15-5" ) );		
		list.Add( Make( 402,"Гостиная","Одно окна за диваном - две шторы","11","15-6","15-7" ) );
		list.Add( Make( 403,"Гостиная","Одно окно у стола","12","15-8","15-9" ) );	
		list.Add( Make( 404,"Гостиная","Дверь у стола -2","13","15-10","15-11" ) );	
		list.Add( Make( 405,"Гостиная","Дверь у камина","14","15-12","15-13" ) );	
		list.Add( Make( 406,"Гостиная","Два окна у стола","15","15-14","15-15" ) );	
		list.Add( Make( 407,"Гостиная","Окно у камина","16","15-16","16-1" ) );	
		list.Add( Make( 408,"Гостиная","Два окна у дивана","17","16-2","16-3" ) );	
		list.Add( Make( 908,"Детский СУ","Рулонная штора","18","21-8","21-9" ) );	
		list.Add( Make( 910,"Хозяйский СУ","Рулонка у окна","19","11-5","11-4" ) );	
		list.Add( Make( 911,"Хозяйский СУ","Рулонка ванная","20","10-13","10-12" ) );	
		list.Add( Make( 912,"Хозяйский СУ","Рулонка туалет","21","21-13","21-12" ) );	
		list.Add( Make( 913,"Хозяйская спальня","Римская штора у тв","22","10-14","10-15" ) );		
		list.Add( Make( 914,"Гостевая спальня","Римская штора у су Гс","23","21-11","21-10" ) );		
		list.Add( Make( 915,"Гостевая спальня","Римская штора у тв Гс","24","11-3","11-2" ) );	
		list.Add( Make( 916,"Театр","Экран","25","9-13","9-12" ) );	
		list.Add( Make( 917,"Беседка","Беседка Жалюзи 9 приводов","26","20-10","20-11" ) );	
		list.Add( Make( 918,"Беседка","Беседка Жалюзи 3 привода","27","20-12","20-13" ) );	


		return list;
	}

	//public static void Initialize(Device device) { }

	public static void Initialize(Device device, bool onlySelf)
	{
		if ( onlySelf ) return;
	//	Debug.Log("CURTAIN.Initialize "+device.GetValueByName("Название")+", driver="+device.driver);
		int position = int.Parse( device.GetValueByName("адрес") );
		ModbusClient.SendGet(BeckhoffItems.CURTAINS, position);
		return ;

//		string open = device.GetValueByName("триггер открытия");
//		string[] openArray = open.Split('-');
//		int openPos1 = int.Parse( openArray[0] );
//		int openPos2 = int.Parse( openArray[1] );
//		float openTime = float.Parse(device.GetValueByName("время открытия"));
//		SetTrigger( openPos1, openPos2, openTime );

		//int position = int.Parse( device.GetValueByName("адрес") );
		//ModbusClient.SendGet(BeckhoffItems.DIMMER, position);
	}

	public static void OnReceiveMessageFromModbus(int addressIndex, int value)
	{
		Device device = null;
		//Debug.Log("CURTAIN.OnReceiveMessageFromModbus: CURTAIN["+addressIndex+"]="+value );
		device = Master.instance.GetDevice("CURTAIN","адрес",addressIndex.ToString() );
		if ( device == null ) { Debug.LogWarning("CURTAIN.OnReceiveMessageFromModbus: not found CERTAIN["+addressIndex+"]="+value ); return; }
		// value is number AND 16#F800 where 0xf800 is 100% and 0x0000 is 0%
		int percent = value;// Mathf.RoundToInt( 100f * (float)value / (float) 0xF800 );
		//if ( percent < 0 ) percent = 0; if ( percent > 100 ) percent = 100;
		device.SetValueByName( percent.ToString(), "уровень" );
		device.SetValueByName( percent==0?"0":"1", "Включено" );

		ClientControl.SendDeviceParameter(device, device.GetParameterByName("Включено") );	
		ClientControl.SendDeviceParameter(device, device.GetParameterByName("уровень") );

		if ( !Master.isOnInit )
			Debug.Log("CURTAIN: "+ device.id + " " + device.GetValueByName("Комната")+" "+device.GetValueByName("Название")+" Включено="+device.GetValueByName("Включено")+", уровень="+device.GetValueByName("уровень"));		
	}

	public static string OnClientSetParameter( Device device, Device.Parameter newPar )
	{
		string result = "true";
		if ( newPar.name == "Включено" )
		{
			int value = 0;
			if ( newPar.value == "1" ) value = 100;
			int position = int.Parse( device.GetValueByName("адрес") );
			int beckhoffValue = value<0?0: value>100?100 : value;
			ModbusClient.SendSet(BeckhoffItems.CURTAINS, position, beckhoffValue);
		}
		else if ( newPar.name == "уровень" )
		{
		    int value = int.Parse(newPar.value);
		    int beckhoffValue = value < 0 ? 0 : value > 100 ? 100 : value;

            device.SetValueByName(beckhoffValue == 0 ? "0" : "1", "Включено");
		    device.SetValueByName(beckhoffValue.ToString(), "уровень");

            int position = int.Parse( device.GetValueByName("адрес") );

			ModbusClient.SendSet(BeckhoffItems.CURTAINS, position, beckhoffValue);

			// сначала рассылаем 
	//		ClientControl.SendDeviceParameter(device, newPar);	
			// а потом пишем в базу
	//		Device.Parameter par = device.SetValueByName( newPar.value, newPar.name );		
		//	SetLevel(device);
	//		Master.IsChanged = true;
		}
		else {
			Debug.LogWarning("CURTAIN.OnClientSetParameter: unknown par: "+newPar.ToString()+", newPar.name='"+newPar.name+"'");
			result = "false";
		}

		return result;
	}

//	private static void SetLevel(Device device )
//	{
//		int level = int.Parse( device.GetValueByName("уровень") );
//		if ( level < 0 ) level = 0;
//		if ( level > 100 ) level = 100;
//	}
//
//	[System.Serializable]
//	class TriggerTimer
//	{
//		int openPos1; int openPos2; float openTime;
//		public TriggerTimer( int openPos1, int openPos2, float openTime )
//		{
//			this.openPos1 = openPos1;
//			this.openPos2 = openPos2;
//			this.openTime = openTime;
//			Master.instance.StartCoroutine(TriggerTimerCoroutine());
//		}
//	}
//
//	public static IEnumerator TriggerTimerCoroutine ()
//	{
//		yield break;
//	}
//
//	private static List<TriggerTimer> triggerTimerList = new List<TriggerTimer>();
//
//	private static void SetTrigger( int openPos1, int openPos2, float openTime )
//	{
//		triggerTimerList.Add( new TriggerTimer(openPos1, openPos2, openTime) );
//	}
}

