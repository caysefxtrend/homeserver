﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SENSOR 
{
	public static string CreateDefault()
	{
		string randomName = new System.Random().Next(11111,99999).ToString();
		return Make( "", "Сенсор "+randomName, "0" );		
	}

	private static string Make(string room, string name, string address)
	{
		return "Тип=SENSOR,Комната="+room+",Название="+name+
			",Включено=0,движение(bool)=1,адрес(percent)="+address+
			",Включить(Команда)="+",Выключить(Команда)="+
			"";
	}

	public static List<string> GetDefaultDevices()
	{
		List<string> list = new List<string>();	
		list.Add( Make( "Щитовая 0 этаж","Сенсор Кладовка","1" ) );		
		list.Add( Make( "Зал 0 этаж СУ","Сенсор СУ 0 этаж","2" ) );		
		list.Add( Make( "Зал 0 этаж","Сенсор Постирочная","3" ) );		
		list.Add( Make( "Зал 1 этаж","Сенсор Входная дверь","4" ) );		
		list.Add( Make( "Гостиная","Сенсор Вход в гостиную","5" ) );		
		list.Add( Make( "Зал 1 этаж","Сенсор Выход к лестнице","6" ) );		
		list.Add( Make( "Гостевой СУ","Сенсор Гостевой СУ","7" ) );		
		list.Add( Make( "Гостиная","Сенсор Гостиная Камин","8" ) );		
		list.Add( Make( "Гостиная","Сенсор Гостиная Диван","9" ) );		
		list.Add( Make( "Гостиная","Сенсор Гостиная Стол","10" ) );		
		list.Add( Make( "Гостиная","Сенсор Гостиная Кухня","11" ) );		
		list.Add( Make( "Детская","Сенсор Детская","12" ) );		
		list.Add( Make( "Детский СУ","Сенсор Детский СУ","13" ) );		
		list.Add( Make( "Хозяйская спальня","Сенсор Хозяйская спальня","14" ) );		
		list.Add( Make( "Хозяйская гардеробная","Сенсор Хозяйская гардеробная","15" ) );		
		list.Add( Make( "Хозяйский СУ","Сенсор Хозяйский СУ","16" ) );		
		list.Add( Make( "Гостевая спальня","Сенсор Гостевая спальня","17" ) );		
		list.Add( Make( "Гостевая спальня СУ","Сенсор Гостевая спальня СУ","18" ) );		
		list.Add( Make( "Кабинет","Сенсор Кабинет","19" ) );		
		list.Add( Make( "Серверная","Сенсор Серверная","20" ) );		
		list.Add( Make( "Зал 3 этаж","Сенсор Лестница","21" ) );		
		list.Add( Make( "СУ 3 этаж","Сенсор СУ 3 этаж","22" ) );		
		list.Add( Make( "Кальянная","Сенсор Кальянная","23" ) );		
		list.Add( Make( "Театр","Сенсор Театр","24" ) );		
		list.Add( Make( "Зал 0 этаж","Сенсор Коридор 0 этаж","25" ) );		
		list.Add( Make( "Зал 0 этаж","Сенсор Зал 0 этаж","26" ) ); // нет в экселе 0.5
		list.Add( Make( "Зал 2 этаж","Сенсор Лестница 2 этаж","27" ) );		
		list.Add( Make( "Зал 1 этаж","Геркон Шкаф 1 этаж","28" ) );		
		list.Add( Make( "Гостевая спальня","Геркон Гостевая спальня Гардероб","29" ) );		
		list.Add( Make( "Гостевая спальня СУ","Геркон Гостевая спальня СУ","30" ) );		
		list.Add( Make( "СПА","Кнопка запуска воды в душе","31" ) );		

		list.Add( Make( "Водоподготовочная","* Сенсор водоподготовочная","" ) );		
		list.Add( Make( "Беседка","* Сенсор беседка вход","" ) );		

		list.Add( Make( "Гостиная","* Геркон дверь у дивана","" ) );		
		list.Add( Make( "Зал 1 этаж","* Геркон дверь главный вход","" ) );		
		list.Add( Make( "Беседка","* Геркон дверь беседка","" ) );		
		list.Add( Make( "Бассейн","* Геркон дверь бассейн","" ) );		
		list.Add( Make( "Гостиная","* Геркон дверь у обеденного стола","" ) );		
		list.Add( Make( "Хозяйский СУ","* Геркон дверь в хозяйском СУ","" ) );		
		list.Add( Make( "Кабинет","* Геркон дверь в кабинете","" ) );		
		list.Add( Make( "Зал 2 этаж","* Геркон дверь зал 2 этаж","" ) );		

		list.Add( Make( "СПА","* ДП подвал с насосами","" ) );		
		list.Add( Make( "СПА","* ДП подвал с насосами - уровень в приямке","" ) );		
		list.Add( Make( "Щитовая 0 этаж","* ДП увлажнитель бани","" ) );		
		list.Add( Make( "Зал 0 этаж СУ","* ДП 0 этаж раковина","" ) );		
		list.Add( Make( "Зал 0 этаж СУ","* ДП 0 этаж гребенки","" ) );		
		list.Add( Make( "Водоподготовочная","* ДП насосы","" ) );		
		list.Add( Make( "Водоподготовочная","* ДП фильтры 1","" ) );		
		list.Add( Make( "Водоподготовочная","* ДП фильтры 2","" ) );		
		list.Add( Make( "Водоподготовочная","* ДП бак бассейна","" ) );		
		list.Add( Make( "Водоподготовочная","* ДП ввод","" ) );		
		list.Add( Make( "Зал 0 этаж","* ДП постирочная машина","" ) );		
		list.Add( Make( "Зал 0 этаж","* ДП постирочная под бассейном","" ) );		
		list.Add( Make( "Гостевой СУ","* ДП СУ 1 этаж раковина","" ) );		
		list.Add( Make( "Гостевой СУ","* ДП СУ 1 этаж батарея","" ) );		
		list.Add( Make( "Гостиная","* ДП гостиная раковина и посудомойка","" ) );		
		list.Add( Make( "Детский СУ","* ДП детский СУ ванная","" ) );		
		list.Add( Make( "Гостевая спальня СУ","* ДП гостевая спальня СУ ванная","" ) );		
		list.Add( Make( "Хозяйский СУ","* ДП хозяйский СУ ванная","" ) );		
		list.Add( Make( "Зал 2 этаж","* ДП 2 этаж гребенка ТП","" ) );		
		list.Add( Make( "Зал 2 этаж","* ДП 2 этаж гребенка батареи","" ) );		
		list.Add( Make( "СУ 3 этаж","* ДП 3 этаж СУ","" ) );		
		list.Add( Make( "Театр","* ДП театр кухня раковина","" ) );		
		list.Add( Make( "Зал 3 этаж","* ДП 3 этаж гребенка ТП","" ) );		
		list.Add( Make( "Зал 3 этаж","* ДП 3 этаж гребенка батареи","" ) );		
		list.Add( Make( "Беседка","* ДП беседка раковина","" ) );		

		list.Add( Make( "Беседка","* Сигнал беседка","" ) );		
		list.Add( Make( "Беседка","* Сигнал беседка 3 привода","" ) );		
		list.Add( Make( "Беседка","* Наличие 220 вольт","" ) );		
		list.Add( Make( "Зал 0 этаж","* Пожарная Зал 0 этаж","" ) );		
		list.Add( Make( "Зал 1 этаж","* Пожарная Зал 1 этаж","" ) );		
		list.Add( Make( "Зал 2 этаж","* Пожарная Зал 2 этаж","" ) );		
		list.Add( Make( "Зал 3 этаж","* Пожарная Зал 3 этаж","" ) );		
		list.Add( Make( "Гараж","* Пожарная Гараж","" ) );		
		list.Add( Make( "Бассейн","* Авария бассейн","" ) );		
		list.Add( Make( "Джакузи","* Авария джакузи","" ) );		

		list.Add( Make( "Улица","* Ворота въезд барьер ик улица","" ) );		
		list.Add( Make( "Улица","* Ворота въезд концевик закрытия ворот","" ) );		
		list.Add( Make( "Улица","* Ворота въезд петля безопасности двор","" ) );		
		list.Add( Make( "Улица","* Прием сигнала радио пульта открытия ворот","" ) );		
		list.Add( Make( "Гараж","* Концевик открытия гаража","" ) );		
		list.Add( Make( "Гараж","* Прием сигнала радио пульта открытия гаража","" ) );		
		list.Add( Make( "Улица","* Поплавок насоса дренажа улица","" ) );		
		list.Add( Make( "Улица","* Подача питания 220В дренажный насос","" ) );		
		list.Add( Make( "Бойлерная","* ДП бойлерная 1","" ) );		
		list.Add( Make( "Бойлерная","* ДП бойлерная 2","" ) );		
		list.Add( Make( "Бойлерная","* ДП бойлерная 3","" ) );		
		list.Add( Make( "Бойлерная","* ДП бойлерная 4","" ) );		
		list.Add( Make( "Бойлерная","* ДП бойлерная бак ВП","" ) );		
		list.Add( Make( "Бойлерная","* ДП бойлерная бак переполнение","" ) );		
		list.Add( Make( "Бойлерная","* ДП бойлерная бак уровень в норме","" ) );		
	//	list.Add( Make( "Бойлерная","* ДП бойлерная бак уровень в норме","" ) );		
		list.Add( Make( "Бойлерная","* Питание котла","" ) );		
		list.Add( Make( "Бойлерная","* Пожарная бойлерная","" ) );		
		list.Add( Make( "Генераторная","* Пожарная генераторная","" ) );		
		list.Add( Make( "Гараж","* Пожарная гараж","" ) );		
		list.Add( Make( "Улица","* ДП Бункер кухня","" ) );		
		list.Add( Make( "Улица","* ДП Бункер щитовая","" ) );		
		list.Add( Make( "Улица","* Камера движение 1","" ) );		
		list.Add( Make( "Улица","* Камера движение 2","" ) );		
		list.Add( Make( "Улица","* Камера движение 3","" ) );		
		list.Add( Make( "Улица","* Камера движение 4","" ) );		
		list.Add( Make( "Улица","* Камера движение 5","" ) );		
		list.Add( Make( "Улица","* Камера движение 6","" ) );		
		list.Add( Make( "Улица","* Камера движение 7","" ) );		
		list.Add( Make( "Улица","* Камера движение 8","" ) );		
		list.Add( Make( "Улица","* Камера движение 9","" ) );		
		list.Add( Make( "Улица","* Камера движение 10","" ) );		
		list.Add( Make( "Улица","* Камера движение 11","" ) );		
		list.Add( Make( "Улица","* Камера движение 12","" ) );		
//		list.Add( Make( "Улица","* ","" ) );		

		return list;
	}
	public static string OnClientSetParameter( Device device, Device.Parameter newPar )
	{
		string result = "true";
		if ( newPar.name == "Включено" )
		{
			if ( device.GetValueByName("адрес").Length == 0 ) return result;

			int position = int.Parse( device.GetValueByName("адрес") );
			int value = newPar.value == "0" ? 0 : 1;
			ModbusClient.SendSet(BeckhoffItems.MS, position, value);
		}
		else {
			Debug.LogWarning("SENSOR.OnClientSetParameter: unknown par: "+newPar.ToString()+", newPar.name='"+newPar.name+"'");
			result = "false";
		}

		return result;
	}

	public static void Initialize(Device device, bool onlySelf)
	{
		if ( onlySelf ) return;
		if ( device.GetValueByName("адрес").Length == 0 ) return;
		int position = int.Parse( device.GetValueByName("адрес") );
		ModbusClient.SendGet(BeckhoffItems.MS, position);
	}

	public static void OnReceiveMessageFromModbus(int addressIndex, int value)
	{
		//int value1 = value&0xFF; 
		int value2 = (value>>8)&0xFF;
		Device device = Master.instance.GetDevice("SENSOR","адрес",addressIndex.ToString() );
		if ( device == null ) return;
		device.SetValueByName( value2.ToString(), "движение" );				
		device.SetValueByName( value == 0 ? "1" : "0", "Включено" );				
		ClientControl.SendDeviceParameter(device, device.GetParameterByName("движение"));
		ClientControl.SendDeviceParameter(device, device.GetParameterByName("Включено"));
		if ( !Master.isOnInit )
			Debug.Log("SENSOR: "+device.GetValueByName("Комната")+" "+device.GetValueByName("Название")+" Включено="+device.GetValueByName("Включено")+", движение="+device.GetValueByName("движение"));				
		
	}

	public static void OnClientCommand( Device device, Device.Parameter par, Device client )
	{
		if ( par.name == "Включить" )
		{
			OnClientSetParameter( device, new Device.Parameter("Включено","","1") );
		}
		else if ( par.name == "Выключить" )
		{
			OnClientSetParameter( device, new Device.Parameter("Включено","","0") );
		}
		else 
		{
			ClientControl.AddCmd( client, null, null, Device.Command.Message, 
				"Устройство \""+device.GetValueByName("Название")+
				"\" не готово выполнить команду \""+par.name+"\"");
		}
	}
}

