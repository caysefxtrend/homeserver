﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class SAMSUNGTV 
{
	//	public static string CreateDefault()
	//	{
	//		string randomName = new System.Random().Next(11111,99999).ToString();
	//		return Make( "", "Замок "+randomName, "0" );		
	//	}

	private static string Make(string name, string room, string ip2ir, string irport, string zone)
	{
		return "Тип=SAMSUNGTV,Название="+name+
			",Комната="+room+
			//",Включено=0"+ // должно определяться пингом до телевизора
			",ip(string)="+
			",Зона(string)="+zone+
			",ip2ir(string)="+ip2ir+",irindex(string)="+irport+
			//",подключено(string)=0"+
			//,адрес(percent)="+address
			//",Включить(Команда)="+
			//",Выключить(Команда)="+
			",CURSOR DOWN(Команда)="+
			",CURSOR ENTER(Команда)="+
			",CURSOR LEFT(Команда)="+
			",CURSOR RIGHT(Команда)="+
			",CURSOR UP(Команда)="+
			",CHANNEL UP(Команда)="+
			",CHANNEL DOWN(Команда)="+
			",DIGIT 0(Команда)="+
			",DIGIT 1(Команда)="+
			",DIGIT 2(Команда)="+
			",DIGIT 3(Команда)="+
			",DIGIT 4(Команда)="+
			",DIGIT 5(Команда)="+
			",DIGIT 6(Команда)="+
			",DIGIT 7(Команда)="+
			",DIGIT 8(Команда)="+
			",DIGIT 9(Команда)="+
			",ENTER(Команда)="+
			",EXIT(Команда)="+
			",MEDIA PLAY(Команда)="+
			",MENU(Команда)="+
			",PAUSE(Команда)="+
			",PLAY(Команда)="+
			",POWER ON(Команда)="+
			",POWER OFF(Команда)="+
			",SMART(Команда)="+
			",INPUT HDMI2(Команда)="+
			",INPUT ANTENNA(Команда)="+
			"";
	}

	public static List<string> GetDefaultDevices()
	{
		List<string> list = new List<string>();	
		list.Add( Make( "Samsungtv 0 этаж отдых","Зал 0 этаж","10.0.1.50","1","1" ) );		
		list.Add( Make( "Samsungtv 0 этаж спорт","Зал 0 этаж","10.0.1.50","1","2" ) );		
		list.Add( Make( "Samsungtv гостиная","Гостиная","10.0.1.52","1","1" ) );	 // ip of tv is 10.0.1.102	
		list.Add( Make( "Samsungtv гостиная стол","Гостиная","10.0.1.52","3","2" ) );		

		list.Add( Make( "Samsungtv детская","Детская","10.0.1.50","2","1" ) );		
		list.Add( Make( "Samsungtv гостевая","Гостевая спальня","10.0.1.50","2","1" ) );		
		list.Add( Make( "Samsungtv хозяйская","Хозяйская спальня","10.0.1.50","2","1" ) );		
		list.Add( Make( "Samsungtv кабинет","Кабинет","10.0.1.50","2","1" ) );		
		list.Add( Make( "Samsungtv кальянная","Кальянная","10.0.1.50","2","1" ) );		
		list.Add( Make( "Samsungtv театр","Театр","10.0.1.50","2","1" ) );		
		return list;
	}

	public static string OnClientSetParameter( Device device, Device.Parameter newPar )
	{
		string result = "true";
		if ( newPar.name == "Включено" )
		{
//			device.SetValueByName( newPar.value, newPar.name );	
//			ClientControl.SendDeviceParameter(device, newPar);	
//			Master.IsChanged = true;
		}
		else {
			Debug.LogWarning("SCENARIO.OnClientSetParameter: unknown par: "+newPar.ToString()+", newPar.name='"+newPar.name+"'");
			result = "false";
		}

		return result;
	}

	public static void Initialize( Device device, bool onlySelf )
	{
//		Debug.Log("SAMSUNGTV.Initialize "+device.GetValueByName("Название")+", driver="+device.driver);
		if ( !onlySelf && device.driver == null )
		{
			device.driver = TcpClientDriver2.Create(device,
				device.GetValueByName("ip2ir"),4998);
			if ( device.driver == null ) Debug.LogWarning("Device "+device.GetValueByName("Название")+" receive null driver");
			//OnDriverConnect( device );
		}
	}

	public static void OnReceiveMessageFromModbus(int addressIndex, int value) { }

	public static void OnClientCommand( Device device, Device.Parameter par, Device client )
	{
		if ( device.driver == null ) {
			ClientControl.AddCmd( client, null, null, Device.Command.Message, 
				"Устройство \""+device.GetValueByName("Название")+
				"\" не имеет рабочего драйвера");
		}
		else if ( par.name == "CURSOR DOWN" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"19,38000,1,1,172,172,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,64,22,21,22,21,22,21,22,21,22,64,22,64,22,21,22,21,22,64,22,64,22,64,22,64,22,21,22,21,22,64,22,1820"
				+"\r",
				device);
		}
		else if ( par.name == "CURSOR ENTER" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"20,38000,1,1,172,172,23,62,23,62,23,62,23,19,23,19,23,19,23,19,23,19,23,62,23,62,23,62,23,19,23,19,23,19,23,19,23,19,23,19,23,19,23,19,23,62,23,19,23,62,23,62,23,19,23,62,23,62,23,62,23,19,23,62,23,19,23,19,23,62,23,1862"
				+"\r",
				device);
		}
		else if ( par.name == "CURSOR LEFT" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"21,38000,1,1,172,172,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,64,22,21,22,64,22,21,22,21,22,64,22,64,22,21,22,21,22,64,22,21,22,64,22,64,22,21,22,21,22,64,22,1820"
				+"\r",
				device);
		}
		else if ( par.name == "CURSOR RIGHT" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"22,38000,1,1,172,172,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,21,22,64,22,21,22,21,22,21,22,64,22,64,22,21,22,64,22,21,22,64,22,64,22,64,22,21,22,21,22,64,22,1820"
				+"\r",
				device);
		}
		else if ( par.name == "CURSOR UP" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"23,38000,1,1,172,172,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,21,22,21,22,21,22,21,22,21,22,64,22,64,22,21,22,64,22,64,22,64,22,64,22,64,22,21,22,21,22,64,22,1820"
				+"\r",
				device);
		}
		else if ( par.name == "CHANNEL UP" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"16,38000,1,1,172,172,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,21,22,64,22,21,22,21,22,64,22,21,22,21,22,21,22,64,22,21,22,64,22,64,22,21,22,64,22,64,22,64,22,1820"
				+"\r",
				device);
		}
		else if ( par.name == "CHANNEL DOWN" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"14,38000,1,1,172,172,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,21,22,21,22,21,22,21,22,64,22,21,22,21,22,21,22,64,22,64,22,64,22,64,22,21,22,64,22,64,22,64,22,1820"
				+"\r",
				device);
		}
		else if ( par.name == "DIGIT 0" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"24,38000,1,1,172,172,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,64,22,21,22,21,22,21,22,64,22,21,22,21,22,21,22,21,22,64,22,64,22,64,22,21,22,64,22,64,22,64,22,1820"
				+"\r",
				device);
		}
		else if ( par.name == "DIGIT 1" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"25,38000,1,1,172,172,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,21,22,21,22,64,22,21,22,21,22,21,22,21,22,21,22,64,22,64,22,21,22,64,22,64,22,64,22,64,22,64,22,1820"
				+"\r",
				device);
		}
		else if ( par.name == "DIGIT 2" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"27,38000,1,1,172,172,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,64,22,21,22,64,22,21,22,21,22,21,22,21,22,21,22,21,22,64,22,21,22,64,22,64,22,64,22,64,22,64,22,1820"
				+"\r",
				device);
		}
		else if ( par.name == "DIGIT 3" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"28,38000,1,1,172,172,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,21,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,64,22,21,22,21,22,64,22,64,22,64,22,64,22,64,22,1820"
				+"\r",
				device);
		}
		else if ( par.name == "DIGIT 4" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"29,38000,1,1,172,172,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,21,22,21,22,21,22,64,22,21,22,21,22,21,22,21,22,64,22,64,22,64,22,21,22,64,22,64,22,64,22,64,22,1820"
				+"\r",
				device);
		}
		else if ( par.name == "DIGIT 5" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"30,38000,1,1,172,172,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,64,22,21,22,21,22,64,22,21,22,21,22,21,22,21,22,21,22,64,22,64,22,21,22,64,22,64,22,64,22,64,22,1820"
				+"\r",
				device);
		}
		else if ( par.name == "DIGIT 6" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"31,38000,1,1,172,172,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,21,22,64,22,21,22,64,22,21,22,21,22,21,22,21,22,64,22,21,22,64,22,21,22,64,22,64,22,64,22,64,22,1820"
				+"\r",
				device);
		}
		else if ( par.name == "DIGIT 7" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"32,38000,1,1,172,172,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,21,22,21,22,64,22,64,22,21,22,21,22,21,22,21,22,64,22,64,22,21,22,21,22,64,22,64,22,64,22,64,22,1820"
				+"\r",
				device);
		}	
		else if ( par.name == "DIGIT 8" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"32,38000,1,1,172,172,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,21,22,21,22,64,22,64,22,21,22,21,22,21,22,21,22,64,22,64,22,21,22,21,22,64,22,64,22,64,22,64,22,1820"
				+"\r",
				device);
		}
		else if ( par.name == "DIGIT 9" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"34,38000,1,1,172,172,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,21,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,64,22,21,22,21,22,21,22,64,22,64,22,64,22,64,22,1820"
				+"\r",
				device);
		}
		else if ( par.name == "ENTER" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"44,38000,1,1,172,172,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,21,22,21,22,21,22,64,22,21,22,64,22,64,22,21,22,64,22,64,22,64,22,21,22,64,22,21,22,21,22,64,22,1820"
				+"\r",
				device);
		}
		else if ( par.name == "EXIT" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"45,38000,1,1,172,172,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,64,22,21,22,64,22,64,22,21,22,64,22,21,22,21,22,21,22,64,22,21,22,21,22,64,22,21,22,64,22,64,22,1820"
				+"\r",
				device);
		}
		else if ( par.name == "MEDIA PLAY" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"101,38000,1,1,171,171,20,64,20,64,20,64,20,20,20,20,20,20,20,20,20,20,20,64,20,64,20,64,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,64,20,64,20,20,20,20,20,20,20,64,20,64,20,64,20,20,20,20,20,64,20,64,20,64,20,20,20,1790"
				+"\r",
				device);
		}
		else if ( par.name == "MENU" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"103,38000,1,1,172,172,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,21,22,64,22,21,22,64,22,64,22,21,22,21,22,21,22,64,22,21,22,64,22,21,22,21,22,64,22,64,22,64,22,1820"
				+"\r",
				device);
		}
		else if ( par.name == "PAUSE" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"109,38000,1,1,171,171,21,65,21,65,21,65,21,21,21,21,21,21,21,21,21,21,21,65,21,65,21,65,21,21,21,21,21,21,21,21,21,21,21,21,21,65,21,21,21,65,21,21,21,21,21,65,21,21,21,65,21,21,21,65,21,21,21,65,21,65,21,21,21,65,21,1792"
				+"\r",
				device);
		}
		else if ( par.name == "PLAY" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"127,38000,1,1,169,170,21,64,21,64,21,64,21,21,21,21,21,21,22,21,21,21,21,64,21,64,21,64,21,21,21,22,21,21,21,21,21,21,22,63,22,63,21,64,21,22,21,21,21,21,21,64,21,21,22,21,21,21,21,21,21,64,21,64,21,64,21,21,22,63,22,1757"
				+"\r",
				device);
		}
		else if ( par.name == "POWER ON" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"129,38000,1,1,172,172,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,64,22,21,22,21,22,64,22,64,22,21,22,21,22,64,22,21,22,64,22,64,22,21,22,21,22,64,22,64,22,21,22,1820"
				+"\r",
				device);
		}
		else if ( par.name == "POWER OFF" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"128,38000,1,1,173,173,21,65,21,65,21,65,21,21,21,21,21,21,21,21,21,21,21,65,21,65,21,65,21,21,21,21,21,21,21,21,21,21,21,21,21,21,21,21,21,65,21,65,21,21,21,21,21,65,21,65,21,65,21,65,21,21,21,21,21,65,21,65,21,21,21,1832"
				+"\r",
				device);
		}
		else if ( par.name == "SMART" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"141,38000,1,1,171,171,21,65,21,65,21,65,21,21,21,21,21,21,21,21,21,21,21,65,21,65,21,65,21,21,21,21,21,21,21,21,21,21,21,65,21,21,21,21,21,65,21,65,21,65,21,65,21,21,21,21,21,65,21,65,21,21,21,21,21,21,21,21,21,65,21,1792"
				+"\r",
				device);
		}
		else if ( par.name == "INPUT HDMI2" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"79,38000,1,1,173,173,21,65,21,65,21,65,21,21,21,21,21,21,21,21,21,21,21,65,21,65,21,65,21,21,21,21,21,21,21,21,21,21,21,21,21,65,21,65,21,65,21,65,21,65,21,21,21,65,21,65,21,21,21,21,21,21,21,21,21,21,21,65,21,21,21,1832"
				+"\r",
				device);
		}
		else if ( par.name == "INPUT ANTENNA" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"68,38000,1,1,172,172,21,64,21,64,21,64,21,21,21,21,21,21,21,21,21,21,21,64,21,64,21,64,21,21,21,21,21,21,21,21,21,21,21,64,21,64,21,21,21,64,21,64,21,21,21,21,21,21,21,21,21,21,21,64,21,21,21,21,21,64,21,64,21,64,21,1673"
				+"\r",
				device);
		}
		else 			
		{
			ClientControl.AddCmd( client, null, null, Device.Command.Message, 
				"Устройство \""+device.GetValueByName("Название")+
				"\" не готово выполнить команду \""+par.name+"\"");
		}
	}
}

