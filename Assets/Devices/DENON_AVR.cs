﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class DENON_AVR 
{
	public static string CreateDefault()
	{
		string randomName = new System.Random().Next(11111,99999).ToString();
		return Make( "", "Денон "+randomName, "0.0.0.0","","","" );		
	} // сделать блю рай как устройство 

	private static string Make(string room, string name, string ip, string zone1, string zone2, string zone3)
	{
		return "Тип=DENON_AVR,Комната="+room+",Название="+name+
			",Включено=0,ip(string)="+ip+",подключено(string)=0"+
			",Зона1(string)="+zone1+",Зона2(string)="+zone2+",Зона3(string)="+zone3+
			",главная зона - включено(bool)=0"+
			",вторая зона - включено(bool)=0"+
			",третья зона - включено(bool)=0"+
			",главная зона - громкость(percent)=0"+
			",вторая зона - громкость(percent)=0"+
			",третья зона - громкость(percent)=0"+
			",главная зона - тишина(bool)=0"+
			",вторая зона - тишина(bool)=0"+
			",третья зона - тишина(bool)=0"+
			",главная зона - источник(string)=TUNER"+
			",вторая зона - источник(string)=CD"+
			",третья зона - источник(string)=TV"+
			",POWER ON(Команда)="+
			",POWER STANDBY(Команда)="+
			",MV UP(Команда)="+
			",MV DOWN(Команда)="+
			",MU ON(Команда)="+
			",MU OFF(Команда)="+
			",ZM ON(Команда)="+
			",ZM OFF(Команда)="+
			",SI BD(Команда)="+
			",SI SATCBL(Команда)="+
			",SI TUNER(Команда)="+
			",SI IRADIO(Команда)="+
			",SI TV(Команда)="+
			",SI MPLAY(Команда)="+
			",MV 0(Команда)="+
			",MV 10(Команда)="+
			",MV 20(Команда)="+
			",MV 30(Команда)="+
			",MV 40(Команда)="+
			",MV 50(Команда)="+
			",MV 60(Команда)="+
			",MV 70(Команда)="+
			",MV 80(Команда)="+
			",MV 90(Команда)="+
			",MV 98(Команда)="+
			",CURSOR DOWN(Команда)="+
			",CURSOR ENTER(Команда)="+
			",CURSOR LEFT(Команда)="+
			",CURSOR RIGHT(Команда)="+
			",CURSOR UP(Команда)="+
			"";
	}

	public static List<string> GetDefaultDevices()
	{
		List<string> list = new List<string>();
		list.Add( Make("Детская","AVR-Childroom","10.0.1.125","Детская","Детский СУ","") );
		///*
		list.Add( Make("","DENON-AVR-3313 room?","10.0.1.39","Неизвестно","","") );
		list.Add( Make("Гостевая спальня","AVR-Guest-bedroom","10.0.1.125","Гостевая спальня","Гостевая спальня СУ","") );
		list.Add( Make("Гостиная","Denon-X3100W","10.0.1.121","Диваны","Обеденная","") );
		list.Add( Make("Зал 2 этаж","AVR-Common room?","10.0.1.126","Залы и туалеты","Бассейн","") );
		list.Add( Make("Театр","AVR-Lounge-3 room?","10.0.1.127","Проектор","ТВ стол","") );
		list.Add( Make("Кабинет","AVR-Kabinet","10.0.1.124","Кабинет","","") );
		list.Add( Make("Хозяйская спальня","AVR-Master-Bedroom","10.0.1.123","Спальня","Санузул","Гардеробная") );
		list.Add( Make("Зал 0 этаж","AVR-Lounge-0","10.0.1.120","Отдых","Спортзал","Джакузи") );
		//*/
		//list.Add( Make("","AVR-Guest-bedroom","10.0.1.120") );
//		list.Add( "Тип=DENON_AVR,Комната=Детская"+
//			",Название=AVR-Childroom,Включено=0,громкость(percent)=0"+
//			",ip(string)=10.0.1.122"
//		);
		return list;
	}

	public static void Initialize( Device device, bool onlySelf )
	{
		if ( !onlySelf && device.driver == null )
		{
			device.driver = TcpClientDriver.Create(device,device.GetValueByName("ip"),23);
			//OnDriverConnect( device );
		}
	}

	public static void OnDriverConnect( Device device )
	{
		//return;
		IDriver driver = device.driver;
		// get to know:
		driver.SendCommand("PW?\r",device); // device power status
		// answer: PWON
		driver.SendCommand("ZM?\r",device); // main zome power status
		// answer: ZMON
		driver.SendCommand("Z2?\r",device); // second zone power status
		// answer: Z2OFF + Z2TUNERZ254 + SVOFF
		driver.SendCommand("Z3?\r",device); // 3 zone power status
		driver.SendCommand("MV?\r",device); // main zome volume
		// answer: MV385MVMAX 98
	//	driver.SendCommand("Z2CV?\r"); // 2 zome volume НЕНАДО
		// Z2CVFL 50 + 	Z2CVFR 50
	//	driver.SendCommand("Z3CV?\r"); // 3 zome volume
		driver.SendCommand("SI?\r",device); // main zome source
		// SITV
		driver.SendCommand("MU?\r",device); // main zome mute
		// MUOFF
		driver.SendCommand("Z2MU?\r",device); // zone 2 mute
		// Z2MUOFF
		driver.SendCommand("Z3MU?\r",device); // zone 2 mute
	}

	public static string OnClientSetParameter( Device device, Device.Parameter newPar )
	{
		string cmd = "";
		int intValue = 0;
		switch( newPar.name )
		{
		case "Включено":
			cmd = newPar.value == "1" ? "PWON" : "PWSTANDBY"; break;
		case "главная зона - включено":
			cmd = newPar.value == "1" ? "ZMON" : "ZMOFF"; break;
		case "вторая зона - включено":
			cmd = newPar.value == "1" ? "Z2ON" : "Z2OFF"; break;
		case "третья зона - включено":
			cmd = newPar.value == "1" ? "Z3ON" : "Z3OFF"; break;
		case "главная зона - громкость":
			if ( int.TryParse( newPar.value, out intValue) ) 
				cmd = "MV"+ Mathf.Clamp(intValue, 0, 98 ); 
			break;
		case "вторая зона - громкость": // 0-98
			if ( int.TryParse( newPar.value, out intValue) ) 
				cmd = "Z2"+ Mathf.Clamp(intValue, 0, 98 ); 
			break;
		case "третья зона - громкость": // 0-98
			if ( int.TryParse( newPar.value, out intValue) ) 
				cmd = "Z3"+ Mathf.Clamp(intValue, 0, 98 ); 
			break;
		case "главная зона - тишина":
			cmd = newPar.value == "1" ? "MUON" : "MUOFF"; break;
		case "вторая зона - тишина":
			cmd = newPar.value == "1" ? "Z2MUON" : "Z2MUOFF"; break;
		case "третья зона - тишина":
			cmd = newPar.value == "1" ? "Z3MUON" : "Z3MUOFF"; break;
		case "главная зона - источник":
			if ( newPar.value == "SATCBL" ) newPar.value = "SAT/CBL";
			cmd = "SI"+newPar.value; break;
		case "вторая зона - источник":
			if ( newPar.value == "SATCBL" ) newPar.value = "SAT/CBL";
			cmd = "Z2"+newPar.value; break;
		case "третья зона - источник":
			if ( newPar.value == "SATCBL" ) newPar.value = "SAT/CBL";
			cmd = "Z3"+newPar.value; break;
		}
		if ( cmd.Length == 0 ) 
		{
			Debug.LogWarning("DENON_AVR.OnClientSetParameter: unknown par: "+newPar.ToString()+", newPar.name='"+newPar.name+"'");
			return "false";
		}
		else 
			device.driver.SendCommand( cmd + "\r", device );
		return "true";
	}

	private static void ToClient( Device device, string name, string value )
	{
		if ( name.Length > 0 && value.Length > 0 )
		{
			device.SetValueByName( value, name );		
			ClientControl.SendDeviceParameter(device, device.GetParameterByName(name) );	
		}
	}

	public static void OnDriverReceiveString( Device device, string msg )
	{
		msg = msg.Remove(msg.Length-1);
		//string type = device.GetValueByName("Тип");
		//if ( type == "DENON_AVR" ) DENON_AVR.OnDriverReceiveString( device, msg );
//		Debug.LogWarning("OnDriverReceiveString: device="+device+", msg=\""+msg+"\"");
		//string name="", value="";
		if ( msg.Contains("PWON") ) { ToClient(device, "Включено", "1"); }
		if ( msg.Contains("PWSTANDBY") ) { ToClient(device, "Включено", "0"); }
		if ( msg.Contains("ZMON") ) { ToClient(device, "главная зона - включено", "1"); }
		if ( msg.Contains("ZMOFF") ) { ToClient(device, "главная зона - включено", "0"); }
		if ( msg.Contains("Z2ON") ) { ToClient(device, "вторая зона - включено", "1"); }
		if ( msg.Contains("Z2OFF") ) { ToClient(device, "вторая зона - включено", "0"); }
		if ( msg.Contains("Z3ON") ) { ToClient(device, "третья зона - включено", "1"); }
		if ( msg.Contains("Z3OFF") ) { ToClient(device, "третья зона - включено", "0"); }

		if ( msg.Contains("MV") ) 
		{ 
			int index = msg.IndexOf("MV");
			if ( index + 3 < msg.Length )
			{
				string numberString = msg.Substring(index+2, 2);
				int intValue = -1;
				if ( !int.TryParse( numberString, out intValue) ) numberString=""; 
				ToClient(device, "главная зона - громкость", numberString );
			}
		}
		if ( msg.Contains("Z2") ) 
		{ 
			int index = msg.IndexOf("Z2");
			if ( index + 3 < msg.Length )
			{				
				string numberString = msg.Substring(index+2, 2);
				int intValue = -1;
				if ( !int.TryParse( numberString, out intValue) ) numberString=""; 
				ToClient(device, "вторая зона - громкость", numberString );
			}
		}
		if ( msg.Contains("Z3") ) 
		{ 
			int index = msg.IndexOf("Z3");
			if ( index + 3 < msg.Length )
			{
				string numberString = msg.Substring(index+2, 2);
				int intValue = -1;
				if ( !int.TryParse( numberString, out intValue) ) numberString="";
				ToClient(device, "третья зона - громкость", numberString );
			}
		}

		if ( msg.Contains("MUON") ) {
			int index = msg.IndexOf("MUON");
			bool ok = true;
			if ( index > 1 )
			{
				string two = msg.Substring(index-2,2);
				if ( two == "Z2" || two == "Z3" ) ok = false;
			}
			if ( ok ) ToClient(device, "главная зона - тишина", "1"); 
		}
		if ( msg.Contains("MUOFF") ) { 
			int index = msg.IndexOf("MUOFF");
			bool ok = true;
			if ( index > 1 )
			{
				string two = msg.Substring(index-2,2);
				if ( two == "Z2" || two == "Z3" ) ok = false;
			}
			if ( ok ) ToClient(device, "главная зона - тишина", "0"); 
		}

		if ( msg.Contains("Z2MUON") ) { ToClient(device, "вторая зона - тишина", "1"); }
		if ( msg.Contains("Z2MUOFF") ) { ToClient(device, "вторая зона - тишина", "0"); }
		if ( msg.Contains("Z3MUON") ) { ToClient(device, "третья зона - тишина", "1"); }
		if ( msg.Contains("Z3MUOFF") ) { ToClient(device, "третья зона - тишина", "0"); }

		if ( msg.Contains("SIIRADIO") ) { ToClient(device, "главная зона - источник", "IRADIO"); }
		if ( msg.Contains("SIPHONO") ) { ToClient(device, "главная зона - источник", "PHONO"); }
		if ( msg.Contains("SICD") ) { ToClient(device, "главная зона - источник", "CD"); }
		if ( msg.Contains("SITUNER") ) { ToClient(device, "главная зона - источник", "TUNER"); }
		if ( msg.Contains("SIDVD") ) { ToClient(device, "главная зона - источник", "DVD"); }
		if ( msg.Contains("SIBD") ) { ToClient(device, "главная зона - источник", "BD"); }
		if ( msg.Contains("SITV") ) { ToClient(device, "главная зона - источник", "TV"); }
		if ( msg.Contains("SISAT/CBL") ) { ToClient(device, "главная зона - источник", "SATCBL"); }
		if ( msg.Contains("SIMPLAY") ) { ToClient(device, "главная зона - источник", "MPLAY"); }
		if ( msg.Contains("SIGAME") ) { ToClient(device, "главная зона - источник", "GAME"); }
		if ( msg.Contains("SINET") ) { ToClient(device, "главная зона - источник", "NET"); }
		if ( msg.Contains("SIUSB") ) { ToClient(device, "главная зона - источник", "USB"); }

		if ( msg.Contains("Z2IRADIO") ) { ToClient(device, "вторая зона - источник", "IRADIO"); }
		if ( msg.Contains("Z2PHONO") ) { ToClient(device, "вторая зона - источник", "PHONO"); }
		if ( msg.Contains("Z2CD") ) { ToClient(device, "вторая зона - источник", "CD"); }
		if ( msg.Contains("Z2TUNER") ) { ToClient(device, "вторая зона - источник", "TUNER"); }
		if ( msg.Contains("Z2DVD") ) { ToClient(device, "вторая зона - источник", "DVD"); }
		if ( msg.Contains("Z2BD") ) { ToClient(device, "вторая зона - источник", "BD"); }
		if ( msg.Contains("Z2TV") ) { ToClient(device, "вторая зона - источник", "TV"); }
		if ( msg.Contains("Z2SATCBL") ) { ToClient(device, "вторая зона - источник", "SATCBL"); }
		if ( msg.Contains("Z2MPLAY") ) { ToClient(device, "вторая зона - источник", "MPLAY"); }
		if ( msg.Contains("Z2GAME") ) { ToClient(device, "вторая зона - источник", "GAME"); }
		if ( msg.Contains("Z2NET") ) { ToClient(device, "вторая зона - источник", "NET"); }
		if ( msg.Contains("Z2USB") ) { ToClient(device, "вторая зона - источник", "USB"); }

		if ( msg.Contains("Z3IRADIO") ) { ToClient(device, "третья зона - источник", "IRADIO"); }
		if ( msg.Contains("Z3PHONO") ) { ToClient(device, "третья зона - источник", "PHONO"); }
		if ( msg.Contains("Z3CD") ) { ToClient(device, "третья зона - источник", "CD"); }
		if ( msg.Contains("Z3TUNER") ) { ToClient(device, "третья зона - источник", "TUNER"); }
		if ( msg.Contains("Z3DVD") ) { ToClient(device, "третья зона - источник", "DVD"); }
		if ( msg.Contains("Z3BD") ) { ToClient(device, "третья зона - источник", "BD"); }
		if ( msg.Contains("Z3TV") ) { ToClient(device, "третья зона - источник", "TV"); }
		if ( msg.Contains("Z3SATCBL") ) { ToClient(device, "третья зона - источник", "SATCBL"); }
		if ( msg.Contains("Z3MPLAY") ) { ToClient(device, "третья зона - источник", "MPLAY"); }
		if ( msg.Contains("Z3GAME") ) { ToClient(device, "третья зона - источник", "GAME"); }
		if ( msg.Contains("Z3NET") ) { ToClient(device, "третья зона - источник", "NET"); }
		if ( msg.Contains("Z3USB") ) { ToClient(device, "третья зона - источник", "USB"); }

//		if ( name.Length > 0 && value.Length > 0 )
//		{
//			device.SetValueByName( value, name );		
//			ClientControl.SendDeviceParameter(device, device.GetParameterByName(name) );	
//		}
	}

	public static void OnClientCommand( Device device, Device.Parameter par, Device client )
	{
		//Device.Parameter newPar = null;// = new Device.Parameter("","","");
		string cmd = "";
		if ( par.name == "POWER ON" )
		{
			cmd = "PWON";
			//newPar = new Device.Parameter("Включено","","0");
		}
		else if ( par.name == "POWER STANDBY" )
		{
			//newPar = new Device.Parameter("Включено","","1");
			cmd = "PWSTANDBY";
		}
		else if ( par.name == "MV UP" )
		{
			cmd = "MVUP";
		}		
		else if ( par.name == "MV DOWN" )
		{
			cmd = "MVDOWN";
		}		
		else if ( par.name == "MU ON" )
		{
			cmd = "MUON";
		}		
		else if ( par.name == "MU OFF" )
		{
			cmd = "MUOFF";
		}		
		else if ( par.name == "ZM ON" )
		{
			cmd = "ZMON";
		}		
		else if ( par.name == "ZM OFF" )
		{
			cmd = "ZMOFF";
		}		
		else if ( par.name == "SI BD" )
		{
			cmd = "SIBD";
		}		
		else if ( par.name == "SI SATCBL" )
		{
			cmd = "SISAT/CBL";
		}	
		else if ( par.name == "SI TUNER" )
		{
			cmd = "SITUNER";
		}		
		else if ( par.name == "SI IRADIO" )
		{
			cmd = "SIIRADIO";
		}		
		else if ( par.name == "SI TV" )
		{
			cmd = "SITV";
		}		
		else if ( par.name == "SI MPLAY" )
		{
			cmd = "SIMPLAY";
		}		
		else if ( par.name == "MV 0" )
		{
			cmd = "MV00";
		}		
		else if ( par.name == "MV 10" )
		{
			cmd = "MV10";
		}		
		else if ( par.name == "MV 20" )
		{
			cmd = "MV20";
		}		
		else if ( par.name == "MV 30" )
		{
			cmd = "MV30";
		}		
		else if ( par.name == "MV 40" )
		{
			cmd = "MV40";
		}		
		else if ( par.name == "MV 50" )
		{
			cmd = "MV50";
		}		
		else if ( par.name == "MV 60" )
		{
			cmd = "MV60";
		}		
		else if ( par.name == "MV 70" )
		{
			cmd = "MV70";
		}		
		else if ( par.name == "MV 80" )
		{
			cmd = "MV80";
		}		
		else if ( par.name == "MV 90" )
		{
			cmd = "MV90";
		}		
		else if ( par.name == "MV 98" )
		{
			cmd = "MV98";
		}		

		else if ( par.name == "CURSOR DOWN" )
		{
			cmd = "NS91";
		}
		else if ( par.name == "CURSOR ENTER" )
		{
			cmd = "NS94";
		}
		else if ( par.name == "CURSOR LEFT" )
		{
			cmd = "NS92";
		}
		else if ( par.name == "CURSOR RIGHT" )
		{
			cmd = "NS93";
		}
		else if ( par.name == "CURSOR UP" )
		{
			cmd = "NS90";
		}

		else 
		{
			ClientControl.AddCmd( client, null, null, Device.Command.Message, 
				"Устройство denon_avr \""+device.GetValueByName("Название")+
				"\" не готово выполнить команду \""+par.name+"\"");
		}
		if ( cmd.Length != 0 ) 
			device.driver.SendCommand( cmd + "\r", device );
	}

}

