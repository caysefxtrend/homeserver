﻿using UnityEngine;
using System.Collections;
using System;
using System.Reflection;

public static class DEFAULT
{
	public static void OnClientCommand( Device device, Device.Parameter par, Device client )
	{
		string result = "";
		string type = device.GetValueByName("Тип");
		if ( type == "SERVER" ) SERVER.OnClientCommand( device, par, client );
		else if ( type == "DENON_AVR" ) DENON_AVR.OnClientCommand( device, par, client );
		else if ( type == "SAMSUNGTV" ) SAMSUNGTV.OnClientCommand( device, par, client );
		else if ( type == "APPLETV" ) APPLETV.OnClientCommand( device, par, client );
		else if ( type == "TRIGGER" ) TRIGGER.OnClientCommand( device, par, client );
		else if ( type == "DIMMER" ) DIMMER.OnClientCommand( device, par, client );
		else if ( type == "DMX" ) DMX.OnClientCommand( device, par, client );
		else if ( type == "SENSOR" ) SENSOR.OnClientCommand( device, par, client );
		else 
		{
			result = "Нет обработчика команд для типа = "+type;
			Debug.LogWarning("DEFAULT.OnClientSetParameter: send to client: \""+result+"\" device="+device.GetValueByName("Название")+", parameter='"+par+", client="+client.GetValueByName("Название"));
			ClientControl.AddCmd( client, null, null, Device.Command.Message, result);
		}
	}
	
	public static bool OnClientSetParameter( Device device, Device.Parameter newPar, Device client )
	{
		string type = device.GetValueByName("Тип");
		string result = "false";
		if ( newPar.name == "Название" )
		{ 
			if ( Master.instance.IsGoodName( newPar.value ) == false )
			{
				ClientControl.AddCmd( client, null, null, Device.Command.Message, "Подобное название уже есть в конфигурации");
				return false;
			}
		//	return false;
		}
		if ( type == "SERVER" ) result = SERVER.OnClientSetParameter( device, newPar );
		else if ( type == "CLIENT" ) result = CLIENT.OnClientSetParameter( device, newPar );
		else if ( type == "CURTAIN" ) result = CURTAIN.OnClientSetParameter( device, newPar );
		else if ( type == "DMX" ) result = DMX.OnClientSetParameter( device, newPar );
		else if ( type == "TRIGGER" ) result = TRIGGER.OnClientSetParameter( device, newPar );
		else if ( type == "DIMMER" ) result = DIMMER.OnClientSetParameter( device, newPar );
		else if ( type == "SENSOR" ) result = SENSOR.OnClientSetParameter( device, newPar );
		else if ( type == "SCENARIO" ) result = SCENARIO.OnClientSetParameter( device, newPar );
		else if ( type == "DENON_AVR" ) result = DENON_AVR.OnClientSetParameter( device, newPar );
		else if ( type == "LOCK" ) result = LOCK.OnClientSetParameter( device, newPar );
		else if ( type == "BERKER" ) result = BERKER.OnClientSetParameter( device, newPar );
		else if ( type == "DOOR" ) result = DOOR.OnClientSetParameter( device, newPar );
		else if ( type == "MEDIA" ) result = MEDIA.OnClientSetParameter( device, newPar );
		else {
			result = "Нет установщика параметров для типа = "+type;
			Debug.LogWarning("DEFAULT.OnClientSetParameter: send to client: \""+result+"\" device="+device.GetValueByName("Название")+", parameter='"+newPar+", client="+client.GetValueByName("Название"));
			ClientControl.AddCmd( client, null, null, Device.Command.Message, result);
			return false;
		}
		//if ( result == true ) return true;
		if ( result == "true" ) {}
		else if ( result != "false" ) 
		{
			if ( client != null )
			{
				Debug.LogWarning("DEFAULT.OnClientSetParameter: send to client: \""+result+"\" device="+device.GetValueByName("Название")+", parameter="+newPar+", client="+client.GetValueByName("Название"));
				ClientControl.AddCmd( client, null, null, Device.Command.Message, result);
			} else Debug.LogWarning("DEFAULT.OnClientSetParameter: не могу обработать - '"+result+"': device="+device.GetValueByName("Название")+", parameter="+newPar);
			return false;
		}
		else if ( result == "false" ) 
		{
			if ( newPar.name == "Комната" )
			{
				ClientControl.SendDeviceParameter(device, newPar);	
				device.SetValueByName( newPar.value, newPar.name );		
			}
			else if ( newPar.name == "Название" )
			{
				ClientControl.SendDeviceParameter(device, newPar);	
				device.SetValueByName( newPar.value, newPar.name );		
			}
			else 
			{
				Debug.LogWarning("DEFAULT.OnClientSetParameter: wrong: device="+device.GetValueByName("Название")+", parameter='"+newPar+", client="+client.GetValueByName("Название"));
				ClientControl.AddCmd( client, null, null, Device.Command.Message, "Не знаю как реагировать на \""+newPar+"\"");
				return false;
			}
		}
		return true;
	}

	public static void InitializeDevice(Device device, bool onlySelf)
	{
		try 
		{
			string deviceType = device.GetValueByName("Тип");
			Type type = Type.GetType(deviceType);
			if ( type == null ) return;
			MethodInfo method = type.GetMethod("Initialize");
	//		Debug.LogWarning("Initialize type="+type.ToString()+", device="+device.GetValueByName("Название")+", method="+method);
			method.Invoke(type, new object[] {device, onlySelf} );
		}
		catch (Exception exc)
		{
			Debug.LogWarning("No Initialize for device="+device.GetValueByName("Название")+", Exception="+exc);
		}
	}

	public static void OnDriverConnect( Device device )
	{
		string type = device.GetValueByName("Тип");
		if ( type == "DENON_AVR" ) DENON_AVR.OnDriverConnect( device );
		else Debug.LogWarning("OnDriverConnect: unknown device="+device.GetValueByName("Название"));
	}

	public static void OnDriverReceiveString( Device device, string msg )
	{
		string type = device.GetValueByName("Тип");
		if ( type == "DENON_AVR" ) DENON_AVR.OnDriverReceiveString( device, msg );
		else Debug.LogWarning("OnDriverReceiveString: no code for - device="+device.GetValueByName("Название")+", msg="+msg);
	}

	public static string GetDefaultDeviceString(string devType)
	{
		try 
		{
			//string deviceType = device.GetValueByName("Тип");
			Type type = Type.GetType(devType);
			MethodInfo method = type.GetMethod("CreateDefault");
			return (string)method.Invoke(type, new object[] {} );
		}
		catch//(Exception exc)
		{
			//string log = exc;
			//return "";
			//Debug.LogWarning("No Initialize for device="+device+", Exception="+exc);
		}
		return "";
	}

	public static void OnReceiveMessageFromModbus( int addressType, int addressIndex, int value)
	{
		Device.Parameter par = Master.instance.GetDevice("Сервер").GetParameterByName("Сообщений от бекофа");
		par.value = (int.Parse(par.value) + 1).ToString();
		Master.IsChanged = true;

		int value1 = value&0xFF; int value2 = (value>>8)&0xFF;
		int index2 = addressIndex&0xFF; int index1 = (addressIndex>>8)&0xFF;
		//Device device = null;
		//Debug.Log("OnReceiveMessageFromModbus: "+addressType+"["+addressIndex+"="+index1+", "+index2+"]="+value );
		switch((BeckhoffItems)addressType)
		{
		case BeckhoffItems.DIMMER:
			HandleLog.LogBeckhoff("OnReceiveMessageFromModbus: DIMMER["+addressIndex+"]="+value );
			DIMMER.OnReceiveMessageFromModbus(addressIndex,value);
			break;
		case BeckhoffItems.TRIGGER:
			HandleLog.LogBeckhoff("OnReceiveMessageFromModbus: TRIGGER["+index1+", "+index2+"]="+value );
			TRIGGER.OnReceiveMessageFromModbus(BeckhoffItems.TRIGGER, addressIndex, value);
			break;
		case BeckhoffItems.UBIQ:
			HandleLog.LogBeckhoff("OnReceiveMessageFromModbus: UBIQ["+addressIndex+"]="+value );
			break;
		case BeckhoffItems.MS:
			HandleLog.LogBeckhoff("OnReceiveMessageFromModbus: MS["+addressIndex+"]=(выкл:"+value1+", двж:"+value2+")" );
			SENSOR.OnReceiveMessageFromModbus(addressIndex,value);
			break;
		case BeckhoffItems.SB:
			HandleLog.LogBeckhoff("OnReceiveMessageFromModbus: SB["+addressIndex+"="+index1+", "+index2+"]="+value );
			break;
		case BeckhoffItems.CURTAINS:
			HandleLog.LogBeckhoff("OnReceiveMessageFromModbus: CURTAINS["+addressIndex+"]="+value );
			CURTAIN.OnReceiveMessageFromModbus(addressIndex, value);
			break;
		case BeckhoffItems.ZONE:
			HandleLog.LogBeckhoff("OnReceiveMessageFromModbus: ZONE["+addressIndex+"]="+value );
			break;
		case BeckhoffItems.DMX:
			HandleLog.LogBeckhoff("OnReceiveMessageFromModbus: DMX["+addressIndex+"]="+value+", color="+Utility2.IntToRgbString(value));
			DMX.OnReceiveMessageFromModbus(addressIndex, value);
			break;
		case BeckhoffItems.WINTEK:
			HandleLog.LogBeckhoff("OnReceiveMessageFromModbus: WINTEK["+addressIndex+"]="+value );
			break;
		case BeckhoffItems.HVAC:
			HandleLog.LogBeckhoff("OnReceiveMessageFromModbus: HVAC["+addressIndex+"]="+value );
			break;
		case BeckhoffItems.LOCK:
			HandleLog.LogBeckhoff("OnReceiveMessageFromModbus: LOCK["+addressIndex+"]="+value );
			LOCK.OnReceiveMessageFromModbus(addressIndex, value);
			break;
		case BeckhoffItems.BTRIGGER:
			HandleLog.LogBeckhoff("OnReceiveMessageFromModbus: BTRIGGER["+index1+", "+index2+"]="+value );
			//HandleLog.LogBeckhoff("OnReceiveMessageFromModbus: BTRIGGER["+addressIndex+"]="+value );
			TRIGGER.OnReceiveMessageFromModbus(BeckhoffItems.BTRIGGER, addressIndex, value);
			break;
		default:
			HandleLog.LogBeckhoff("OnReceiveMessageFromModbus: unknown "+addressType+"["+addressIndex+"]="+value );
			break;
		}
		//Master.IsChanged = true;
	}

    public static string OnClientSimpleCommand(Device device, string typeString, string value)
    {
        if (device == null) return "device is null";
        //if (string.IsNullOrEmpty(connection.Device)) return "connection.Device is empty";
        //var device = Master.GetDeviceByName(connection.Device);
        //if (device == null) return "connection.Device=" + connection.Device + " not found";
        //var par = device.GetParameterByName(connection.Component);
        //if (par == null) return "connection.Device=" + connection.Device + " component=" + connection.Component + " not found";
        Device.Parameter newPar = new Device.Parameter(typeString, "", value);
        var cmd = "OnClientSetParameter";
        try
        {
            Type type = Type.GetType(device.GetValueByName("Тип"));
            MethodInfo method = type.GetMethod(cmd);
            return (string)method.Invoke(type, new object[] { device, newPar });
        }
        catch (Exception exc)
        {
            return "OnClientSimpleCommand: No cmd=" + cmd + " for device[" + device.GetValueByName("Тип") + "]=" + device.GetValueByName("Название") + ", Exception=" + exc;
        }
    }

    public static string OnClientSimpleCommand(Connection connection, string typeString, string value)
	{
		if (connection == null)	return "connection is null";
		if (string.IsNullOrEmpty(connection.Device)) return "connection.Device is empty";
		var device = Master.GetDeviceByName(connection.Device);
        if (device == null) return "connection.Device=" + connection.Device + " not found";
        //var par = device.GetParameterByName(connection.Component);
        //if (par == null) return "connection.Device=" + connection.Device + " component=" + connection.Component + " not found";
        Device.Parameter newPar = new Device.Parameter(typeString, "", value);
		var cmd = "OnClientSetParameter";
		try 
		{
			Type type = Type.GetType(device.GetValueByName("Тип"));
			MethodInfo method = type.GetMethod(cmd);
			return (string)method.Invoke(type, new object[] {device, newPar} );
		}
		catch (Exception exc)
		{
			return "OnClientSimpleCommand: No cmd="+cmd+" for device["+device.GetValueByName("Тип")+"]="+device.GetValueByName("Название")+", Exception="+exc;
		}
	}

}

