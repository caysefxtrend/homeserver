﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class BERKER 
{
	public static string CreateDefault()
	{
		string randomName = new System.Random().Next(11111,99999).ToString();
		return Make( "", "Беркер "+randomName, "0" );		
	}

	private static string Make(string room, string name, string address)
	{
		return "Тип=BERKER,Комната="+room+",Название="+name+
			",Включено=0,адрес(percent)="+address+
			",Кнопка 1(Команда)="+
			",Кнопка 2(Команда)="+
			",Кнопка 3(Команда)="+
			",Кнопка 4(Команда)="+
			",Кнопка 5(Команда)="+	
			",Кнопка 6(Команда)="+
			",Кнопка 7(Команда)="+	
			",Кнопка 8(Команда)=";
	}

	public static List<string> GetDefaultDevices()
	{
		List<string> list = new List<string>();	
		list.Add( Make( "Щитовая 0 этаж","Беркер6 Кладовка","1" ) );		
		list.Add( Make( "Зал 0 этаж СУ","Беркер6 Гостевой СУ Этаж 0","2" ) );		
		list.Add( Make( "Зал 0 этаж","Беркер6 Постирочная","3" ) );		
		list.Add( Make( "СПА","Беркер8 СПА Джакузи","4" ) );		
		list.Add( Make( "Гостевой СУ","Беркер6 Гостевой СУ","5" ) );		
		list.Add( Make( "Бассейн","Беркер8 Бассейн","6" ) );		
		list.Add( Make( "Гостиная","Беркер8 Гостиная У камина","7" ) );		
		list.Add( Make( "Гостиная","Беркер6 Гостиная У стола","8" ) );		
		list.Add( Make( "Зал 2 этаж","Беркер6 Лестница этаж 2","9" ) );		
		list.Add( Make( "Детская","Беркер8 Детская","10" ) );		
		list.Add( Make( "Детский СУ","Беркер6 Детский СУ","11" ) );		
		list.Add( Make( "Хозяйская спальня","Беркер8 Хозяйская спальня Слева","12" ) );		
		list.Add( Make( "Хозяйская спальня","Беркер8 Хозяйская спальня Справа","13" ) );		
		list.Add( Make( "Хозяйская спальня","Беркер6 Хозяйская спальня Гардероб","14" ) );		
		list.Add( Make( "Хозяйский СУ","Беркер6 Хозяйский СУ Зеркало слева","15" ) );		
		list.Add( Make( "Хозяйский СУ","Беркер6 Хозяйский СУ Зеркало справа","16" ) );		
		list.Add( Make( "Хозяйский СУ","Беркер8 Хозяйский СУ Колонна у ванны","17" ) );		
		list.Add( Make( "Хозяйский СУ","Беркер6 Хозяйский СУ На балкон","18" ) );		
		list.Add( Make( "Гостевая спальня","Беркер6 Гостевая спальня","19" ) );		
		list.Add( Make( "Кабинет","Беркер8 Кабинет У дивана","20" ) );		
		list.Add( Make( "Кабинет","Беркер6 Кабинет На балкон","21" ) );		
		list.Add( Make( "Кабинет","Беркер8 У стола","22" ) );		
		list.Add( Make( "СУ 3 этаж","Беркер6 СУ 3 этаж","23" ) );		

		return list;
	}

	public static string OnClientSetParameter( Device device, Device.Parameter newPar )
	{
		string result = "true";
		if ( newPar.name == "Включено" )
		{
			// сначала рассылаем новое имя
			ClientControl.SendDeviceParameter(device, newPar);	
			// а потом пишем в базу
			device.SetValueByName( newPar.value, newPar.name );		
			Master.IsChanged = true;
		}
		else {
			Debug.LogWarning("BERKER.OnClientSetParameter: unknown par: "+newPar.ToString()+", newPar.name='"+newPar.name+"'");
			result = "false";
		}

		return result;
	}

	public static void Initialize(Device device, bool onlySelf)
	{
	}

	public static void OnReceiveMessageFromModbus(int addressIndex, int value)
	{
		if ( !Master.isOnInit )
			Debug.Log("BERKER: "+addressIndex+" "+value);				
			//Debug.Log("TRIGGER: "+device.GetValueByName("Комната")+" "+device.GetValueByName("Название")+" Включено="+device.GetValueByName("Включено"));				
	}
}

