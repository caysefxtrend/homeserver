﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class SCENARIO
{
	private static string Make(int id, string room, string name, string commands)
	{
		return "Id="+id+",Тип=SCENARIO,Комната="+room+",Название="+name+
		       ",Включено=0"+(string.IsNullOrEmpty(commands)?"":","+commands);
	}
	
	public static List<string> GetDefaultDevices() // id: [1100-1100]
	{
		List<string> list = new List<string>();
		list.Add( Make(1100,"Гостиная","Сценарий в гостиной 1", 
			"301(bool)=0,105(bool)=0,103(bool)=0" ) );
		return list;
	}
	
	public static string OnClientSetParameter( Device device, Device.Parameter newPar )
	{
		string result = "true";
		if ( newPar.name == "Включено" )
		{
			device.SetValueByName( newPar.value, newPar.name );
			Debug.Log("SCENARIO: ("+newPar.name+") "+device);
			ClientControl.SendDeviceParameter(device, newPar);	
			Master.IsChanged = true;

			var pars = device.GetParametersByType("bool");
			foreach (var par in pars)
			{
				var deviceId = int.Parse(par.name);
				var defaultValue = int.Parse(par.value) == 0 ? false : true;
				if (newPar.value == "1")
					defaultValue = !defaultValue;
				var cmdDevice = Master.GetDeviceById(deviceId);
				result += DEFAULT.OnClientSimpleCommand(cmdDevice, "Включено", defaultValue?"1":"0");
			}
		}
		else {
			Debug.LogWarning("SCENARIO.OnClientSetParameter: unknown par: "+newPar.ToString()+", newPar.name='"+newPar.name+"'");
			result = "false";
		}

		return result;
	}

	public static void Initialize(Device device, bool onlySelf)
	{
	}
}

