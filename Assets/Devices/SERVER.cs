﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class SERVER
{
	public static List<string> GetDefaultDevices() // Id: [1]
	{
		List<string> list = new List<string>();
		list.Add( "Id=1,Тип=SERVER,Название=Сервер"+
			",Время работы сервера(string)=0 минут"+
			",Время работы бекофа(string)=0 минут"+
			",Сообщений от бекофа(string)=0"+
			",Сообщений от клиентов(string)=0"+
			",Бекоф подключён(bool)=0"+
			",Интернет(percent)=1"+ // 0-no inet, 1-inet ok, 2-inet unpaid
			",Версия(string)=1.01"+
	//		",Перезапустить программу сервера(Команда)=" +
	//		",Перезапустить ОС сервера(Команда)=" +
	//		",Перезапустить бекоф(Команда)="+
			",Просмотреть лог сервера(Команда)="+
			",Сохранить конфигурацию(Команда)="+
			",Загрузить конфигурацию(Команда)=" 
		);
		return list;
	}

	public static string OnClientSetParameter( Device device, Device.Parameter newPar )
	{
		string result = "true";
		if ( newPar.name == "Сохранить конфигурацию на сервере" )
		{
			Debug.LogWarning("TODO Сохранить конфигурацию на сервере");
		}
		else if ( newPar.name == "Загрузить конфигурацию на сервере" )
		{
			Debug.LogWarning("TODO Загрузить конфигурацию на сервере");
			// сначала рассылаем новое имя
			//ClientControl.SendDeviceParameter(device, newPar);	
			// а потом пишем в базу
			//device.SetValueByName( newPar.value, newPar.name );		
			//Master.IsChanged = true;
		}
		else if ( newPar.name == "Название" )
		{ 
			//ClientControl.SendMessage("Нельзя менять название device, newPar);
			return "false";
		}
		else {
			Debug.LogWarning("SERVER.OnClientSetParameter: unknown par: "+newPar.ToString());
			result = "false";
		}

		return result;
	}

	public static void Initialize(Device device, bool onlySelf)
	{
	}

	public static void OnClientCommand( Device device, Device.Parameter par, Device client )
	{
		if ( par.name == "Сохранить конфигурацию" )
		{
			string filename = Master.SaveConfiguration();
			ClientControl.AddCmd( client, null, null, Device.Command.Message, "Устройство \""+device.GetValueByName("Название")+"\" сохранило конфигурацию под именем \""+filename+"\"");
		}
		else if ( par.name == "Загрузить конфигурацию" )
		{
			List<string> filenames = Master.GetConfigurations();
			string text = "";
			foreach(string t in filenames) text += t+",";
			text = text.Remove(text.Length-1);
			ClientControl.AddCmd( client, null, null, Device.Command.ConfigsList, text);
		}
		else 
		{
			ClientControl.AddCmd( client, null, null, Device.Command.Message, "Устройство \""+device.GetValueByName("Название")+"\" не готово выполнить команду \""+par.name+"\"");
		}

	}
}

