﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class DIMMER
{
	public static string CreateDefault()
	{
		string randomName = new System.Random().Next(11111,99999).ToString();
		return Make( -1, "", "Диммер "+randomName, "0" );		
	}

	private static string Make(int id, string room, string name, string address)
	{
		return "Тип=DIMMER,Комната="+room+",Название="+name+
			",Включено=0,яркость(percent)=0,адрес(percent)="+address+
			",Включить(Команда)="+",Выключить(Команда)="+
			""+",Id="+id;
	}

	public static List<string> GetDefaultDevices() // id: [1000-1015]
	{
		List<string> list = new List<string>();
		list.Add( Make(1000,"Гостевая спальня","Потолочные карданы -1","1" ) );
		list.Add( Make(1001,"Гостевой СУ","Потолочные карданы -2","2" ) );
		list.Add( Make(1002,"Детская","Два 4-х секционных потолочных кардана","3" ) );
		list.Add( Make(1003, "Гостевая спальня","Потолочные карданы -3","4" ) );
		list.Add( Make(1004, "Хозяйская спальня","Карданы над проходом","5" ) );
		list.Add( Make(1005, "Кабинет","Потолочные карданы -4","6" ) );
		list.Add( Make(1006, "Хозяйская спальня","Светильник в нише на потолке - дальше от изголовья -2","7" ) );
		list.Add( Make(1007, "Хозяйская спальня","Светильник в нише на потолке - ближе к изголовью -2","8" ) );
		list.Add( Make(1008, "Бассейн","* Подсветка стены","9" ) );
		list.Add( Make(1009, "Кабинет","Два бра","10" ) );
		list.Add( Make(1010, "Детская","Бра за диваном","11" ) );
		list.Add( Make(1011, "Гостевая спальня","Бра в гостевой спальне","12" ) );
		list.Add( Make(1012, "Хозяйский СУ","Подсветка потолка три одинарных кардана по оси двери Диммер","13" ) );
		list.Add( Make(1013, "Хозяйский СУ","Потолочное освещение ванна 2 кардана двойных","14" ) );
		list.Add( Make(1014, "Хозяйский СУ","Потолочное освещение 2 кардана центральная линия","15" ) );
		list.Add( Make(1015, "Зал 0 этаж СУ","Потолочные карданы","16" ) );
		return list;
	}

	public static void Initialize(Device device, bool onlySelf)
	{
		if ( onlySelf ) return;
		int position = int.Parse( device.GetValueByName("адрес") );
		ModbusClient.SendGet(BeckhoffItems.DIMMER, position);
	}

	public static string OnClientSetParameter( Device device, Device.Parameter newPar )
	{
		string result = "true";
		if ( newPar.name == "Включено" )
		{
			int value = 0;
			if ( newPar.value == "1" )
			{
				value = int.Parse( device.GetValueByName("яркость") );
				if ( value == 0 ) value = 100;
			}
			int position = int.Parse( device.GetValueByName("адрес") );
			int beckhoffValue = value * 0x7F00 / 100;
			ModbusClient.SendSet(BeckhoffItems.DIMMER, position, beckhoffValue);
		}
		else if ( newPar.name == "яркость" )
		{
		    device.SetValueByName(newPar.value == "0" ? "0" : "1", "Включено");
		    device.SetValueByName(newPar.value, "яркость");

            int position = int.Parse( device.GetValueByName("адрес") );
				int value = int.Parse( newPar.value );
				int beckhoffValue = value * 0x7F00 / 100;
				ModbusClient.SendSet(BeckhoffItems.DIMMER, position, beckhoffValue);
            /*
            int percent = int.Parse(newPar.value);
            if (percent < 0) percent = 0; if (percent > 100) percent = 100;
            device.SetValueByName(percent > 0 ? "1" : "0", "Включено");
            if (percent > 0) device.SetValueByName(percent.ToString(), "яркость");
            */
        }
		else {
			Debug.LogWarning("DIMMER.OnClientSetParameter: unknown par: "+newPar.ToString()+", newPar.name='"+newPar.name+"'");
			result = "false";
		}

		return result;
	}

	public static void OnReceiveMessageFromModbus(int addressIndex, int value)
	{
	//	Debug.Log("OnReceiveMessageFromModbus: DIMMER["+addressIndex+"]="+value );
		Device device = Master.instance.GetDevice("DIMMER","адрес",addressIndex.ToString() );
		if ( device == null ) return;
		// value is number AND 16#F800 where 0xf800 is 100% and 0x0000 is 0%
		int percent = Mathf.RoundToInt( 100f * (float)value / (float) 0x7F00 );
		if ( percent < 0 ) percent = 0; if ( percent > 100 ) percent = 100;
		// TODO maybe we must set onoff by percent==0 ?
		if ( percent > 0 ) 
		{
			device.SetValueByName( percent.ToString(), "яркость" );		
			device.SetValueByName( "1", "Включено" );		
		}
		else {
			//device.SetValueByDesc( "0", "включено" );		
			//device.SetValueByDesc( percent.ToString(), "яркость" );		
			device.SetValueByName( "0", "Включено" );		
		}
		ClientControl.SendDeviceParameter(device, device.GetParameterByName("яркость") );				
		ClientControl.SendDeviceParameter(device, device.GetParameterByName("Включено") );	
		if ( !Master.isOnInit )
			Debug.Log("DIMMER: "+device.GetValueByName("Комната")+" "+device.GetValueByName("Название")+" Включено="+device.GetValueByName("Включено")+", яркость="+device.GetValueByName("яркость"));
	}

	public static void OnClientCommand( Device device, Device.Parameter par, Device client )
	{
		if ( par.name == "Включить" )
		{
			OnClientSetParameter( device, new Device.Parameter("Включено","","1") );
		}
		else if ( par.name == "Выключить" )
		{
			OnClientSetParameter( device, new Device.Parameter("Включено","","0") );
		}
		else 
		{
			ClientControl.AddCmd( client, null, null, Device.Command.Message, 
				"Устройство \""+device.GetValueByName("Название")+
				"\" не готово выполнить команду \""+par.name+"\"");
		}
	}

}

