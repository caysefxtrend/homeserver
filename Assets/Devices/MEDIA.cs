﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class MEDIA 
{
//	public static string CreateDefault()
//	{
//		string randomName = new System.Random().Next(11111,99999).ToString();
//		return Make( "", "Замок "+randomName, "0" );		
//	}

	private static string Make(string name,string room,string room1,string room2,string room3,string denon)
	{
		return "Тип=MEDIA,Название="+name+
			",Комната="+room+
			",Зона1(string)="+room1+",Зона2(string)="+room2+",Зона3(string)="+room3+
			",Включено=0,"+"Denon(string)="+denon+
			//,адрес(percent)="+address
			//+",Команда=Открыть-Закрыть"
			"";
	}

	public static List<string> GetDefaultDevices()
	{
		List<string> list = new List<string>();	
		list.Add( Make( "Медиа зона 0 этаж","Зал 0 этаж","Отдых","Спортзал","Джакузи","AVR-Lounge-0" ) );		
		list.Add( Make( "Медиа зона гостиная","Гостиная","Диваны","Обеденная","", "Denon-X3100W" ) );		
		list.Add( Make( "Медиа зона залы","Серверная","Залы и туалеты","Бассейн","","" ) );		
		list.Add( Make( "Медиа зона детская","Детская","Детская","Детский СУ","","AVR-Childroom" ) );		
		list.Add( Make( "Медиа зона гостевая","Гостевая спальня","Гостевая спальня","Гостевая спальня СУ","","AVR-Guest-bedroom" ) );		
		list.Add( Make( "Медиа зона хозяйская","Хозяйская спальня","Хозяйская спальня","Хозяйский СУ","Хозяйская гардеробная","AVR-Master-Bedroom" ) );
		list.Add( Make( "Медиа зона кабинет","Кабинет","Кабинет","","","AVR-Kabinet" ) );
		list.Add( Make( "Медиа зона кальянная","Кальянная","Кальянная","","","" ) );
		list.Add( Make( "Медиа зона театр","Театр","Проектор","ТВ стол","","AVR-Lounge-3 room?" ) );

		return list;
	}

	public static string OnClientSetParameter( Device device, Device.Parameter newPar )
	{
		string result = "true";
		if ( newPar.name == "Включено" )
		{
			device.SetValueByName( newPar.value, newPar.name );	
			ClientControl.SendDeviceParameter(device, newPar);	
			Master.IsChanged = true;
		}
		else {
			Debug.LogWarning("SCENARIO.OnClientSetParameter: unknown par: "+newPar.ToString()+", newPar.name='"+newPar.name+"'");
			result = "false";
		}

		return result;
	}

	public static void Initialize(Device device, bool onlySelf) { }

	public static void OnReceiveMessageFromModbus(int addressIndex, int value) { }
}

