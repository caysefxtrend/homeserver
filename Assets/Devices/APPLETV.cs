﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class APPLETV 
{
	//	public static string CreateDefault()
	//	{
	//		string randomName = new System.Random().Next(11111,99999).ToString();
	//		return Make( "", "Замок "+randomName, "0" );		
	//	}

	private static string Make(string name, string room, string ip2ir, string irport)
	{
		return "Тип=APPLETV,Название="+name+
			",Комната="+room+
		//	",Включено=0"+
			",ip2ir(string)="+ip2ir+",irindex(string)="+irport+
			//",подключено(string)=0"+
			//,адрес(percent)="+address
			",CURSOR DOWN(Команда)="+
			",CURSOR ENTER(Команда)="+
			",CURSOR LEFT(Команда)="+
			",CURSOR RIGHT(Команда)="+
			",CURSOR UP(Команда)="+
			",MENU(Команда)="+
			",PLAY(Команда)="+
			",STOP(Команда)="+
			"";
	}

	public static List<string> GetDefaultDevices()
	{
		List<string> list = new List<string>();	
		list.Add( Make( "Appletv 0 этаж","Зал 0 этаж","10.0.1.50","2" ) );		
		list.Add( Make( "Appletv гостиная","Гостиная","10.0.1.52","2" ) );		
		//list.Add( Make( "Appletv залы","Залы и туалеты","Бассейн","","" ) );		
		list.Add( Make( "Appletv детская","Детская","10.0.1.50","2" ) );		
		list.Add( Make( "Appletv гостевая","Гостевая спальня","10.0.1.50","2" ) );		
		list.Add( Make( "Appletv хозяйская","Хозяйская спальня","10.0.1.50","2" ) );		
		list.Add( Make( "Appletv кабинет","Кабинет","10.0.1.50","2" ) );		
		list.Add( Make( "Appletv кальянная","Кальянная","10.0.1.50","2" ) );		
		list.Add( Make( "Appletv театр","Театр","10.0.1.50","2" ) );		
		return list;
	}

	public static string OnClientSetParameter( Device device, Device.Parameter newPar )
	{
		string result = "true";
		if ( newPar.name == "Включено" )
		{
//			device.SetValueByName( newPar.value, newPar.name );	
//			ClientControl.SendDeviceParameter(device, newPar);	
//			Master.IsChanged = true;
		}
		else {
			Debug.LogWarning("SCENARIO.OnClientSetParameter: unknown par: "+newPar.ToString()+", newPar.name='"+newPar.name+"'");
			result = "false";
		}

		return result;
	}

	public static void Initialize( Device device, bool onlySelf )
	{
		if ( !onlySelf && device.driver == null )
		{
			device.driver = TcpClientDriver2.Create(device,
				device.GetValueByName("ip2ir"),4998);
			//OnDriverConnect( device );
		}
	}

	public static void OnReceiveMessageFromModbus(int addressIndex, int value) { }

	public static void OnClientCommand( Device device, Device.Parameter par, Device client )
	{
		if ( par.name == "CURSOR DOWN" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"171,38000,1,1,341,172,21,21,21,65,21,65,21,65,21,21,21,65,21,65,21,65,21,65,21,65,21,65,21,21,21,21,21,21,21,21,21,65,21,21,21,21,21,65,21,65,21,21,21,21,21,21,21,21,21,65,21,65,21,21,21,21,21,21,21,65,21,21,21,21,21,1508,341,85,21,3648"
				+"\r",
				device);
//			TcpClientDriver2.SendCommand(device.GetValueByName("ip2ir"),
//				4889,
//				"sendir,1:2,171,38000,1,1,341,172,21,21,21,65,21,65,21,65,21,21,21,65,21,65,21,65,21,65,21,65,21,65,21,21,21,21,21,21,21,21,21,65,21,21,21,21,21,65,21,65,21,21,21,21,21,21,21,21,21,65,21,65,21,21,21,21,21,21,21,65,21,21,21,21,21,1508,341,85,21,3648\n",
//				device);
		}
		else if ( par.name == "CURSOR ENTER" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"172,38000,1,1,341,172,21,21,21,65,21,65,21,65,21,21,21,65,21,65,21,65,21,65,21,65,21,65,21,21,21,21,21,21,21,21,21,65,21,65,21,21,21,65,21,21,21,21,21,21,21,21,21,21,21,65,21,65,21,21,21,21,21,21,21,65,21,21,21,21,21,1508,341,85,21,3648"
				+"\r",
				device);
		}
		else if ( par.name == "CURSOR LEFT" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"173,38000,1,1,341,172,21,21,21,65,21,65,21,65,21,21,21,65,21,65,21,65,21,65,21,65,21,65,21,21,21,21,21,21,21,21,21,65,21,65,21,21,21,21,21,65,21,21,21,21,21,21,21,21,21,65,21,65,21,21,21,21,21,21,21,65,21,21,21,21,21,1508,341,85,21,3648"
				+"\r",
				device);
		}
		else if ( par.name == "CURSOR RIGHT" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"174,38000,1,1,341,172,21,21,21,65,21,65,21,65,21,21,21,65,21,65,21,65,21,65,21,65,21,65,21,21,21,21,21,21,21,21,21,65,21,21,21,65,21,65,21,21,21,21,21,21,21,21,21,21,21,65,21,65,21,21,21,21,21,21,21,65,21,21,21,21,21,1508,341,86,21,3648"
				+"\r",
				device);
		}
		else if ( par.name == "CURSOR UP" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"175,38000,1,1,341,172,21,21,21,65,21,65,21,65,21,21,21,65,21,65,21,65,21,65,21,65,21,65,21,21,21,21,21,21,21,21,21,65,21,21,21,65,21,21,21,65,21,21,21,21,21,21,21,21,21,65,21,65,21,21,21,21,21,21,21,65,21,21,21,21,21,1508,341,85,21,3648"
				+"\r",
				device);
//			TcpClientDriver2.SendCommand(device.GetValueByName("ip2ir"),
//				4889,
//				"sendir,1:2,175,38000,1,1,341,172,21,21,21,65,21,65,21,65,21,21,21,65,21,65,21,65,21,65,21,65,21,65,21,21,21,21,21,21,21,21,21,65,21,21,21,65,21,21,21,65,21,21,21,21,21,21,21,21,21,65,21,65,21,21,21,21,21,21,21,65,21,21,21,21,21,1508,341,85,21,3648\n",
//				device);
		}
		else if ( par.name == "MENU" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"176,38000,1,1,341,172,21,21,21,65,21,65,21,65,21,21,21,65,21,65,21,65,21,65,21,65,21,65,21,21,21,21,21,21,21,21,21,65,21,65,21,65,21,21,21,21,21,21,21,21,21,21,21,21,21,65,21,65,21,21,21,21,21,21,21,65,21,21,21,21,21,1508,341,85,21,3648"
				+"\r",
				device);
		}
		else if ( par.name == "PLAY" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"179,38000,1,1,341,172,21,21,21,65,21,65,21,65,21,21,21,65,21,65,21,65,21,65,21,65,21,65,21,21,21,21,21,21,21,21,21,65,21,65,21,21,21,65,21,21,21,21,21,21,21,21,21,21,21,65,21,65,21,21,21,21,21,21,21,65,21,21,21,21,21,1509,341,86,21,3649"
				+"\r",
				device);
		}
		else if ( par.name == "STOP" )
		{
			device.driver.SendCommand(
				"sendir,1:"+device.GetValueByName("irindex")+","+
				"183,38000,1,1,343,171,21,64,21,21,22,63,22,21,22,21,21,64,21,64,21,64,21,64,21,64,21,64,21,21,22,21,22,21,22,21,21,64,21,64,21,21,22,21,22,21,21,64,21,64,21,21,22,21,22,63,22,21,21,21,22,21,22,21,22,21,21,21,22,21,22,1654,342,85,21,3654"
				+"\r",
				device);
		}
		else 
		{
			ClientControl.AddCmd( client, null, null, Device.Command.Message, 
				"Устройство \""+device.GetValueByName("Название")+
				"\" не готово выполнить команду \""+par.name+"\"");
		}
	}

	private static string GetCmdByName(string name)
	{
		return "";
	}

}

