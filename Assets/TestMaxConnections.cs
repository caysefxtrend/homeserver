﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System;
using System.Text;

public class TestMaxConnections : MonoBehaviour
{
	[System.Serializable]
	public class Connection
	{
		public TcpClient client = null;
		public string status = "";
		public int index = -1;
	}

	public List<Connection> connections = new List<Connection>();
	public int maxConnections = 100;
	public string host = "localhost";
	public int port = 23;
	public int failCount = 0;
	public int successCount = 0;
	public bool isDisabled = false;
	// Use this for initialization
	void Start ()
	{
	
	}

	private float LastTime = 0;
	// Update is called once per frame
	void Update ()
	{
		if ( Time.time - LastTime > 1f ) // every second
		{
			LastTime = Time.time;
			int failCount = 0;
			int successCount = 0;
			foreach(Connection con in connections)
			{
				if ( con.client.Connected )	
				{ 
					con.status = "success connected"; successCount ++; 
				}
				else { con.status = "fail - not connected"; failCount++; }
			}
			this.successCount = successCount;
			this.failCount = failCount;
		}
	
	}

	void OnEnable()
	{
		isDisabled = false;
		failCount = 0;
		successCount = 0;
		for(int i = 0; i < maxConnections; i++)
		{
			Connection con = new Connection();
			con.index = i;
			connections.Add( con );
			CreateConnection( con );
		}
	}

	void OnDisable()
	{
		isDisabled = true;
		//foreach(Connection con in connections) if ( con.client != null ) con.client.EndConnect();
		foreach(Connection con in connections)
		{
			if ( con.client != null )
			{
				if ( con.client.Connected ) Debug.Log("Close connected "+con.index);
				con.client.Close();
				con.client = null;
			}
		}
		connections.Clear();
	}

	void CreateConnection(Connection con)
	{
		try {
			con.status = "not inited";
			con.client = new System.Net.Sockets.TcpClient(AddressFamily.InterNetwork);
			con.status = "client created";
			con.client.BeginConnect(host, port, new AsyncCallback(ConnectCallback), con);
			con.status = "begin connect";
		} catch (Exception e) 
		{	Debug.LogError ("CreateConnection: "+host+":"+port+" error: " + e); }
	}

	void ConnectCallback(IAsyncResult ar)
	{	
		if ( isDisabled ) Debug.LogWarning("disabled ConnectCallback");
		Connection con = (Connection) ar.AsyncState;	
		con.status = "connect fail";
		failCount ++;
		con.client.EndConnect(ar); // thread break if connection fail !
		failCount --;
		con.status = "connect success";
		Debug.Log("success = "+con.client.Connected+" "+con.index);
		successCount ++;
		WriteSocket( con, "test "+con.index );
	}

	public void WriteSocket (Connection con, string cmd)
	{		
		Debug.Log("try WriteSocket "+con.index);
		try {	
			//	Debug.Log(Time.time.ToString()+" : "+state);
			byte[] byteBuffer = Encoding.UTF8.GetBytes (cmd);
			con.client.GetStream().Write(byteBuffer,0,byteBuffer.Length);
			//con.client.GetStream().Flush();
		} catch (Exception e) 
		{	
			Debug.Log ("WriteSocket error: " + e); 
			//CloseSocket();
		}
	}
}

