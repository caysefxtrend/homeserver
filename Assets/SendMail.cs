﻿using UnityEngine;
using System.Collections;
using System.Net.Mail;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System;
using UnityEngine.UI;

public class SendMail : MonoBehaviour {
	public bool send = false;
	public string toMail = "ter_s@mail.ru";
	public float everyMinutes = 120;
	float lastTime = 0;
	public InputField inputFieldEmail;
	public InputField inputFieldMinutes;
	// Use this for initialization
	void Start () {
		lastTime = Time.time;
	//	Debug.Log("i="+inputFieldEmail.text+", ema="+toMail);
	//	if ( inputFieldEmail != null ) inputFieldEmail.text = toMail;
	//	Debug.Log("i="+inputFieldEmail.text+", ema="+toMail);
	//	if ( inputFieldMinutes != null ) inputFieldMinutes.text = everyMinutes.ToString();
	}

	void OnEnable() 
	{
		OnEndEditStrings();
	}

	public void OnEndEditStrings()
	{
		if ( inputFieldEmail != null && inputFieldMinutes != null && 
			inputFieldEmail.text.Length != 0 && inputFieldMinutes.text.Length != 0 &&
			float.TryParse( inputFieldMinutes.text, out everyMinutes ) )
		{
			toMail = inputFieldEmail.text;
		}	
		else 
		{
			toMail = "";
			everyMinutes = 0;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if ( send )
		{
			Send();
			send = false;
		}

		if ( everyMinutes != 0 && toMail.Length != 0 &&
			Time.time - lastTime > everyMinutes * 60  ) // every second
		{
			lastTime = Time.time;
			Send();
		}
	}

	public void SendImmidiately()
	{
		OnEndEditStrings();
		Send();
	}

	private string Body()
	{
		string t = "";
		t += "За день:\n";
		t += "Садовник открыл\\закрыл ворота 0\\0 раз\n";
		t += "Садовник открыл\\закрыл дом 0\\0 раз\n";
		t += "Садовник открыл\\закрыл гостевой дом 0\\0 раз\n";
		t += "Садовник открыл\\закрыл беседку 0\\0 раз\n";
		t += "Клиент иридиума не подключался ни разу, сервер не был перезагружен, потери связи с Beckhoff не было, связь с интернетом не была потеряна\n";
		t += "\nЗа неделю:\n";
		t += "Садовник открыл\\закрыл ворота 0\\0 раз\n";
		t += "Садовник открыл\\закрыл дом 0\\0 раз\n";
		t += "Садовник открыл\\закрыл гостевой дом 0\\0 раз\n";
		t += "Садовник открыл\\закрыл беседку 0\\0 раз\n";
		t += "Клиент иридиума не подключался ни разу, сервер не был перезагружен, потери связи с Beckhoff не было, связь с интернетом не была потеряна\n";
		t += "\nЗа месяц:\n";
		t += "Садовник открыл\\закрыл ворота 0\\0 раз\n";
		t += "Садовник открыл\\закрыл дом 0\\0 раз\n";
		t += "Садовник открыл\\закрыл гостевой дом 0\\0 раз\n";
		t += "Садовник открыл\\закрыл беседку 0\\0 раз\n";
		t += "Клиент иридиума не подключался ни разу, сервер не был перезагружен, потери связи с Beckhoff не было, связь с интернетом не была потеряна\n";
		return t;
	}

	private void Send() 
	{
		try{
			MailMessage mail = new MailMessage();

			mail.From = new MailAddress("caysefxtrend@gmail.com");
			mail.To.Add(toMail);
			mail.Subject = "Солослово отчет дома";
		//	mail.Body = "Ну типа всё ок и тому подобное..., если что можно лог приложить текстовым файлом размером в пару мегабайт.";
			mail.Body = Body();
			mail.Attachments.Add( new Attachment(Master.Datapath + "/"+"log.log") );

		SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
		smtpServer.Port = 587;
		smtpServer.Credentials = new System.Net.NetworkCredential("caysefxtrend@gmail.com", "vilafxcs3") as ICredentialsByHost;
		smtpServer.EnableSsl = true;
		ServicePointManager.ServerCertificateValidationCallback = 
			delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) 
			{ return true; };
			smtpServer.Send(mail);
			Debug.Log("Email (every "+everyMinutes+") sended to "+toMail);
		} catch(Exception exc)
		{
			Debug.Log("Error: exc="+exc);	
		}
	}

}
