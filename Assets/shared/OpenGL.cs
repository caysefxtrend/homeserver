using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Camera))]
public class OpenGL : MonoBehaviour {
	public class Circle {
		//Vector3 zoom = new Vector3 (0, 0, 1);
		private Vector3[] pArray;
		private Color[] cArray;

		public Vector2[] points;
		//Color colorStart, colorEnd;
		public bool active = false;
		float depth;
		OpenGL openGL = null;

		public Vector3 dir;
		public Vector3 center;
		public float radius;
		
		public Circle (OpenGL openGL, Vector2 center, float radius, Color colorStart, Color colorEnd) {
			this.openGL = openGL;
			depth = 0;
			RefreshFast (8, center, radius, colorStart, colorEnd);
		}

		public Circle Show () {
			active = true;
			return this;
		}

		public void Remove () {
			openGL.RemoveCircle (this);
		}
		
		public void SetLayer (float depth) {
			this.depth = depth;
		}

		public Circle Set(Vector3 center, float radius, Vector3 dir) { 
			this.dir = dir; this.center = center; this.radius = radius; 
			return this; 
		}

//		public Circle Set(Color colorStart, Color colorEnd) { 
//			this.colorStart = colorStart; this.colorEnd = colorEnd;
//			return this; 
//		}

		public void Destroy () { openGL.RemoveCircle (this); }

		static public void Destroy (Circle circle) { circle.Destroy (); }

		public void Refresh() {
			int count = 20;
			if (count < 4 || count > 999) throw new System.Exception ("wrong count " + count);
			points = new Vector2[count];
			float delta = 2f / count * Mathf.PI;
			Vector2 point;
			for (int i = 0; i<count; i++) {
				point = radius * new Vector2 ( Mathf.Sin (delta * i), Mathf.Cos (delta * (float)i) );
				points [i] = point;
			}
		}

		public void Refresh (Vector2 center, float radius, Color colorStart, Color colorEnd) {
			//if (active == false) return;
			this.center = center;
			this.radius = radius;
			int count = 20;
			if (count < 4 || count > 999) throw new System.Exception ("wrong count " + count);
			points = new Vector2[count];
			float delta = 2f / count * Mathf.PI;
			for (int i = 0; i<count; i++) {
				points [i] = new Vector2 (center.x + radius * Mathf.Sin (delta * (float)i), 
				                          center.y + radius * Mathf.Cos (delta * (float)i));
				//points [i].y = -points [i].y;
			}
		//	this.colorStart = colorStart;
		//	this.colorEnd = colorEnd;
		}

		public void RefreshFast (int count, Vector2 center, float radius, Color colorStart, Color colorEnd) {
			if (count < 3 || count > 999) throw new System.Exception ("wrong count " + count);
			//if (active == false) return;
			this.center = center;
			this.radius = radius;
		//	this.colorStart = colorStart;
		//	this.colorEnd = colorEnd;
			pArray = new Vector3[count];
			cArray = new Color[count];
			float delta = 2f / (float)count * Mathf.PI;
			Color dColor = (colorEnd - colorStart) / (float)count;
			for (int i = 0; i<count; i++) {
				pArray [i] = new Vector3 (center.x + radius * Mathf.Sin (delta * (float)i), 
				                          center.y + radius * Mathf.Cos (delta * (float)i), depth);
				cArray[i] = colorStart + dColor * i;
			}
		}

		public void OnPostRenderFast () {
			if (active == false) return;
			//if (this == null) throw new System.Exception("this is null");
			//if (points == null) { Debug.LogError("no points"); return; }// throw new System.Exception("no points");
			for (int i = 0; i < pArray.Length-1; i++) {				
				GL.Color ( cArray[i] );
				GL.Vertex ( pArray[i] );
				GL.Vertex ( pArray[i+1] );
			}
			GL.Vertex ( pArray[pArray.Length-1] );
			GL.Vertex ( pArray[0] );
		}


//		public void RefreshZoom (Vector3 zoom) {
//			this.zoom = zoom;
//		}
		
		void GLVertex (int i) {
			if(dir==Vector3.up) GL.Vertex ( center + new Vector3 (points [i].x, 0, points [i].y) );
			else if(dir==Vector3.forward) GL.Vertex ( center + new Vector3 (points [i].x, points [i].y,0) );
			else if(dir==Vector3.right) GL.Vertex ( center + new Vector3 (0,points [i].y, points [i].x) );

			//GL.Vertex( new Vector3(points[i].x*zoom.z + OpenGL.instance.cam.transform.position.x+zoom.x, 
			//	-points[i].y*zoom.z + OpenGL.instance.cam.transform.position.y-zoom.y, 0 ));
			else GL.Vertex (new Vector3 (points [i].x, points [i].y, depth));
		}
		
		public void OnPostRender () {
			OnPostRenderFast(); return;
//			if (active == false) return;
//			//if (this == null) throw new System.Exception("this is null");
//			//if (points == null) { Debug.LogError("no points"); return; }// throw new System.Exception("no points");
//			Color dColor = (colorEnd - colorStart) / points.Length;
//			for (int i = 0; i < points.Length-1; i++) {				
//				GL.Color (colorStart + dColor * i);
//				GLVertex (i);
//				GLVertex (i + 1);
//			}
//			GLVertex (points.Length - 1);
//			GLVertex (0);
		}
	}
	
	public class Line {
		Vector3 zoom = new Vector3 (0, 0, 1);
		public Vector3[] line_points;
		//public Book<Vector3i> pointsBook = null;
		Color colorStart;
		Color colorEnd;
		//	float depth;
		OpenGL openGL = null;
		bool show = false;
		private float width = 1;
		public List<Vector2> coords;
		public List<Color> colors;
		public List<float> widths;

		public Line SetWidth( float width ) {
			this.width = width;
			return this;
		}

		public Line SetColors( Color colorStart, Color colorEnd ) {
			this.colorStart = colorStart;
			this.colorEnd = colorEnd;
			return this;
		}

		public Line (OpenGL openGL, Vector2 source, Vector2 dest, Color colorStart, Color colorEnd) {
			this.openGL = openGL;
			//depth = 0;
			Refresh (source, dest, colorStart, colorEnd);
		}
	
		public Line (OpenGL openGL, Vector3 source, Vector3 dest, Color colorStart, Color colorEnd) {
			this.openGL = openGL;
			//depth = 0;
			Refresh (source, dest, colorStart, colorEnd);
		}

		public void Remove () {
			openGL.RemoveLine (this);
		}

		public void Destroy () {
			openGL.RemoveLine (this);
		}

		static public void Destroy (Line line) {
			line.Remove ();
		}

		public Line Show () {
			show = true;
			return this;
		}

		public Line Hide () {
			show = false;
			return this;
		}

		static public void Show (Line line) {
			line.show = true;
		}

		static public void Hide (Line line) {
			line.show = false;
		}
		
		public void SetLayer (float depth) {
			//this.depth = depth;
			line_points [0].z = depth;
			line_points [1].z = depth;
		}
		
		public void Refresh (Vector3 source, Vector3 dest, Color colorStart, Color colorEnd) {
			line_points = new Vector3[2];
			line_points [0] = source;
			//points[0].y = points[0].y;
			line_points [1] = dest;
			//points[1].y = points[1].y;
			this.colorStart = colorStart;
			this.colorEnd = colorEnd;
		}

		public void SetPoints( Vector3[] line_points ) {
			this.line_points = line_points;
//			int len = path.Length; // ok
//			line_points = new Vector3[0]; // ok
//			//OpenGL.Stop();
//			Vector3[] points = new Vector3[len]; 
//			line_points = points;
//			//line_points = new Vector3[len]; // bad
//			// OpenGL.Start();
//			//points = path;
			//pointsBook = book;
		}

		public void RefreshZoom (Vector3 zoom) {
			this.zoom = zoom;
		}

		public void OnPostRender () {
			if (!show) return;
			if (width != 1) return;
			if ( line_points.Length <= 1) return;
			//GL.Color (colorStart);
//			if ( pointsBook != null ) {
//				int length = pointsBook.Size();
//				for (int i = 1; i < length; i++) {
//					GL.Color (colorStart);
//				//	else    		GL.Color (colorEnd);
//					GL.Vertex ( pointsBook.Get(i-1).toFloat );	
//					GL.Color (colorEnd);
//					GL.Vertex ( pointsBook.Get(i).toFloat );	
//				}
			//			} else 
			Color dColor = (colorEnd - colorStart) / line_points.Length;
			//for (int i = 0; i < points.Length-1; i++) {				
			//	GL.Color (colorStart + dColor * i);
			if ( line_points.Length == 2 ) {
				for (int i = 0; i < line_points.Length; i++) {
					if (i == 0) GL.Color (colorStart);
					else    		GL.Color (colorEnd);
					//GL.Color (colorStart + dColor * i);
					GL.Vertex (new Vector3 (line_points [i].x * zoom.z + zoom.x, line_points [i].y * zoom.z + zoom.y, line_points [i].z));	
				}
			} else {
				for (int i = 1; i < line_points.Length; i++) {
					GL.Color (colorStart + dColor * (i-1));
					GL.Vertex ( line_points[i-1] );	
					GL.Color (colorStart + dColor * i);
					GL.Vertex ( line_points[i] );	
				}
			}
		}

		public void OnPostRenderQuads () {
			if (!show) return;
			if (width == 1) return;
			Vector3[] _points;
			if ( coords != null ) {
				List<Vector3> pointsList = new List<Vector3>();
				for (int i = 0; i < coords.Count; i++) {
					pointsList.Add( (Vector3)coords[i] );
				}
				_points = pointsList.ToArray();
			} else _points = this.line_points;
			//Vector2 lastVX1 = Vector2.zero;
		//	Vector2 lastVX2 = Vector2.zero;
			Vector3 lastPerpendicular = Vector3.zero;
			for (int i = 0; i < _points.Length-1; i++) {
				Vector3 p1 = new Vector3 ( _points [i + 1].y, _points [i].x, _points [i].z );
				Vector3 p2 = new Vector3 ( _points [i].y, _points [i + 1].x, _points [i].z );
//				Vector3 p1 = new Vector3 ( points [i + 1].x, points [i].y, points [i].z );
//				Vector3 p2 = new Vector3 ( points [i].x, points [i + 1].y, points [i].z );
				Vector3 v1 = _points [i];
				Vector3 v2 = _points [i + 1];
				Vector3 perpendicular = (p1 - p2).normalized;
				if ( widths != null ) perpendicular *= widths[i];
				else perpendicular *= width;
				//perpendicular = new Vector3( 0.5f, 0.5f, 0);
				//perpendicular.y = - perpendicular.y;
//				GL.Vertex (openGL.camera.ViewportToWorldPoint (v1 - perpendicular));
//				GL.Vertex (openGL.camera.ViewportToWorldPoint (v1 + perpendicular));
//				GL.Vertex (openGL.camera.ViewportToWorldPoint (v2 + perpendicular));
//				GL.Vertex (openGL.camera.ViewportToWorldPoint (v2 - perpendicular));		
				Vector2 vx1;
				Vector2 vx2;
				if ( lastPerpendicular != Vector3.zero ) {
					vx1 = v1 - lastPerpendicular;
					vx2 = v1 + lastPerpendicular;
				}
				else {
					vx1 = v1 - perpendicular;
					vx2 = v1 + perpendicular;
				}
				perpendicular = (p1 - p2).normalized;
				if ( widths != null ) perpendicular *= widths[i+1];
				else perpendicular *= width;
				Vector2 vx3 = v2 + perpendicular;
				Vector2 vx4 = v2 - perpendicular;
				lastPerpendicular = perpendicular;
//				vx1.y = -vx1.y;
//				vx2.y = -vx2.y;
//				vx3.y = -vx3.y;
//				vx4.y = -vx4.y;
				Color color = Color.Lerp( colorStart, colorEnd, (float)i/(float)_points.Length );
				GL.Color (color);
				//if ( lastVX1 != Vector2.zero ) vx1 = lastVX1;
				//if ( lastVX2 != Vector2.zero ) vx2 = lastVX2;
				GL.Vertex ( vx1 );
				GL.Vertex ( vx2 );
				GL.Color (color);
				GL.Vertex ( vx3 );
				GL.Vertex ( vx4 );
				//lastVX1 = vx4;
				//lastVX2 = vx3;
			}
		}
	}

	public class Cube {

		bool show = false;
		public OpenGL openGL;
		public Vector3 position;
		public Vector3 length;
		public Color color;
		//private VectorCube vCube = null;

		public Cube (OpenGL openGL, Vector3 position, Vector3 length, Color color) {
			this.openGL = openGL;
			Refresh (position, length, color);
		}

		public void Refresh (Vector3 position, Vector3 length, Color color) {
			this.position = position;
			this.length = length;
			this.color = color;
		}

//		public void SetVCube( VectorCube vCube ) {
//			this.vCube = vCube;
//		}

		public Cube Destroy () { openGL.RemoveCube (this); return null; }

		public void Show () { show = true; }
		public void Hide () { show = false; }

		public void OnPostRender () {
			if (!show) return;
			//if ( vCube != null ) { OnPostRenderVCube(); return; }
			GL.Color (color);
			// bottom squad
			GL.Vertex (position);
			GL.Vertex (position + new Vector3 (length.x, 0, 0));

			GL.Vertex (position + new Vector3 (length.x, 0, 0));
			GL.Vertex (position + new Vector3 (length.x, 0, length.z));

			GL.Vertex (position + new Vector3 (length.x, 0, length.z));
			GL.Vertex (position + new Vector3 (0, 0, length.z));

			GL.Vertex (position + new Vector3 (0, 0, length.z));
			GL.Vertex (position);
			// top squad
			GL.Vertex (position + new Vector3 (0, length.y, 0));
			GL.Vertex (position + new Vector3 (length.x, length.y, 0));

			GL.Vertex (position + new Vector3 (length.x, length.y, 0));
			GL.Vertex (position + new Vector3 (length.x, length.y, length.z));

			GL.Vertex (position + new Vector3 (length.x, length.y, length.z));
			GL.Vertex (position + new Vector3 (0, length.y, length.z));

			GL.Vertex (position + new Vector3 (0, length.y, length.z));
			GL.Vertex (position + new Vector3 (0, length.y, 0));
			// walls
			GL.Vertex (position);
			GL.Vertex (position + new Vector3 (0, length.y, 0));
			GL.Vertex (position + new Vector3 (length.x, 0, 0));
			GL.Vertex (position + new Vector3 (length.x, length.y, 0));
			GL.Vertex (position + new Vector3 (length.x, 0, length.z));
			GL.Vertex (position + new Vector3 (length.x, length.y, length.z));
			GL.Vertex (position + new Vector3 (0, 0, length.z));
			GL.Vertex (position + new Vector3 (0, length.y, length.z));

		}

//		public void OnPostRenderVCube () {
//			GL.Color (color);
//			// bottom squad
//			GL.Vertex ( vCube.GetPoint(0) ); GL.Vertex ( vCube.GetPoint(1) );
//			GL.Vertex ( vCube.GetPoint(1) ); GL.Vertex ( vCube.GetPoint(2) );
//			GL.Vertex ( vCube.GetPoint(2) ); GL.Vertex ( vCube.GetPoint(3) );
//			GL.Vertex ( vCube.GetPoint(3) ); GL.Vertex ( vCube.GetPoint(0) );
//			// top squad
//			GL.Vertex ( vCube.GetPoint(4) ); GL.Vertex ( vCube.GetPoint(5) );
//			GL.Vertex ( vCube.GetPoint(5) ); GL.Vertex ( vCube.GetPoint(6) );
//			GL.Vertex ( vCube.GetPoint(6) ); GL.Vertex ( vCube.GetPoint(7) );
//			GL.Vertex ( vCube.GetPoint(7) ); GL.Vertex ( vCube.GetPoint(4) );
//			// walls
//			GL.Vertex ( vCube.GetPoint(0) ); GL.Vertex ( vCube.GetPoint(4) );
//			GL.Vertex ( vCube.GetPoint(1) ); GL.Vertex ( vCube.GetPoint(5) );
//			GL.Vertex ( vCube.GetPoint(2) ); GL.Vertex ( vCube.GetPoint(6) );
//			GL.Vertex ( vCube.GetPoint(3) ); GL.Vertex ( vCube.GetPoint(7) );
//		}

	}

	public OpenGL.Cube CreateCube (Vector3 position, Vector3 length, Color color) {
		Cube cube = new Cube (this, position, length, color);
		listOfCubes.Add (cube);
		numberOfCubes = listOfCubes.Count;
		return cube;
	}
	
	static public OpenGL.Cube CreateCube (Camera camera, Vector3 position, Vector3 length, Color color) {
		if (camera == null) throw new System.Exception ("camera == null");
		OpenGL opengl = (OpenGL)(camera.GetComponent ("OpenGL"));
		if (opengl == null) 	throw new System.Exception ("opengl == null");
		return opengl.CreateCube (position, length, color);
	}

	public void RemoveCube (Cube cube) {
		listOfCubes.Remove (cube);
		numberOfCubes = listOfCubes.Count;
	}

	public Material lineMaterial;
 
	static public OpenGL instance = null;
	void Awake () { instance = this; }
	
	[SerializeField] public List<Circle> 	listOfCircles = new List<Circle> ();
	[SerializeField] public List<Line> listOfLines = new List<Line> ();
	[SerializeField] public List<Cube> listOfCubes = new List<Cube> ();
	public int numberOfLines = 0;
	public int numberOfCircles = 0;
	public int numberOfCubes = 0;
	//public Material material = null;

	public OpenGL.Line CreateLine (Vector2 source, Vector2 dest, Color colorStart, Color colorEnd) {
		Line line = new Line (this, source, dest, colorStart, colorEnd);
		listOfLines.Add (line);		
		numberOfLines = listOfLines.Count;
		return line;
	}
	
	public OpenGL.Line CreateLine (Vector3 source, Vector3 dest, Color colorStart, Color colorEnd) {
		Line line = new Line (this, source, dest, colorStart, colorEnd);
		listOfLines.Add (line);		
		numberOfLines = listOfLines.Count;
		return line;
	}
	
	static public OpenGL.Line CreateLine (Camera camera, Vector3 source, Vector3 dest, Color colorStart, Color colorEnd) {
		if (camera == null) throw new System.Exception ("camera == null");
		OpenGL opengl = (OpenGL)(camera.GetComponent ("OpenGL"));
		if (opengl == null) 	throw new System.Exception ("Camera " + camera.name + " must have opengl component");
		return opengl.CreateLine (source, dest, colorStart, colorEnd);
	}
	
	static public OpenGL.Line CreateLine (Camera camera, Vector2 source, Vector2 dest, Color colorStart, Color colorEnd) {
		if (camera == null)
			throw new System.Exception ("camera == null");
		OpenGL opengl = (OpenGL)(camera.GetComponent ("OpenGL"));
		if (opengl == null)
			throw new System.Exception ("Camera " + camera.name + " must have opengl component");
		return opengl.CreateLine (source, dest, colorStart, colorEnd);
	}
	
	public OpenGL.Circle CreateCircle (Vector2 coords, float radius, Color colorStart, Color colorEnd) {
		Circle circle = new Circle (this, coords, radius, colorStart, colorEnd);
		listOfCircles.Add (circle);	
		numberOfCircles = listOfCircles.Count;
		//arrayOfCircles.
		return circle;
	}

//	static public OpenGL.Circle CreateCircle (Vector2 coords, float radius, Color colorStart, Color colorEnd) {
////		if (camera == null) {
////			//camera = 
////			throw new System.Exception ("camera == null");
////		}
//		OpenGL opengl = OpenGL.instance;
//		if (opengl == null) throw new System.Exception ("Camera " + camera.name + " must have opengl component");
//		return opengl.CreateCircle (coords, radius, colorStart, colorEnd);
//	}

	static public OpenGL.Circle CreateCircle (Camera camera, Vector2 coords, float radius, Color colorStart, Color colorEnd) {
		if (camera == null) {
			//camera = 
			throw new System.Exception ("camera == null");
		}
		OpenGL opengl = (OpenGL)(camera.GetComponent ("OpenGL"));
		if (opengl == null) throw new System.Exception ("Camera " + camera.name + " must have opengl component");
		return opengl.CreateCircle (coords, radius, colorStart, colorEnd);
	}

	public void RemoveLine (Line line) {
		listOfLines.Remove (line);
		numberOfLines = listOfLines.Count;
	}
	
	public void RemoveCircle (Circle circle) {
		listOfCircles.Remove (circle);
		numberOfCircles = listOfCircles.Count;
	}
	
	// Creates a simple two point line
	void Start () {
		Shader shader = Shader.Find("OpenGL");
		if (shader==null) throw new System.Exception ("Shader OpenGL cannot be found in assets folder!");
		lineMaterial = new Material(shader);
//		lineMaterial = new Material ("Shader \"Lines/Colored Blended\" {" +
//			"SubShader { Pass {" +
//		    "   BindChannels { Bind \"vertex\", vertex Bind \"color\", color }" +
//			"   Blend SrcAlpha OneMinusSrcAlpha" +
//			"   ZWrite Off Cull Off Fog { Mode Off }" +
//			"} } }");
		lineMaterial.hideFlags = HideFlags.HideAndDontSave;
		lineMaterial.shader.hideFlags = HideFlags.HideAndDontSave;
		
		//	linePoints = new Vector2[2];
	}
 
	// Sets line endpoints to center of screen and mouse position
	void Update () {
		//linePoints[0] = new Vector2(0.5f, 0.5f);
		//linePoints[1] = new Vector2(Input.mousePosition.x/Screen.width, Input.mousePosition.y/Screen.height);
	}
 
	//void OnPostRender () {
	void OnRenderObject () {
			//	 lineMaterial.SetPass( 0 ); 
//	    GL.Begin( GL.LINES ); 
//
//		GL.Color(Color.white);
//        GL.Vertex( new Vector3(4.3f, 12.2f, -3.2f ));
//        GL.Vertex( new Vector3(7.1f, 12.4f, -3.2f ));
//            GL.Vertex( new Vector3(0, 0, 0 ));
//            GL.Vertex( new Vector3(7.1f, 12.4f, -3.2f ));
//            GL.Vertex( new Vector3(0, 0, 0 ));
//            GL.Vertex( new Vector3(20.1f, -20.4f, -3.2f ));
//
//	    GL.End(); 

		

//		if (!drawLines || linePoints == null || linePoints.Length < 2)
//			return;
 
//		float nearClip = cam.nearClipPlane + 0.00001f;
//		int end = linePoints.Length - 1;
//		float thisWidth = 1f/Screen.width * lineWidth * 0.5f;
		//if ( material != null ) material.SetPass(0);
		//else
			lineMaterial.SetPass (0);
		//	GL.Color(lineColor);
 
//		if (lineWidth == 1)
//		{

		GL.Begin (GL.LINES);
		foreach (Line line in listOfLines)
			line.OnPostRender ();
		foreach (Circle circle in listOfCircles)
			circle.OnPostRender ();
		foreach (Cube cube in listOfCubes)
			cube.OnPostRender ();
		GL.End ();
		//GL.
		GL.Begin (GL.QUADS);
		foreach (Line line in listOfLines)
			line.OnPostRenderQuads ();
		GL.End ();

		// for (int i = 0; i < circle.points.Length; ++i)
		//	{
		//	GL.Vertex( new Vector3(circle.points[i].x*circle.zoom, circle.points[i].y*circle.zoom, 0 ));
		//GL.Vertex(cam.ViewportToWorldPoint(new Vector3(array[i].x, array[i].x, nearClip)));
		//GL.Vertex(cam.ViewportToWorldPoint(new Vector3(linePoints[i+1].x, linePoints[i+1].y, nearClip)));
		//UnityEngine.Debug.Log ( array[i] );
		//UnityEngine.Debug.Log (cam.ViewportToWorldPoint(new Vector3(linePoints[i+1].x, linePoints[i+1].y, nearClip)));
		//}
		//GL.Color(Color.white);
		// GL.Vertex( new Vector3(0, 0, 0 ));
		//GL.Vertex( new Vector3(20.1f, -20.4f, 0 ));
		//GL.Vertex( new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0 ));
		//UnityEngine.Debug.Log (cam.ViewportToWorldPoint(new Vector3(linePoints[i+1].x, linePoints[i+1].y, nearClip)));
		//linePoints[1] = new Vector2(Input.mousePosition.x/Screen.width, Input.mousePosition.y/Screen.height);
//    	}
//    	else
//		{
//	        GL.Begin(GL.QUADS);
//	        for (int i = 0; i < end; ++i)
//			{
//	            Vector3 perpendicular = (new Vector3(linePoints[i+1].y, linePoints[i].x, nearClip) -
//	                                 new Vector3(linePoints[i].y, linePoints[i+1].x, nearClip)).normalized * thisWidth;
//	            Vector3 v1 = new Vector3(linePoints[i].x, linePoints[i].y, nearClip);
//	            Vector3 v2 = new Vector3(linePoints[i+1].x, linePoints[i+1].y, nearClip);
//	            GL.Vertex(cam.ViewportToWorldPoint(v1 - perpendicular));
//	            GL.Vertex(cam.ViewportToWorldPoint(v1 + perpendicular));
//	            GL.Vertex(cam.ViewportToWorldPoint(v2 + perpendicular));
//	            GL.Vertex(cam.ViewportToWorldPoint(v2 - perpendicular));
//        	}
//    	}
	}
 
	void OnApplicationQuit () {
		DestroyImmediate (lineMaterial);
	}
}