using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class KeyValue <TKey, TValue> {
	Dictionary<TKey, TValue> map = new Dictionary<TKey, TValue>();
	List<TKey> keys = new List<TKey> ();
	List<TValue> values = new List<TValue> ();

	public Dictionary<TKey, TValue>.ValueCollection Values() { return map.Values; }
	public int Count { get { return map.Count; } }
	public TValue this[int index] { get { return values[index]; } set { values[index] = value; map[keys[index]]=value; } }

	public TValue TryGetValue(TKey key) { TValue value; map.TryGetValue(key,out value); return value; }
	public void Clear() { keys.Clear(); values.Clear(); map.Clear(); }
	public void Add (TKey key, TValue value) { map.Add( key, value ); keys.Add( key ); values.Add( value ); }
	public TValue ValueOfKey ( TKey key ) { return map[key]; }
	// slow because list.IndexOf
	public void Remove (TKey key ) { map.Remove( key ); int index = keys.IndexOf( key ); keys.RemoveAt(index); values.RemoveAt(index); }
	// slow because list.IndexOf
	public void Replace (TKey key, TValue value) { map[key] = value; int index = keys.IndexOf( key ); values[index] = value; }
	public bool ContainsKey( TKey key ) { return map.ContainsKey( key ); }
	public TValue ValueByIndex( int index ) { return values[index]; }
	public TKey KeyByIndex( int index ) { return keys[index]; }
	// this is really slow
	//public int IndexOfKey( TKey key ) { return keys.IndexOf( key ); }
	public int Size () { return map.Count; }
	override public string ToString() { string t = ""; for( int i = 0; i < keys.Count; i++ ) t += ("("+keys[i]+", "+values[i]+")"); return t; }
	//public TKey FirstKey() { return keys[0]; }	
	//public TValue FirstValue() { return values[0]; }
	//public TKey LastKey() { return keys[keys.Count-1]; }
	//public TValue LastValue() { return values[values.Count-1]; }



	//public int Count { get { return Size(); } }

//	public TValue this[int index] { get { return ValueByIndex(index); } // //set { Add( key, value ); }
//	}

//	public KeyValue () {}
	//public KeyValue ( int Size ) {}
//	public Book<TValue> Values () {	return values; }


//	static public Packet ToPacket( KeyValue <String, int> map ) {
//		Packet packet = new Packet();
//		packet.putUtfStringArray( "keys", map.keys );
//		packet.putIntArray( "values", map.values );
//		return packet;
//	}

//	public Packet ToPacket( KeyValue <String, int> map ) {
//		Packet packet = new Packet();
//		packet.PutStringsBook( "keys", map.keys );
//		packet.PutIntsBook( "values", map.values );
//		return packet;
//	}
//
//	static public KeyValue <String, int> FromPacket( Packet packet ) {
//		if ( packet == null ) return null;
//		KeyValue <String, int> map = new KeyValue<String, int>();
//		Book<String> strings = packet.GetStringsBook( "keys" );
//		Book<int> ints = packet.GetIntsBook( "values" );
//		if ( strings.Size() != ints.Size() ) throw new Exception("strings != ints");
//		//		if ( strings.Size() == 0 ) throw new Exception("size == 0");
//		for( int i = 0; i < strings.Size(); i++ )
//			map.Add( strings.Get(i), ints.Get(i) );
//		return map;
//	}
//
//	public Packet ToPacket( KeyValue <String, Vector2> map ) {
//		Packet packet = new Packet();
//		packet.PutStringsBook( "keys", map.keys );
//		packet.PutVector2Book( "values", map.values );
//		return packet;
//	}
//
//	static public KeyValue <String, Vector2> StringVector2FromPacket( Packet packet ) {
//		if ( packet == null ) return null;
//		KeyValue <String, Vector2> map = new KeyValue<String, Vector2>();
//		Book<String> strings = packet.GetStringsBook( "keys" );
//		Book<Vector2> ints = packet.GetVector2Book( "values" );
//		if ( strings.Size() != ints.Size() ) throw new Exception("strings != ints");
//		//		if ( strings.Size() == 0 ) throw new Exception("size == 0");
//		for( int i = 0; i < strings.Size(); i++ )
//			map.Add( strings.Get(i), ints.Get(i) );
//		return map;
//	}

//	public Packet ToPacket( KeyValue <String, Player.KnownTypes> map ) {
//		Packet packet = new Packet();
//		packet.PutStringsBook( "keys", map.keys );
//		packet.PutKnownTypesBook( "values", map.values );
//		return packet;
//	}
//
//	static public KeyValue <String, Player.KnownTypes> FromPacket(KeyValue<String, Player.KnownTypes>someMap, Packet packet ) {
//		if ( packet == null ) return null;
//		KeyValue <String, Player.KnownTypes> map = new KeyValue<String, Player.KnownTypes>();
//		Book<String> strings = packet.GetStringsBook( "keys" );
//		Book<Player.KnownTypes> types = packet.GetKnownTypesBook( "values" );
//		if ( strings.Size() != types.Size() ) throw new Exception("strings != ints");
//		//		if ( strings.Size() == 0 ) throw new Exception("size == 0");
//		for( int i = 0; i < strings.Size(); i++ )
//			map.Add( strings.Get(i), types.Get(i) );
//		return map;
//	}


//	public Packet ToPacket( KeyValue <String, Integer> map ) {
//		Packet packet = new Packet();
//		packet.putUtfStringArray( "keys", map.keys );
//		packet.putIntArray( "values", map.values );
//		return packet;
//	}

//	public Packet ToPacket() {
//		if ( typeof(TKey).Name != "String" ) throw new Exception("TKey not a String ("+typeof(TKey).Name+")");
//		if ( typeof(TValue).Name != "int" ) throw new Exception("TValue not a int ("+typeof(TValue).Name+")");
//		Packet packet = new Packet();
//		packet.putUtfStringArray( "keys", KeysToStrings() );
//		packet.putIntArray( "values", ValuesToInts() );
//		//packet.putIntArray( "values", values );
//		// TODO
//		return packet;
//	}
//
//	public Book<String> KeysToStrings() {
//		Book<String> strings = new Book<String>();
//		for ( int i = 0; i <keys.Size(); i++ )
//			strings.Add( keys.Get(i).ToString() );
//		return strings;
//	}
//
//	public Book<int> ValuesToInts() {
//		Book<int> ints = new Book<int>();
//		for ( int i = 0; i <values.Size(); i++ )
//			ints.Add( (int)(object)values.Get(i) );
//		return ints;
//	}

//	public void Add (KeyValue<TKey,TValue> map) {
//		for( int i = 0; i < map.Size(); i++) Add ( map.KeyByIndex(i), map.ValueByIndex(i) );
//	}


//	public void Remove (TKey key ) {
////		for ( int i  = size (); i >= 0; i ++ ) {
////			if ( keys.Get(i) == key ) {
////				keys.RemoveAt( i );
////				values.RemoveAt( i  );
////			}
////		}
//		int index = keys.IndexOf( key );
//		if ( index < 0 ) throw new Exception("key("+key.ToString()+") not in keys");
//		keys.RemoveAt( index );
//		values.RemoveAt( index  );
//	}


//	public Book<TKey> KeysByValue( TValue value ) {
//		Book<TKey> book = new Book<TKey>();
//		for ( int index = 0; index < values.Size(); index ++ )
//			if ( values.Get( index ).Equals( value ) ) book.Add( keys.Get(index) );
//			//if ( values.Get( index ) ==  value ) book.Add( keys.Get(index) );
//	//	Debug.Log ("Get: "+value.ToString()+" book: "+book.ToString());
//		return book;
//	}


//	public void Clear() { keys.Clear(); values.Clear(); }

//	public int NumberOfValues( TValue value ) {
//		int size = 0;
//		for ( int index = 0; index < values.Size(); index ++ )
//			if ( values.Get( index ).Equals( value ) ) size ++;
//		return size;
//	}


//	public boolean IsEquals(KeyValue<TKey, TValue> map ) {
//		if ( Size() != map.Size() ) return false;
//		for ( int c = 0; c < Size(); c++) 
//			if ( KeyByIndex( c ) != map.KeyByIndex( c ) || ValueByIndex( c ) != map.ValueByIndex( c ) ) return false;
//		return true;
//	}


}

