﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UISpriteAtlas : MonoBehaviour {
	static private UISpriteAtlas[] atlases = null;
	static private Hashtable hashtable = new Hashtable();

	public static UISpriteAtlas GetSpriteAtlasForSprite (string spriteName) {
		//UISpriteAtlas[] atlases = null;
		//if ( atlasesList  == null ) atlases = ClientGame.instance.gameObject.GetComponentsInChildren<UISpriteAtlas> ();
		//else atlases = atlasesList;
		return (UISpriteAtlas)hashtable[spriteName];

//		foreach (UISpriteAtlas atlas in atlases)
//			if (atlas.IsSprite (spriteName))
//				return atlas;
//		//return null;
//		throw new System.Exception ("Possible wrong sprite name \"" + spriteName + "\"");
	}

	public static UISpriteAtlas GetSpriteAtlas (string atlasName) {
//		UISpriteAtlas[] atlases = null;
//		if ( atlasesList  == null ) atlases = ClientGame.instance.gameObject.GetComponentsInChildren<UISpriteAtlas> ();
//		else atlases = atlasesList;
//		UISpriteAtlas[] atlases = ClientGame.instance.gameObject.GetComponentsInChildren<UISpriteAtlas> ();
		foreach (UISpriteAtlas atlas in atlases)
			if (atlas.name == atlasName )
				return atlas;
		//return null;
		throw new System.ArgumentException ("Possible wrong atlas name \"" + atlasName + "\"");
	}

	public static Sprite GetSprite(string textureName) {
		UISpriteAtlas atlas = GetSpriteAtlasForSprite( textureName );
		if ( atlas == null ) throw new System.Exception("not found texture in atlases: "+textureName);
		Sprite sprite = atlas.GetSprite(textureName, false, Vector2.zero);
		return sprite;
	}

	public static Texture GetTexture(string textureName) {
		UISpriteAtlas atlas = GetSpriteAtlasForSprite( textureName );
		Sprite sprite = atlas.GetSprite(textureName, false, Vector2.zero);
		Texture tex = sprite.texture;
		//textureName = "Cutout/PlanetCircleMask_12x12";
		//tex = Resources.Load<Texture> (textureName);
		return tex;
	}

	public static bool IsExist (string spriteName) {
		//UISpriteAtlas[] atlases = ClientGame.instance.gameObject.GetComponentsInChildren<UISpriteAtlas> ();
		foreach (UISpriteAtlas atlas in atlases)
			if (atlas.IsSprite (spriteName))
				return true;
		//return null;
		return false;
	}

	public bool pack = true;
	public Material material;
	//public Material materialForQuads;
	//public Camera defaultCamera;
	//[SerializeField] List<string> paths = new List<string>();
	[SerializeField] public string[] pathToSprites = new string[0];
	//public Texture2D[] textures;
	[SerializeField] public string[] textures;
	//[SerializeField] public string[] textures;
	public Texture2D atlas;
	public Rect[] rects;
	public int atlasSize = 512;
	public bool mipMaps = false;
//	public int atlasWidth = 512;
//	public int atlasHeight = 512;
	[SerializeField] int textureAtlasPadding = 2;
	public FilterMode filterMode = FilterMode.Point;
	public bool SavePackedTextures;

	public List<UIMeshSprite> meshSprites = new List<UIMeshSprite>();

	public bool IsSprite (string spriteName) {
//		List<string> texturesList = new List<string>(textures);
//		if ( texturesList.Contains( spriteName ) ) return true;
//		return false;
		foreach (string texture in textures)	
			if (spriteName.Equals( texture ) )
				return true;
		//Debug.Log("found texture "+texture.name);
		return false;
	}

	// Use this for initialization
	void Awake () {
		SavePackedTextures = false;
		if ( transform.parent == null ) atlases = FindObjectsOfType<UISpriteAtlas>();
		else atlases = transform.parent.GetComponentsInChildren<UISpriteAtlas> ();

		//Debug.Log ( "UISpriteAtlas.Awake realtimeSinceStartup = "+Time.realtimeSinceStartup );
		//Texture2D[] textures = new Texture2D[0];
		List<Texture2D> textures = new List<Texture2D>();
		for ( int i = 0; i < pathToSprites.Length; i++ ) {
			Texture2D[] array = Resources.LoadAll<Texture2D> (pathToSprites[i]);
			if ( array.Length == 0 ) Debug.LogWarning("No sprites at path: "+pathToSprites[i]);
			for ( int t = 0; t < array.Length; t++ )
				textures.Add( array[t] );
		}
		this.textures = new string[textures.Count];
		for (int i = 0; i < textures.Count; i++) {
			this.textures [i] = textures [i].name;
			if ( hashtable.ContainsKey(  textures [i].name ) ) Debug.LogWarning("texture '"+textures [i].name+"' already contains");
			else hashtable.Add( textures [i].name, this );
		}
		// TEST Read/Write Enabled
		if (pack) {
			for (int i = 0; i < textures.Count; i++) {
				//Debug.LogWarning ("("+textures [i].name+")");
				if ( textures[i].mipmapCount > 1 ) Debug.LogWarning("Texture '" + textures [i].name + "' have mip map ("+textures[i].mipmapCount+") set - its can make error in packed mode");
				try {
					textures [i].GetPixel (0, 0);
				} catch (UnityException e) {
					if (e.Message.StartsWith ("Texture '" + textures [i].name + "' is not readable")) {
						Debug.LogError ("Please enable read/write on texture [" + textures [i].name + "]");
					} else
						Debug.LogError ("UnityError: ("+textures [i].name+") " + e.Message);
				}
				catch (System.Exception e) {
					Debug.LogError ("Error: ("+textures [i].name+") " + e.Message);
				}
			}
		}
		if (pack && atlas==null) {
			atlas = new Texture2D (atlasSize, atlasSize, TextureFormat.ARGB32, mipMaps, true);
			//Debug.Log("texelSize: "+atlas.texelSize );
			if ( mipMaps ) {
				atlas.SetPixels(atlas.GetPixels());
				atlas.Apply(true);
			}

			atlas.filterMode = filterMode;
			rects = atlas.PackTextures (textures.ToArray(), textureAtlasPadding, atlasSize); // padding = 2 ?
			//Debug.Log("-texelSize: "+atlas.texelSize );
			for (int i = 0; i < rects.Length; i++) {
				rects [i].x *= atlas.width;
				rects [i].y *= atlas.height;
				rects [i].width *= atlas.width;
				rects [i].height *= atlas.height;
			}
			// TEST for compressing
			for (int i = 0; i < rects.Length; i++) {
				if (rects [i].width != textures [i].width || rects [i].height != textures [i].height) {
					//Debug.Log (" rects[0]: " + rects [0] + ", texWH: " + textures [0].width + " x " + textures [0].height);
					//throw new Exception 
					Debug.LogError
						("Atlas '" + gameObject.name + "' is compressed ! [" +
						textures [i].name + "](" + rects [i].width + "<-" + textures [i].width + ", " + rects [i].height + "<-" + textures [i].height + ")");
				}
			}			

		} else {
			rects = new Rect[textures.Count];
			for (int i = 0; i < rects.Length; i++) {
				rects [i].x = 0;
				rects [i].y = 0;
				rects [i].width = textures [i].width;
				rects [i].height = textures [i].height;
			}
		}
		//foreach(Texture2D texture in textures)	Debug.Log("found texture "+texture.name);
	}

	public Rect GetTextureRect (string spriteName) {
		for (int i = 0; i < textures.Length; i++)	
			if (spriteName == textures [i])
				return rects [i];
		throw new System.ArgumentOutOfRangeException (spriteName);
	}

	public Texture2D GetTextureFromAtlas (string spriteName) {
		if (pack) return atlas;
		for (int i = 0; i < textures.Length; i++) {
			if (spriteName == textures [i]) {	
				Texture2D texture = null;
				for ( int t = 0; t < pathToSprites.Length; t++ ) {
					texture = (Texture2D)Resources.Load (pathToSprites[t] + "/" + spriteName, typeof(Texture2D));
					if (texture != null) break;
					//throw new System.Exception ("path = \"" + gameObject.name + "/" + spriteName + "\"");
				}
				if (texture == null) throw new System.Exception ("path = \"" + gameObject.name + "/" + spriteName + "\"");
				return texture;
			}
		}
		throw new System.Exception ("path = \"" + gameObject.name + "/" + spriteName + "\"");
	}

	public Sprite GetSprite (string spriteName, bool centered, Vector2 size) {
		Vector2 pivot = centered ? new Vector2 (0.5f, 0.5f) : Vector2.zero;
		float pixelsToUnits = 1;
		uint extrude = 0u;
		SpriteMeshType meshType = SpriteMeshType.FullRect;
		if (pack) {
			Rect rect = GetTextureRect (spriteName);
			if ( size != Vector2.zero ) { rect.width = size.x; rect.height = size.y; }
			Sprite sprite = Sprite.Create (atlas, rect, pivot, pixelsToUnits, extrude, meshType);
			if (sprite == null)
				throw new System.Exception ("path = \"" + gameObject.name + "/" + spriteName + "\"");
			return sprite;
		} else {
			for (int i = 0; i < textures.Length; i++) {
				if (spriteName == textures [i]) {	
					//Debug.Log ("path = \""+pathToSprites+"\\spriteName"+"\"");
					Rect rect = rects [i];
					if ( size != Vector2.zero ) { rect.width = size.x; rect.height = size.y; }
					Texture2D texture = null;
					for ( int t = 0; t < pathToSprites.Length; t++ ) {
						texture = (Texture2D)Resources.Load (pathToSprites[t] + "/" + spriteName, typeof(Texture2D));
						//Debug.Log("path = \"" + pathToSprites[t] + "/" + spriteName + "\" texture "+(texture != null));
						if (texture != null) break;
						//throw new System.Exception ("path = \"" + gameObject.name + "/" + spriteName + "\"");
					}
					if (texture == null) throw new System.Exception ("path = \"" + gameObject.name + "/" + spriteName + "\"");
					return Sprite.Create (texture, rect, pivot, pixelsToUnits);
				}
			}
		}
		throw new System.ArgumentOutOfRangeException (spriteName);
	}

	void Start () {
	}

//	void Update () {
//		if ( SavePackedTextures ) {
//			SavePackedTextures = false;
//			if ( atlas == null ) { Debug.LogError("cant save textures coz atlas texture is empty!"); return; }
//			string filename = "JSON/"+name;
//			KeyValue<String,object> json = Utility.LoadJsonKeyValue( filename );
//			SaveTexturesByJson(json);
//		}
//	}

//	void FixedUpdate() {
//		if ( meshSprites.Count > 0 ) {
//			UIMeshSprite mSprite = meshSprites[0];
//			mSprite.UpdateMeshes();
//		}
//	}

	/*
	public List<Texture2D> texturesList = new List<Texture2D>();
	private void SaveTexturesByJson(KeyValue<String,object> json) {
		string atlasname = (String)json.ValueOfKey("atlasname");
		if ( atlasname != name ) Debug.LogWarning("this atlas name '"+name+"' but json atlas name is '"+atlasname+"'");
		ArrayList images = (ArrayList)json.ValueOfKey("images");
		Debug.Log("found "+images.Count+" textures inside atlas "+atlasname);
		rects = new Rect[images.Count];
		Color32[] from = atlas.GetPixels32();
		for ( int i = 0; i < images.Count; i++ ) {
			KeyValue<String,object> image = (KeyValue<String,object>)images[i];
			// create image
			string imagePath = (String)image.ValueOfKey("image path");
			int x = (int)(double) image.ValueOfKey( "x" );
			int y = (int)(double) image.ValueOfKey( "y" );
			int w = (int)(double) image.ValueOfKey( "w" );
			int h = (int)(double) image.ValueOfKey( "h" );
			// new texture
			Texture2D newTex = new Texture2D( w, h, TextureFormat.ARGB32, false, false );
			Color32[] to = newTex.GetPixels32();
			Vector2 fromPos = new Vector2(x, atlas.height-y-h);
			CopyPixels( from, new Vector2(atlas.width, atlas.height), fromPos, to, new Vector2(w,h), Vector2.zero, new Vector2(w,h) );
			newTex.SetPixels32( to );
			newTex.Apply();
			Rect rect = new Rect( x, y, w, h );
			//Sprite sprite = Sprite.Create (atlas, rect, new Vector2 (0.5f, 0.5f), 1, 0, SpriteMeshType.FullRect);
			//if (sprite == null)
			//	throw new System.Exception ("Cant create texture path = \"" + imagePath + "\" rect: " + rect);
			texturesList.Add( newTex );
			rects[i] = rect;
			// save
			string saveAs = imagePath;
			byte[] png= newTex.EncodeToPNG();
			// Go to Edit -> Project Settings -> Player -> Other Settings and change Write Access to External.
			string path2 = Application.dataPath.Substring (0, Application.dataPath.Length - 7); 
			string path = path2 + "/"+imagePath+"";
			string dir = path.Substring(0, path.LastIndexOf('/') );
			System.IO.Directory.CreateDirectory( dir );
			System.IO.File.WriteAllBytes(path, png);
			Debug.LogWarning("File saved: "+path);
//			string temp= System.Convert.ToBase64String(byteArray);
//			PlayerPrefs.SetString(saveAs,temp); /// save it to file if u want.
//			PlayerPrefs.SetInt(saveAs+"_w",sprite.texture.width);
//			PlayerPrefs.SetInt(saveAs+"_h",sprite.texture.height);
		}
	} //*/

	private void CopyPixels( Color32[] from, Vector2 fromTexSize, Vector2 fromPos, 
	                        Color32[] to, Vector2 toTexSize, Vector2 toPos, Vector2 size ) {
		for ( float y = 0; y < size.y; y++ ) {
			int sourceIndex = (int) ( (fromPos.y+y) * fromTexSize.x + fromPos.x );
			int destIndex = (int) ( (toPos.y+y) * toTexSize.x + toPos.x );
			try {
				System.Array.Copy( from, sourceIndex, to, destIndex, (int)size.x );
			} catch( System.ArgumentException e ) {
				throw new System.Exception("ArgEx. sourceIndex:"+sourceIndex+"["+from.Length+"], destIndex:"+destIndex
				                    +"["+to.Length+"], size:"+(int)size.x+", "+e.ToString() ); }
		}		
	}
	
}
