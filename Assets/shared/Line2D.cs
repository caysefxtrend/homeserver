using UnityEngine;
using System.Collections;

public class Line2D
{
	/**
     * Returns an indicator of where the specified point
     * {@code (px,py)} lies with respect to the line segment from
     * {@code (x1,y1)} to {@code (x2,y2)}.
     * The return value can be either 1, -1, or 0 and indicates
     * in which direction the specified line must pivot around its
     * first end point, {@code (x1,y1)}, in order to point at the
     * specified point {@code (px,py)}.
     * <p>A return value of 1 indicates that the line segment must
     * turn in the direction that takes the positive X axis towards
     * the negative Y axis.  In the default coordinate system used by
     * Java 2D, this direction is counterclockwise.
     * <p>A return value of -1 indicates that the line segment must
     * turn in the direction that takes the positive X axis towards
     * the positive Y axis.  In the default coordinate system, this
     * direction is clockwise.
     * <p>A return value of 0 indicates that the point lies
     * exactly on the line segment.  Note that an indicator value
     * of 0 is rare and not useful for determining colinearity
     * because of floating point rounding issues.
     * <p>If the point is colinear with the line segment, but
     * not between the end points, then the value will be -1 if the point
     * lies "beyond {@code (x1,y1)}" or 1 if the point lies
     * "beyond {@code (x2,y2)}".
     *
     * @param x1 the X coordinate of the start point of the
     *           specified line segment
     * @param y1 the Y coordinate of the start point of the
     *           specified line segment
     * @param x2 the X coordinate of the end point of the
     *           specified line segment
     * @param y2 the Y coordinate of the end point of the
     *           specified line segment
     * @param px the X coordinate of the specified point to be
     *           compared with the specified line segment
     * @param py the Y coordinate of the specified point to be
     *           compared with the specified line segment
     * @return an integer that indicates the position of the third specified
     *                  coordinates with respect to the line segment formed
     *                  by the first two specified coordinates.
     * @since 1.2
     */
	public static int relativeCCW (double x1, double y1,
	                              double x2, double y2,
	                              double px, double py)
	{
		x2 -= x1;
		y2 -= y1;
		px -= x1;
		py -= y1;
		double ccw = px * y2 - py * x2;
		if (ccw == 0.0) {
			// The point is colinear, classify based on which side of
			// the segment the point falls on.  We can calculate a
			// relative value using the projection of px,py onto the
			// segment - a negative value indicates the point projects
			// outside of the segment in the direction of the particular
			// endpoint used as the origin for the projection.
			ccw = px * x2 + py * y2;
			if (ccw > 0.0) {
				// Reverse the projection to be relative to the original x2,y2
				// x2 and y2 are simply negated.
				// px and py need to have (x2 - x1) or (y2 - y1) subtracted
				//    from them (based on the original values)
				// Since we really want to get a positive answer when the
				//    point is "beyond (x2,y2)", then we want to calculate
				//    the inverse anyway - thus we leave x2 & y2 negated.
				px -= x2;
				py -= y2;
				ccw = px * x2 + py * y2;
				if (ccw < 0.0) {
					ccw = 0.0;
				}
			}
		}
		return (ccw < 0.0) ? -1 : ((ccw > 0.0) ? 1 : 0);
	}

	public static bool linesIntersect (double var0, double var2, double var4, double var6, double var8, double var10, double var12, double var14)
	{
		return relativeCCW (var0, var2, var4, var6, var8, var10) * relativeCCW (var0, var2, var4, var6, var12, var14) <= 0 && relativeCCW (var8, var10, var12, var14, var0, var2) * relativeCCW (var8, var10, var12, var14, var4, var6) <= 0;
	}

	public static bool linesIntersect (Vector2 t0, Vector2 t1, Vector2 t2, Vector2 t3)
	{
		return relativeCCW (t0.x, t0.y, t1.x, t1.y, t2.x, t2.y) * relativeCCW (t0.x, t0.y, t1.x, t1.y, t3.x, t3.y) <= 0 && relativeCCW (t2.x, t2.y, t3.x, t3.y, t0.x, t1.y) * relativeCCW (t2.x, t2.y, t3.x, t3.y, t1.x, t1.y) <= 0;
	}

	public static bool linesIntersect2 (Vector2 t0, Vector2 t1, Vector2 t2, Vector2 t3)
	{
		return linesIntersect2 (t0.x, t0.y, t1.x, t1.y, t2.x, t2.y, t3.x, t3.y);
	}

	/**
  * Test if the line segment (x1,y1)-&gt;(x2,y2) intersects the line segment 
   * (x3,y3)-&gt;(x4,y4).
   *
  * @param x1 the first x coordinate of the first segment
    * @param y1 the first y coordinate of the first segment 
   * @param x2 the second x coordinate of the first segment
   * @param y2 the second y coordinate of the first segment
    * @param x3 the first x coordinate of the second segment
    * @param y3 the first y coordinate of the second segment
    * @param x4 the second x coordinate of the second segment
   * @param y4 the second y coordinate of the second segment
   * @return true if the segments intersect
   */
	public static bool linesIntersect2 (double x1, double y1,
	                                                                                double x2, double y2,
	                                                                                 double x3, double y3,
	                                                                         double x4, double y4)
	{
		double a1, a2, a3, a4;
	 
		// deal with special cases
		if ((a1 = area2 (x1, y1, x2, y2, x3, y3)) == 0.0) {
			// check if p3 is between p1 and p2 OR
			// p4 is collinear also AND either between p1 and p2 OR at opposite ends
			if (between (x1, y1, x2, y2, x3, y3)) {
				return true;
			} else {
				if (area2 (x1, y1, x2, y2, x4, y4) == 0.0) {
					return between (x3, y3, x4, y4, x1, y1) 
						|| between (x3, y3, x4, y4, x2, y2);
				} else {
					return false;
				}
			}
		} else if ((a2 = area2 (x1, y1, x2, y2, x4, y4)) == 0.0) {
			// check if p4 is between p1 and p2 (we already know p3 is not
			// collinear)
			return between (x1, y1, x2, y2, x4, y4);
		}
		  
		if ((a3 = area2 (x3, y3, x4, y4, x1, y1)) == 0.0) {
			// check if p1 is between p3 and p4 OR
			// p2 is collinear also AND either between p1 and p2 OR at opposite ends
			if (between (x3, y3, x4, y4, x1, y1)) {
				return true;
			} else {
				if (area2 (x3, y3, x4, y4, x2, y2) == 0.0) {
					return between (x1, y1, x2, y2, x3, y3) 
						|| between (x1, y1, x2, y2, x4, y4);
				} else {
					return false;
				}
			}
		} else if ((a4 = area2 (x3, y3, x4, y4, x2, y2)) == 0.0) {
			// check if p2 is between p3 and p4 (we already know p1 is not
			// collinear)
			return between (x3, y3, x4, y4, x2, y2);
		} else {  // test for regular intersection
			return ((a1 > 0.0) ^ (a2 > 0.0)) && ((a3 > 0.0) ^ (a4 > 0.0));
		} 
	}

	/**
  * Returns <code>true</code> if (x3, y3) lies between (x1, y1) and (x2, y2),
    * and false otherwise,  This test assumes that the three points are 
   * collinear, and is used for intersection testing.
   * 
   * @param x1  the x-coordinate of the first point.
   * @param y1  the y-coordinate of the first point.
   * @param x2  the x-coordinate of the second point.
   * @param y2  the y-coordinate of the second point.
    * @param x3  the x-coordinate of the third point.
    * @param y3  the y-coordinate of the third point.
    * 
* @return A boolean.
  */
	private static bool between (double x1, double y1, 
	                                                               double x2, double y2, 
	                                                                  double x3, double y3)
	{
		if (x1 != x2) {
			return (x1 <= x3 && x3 <= x2) || (x1 >= x3 && x3 >= x2);   
		} else {
			return (y1 <= y3 && y3 <= y2) || (y1 >= y3 && y3 >= y2);   
		}
	}


	/**
   * Computes twice the (signed) area of the triangle defined by the three
   * points.  This method is used for intersection testing.
   * 
   * @param x1  the x-coordinate of the first point.
    * @param y1  the y-coordinate of the first point.
    * @param x2  the x-coordinate of the second point.
   * @param y2  the y-coordinate of the second point.
   * @param x3  the x-coordinate of the third point.
    * @param y3  the y-coordinate of the third point.
    * 
    */
	private static double area2 (double x1, double y1,
		                                                             double x2, double y2,
		                                                          double x3, double y3)
	{
		return (x2 - x1) * (y3 - y1) - (x3 - x1) * (y2 - y1);    
	}

}

