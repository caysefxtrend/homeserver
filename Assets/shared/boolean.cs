﻿
public struct boolean {
	private readonly bool value;
	
	public boolean(bool value)
	{
		this.value = value;
	}
	
	public bool Value { get { return value; } }
	
	public static implicit operator boolean(bool s)
	{
		return new boolean(s);
	} 
	
	public static implicit operator bool(boolean p)
	{
		return p.Value;
	}

//	public static bool operator == (boolean a, bool b) {
//		return a.value == b;
//	}
//
//	public static bool operator != (boolean a, bool b) {
//		return a.value != b;
//	}
}
