using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class CameraUpdate : MonoBehaviour
{
	Camera cam;
	public bool oldWay = false;
	// Use this for initialization
	void Start ()
	{
		cam = GetComponent ("Camera") as Camera;
		cam.orthographicSize = Screen.height / 2;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (oldWay) {
			if ((cam.transform.position.x * 2 != Screen.width) || (cam.transform.position.y * 2 != -Screen.height)) {
				// DONT check orientation because orientation change first then width and height next time
				//|| ( scrOrientation != Screen.orientation ) ) {
				cam.orthographicSize = Screen.height / 2;
				cam.transform.position = new Vector3 (Screen.width / 2, Screen.height / 2, -10.0f);			
			}
		} else {
			if ((cam.transform.position.x * 2 != Screen.width) || (cam.transform.position.y * 2 != Screen.height)) {
				// DONT check orientation because orientation change first then width and height next time
				//|| ( scrOrientation != Screen.orientation ) ) {
				cam.orthographicSize = Screen.height / 2;
				//cam.transform.position = new Vector3 (Screen.width / 2, Screen.height / 2, 0);			
			}
		}
	}
}
