using UnityEngine;

[System.Serializable]
public class Vector2i {
	public int x, y;
	
	public override int GetHashCode() { return new Vector2(x,y).GetHashCode(); }
	
	public Vector2i (int x, int y) 	{ this.x = x; this.y = y; } 	
	public Vector2i (float x, float y) { this.x = (int)x; this.y = (int)y; }	
	public Vector2i (Vector2i v) { this.x = v.x; this.y = v.y; }
	public Vector2i (Vector2 v) { this.x = (int)v.x; this.y = (int)v.y; }	
	public Vector2i () { x = 0; y = 0; }
	
	public Vector2 toFloat { get { return new Vector2 (x, y ); } }
	public Vector2 ToFloat() { return new Vector2 (x, y ); }
	public Vector2i ToAbs() { x = x < 0 ? -x : x; y=y<0?-y:y; return this; }
	
	public boolean IsZero () { return 0 == x && 0 == y; }

	// IsEqual is very slow ... why ?
	public boolean IsEqual (Vector2i t) { return t.x == x && t.y == y; }

	public bool IsEqualBool (Vector2i t) { return t.x == x && t.y == y; }


	public boolean IsNotEqual (Vector2i t) { return !(t.x == x && t.y == y); }
	public boolean IsBiggerOrEqual ( Vector2i t) { return x >= t.x && y >= t.y; }
	public boolean IsLesserOrEqual (Vector2i t) { return x <= t.x && y <= t.y; }
	public Vector2i Subtract (Vector2i b) { return new Vector2i (x - b.x, y - b.y); }
	public Vector2i Set (Vector2i b) { x = b.x; y = b.y; return this; }
	static public Vector2i MaxValue() { return new Vector2i( int.MaxValue, int.MaxValue ); }

	public static Vector2i left = new Vector2i( -1, 0 );
	public static Vector2i right = new Vector2i( +1, 0 );
	public static Vector2i top = new Vector2i( 0, +1 );
	public static Vector2i bottom = new Vector2i( 0, -1 );
	public static Vector2i up = new Vector2i( 0, +1 );
	public static Vector2i down = new Vector2i( 0, -1 );

	public int Distance (Vector2i t) { 
		int xr = t.x-x; if ( xr < 0 ) xr = -xr;
		int yr = t.y-y; if ( yr < 0 ) yr = -yr;
		//return Mathf.Abs(t.x-x)+Mathf.Abs(t.y-y); }
		return xr+yr; }

	//	public Vector3i IndexOf( int i ) {
	//		int t = 0;
	//		for(int tx=0;tx<x;tx++)
	//		for(int ty=0;ty<y;ty++)
	//		for(int tz=0;tz<z;tz++) {
	//				if ( t == i ) return new Vector3i( x, y, z );
	//				t++;
	//			}
	//		throw new System.Exception();
	//	}
	//
	//	public int Count{ get { return x*y*z; } }
	public Vector2i normalized { get { return new Vector2i (x>0?1:x<0?-1:0, y>0?1:y<0?-1:0); } }

	public Vector2 vector2 { get { return new Vector2 (x, y); } }
	
	public override string ToString () { return "("+x + ", " + y + ")"; }
	
	public static Vector2i operator + (Vector2i a, Vector2i b)
	{
		return new Vector2i (a.x + b.x, a.y + b.y);
	}
	
	public static Vector2i operator - (Vector2i a, Vector2i b)
	{
		return new Vector2i (a.x - b.x, a.y - b.y );
	}

	public static Vector2i operator - (Vector2i a)
	{
		return new Vector2i (-a.x, -a.y );
	}
	
	public static Vector2i operator / (Vector2i a, Vector2i b) { return new Vector2i (a.x / b.x, a.y / b.y ); }
	public static Vector2i operator / (Vector2i a, int v) { return new Vector2i (a.x / v, a.y / v ); }

	
	public static Vector2i operator * (Vector2i a, Vector2i b)
	{
		return new Vector2i (a.x * b.x, a.y * b.y );
	}
	
	public static Vector2i operator * (Vector2i a, int b)
	{
		return new Vector2i (a.x * b, a.y * b);
	}
	
	public static Vector2i RoundFrom( Vector3 v ) {
		return new Vector2i(Mathf.RoundToInt( v.x ),Mathf.RoundToInt( v.y ) );
	}
	
	//	public boolean IsZero () { return 0 == x && 0 == y; }
	//	public boolean IsEqual (Vector2i t) { return t.x == x && t.y == y; }
	//	public boolean IsNotEqual (Vector2i t) { return !(t.x == x && t.y == y); }
	//	public boolean IsBiggerOrEqual ( Vector2i t) { return x >= t.x && y >= t.y; }
	//	public boolean IsLesserOrEqual (Vector2i t) { return x <= t.x && y <= t.y; }
	
	
	public override bool Equals (object t)
	{
		Vector2i tt = (Vector2i) t;
		//Debug.Log ("equals");
		if (tt.x == x && tt.y == y )
			return true;
		return false;
	}
	
	public static bool operator == (Vector2i a, Vector2i b)
	{
		if (a.x == b.x && a.y == b.y)
			return true;
		return false;
	}
	
	public static bool operator != (Vector2i a, Vector2i b)
	{
		if (a.x == b.x && a.y == b.y)
			return false;
		return true;
	}
	
	//	public static Vector3i From ( Vector3 v ) {
	//		return new Vector3i( v );
	//	}
	
	public static Vector2i zero { get { return new Vector2i (); } }
	public static Vector2i one { get { return new Vector2i (1,1); } }
	public static Vector2i minusOne { get { return new Vector2i (-1,-1); } }
	
	public Vector2i GetIntDirectionTo (Vector2i target)
	{
		Vector2i d = target - this;
		if (d.y > 1)
			d.y = 1;
		if (d.y < -1)
			d.y = -1;
		if (d.x > 1)
			d.x = 1;
		if (d.x < -1)
			d.x = -1;
		return d;
	}
	
}

