using System;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;

//[AddComponentMenu ("UI/MayhemButton", 30)]
//[DisallowMultipleComponent, ExecuteInEditMode, RequireComponent (typeof(RectTransform)), RequireComponent (typeof(CanvasRenderer))]
using System.Collections.Generic;

public class FullScreenTouchNewUI :  Graphic, IEventSystemHandler, IDragHandler, IPointerClickHandler, 
IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler, IScrollHandler  {

	List<TouchData> list = new List<TouchData>();
	//protected MayhemButton() : base() {}
	//EventSystem a;
//	bool ICanvasRaycastFilter.IsRaycastLocationValid (Vector2 screenPoint, Camera eventCamera) {
//		return true;
//	}
//	public enum Type { OnClick, OnMove, OnEnter, OnExit, OnUp, OnDown, OnScroll
//	}

	public class TouchData {
		public UITouchControl.TouchType type;
		public PointerEventData data;
		public TouchData( UITouchControl.TouchType type, PointerEventData data ) {
			this.type = type; this.data = data;
		}
		public override string ToString () {
			return type.ToString()+"["+data.pointerId+"] "+data.ToString();
		}
	}

	public TouchData PopLastData() {
		if ( list.Count == 0 ) return null;
		TouchData last = list[list.Count-1];
		list.RemoveAt( list.Count-1 );
		return last;
	}

//	public bool Raycast (Vector2 sp, Camera eventCamera)
//	{
//		return true;
//	}


	//bool ICanvasElement.IsDestroyed () { return false; }
	
	//void ICanvasElement.Rebuild (CanvasUpdate executing) {}

	private void Log(UITouchControl.TouchType type, PointerEventData eD ) {
		//Debug.Log( " " + type.ToString() + " ( "+eD.ToString()+" )" );
		list.Add( new TouchData(type,eD) );
	}
			
	void IPointerClickHandler.OnPointerClick (PointerEventData eventData) {
		Log( UITouchControl.TouchType.OnClick, eventData);
	}


	void IDragHandler.OnDrag (PointerEventData eventData) {
		Log(UITouchControl.TouchType.OnMove, eventData);
	}

	void IPointerDownHandler.OnPointerDown (PointerEventData eventData) {
		Log(UITouchControl.TouchType.OnDown, eventData);
	}

	void IPointerEnterHandler.OnPointerEnter (PointerEventData eventData) {
		Log(UITouchControl.TouchType.OnEnter, eventData);
	}
	
	void IPointerExitHandler.OnPointerExit (PointerEventData eventData) {
		Log(UITouchControl.TouchType.OnExit, eventData);
	}
	
	void IPointerUpHandler.OnPointerUp (PointerEventData eventData) {
		Log(UITouchControl.TouchType.OnUp, eventData);
	}

	void IScrollHandler.OnScroll(PointerEventData eventData) {
		Log(UITouchControl.TouchType.OnScroll, eventData);
	}
			
}