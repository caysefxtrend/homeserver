using UnityEngine;
using System.Collections;

public class PocketedArray<T> {
	//List<int> a = new List<int>();
	Array<T>[] array = new Array<T>[0];
	
	public int Length { set { SetLength(value); } }
	
	private void SetLength(int newLength) {
		if ( array.Length == newLength ) return;
		Array<T>[] arrayT = new Array<T>[newLength];
		System.Array.Copy(array,arrayT,newLength>array.Length?array.Length:newLength);
		//array.CopyTo(arrayT,0);
		array = arrayT;
		//if ( array.Length > newLength ) array.
	}

	public int Add(int address, T newObject ) {
		if ( array[address] == null ) array[address] = new Array<T>();
		return array[address].Add(newObject);
	}

	public Array<T> Get(int address) {
		return array[address];
	}
	
}

