using UnityEngine;
//using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Array<T>  {
	//List<int> a = new List<int>();
	//[UnityEngine.SerializeField] 
	T[] array = new T[0];
	int count = 0;
	int capacity = 0;

	public T[] GetArray() { return array; }

	public Array(int capacity) {
		SetCapacity( capacity );
	}

	public Array() {	}

	private void SetCapacity(int capacity) {
		//if ( capacity == 0 ) capacity == 1;
		if ( this.capacity == capacity ) return;
		T[] arrayT = new T[capacity];
		System.Array.Copy(array,arrayT,capacity>this.capacity?this.capacity:capacity);
		//array.CopyTo(arrayT,0);
		array = arrayT;
		this.capacity = capacity;
		//if ( array.Length > newLength ) array.
	}

	public int Add(T newCell) {
		if ( count == capacity ) SetCapacity( capacity == 0 ? 1 : capacity * 2 );
		//for( int i = 0; i < array.Length; i++) if (array[i] == null) { array[i] = newCell; return i; }
		//SetLength(array.Length+1);
		array[count] = newCell;
		count ++;
		return count;
	}

//	void SetCount(int newCount) {
//	}

	//public int Length { set { SetLength(value); } get { return array.Length; } }
	public int Count { get { return count; } } //set { SetCount(value); } }
	public int Capacity { get { return capacity; } }

	public void Clear() { count = 0; }

//	private void SetLength(int newLength) {
//		if ( array.Length == newLength ) return;
//		T[] arrayT = new T[newLength];
//		System.Array.Copy(array,arrayT,newLength>array.Length?array.Length:newLength);
//		//array.CopyTo(arrayT,0);
//		array = arrayT;
//		count = newLength;
//		//if ( array.Length > newLength ) array.
//	}

//	public int Add(T newCell) {
//		for( int i = 0; i < array.Length; i++) if (array[i] == null) { array[i] = newCell; return i; }
//		SetLength(array.Length+1);
//		array[array.Length-1] = newCell;
//		return array.Length-1;
//	}

	// TODO we can make it faster by Copy
	public void AddRange(Array<T> other) {
		for( int i = 0; i < other.Count; i++) Add(other.Get(i));
	}

	public T Get(int address) {
		return array[address];
	}

	public T this[int key] { get { return array[key]; } set { array[key] = value; } }

	public T Last { get { return array[Count-1]; } }

	public void Set(int address, T cell) {
		array[address] = cell;
	}

	public T[] GetArrayCopy() { 
		T[] arrayT = new T[count];
		System.Array.Copy(array,arrayT,count);
		return arrayT; 
	}

	public void RemoveAt( int address ) { 
		//int start = address == 0 ? 0 : address - 1;
		//if ( array.Length != count ) throw new System.IndexOutOfRangeException("Count="+count+", Length="+array.Length);
		for( int i = address+1; i < count; i++) 
		{
			//if ( i-1<0 || i>=count ) throw new System.IndexOutOfRangeException("Count="+count+", i="+i+", address="+address);
			array[i-1] = array[i];
		}
		//array[address] = null;
		count --;
		//SetLength( count-1 );
	}

//	public int IndexOf(T cell) {
//		for( int i = 0; i < array.Length; i++) if (array[i].GetHashCode() == cell.GetHashCode()) return i;
//		return -1;
//	}
}

