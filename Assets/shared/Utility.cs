using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Utility {

//	static public Component CreateGameObject (string type, Transform parent) {
//		GameObject go = new GameObject (type);
//		Component c = CreateComponent( type, go );
//		if ( parent != null ) 	{
//			c.transform.parent = parent;
//			c.transform.localPosition = new Vector3 (0, 0, 0);
//			c.transform.localEulerAngles = new Vector3 (0, 0, 0);
//			c.transform.localScale = new Vector3 (1, 1, 1);
//		}
//		return c;
//	}

//	static public Component CreateGameObject (string type, GameObject parent) {
//		return CreateGameObject( type, parent.transform );
//	}

//	static public Component CreateComponent (string type, GameObject parent) {
//		Component c = parent.AddComponent( type );
//		if (c == null) throw new System.Exception ("AddComponent "+type);
////		c.transform.localPosition = new Vector3 (0, 0, 0);
////		c.transform.localEulerAngles = new Vector3 (0, 0, 0);
////		c.transform.localScale = new Vector3 (1, 1, 1);
//		return c;
//	}

	public static Vector2 ScreenToWorld(Camera cam, Vector2 mousePos) {
		Ray ray = cam.ScreenPointToRay (mousePos);
		Vector3 v = ray.origin - ray.direction * (ray.origin.z / ray.direction.z);
		v.y = -v.y;
		return (Vector2)v;
	}

//	public static Vector2 ScreenToWorld(Camera cam, Vector2 mousePos, float depth) {
//		// Screenspace is defined in pixels. The bottom-left of the screen is (0,0); the right-top is (pixelWidth,pixelHeight). 
//		Ray ray = cam.ScreenPointToRay (new Vector3(mousePos.x,mousePos.y,depth) );
//		Vector3 v = ray.origin - ray.direction * (ray.origin.z / ray.direction.z);
//		v.y = -v.y;
//		return (Vector2)v;
//	}

	public static string FormatTire( this int number )
	{
		if ( number == 0 )
			return "-";
		else
		if ( number > 0 )
			return "+" + number;
		else
			return number.ToString();
	}

//	public static int RandomRange( int start, int end ) {
//		return ( int ) System.Math.Round((float)Random.Range((start,end), System.MidpointRounding.AwayFromZero);
//	}
	
	/// <summary>
	/// return index { 0, 1, 2 } if error return 0
	/// </summary>
	public static int GetIndexRandom ( float p0, float p1 ) {
		if ( !float.Equals(p0+p1, 1f) ) Debug.LogError("Sum indexes != 1");
		return GetIndexRandom ( p0, p1, 9f, 9f, 9f, 9f, 9f, 9f, 9f, 9f );
	}
	public static int GetIndexRandom ( float p0, float p1, float p2 ) {
		if ( !float.Equals( p0+p1+p2, 1f ) ) {
			float sum = p0+p1+p2;
			Debug.LogError("Sum indexes("+sum.ToString()+") != ("+(1f).ToString()+")");
		}
		return GetIndexRandom ( p0, p1, p2, 9f, 9f, 9f, 9f, 9f, 9f, 9f );
	}
	public static int GetIndexRandom ( float p0, float p1, float p2, float p3 ) {
		if (  !float.Equals( p0+p1+p2+p3, 1f) ) Debug.LogError("Sum indexes != 1");
		return GetIndexRandom ( p0, p1, p2, p3, 9f, 9f, 9f, 9f, 9f, 9f );
	}
	public static int GetIndexRandom ( float p1, float p2, float p3, float p4, float p5 ) {
		if (  !float.Equals( p1+p2+p3+p4+p5, 1f ) ) Debug.LogError("Sum indexes != 1");
		return GetIndexRandom ( p1, p2, p3, p4, p5, 9f, 9f, 9f, 9f, 9f );
	}
	public static int GetIndexRandom ( float p1, float p2, float p3, float p4, float p5, float p6, float p7 ) {
		if (  !float.Equals( p1+p2+p3+p4+p5+p6+p7, 1f ) ) Debug.LogError("Sum indexes != 1");
		return GetIndexRandom ( p1, p2, p3, p4, p5, p6, p7, 9f, 9f, 9f );
	}
	public static int GetIndexRandom ( float p0, float p1, float p2, float p3, float p4, float p5, float p6, float p7, float p8, float p9 ) {
		float rnd = Random.value;
		// include 0.0f to 1.0f
		int index = 0;
		if ( rnd < p0 )
			index = 0;
		else if ( rnd < ( p0 + p1 ) )
			index = 1;
		else if ( rnd < ( p0 + p1 + p2 ) )
			index = 2;
		else if ( rnd < ( p0 + p1 + p2 + p3 ) )
			index = 3;
		else if ( rnd < ( p0 + p1 + p2 + p3 + p4 ) )
			index = 4;
		else if ( rnd < ( p0 + p1 + p2 + p3 + p4 + p5 ) )
			index = 5;
		else if ( rnd < ( p0 + p1 + p2 + p3 + p4 + p5 + p6 ) )
			index = 6;
		else if ( rnd < ( p0 + p1 + p2 + p3 + p4 + p5 + p6 + p7 ) )
			index = 7;
		else if ( rnd < ( p0 + p1 + p2 + p3 + p4 + p5 + p6 + p7 + p8 ) )
			index = 8;
		else if ( rnd < ( p0 + p1 + p2 + p3 + p4 + p5 + p6 + p7 + p8 + p9 ) )
			index = 9;
//		else
		//	Debug.Error ( "GetIndexRandom error sum = " + ( p1 + p2 + p3 + p4 + p5 + p6 + p7 + p7 + p8 + p9 ) );
		return index;
	}
	
//	private static Hashtable ReorderHashtable( Hashtable table)
//	{
//		return table;
//		Hashtable newTable = new Hashtable();
//		string[] array = new string[table.Count];
//		//System.Array ar= new Object[10];
//		table.Keys.CopyTo(array,0);
//		for(int i = 0; i<array.Length; i++)
//			Log.Add("A:"+array[i]);
//		foreach ( System.Collections.DictionaryEntry item in table ) {
//			Log.Add ( "Reorder key=" + item.Key + " value=" + item.Value );
//			newTable.Add(item.Key,item.Value);
//		}
//		return newTable;
//	}


	static public void OptimizePosition ( ref Vector2 pos, float width, float height ) {
		//Log.Add ( "Screen(" + Screen.width + ", " + Screen.height + ") Pos(" + pos.x + ", " + pos.y + ") size(" + width + ", " + height + ")" );
		Vector2 center = new Vector2 (Screen.width / 2f, Screen.height / 2f);
		if ( pos.x > center.x )
			pos.x -= width;
		float newY = center.y - height / 2f;
		if ( pos.y > newY + height )
			pos.y = pos.y - height;
		else
		if ( pos.y < newY ) {}
			//pos.y = pos.y;
		else
			pos.y = newY;
	}
	
//	public delegate object FormatTextMetod (string T_String );
//	static public string FormatText( FormatTextMetod format, string text ) {
//		string newText = "";
//		int newTextIndex = 0;
//		bool codeBegan = false;
//		string code = "";
//		
//		for ( var i = 0; i < text.Length; i++ ) {
//			if ( codeBegan ) {
//				if ( text [ i ] == '`' ) {
//					codeBegan = false;
//					string formatText = format( code ).ToString();
//					newText += formatText;
//					newTextIndex += formatText.Length;
//				} else 
//					code = code + text [ i ];				
//				continue;
//			} else
//			if ( text [ i ] == '`' ) {
//				codeBegan = true;
//				code = "";
//				continue;
//			} 
//			newText += text [ i ].ToString ();
//			newTextIndex ++;
//		}		
//		return newText;
//	}

	// int max Exclusive
	static public int RandomRange( int min, int max ) {
		return Random.Range( min, max );
	}
	static public int RandomRangeInclusive( int min, int max ) {
		return Random.Range( min, max+1 );
	}
	static public float RandomRange( float min, float max ) {
		return Random.Range( min, max );
	}
	/// <summary>
	/// Return random float value [0...1].
	/// </summary>
	/// <returns>The random value from 0 to 1 inclusive</returns>
	static public float RandomValue() {
		return Random.value;
	}

	static public bool IntersectCircleLine( Vector2 center, float radius, Vector2 p1, Vector2 p2)
	{
		float x01=p1.x-center.x;
		float y01=p1.y-center.y;
		float x02=p2.x-center.x;
		float y02=p2.y-center.y;
		
		float dx=x02-x01;
		float dy=y02-y01;
		
		float a=dx*dx+dy*dy;
		float b=2.0f*(x01*dx+y01*dy);
		float c=x01*x01+y01*y01-radius*radius;
		
		if(-b<0)return (c<0);
		if(-b<(2.0f*a))return (4.0f*a*c-b*b<0);
		return (a+b+c<0);
	}

	static public bool IntersectLineLine( Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4)
	{
		float t = ((p3.x-p1.x)*(p2.y-p1.y) - (p3.y-p1.y)*(p2.x-p1.x)) *			           
		            ((p4.x-p1.x)*(p2.y-p1.y) - (p4.y-p1.y)*(p2.x-p1.x));
		if ( t > 0 ) return false;
		t = ((p1.x-p3.x)*(p4.y-p3.y) - (p1.y-p3.y)*(p4.x-p3.x)) *				 
			((p2.x-p3.x)*(p4.y-p3.y) - (p2.y-p3.y)*(p4.x-p3.x));
		if ( t > 0 ) return false;
		return true;
	}

//	static public Dictionary<String, int> MapFromSFSObject( Sfs2X.Entities.Data.ISFSObject sfso ) {
//		Dictionary<String, int> map = new Dictionary<String, int>();
//		foreach( string key in sfso.GetKeys() ) 
//			map.Add( key, sfso.GetInt( key ) );
//		return map;
//	}

	static public object StringToEnum( string enumString, System.Type enumType  ) {
		//starMapType = Utility.StringToEnum(sfso.GetUtfString( "starMapType" ), typeof( StarMapTypes ) );
		return  System.Enum.Parse( enumType, enumString );
	}

	static string[] byteToString = null;
	public static string SetColor( string text, Color32 color ) {
		if ( byteToString == null ) {
			const string alpha = "0123456789ABCDEF";
			byteToString = new string[256];
			for( int i = 0; i < 256; i++ ) {				
				int j = i % 16;
				int k = (i - j ) /16;
				//Debug.Log("i="+i+", j="+j+", hex="+alpha[j]+", k="+k+", hexH="+alpha[k]+" all="+(alpha[k]+alpha[j]));
				byteToString[i] = alpha[k].ToString() + (string)alpha[j].ToString();
				//Debug.Log("i="+i+", hex="+byteToString[i]);
			}
		}
		//string r = byteToString[color.r];
		// color.r = 0 to 1 == 0 to 255 = 00 to FF
		//Debug.Log( "color="+color+", res="+color.ToHex() );
		//Debug.Log( "!color=#"+byteToString[color.r]+byteToString[color.g]+byteToString[color.b]+"!"+text+"!/color!" );
		return "<color=#"+byteToString[color.r]+byteToString[color.g]+byteToString[color.b]+">"+text+"</color>";
	}

//	static public void SavePacket( string name, Packet packet ) {
//		#if UNITY_EDITOR || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN
//		//String path = Application.dataPath+"/Resources/JSON/"+name+".bin";
//		String path = Application.dataPath+"/"+name+".bin";
//		//Debug.Log ("Utility.Save: "+path);
//		System.IO.File.WriteAllBytes(path, packet.ToBinary().Bytes );
//		#endif
//	}

//	static public Packet LoadPacket( string name ) {
//		bool ready = false;
//		#if UNITY_EDITOR || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN
//		ready = true;
//		#endif
//		if ( !ready ) return null;
//		//String path = Application.dataPath+"/Resources/JSON/"+name+".bin";
//		String path = Application.dataPath+"/"+name+".bin";
//		if ( !System.IO.File.Exists( path ) ) return null;
//		//Debug.Log ("Utility.Save: "+path);
//		//byte[] bytes = System.IO.File.ReadAllBytes(path);
//		//return (Packet)Packet.NewFromBinaryData( new Sfs2X.Util.ByteArray( bytes) );
//		return new Packet( System.IO.File.ReadAllBytes(path) );
//	}

	static public int DegreeGetRotationStep( float angleFrom, float angleTo ) {
		if ( angleFrom < 0 ) angleFrom += 360;
		if ( angleTo < 0 ) angleTo += 360;
		if ( angleFrom >=360 ) angleFrom -= 360;
		if ( angleTo >=360 ) angleTo -= 360;
//		if ( angleFrom < 0 || angleFrom >= 360 || angleTo < 0 || angleTo >= 360 ) {
//			Debug.LogError("angleFrom:"+angleFrom+", angleTo:"+angleTo); return 0; 
//		}
		if ( float.Equals( angleFrom, angleTo ) ) return 0;
		if ( angleFrom < angleTo ) {
			if ( angleFrom + 180 < angleTo ) return -1;
			else return +1;
		} else { // angleFrom > angleTo
			if ( angleFrom - 180 < angleTo ) return -1;
			else return +1;
		}
	}

	static public float VectorToDegree( Vector2 vector ) {
		return Mathf.Atan2 (vector.y, vector.x)  * Mathf.Rad2Deg-90;
	}

	static public Vector2 DegreeToVector( float degree ) {
		return new Vector2 (Mathf.Cos ((degree + 90) * Mathf.Deg2Rad), Mathf.Sin ((degree + 90) * Mathf.Deg2Rad));
	}

	  static public float DegreeDiff( float angleFrom, float angleTo ) {
		if ( angleFrom < 0 ) angleFrom += 360;
		if ( angleTo < 0 ) angleTo += 360;
		if ( angleFrom >=360 ) angleFrom -= 360;
		if ( angleTo >=360 ) angleTo -= 360;
		if ( float.Equals( angleFrom, angleTo ) ) return 0;
		if ( angleFrom < angleTo ) {
			if ( angleFrom + 180 < angleTo ) return 360+angleFrom-angleTo;
			else return angleTo-angleFrom;
		} else { // angleFrom > angleTo
			if ( angleFrom - 180 < angleTo ) return angleFrom-angleTo;
			else return 360-angleFrom+angleTo;
		}
	}
	
	public static void TangentSolver(Mesh theMesh)	{
		int vertexCount = theMesh.vertexCount;
		Vector3[] vertices = theMesh.vertices;
		Vector3[] normals = theMesh.normals;
		Vector2[] texcoords = theMesh.uv;
		int[] triangles = theMesh.triangles;
		int triangleCount = triangles.Length / 3;
		Vector4[] tangents = new Vector4[vertexCount];
		Vector3[] tan1 = new Vector3[vertexCount];
		Vector3[] tan2 = new Vector3[vertexCount];
		int tri = 0;
		for (int i = 0; i < (triangleCount); i++)
		{
			int i1 = triangles[tri];
			int i2 = triangles[tri + 1];
			int i3 = triangles[tri + 2];
			
			Vector3 v1 = vertices[i1];
			Vector3 v2 = vertices[i2];
			Vector3 v3 = vertices[i3];
			
			Vector2 w1 = texcoords[i1];
			Vector2 w2 = texcoords[i2];
			Vector2 w3 = texcoords[i3];
			
			float x1 = v2.x - v1.x;
			float x2 = v3.x - v1.x;
			float y1 = v2.y - v1.y;
			float y2 = v3.y - v1.y;
			float z1 = v2.z - v1.z;
			float z2 = v3.z - v1.z;
			
			float s1 = w2.x - w1.x;
			float s2 = w3.x - w1.x;
			float t1 = w2.y - w1.y;
			float t2 = w3.y - w1.y;
			
			float r = 1.0f / (s1 * t2 - s2 * t1);
			Vector3 sdir = new Vector3((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r, (t2 * z1 - t1 * z2) * r);
			Vector3 tdir = new Vector3((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r, (s1 * z2 - s2 * z1) * r);
			
			tan1[i1] += sdir;
			tan1[i2] += sdir;
			tan1[i3] += sdir;
			
			tan2[i1] += tdir;
			tan2[i2] += tdir;
			tan2[i3] += tdir;
			
			tri += 3;
		}
		
		for (int i = 0; i < (vertexCount); i++)
		{
			Vector3 n = normals[i];
			Vector3 t = tan1[i];
			
			// Gram-Schmidt orthogonalize
			Vector3.OrthoNormalize(ref n, ref t);
			
			tangents[i].x = t.x;
			tangents[i].y = t.y;
			tangents[i].z = t.z;
			
			// Calculate handedness
			tangents[i].w = (Vector3.Dot(Vector3.Cross(n, t), tan2[i]) < 0.0) ? -1.0f : 1.0f;
		}
		theMesh.tangents = tangents;
	}

	static public int IntegerMax( int t1, int t2, int t3, int t4, int t5 ) {
		t1 = t1 > t2 ? t1 : t2; 		
		t1 = t1 > t3 ? t1 : t3;
		t1 = t1 > t4 ? t1 : t4;
		t1 = t1 > t5 ? t1 : t5;
		return t1;
	}

	public static string GetPath(this Transform current) {
		if (current.parent == null)
			return "/" + current.name;
		return current.parent.GetPath() + "/" + current.name;
	}

//	static public String LoadAsString( String filename ) {
//		TextAsset asset = ( TextAsset ) Resources.Load ( filename, typeof( TextAsset ) );
//		if ( asset == null )
//			throw new System.Exception( "Could not find asset file in Resources folder: " + filename );		
//		string text = asset.text;
//		if ( text == null )
//			throw new System.Exception( "Loaded asset text is null for file: " + filename );
//		return text;
//	}
	
//	static public KeyValue<String,object> LoadJsonKeyValue( string filename ) {
//		string jsonString = Utility.LoadAsString( filename );
//		if ( jsonString.Length == 0 )
//			throw new Exception( "Json '"+filename+"', is zero size" );
//		KeyValue<String,object> decodedHash = JsonKeyValue.jsonDecode( jsonString );
//		if ( decodedHash == null )
//			throw new Exception( "Decode json fail for resource '"+filename+"', index[" + JsonKeyValue.getLastErrorIndex () + "], string \n" + JsonKeyValue.getLastErrorSnippet () );
//		return decodedHash;
//	}

	static public Transform FindTransform(Transform parent, string name) {	
		if ( parent == null ) throw new System.Exception("parent is null");
		if (parent.name == name) return parent;
		var transforms = parent.GetComponentsInChildren<Transform>();
		foreach (Transform t in transforms) if (t.name == name) return t;
		throw new System.Exception("cant find ("+name+") in transform("+parent.name+")");
		//return null;
	}
}