﻿using UnityEngine;
using System.Collections;

public class UIUnitySprite : UIBaseSprite {

	public static UIUnitySprite CreateCentered( string spriteName, float depth ) {
		GameObject gO = new GameObject( "Sprite "+spriteName );
		UIUnitySprite sprite = gO.AddComponent<UIUnitySprite>();
		//gO.AddComponent(sprite);
		if ( sprite == null ) throw new System.Exception(spriteName);
		sprite.Initialize( spriteName, depth, true, Vector2.zero );
		return sprite;
	}

	public static UIUnitySprite CreateComponentCentered( GameObject gO, string spriteName, float depth ) {
		UIUnitySprite sprite = gO.AddComponent<UIUnitySprite>();
		if ( sprite == null ) throw new System.Exception(spriteName);
		sprite.Initialize( spriteName, depth, true, Vector2.zero );
		return sprite;
	}

	public static UIUnitySprite CreateComponent( GameObject gO, string spriteName, float depth ) {
		UIUnitySprite sprite = gO.AddComponent<UIUnitySprite>();
		if ( sprite == null ) throw new System.Exception(spriteName);
		sprite.Initialize( spriteName, depth, false, Vector2.zero );
		return sprite;
	}

	public static UIUnitySprite Create( string spriteName, float depth ) {
		GameObject gO = new GameObject( "Sprite "+spriteName );
		UIUnitySprite sprite = gO.AddComponent<UIUnitySprite>();
		//gO.AddComponent(sprite);
		if ( sprite == null ) throw new System.Exception(spriteName);
		sprite.Initialize( spriteName, depth, false, Vector2.zero );
		return sprite;
	}

	public static UIUnitySprite Create( string spriteName, float depth, Vector2 size ) {
		GameObject gO = new GameObject( "Sprite "+spriteName );
		UIUnitySprite sprite = gO.AddComponent<UIUnitySprite>();
		//gO.AddComponent(sprite);
		if ( sprite == null ) throw new System.Exception(spriteName);
		sprite.Initialize( spriteName, depth, false, size );
		return sprite;
	}

	public SpriteRenderer spriteRenderer;
	public Sprite sprite;
	public Vector2 spriteSize;
	UISpriteAtlas atlas; 

	override public float unscaledWidth { get { return spriteSize.x; } }
	override public float unscaledHeight { get { return spriteSize.y; } }

	override public float width { get { 
			//return spriteSize.x;
			if ( parent == null ) return spriteSize.x * transform.localScale.x;
			else return spriteSize.x * transform.localScale.x; 
		} set {
			Vector3 newScale = transform.localScale;
			newScale.x = value / spriteSize.x * 1;
			newScale.y = newScale.x;
			transform.localScale = newScale;
		} }
	override public float height { get { 
			//return spriteSize.y;
			if ( parent == null ) return spriteSize.y * transform.localScale.y; 
			return spriteSize.y * transform.localScale.y; 
		} set {
			Vector3 newScale = transform.localScale;
			newScale.y = value / spriteSize.y * 1;
			newScale.x = newScale.y;
			transform.localScale = newScale;
		} }

	public UIUnitySprite Scale( float scale ) { Scale ( scale, scale ); return this; }
	public void Scale( float scaleX, float scaleY ) {
		Vector3 scale = transform.localScale;
		scale.x = scaleX;
		scale.y = scaleY;
		transform.localScale = scale;
	}

	override public bool isPressed { 
		set { 
			pressed = value;
		} 
		get { return pressed; } 
	}
	
//	public void SetSize( float w, float h ) {
//		//if ( parent ) throw new System.NotImplementedException("parent");
//		float sizeX = Screen.width * w / spriteSize.x;
//		float sizeY = Screen.height * h / spriteSize.y;
//		if ( sizeX > sizeY ) sizeX = sizeY;
//		else sizeY = sizeX;
//		Scale ( sizeX, sizeY );
//	}

	public void SetHeightAsPortionOfScreen( float in_height ) {
		//if ( parent ) throw new System.NotImplementedException("parent");
		float scaleHeight = Screen.height * in_height / spriteSize.y;
		float maxScaleX = Screen.width / spriteSize.x;
		scaleHeight = scaleHeight > maxScaleX ? maxScaleX : scaleHeight;
		Scale ( scaleHeight, scaleHeight );
	}

	private void Initialize( string spriteName, float depth, bool centered, Vector2 size ) {
		this.centered = centered;
	//	UITouchControl.onScreenSize += RefreshPosition;

		this.depth = depth;
		atlas = UISpriteAtlas.GetSpriteAtlasForSprite( spriteName );
		if ( atlas == null ) throw new System.Exception("atlas is null for sprite:"+spriteName);
		name = spriteName;
		
		transform.gameObject.layer = atlas.gameObject.layer;
		if ( transform.parent == null ) transform.parent = atlas.transform;
//		transform.parent = atlas.defaultCamera.transform;
		transform.localPosition = new Vector3(0,0,depth);
		//transform.localScale = new Vector3( 100, 100, 1);
		
		sprite = atlas.GetSprite(spriteName, centered, size);
		
		spriteRenderer = transform.gameObject.AddComponent<SpriteRenderer>();
		if ( spriteRenderer == null ) throw new System.Exception("AddComponent<SpriteRenderer>");
		Hide ();
		spriteRenderer.sprite = sprite;
		spriteRenderer.material = atlas.material;
		spriteSize = new Vector2( sprite.rect.width, sprite.rect.height );
	}

	public UIUnitySprite SetSubSpriteRandom( float totalSubSprites ) {
		Rect rect = sprite.rect;
		float random = Random.Range(0,(int)totalSubSprites);
		rect.width /= totalSubSprites;
		rect.x += rect.width * random; 
		Vector2 pivot = centered ? new Vector2 (0.5f, 0.5f) : Vector2.zero;
		float pixelsToUnits = 1;
		uint extrude = 0u;
		SpriteMeshType meshType = SpriteMeshType.FullRect;
		Sprite subSprite = Sprite.Create ( sprite.texture, rect, pivot, pixelsToUnits, extrude, meshType);
		spriteRenderer.sprite = subSprite;
		//Debug.LogWarning("baseRect:"+sprite.rect+", subRect:"+rect+", random:"+random+" from "+totalSubSprites );
		return this;
	}

	public UIUnitySprite SetSubSpriteByRect( Rect subRect ) {
		Rect rect = sprite.rect;
//		Debug.LogWarning("SetSubSpriteByRect subRect("+subRect+") sprite rect("+rect+")");
		rect.x += subRect.x;
		rect.y += subRect.y;
		rect.width = subRect.width;
		rect.height = subRect.height;
		Vector2 pivot = centered ? new Vector2 (0.5f, 0.5f) : Vector2.zero;
		float pixelsToUnits = 1;
		uint extrude = 0u;
		SpriteMeshType meshType = SpriteMeshType.FullRect;
		Sprite subSprite = Sprite.Create ( sprite.texture, rect, pivot, pixelsToUnits, extrude, meshType);
		spriteRenderer.sprite = subSprite;
		return this;
	}

	//public Rect atlas { get { return UISpriteAtlas.GetSpriteAtlasForSprite( sprite.name ); } }
	public Rect texturePercentRect { 
		get 
		{ 
			Rect tRect = sprite.rect;
			//Debug.LogWarning("sprite name = "+sprite.name);
			//UISpriteAtlas atlas = UISpriteAtlas.GetSpriteAtlasForSprite( sprite.name );
			if ( atlas.pack ) {
				// sample: tRect = 16,16, 10,10 && atlas W=100,H=100
				tRect.xMin /= atlas.atlas.width;
				tRect.yMin /= atlas.atlas.height;
				tRect.xMax /= atlas.atlas.width;
				tRect.yMax /= atlas.atlas.height;
			}
			else {
				tRect = new Rect( 0, 0, 1, 1);
			}
			return tRect; 
		} 
	}

	static public void Destroy(UIUnitySprite sprite) {
		sprite.Destroy();
	}

	new public UIUnitySprite Destroy () {
		base.Destroy();
		GameObject.Destroy( transform.gameObject );
		return null;
	}	

	public UIUnitySprite DestroyAsComponent () {
		base.Destroy();
		Destroy( spriteRenderer );
		Destroy( (Component)this );
// 	UIUnitySprite sprite = gO.AddComponent<UIUnitySprite>();
		return null;
	}	

	public Material material { get { return spriteRenderer.material; } set { spriteRenderer.material = value; } }
	
//	static public void Hide(UIUnitySprite sprite) { sprite.Hide (); }
//	static public void Show( UIUnitySprite sprite) { sprite.Show (); }

	public Color color { get { return spriteRenderer.color; } set {spriteRenderer.color = value;} }

	//bool isUpdate = false;
	[SerializeField] float deltaAlpha = 0;
	public void ShowByAlpha() {
		//if ( !isUpdate ) { isUpdate = true; } //UITouchControl.instance.onUpdate += OnUpdate; }
		if (isHidden) { Color color = this.color; color.a = 0.5f; this.color = color; }
		deltaAlpha = 0.04f;
		Show();
	}

	static public void Show( UIUnitySprite sprite) { sprite.Show(); }
	static public void Hide( UIUnitySprite sprite) { sprite.Hide (); }

	new public void Show() {
		if ( !isHidden ) return;
		spriteRenderer.enabled = true;
		base.Show();
	}

	new public void Hide() {
		if ( isHidden ) return;
		spriteRenderer.enabled = false;
		base.Hide();
	}

	static public void ShowByAlpha( UIUnitySprite sprite) { sprite.ShowByAlpha (); }
	static public void HideByAlpha( UIUnitySprite sprite) { sprite.HideByAlpha (); }

	public void HideByAlpha() {
		if (isHidden) { Color color = this.color; color.a = 0; this.color = color; return; }
		//if ( !isUpdate ) { isUpdate = true; } //UITouchControl.instance.onUpdate += OnUpdate; }
		deltaAlpha = -0.04f;
	}

	public void HideByAlpha(float delta) {
		HideByAlpha();
		deltaAlpha = -delta;
	}

//	public bool isScrollX = false;
//	float scrollXSpeed = 0;
//	float scrollXStart = 0;
//	public bool isScrollY = false;
//	float scrollYSpeed = 0;
//	float scrollYStart = 0;
//
//	public void SetScrollX( bool set, float speed, float startX ) {
//		isScrollX = set;
//		scrollXSpeed = speed;
//		scrollXStart = startX;
//	}
//
//	public void SetScrollY( bool set, float speed, float startX ) {
//		isScrollY = set;
//		scrollYSpeed = speed;
//		scrollYStart = startX;
//	}

//	void FixedUpdate() {
//		if ( deltaAlpha != 0 ) {
//			Color color = this.color;
//			color.a += deltaAlpha;
//			if (color.a <= 0) { color.a = 0; if ( deltaAlpha > 0 ) throw new System.Exception("dA:"+deltaAlpha);
//				deltaAlpha = 0; Hide(); }
//			if (color.a >= 1) { color.a = 1; deltaAlpha = 0; }
//			this.color = color;
//			//Debug.LogWarning("dA:"+deltaAlpha+", color:"+color+", thisC:"+this.color);
//		}
//		if ( isScrollX )
//			material.SetFloat("TextureOffsetX", scrollXStart+Time.time * scrollXSpeed);
//		if ( isScrollY )
//			material.SetFloat("TextureOffsetY", scrollYStart+Time.time * scrollYSpeed);
//	}

	public void Rotate( float aX, float aY, float aZ ) {
		//Debug.Log ("Rotate by "+angle);
		transform.localEulerAngles = new Vector3( aX, aY, aZ );
		//transform.Rotate (new Vector3( 0, 0, angle ));
	}

}
