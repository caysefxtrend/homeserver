using UnityEngine;
using System.Collections;

[System.Serializable]
public class UIMeshSprite : MonoBehaviour
{
	//[SerializeField]
	//bool show = false;
	//[SerializeField]
	//float scale = 1f;
	[Header("Variables:")]
	public Material material;
	[Header("Unused:")]
	//[SerializeField]
	//Vector2 position = Vector2.zero;
	[SerializeField]	UISpriteAtlas atlas;
	[Header("Info:")]
	public int vecticesCount = 0;
	public int uvCount = 0;
	Vector2[] uv;
	MeshRenderer mr;
	MeshFilter mf;
	Mesh mesh;
	//Matrix4x4 transform;
//	Array<CombineInstance> ciArray = new Array<CombineInstance>();
	CombineInstance[] ci = new CombineInstance[0];


	void Awake() {
		mf = GetComponent<MeshFilter>();
		if ( mf == null ) mf = gameObject.AddComponent<MeshFilter>();
		mr = GetComponent<MeshRenderer>();
		if ( mr == null ) mr = gameObject.AddComponent<MeshRenderer>();
		mr.material = material;
		mesh = mf.mesh;
	}

//	public void Add(int x, int y, int w, int h) {
//		CombineInstance ci = new CombineInstance();
//	}

	public void PreTestUpdateMesh(int w, int h) {
		mesh = new Mesh();
		int max = w * h;
		ci = new CombineInstance[max];
		int i = 0;
		for(int x = 0; x < w; x++ ) {
			for(int y = 0; y < h; y++ ) {
				if ( i == 16383 ) break;
				ci[i] = new CombineInstance();
				ci[i].mesh = CreatePlaneMesh(x,y,w,h); // new Mesh(); //GetMesh( atlas.meshSprites[i] );
				Matrix4x4 m = Matrix4x4.identity; // atlas.meshSprites[i].transform;
				m.SetTRS( new Vector3( x, y, 0 ), Quaternion.identity, new Vector3(1,1,1));
				//ci[i].transform.SetTRS( new Vector3( x, y, 0 ), Quaternion.identity, new Vector3(1,1,1));
				ci[i].transform = m;//.SetTRS( new Vector3( x, y, 0 ), Quaternion.identity, new Vector3(1,1,1));
				//ci[i].transform.SetTRS( Vector3.zero, Quaternion.identity, Vector3.one );
				i++;

			}
		}
		mesh.CombineMeshes( ci, true, true );
		uv = mesh.uv;
		mf.mesh = mesh;
		uvCount = uv.Length;
		vecticesCount = mesh.vertexCount;
	}



	public void TestUpdateMesh() {
		mesh.CombineMeshes( ci );
	}

	public void TestUpdateMesh2(int count) {
		if ( count > uvCount ) count = uvCount;	
		for ( int i = 0; i < count; i++ ) {
			uv[i] = new Vector2( Random.Range(0.01f,0.99f), Random.Range(0.01f,0.99f) );
		}
		mesh.uv = uv;
		//mesh.CombineMeshes( ci );
	}

//
//	public static UIMeshSprite CreateCentered( string spriteName, float depth ) {
//		UIMeshSprite sprite = new UIMeshSprite();
//		sprite.Initialize( spriteName, depth );
//		return sprite;
//	}

//	public void Initialize(string spriteName, float depth) {
//		atlas = UISpriteAtlas.GetSpriteAtlasForSprite( spriteName );
//		//spriteNameInAtlas = spriteName;
//		Rect tRect = atlas.GetTextureRect( spriteName );
//		Texture2D texture = atlas.GetTextureFromAtlas(spriteName);
//		atlas.meshSprites.Add( this );
//		MeshRenderer mr = atlas.GetComponent<MeshRenderer>();
//		if ( mr == null ) {
//			mr = atlas.gameObject.AddComponent<MeshRenderer>();
//			mr.material = atlas.material;
//			mr.useLightProbes = false;
//			mr.reflectionProbeUsage = UnityEngine.Rendering.ReflectionProbeUsage.Off;
//			mr.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
//			mr.receiveShadows = false;
//		}
//		mf = atlas.GetComponent<MeshFilter>();
//		if ( mf == null ) mf = atlas.gameObject.AddComponent<MeshFilter>();
//		UpdateMeshes();
//	}

//	public UIMeshSprite Show() {
//		show = true;
//		UpdateMeshes();
//		return this;
//	}

//	public void UpdateMeshes() {
//		Mesh mesh = UpdateMesh();
//		mf.mesh = mesh;
//	}

//	public UIMeshSprite Scale(float scale) {
//		this.scale = scale;
//		return this;
//	}
//
//	public UIMeshSprite Position( Vector2 position ) {
//		this.position = position;
//		return this;
//	}

//	Mesh UpdateMesh() {
//		Mesh mesh = new Mesh();
//		int max = atlas.meshSprites.Count;
//		CombineInstance[] ci = new CombineInstance[max];
//		for(int i = 0; i < max; i++ ) {
//			//if ( !sprite.show ) continue;
//			ci[i] = new CombineInstance();
//			ci[i].mesh = GetMesh( atlas.meshSprites[i] );
//			ci[i].transform = atlas.meshSprites[i].transform;
//		}
//		mesh.CombineMeshes( ci );
//		return mesh;
//	}

//	Mesh GetMesh( UIMeshSprite sprite ) {
//		if ( sprite.mesh == null ) {
//			sprite.mesh = CreatePlaneMesh();
//			sprite.transform = Matrix4x4.identity;
//		}
//		return sprite.mesh;
//	}

	Mesh CreatePlaneMesh(int x, int y, int w, int h)
	{
		Mesh mesh = new Mesh();
		
		Vector3[] vertices = new Vector3[]
		{
			new Vector3( 1, 1, 0),
			new Vector3( 1, 0, 0),
			new Vector3( 0, 1, 0),
			new Vector3( 0, 0, 0),
		};
		
		Vector2[] uv = new Vector2[]
		{
			new Vector2(1, 1),
			new Vector2(1, 0),
			new Vector2(0, 1),
			new Vector2(0, 0),
		};
		
		int[] triangles = new int[]
		{
			0, 1, 2,
			2, 1, 3,
		};		
		
		Color[] colors = new Color[]
		{
			Color.white,
			Color.white,
			Color.white,
			Color.white
		};		
		mesh.vertices = vertices;
		mesh.uv = uv;
		mesh.triangles = triangles;
		mesh.colors = colors;
		//mesh.RecalculateNormals();
		//Utility.TangentSolver( mesh );		
		return mesh;
	}
}

