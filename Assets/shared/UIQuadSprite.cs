//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//
//public class UIQuadSprite : UIBaseSprite {
//	public static UIQuadSprite CreateCentered( string spriteName, float depth ) {
//		GameObject gO = new GameObject( "Sprite "+spriteName );
//		UIQuadSprite quad = gO.AddComponent<UIQuadSprite>();
//		//gO.AddComponent(sprite);
//		if ( quad == null ) throw new System.Exception(spriteName);
//		quad.Initialize( spriteName, depth, true, null );
//		return quad;
//	}
//
//	public static UIQuadSprite CreateCentered( string spriteName, float depth, Material material ) {
//		GameObject gO = new GameObject( "Sprite "+spriteName );
//		UIQuadSprite quad = gO.AddComponent<UIQuadSprite>();
//		//gO.AddComponent(sprite);
//		if ( quad == null ) throw new System.Exception(spriteName);
//		quad.Initialize( spriteName, depth, true, material );
//		return quad;
//	} 
//
//	public static UIQuadSprite CreateComponentCentered( GameObject gO, string spriteName, float depth ) {
//		//GameObject gO = new GameObject( "Sprite "+spriteName );
//		UIQuadSprite quad = gO.AddComponent<UIQuadSprite>();
//		//gO.AddComponent(sprite);
//		if ( quad == null ) throw new System.Exception(spriteName);
//		quad.Initialize( spriteName, depth, true, null );
//		return quad;
//	}
//
//	public static UIQuadSprite CreateComponentCentered( GameObject gO, string spriteName, float depth, Material material ) {
//		if ( gO == null ) throw new System.Exception("game object is null");
//		UIQuadSprite quad = gO.AddComponent<UIQuadSprite>();
//		if ( quad == null ) throw new System.Exception(spriteName);
//		quad.Initialize( spriteName, depth, true, material );
//		return quad;
//	}
//
//	public static UIQuadSprite CreateComponent( GameObject gO, string spriteName, float depth ) {
//		//GameObject gO = new GameObject( "Sprite "+spriteName );
//		UIQuadSprite quad = gO.AddComponent<UIQuadSprite>();
//		//gO.AddComponent(sprite);
//		if ( quad == null ) throw new System.Exception(spriteName);
//		quad.Initialize( spriteName, depth, false, null );
//		return quad;
//	}
//
//	public static UIQuadSprite CreateComponent( GameObject gO, string spriteName, float depth, Material material ) {
//		//GameObject gO = new GameObject( "Sprite "+spriteName );
//		UIQuadSprite quad = gO.AddComponent<UIQuadSprite>();
//		//gO.AddComponent(sprite);
//		if ( quad == null ) throw new System.Exception(spriteName);
//		quad.Initialize( spriteName, depth, false, material );
//		return quad;
//	}
//
//	public static UIQuadSprite Create( string spriteName, float depth ) {
//		GameObject gO = new GameObject( "Sprite "+spriteName );
//		UIQuadSprite quad = gO.AddComponent<UIQuadSprite>();
//		//gO.AddComponent(sprite);
//		if ( quad == null ) throw new System.Exception(spriteName);
//		quad.Initialize( spriteName, depth, false, null );
//		return quad;
//	}
//
//	public static UIQuadSprite Create( string spriteName, float depth, Material material ) {
//		GameObject gO = new GameObject( "Sprite "+spriteName );
//		UIQuadSprite quad = gO.AddComponent<UIQuadSprite>();
//		//gO.AddComponent(sprite);
//		if ( quad == null ) throw new System.Exception(spriteName);
//		quad.Initialize( spriteName, depth, false, material );
//		return quad;
//	}
//
//	UISpriteAtlas atlas; 
////	string spriteNameInAtlas;
//	[SerializeField] public Vector2 baseSize = Vector2.zero;
//	//[SerializeField] int baseHeight;
//	[SerializeField] Mesh quadMesh;
//	[SerializeField] Mesh baseMesh;
//	MeshFilter meshFilter;
//	MeshRenderer meshRender;
//	//[SerializeField] Vector3[] baseVertices = new Vector3[4];
//	//[SerializeField] Vector2[] baseUV = new Vector2[4];
//	[SerializeField] Rect uvRect;
//	[SerializeField] Rect uvRect2;
//	[SerializeField] Vector2 scaleVertices = Vector2.one;
//	[SerializeField] Vector2 tileSize = Vector2.zero;
//	[SerializeField] Vector2 offset = Vector2.zero;
//	[SerializeField] List<Vector2> trail = new List<Vector2>();
//	public List<Vector2> coords;
//	[SerializeField] bool isUseSecondTexture = false;
//	[SerializeField] Color quadColor = Color.white;
//	[SerializeField] string spriteName = "";
//	[SerializeField] Texture2D mainTexture;
//
//	public Color color { 
//		set { 
//			this.quadColor = value;
//			Color[] colors = (Color[])baseMesh.colors.Clone();
//			for ( int i = 0; i < colors.Length; i ++ ) {
//				colors[i] = value;
//			}
//			quadMesh.colors = colors;
//		}
//		get { return this.quadColor; }
//	}
//
//	//public Color color { get { return renderer..color; } set {spriteRenderer.color = value;} }
//
//	public void Initialize( string spriteName, float depth, bool centered, Material customMaterial ) {
//
//		this.centered = centered;
//		this.depth = depth;
//		this.spriteName = spriteName;
//		transform.eulerAngles = Vector3.zero;
//		transform.localScale = Vector3.one;
//		transform.localPosition = Vector3.zero;
//
//		if ( spriteName == "" ) {
//			if ( customMaterial == null ) throw new System.Exception("customMaterial is null");
//			meshFilter = transform.gameObject.AddComponent<MeshFilter>();
//			meshRender = transform.gameObject.AddComponent<MeshRenderer>();
//			meshRender.material = customMaterial;
//			transform.localPosition = new Vector3(0,0,depth);
//			quadMesh = new Mesh();
//			baseMesh = new Mesh();
//			meshFilter.mesh = quadMesh;
//			uvRect = new Rect(0,0,1,1);
//			return;
//		}
//
//		atlas = UISpriteAtlas.GetSpriteAtlasForSprite( spriteName );
//		//spriteNameInAtlas = spriteName;
//		Rect tRect = atlas.GetTextureRect( spriteName );
//		Texture2D texture = atlas.GetTextureFromAtlas(spriteName);
//		baseSize.x = tRect.width;
//		baseSize.y = tRect.height;
//		uvRect = tRect;
//		//if ( atlas.pack ) {
//		uvRect.x /= texture.width;
//		uvRect.y /= texture.height;
//		uvRect.width /= texture.width;
//		uvRect.height /= texture.height;
//	//	}
////		Debug.LogWarning("tRect: "+tRect+", uvRect:"+uvRect);
//		
//		transform.gameObject.layer = atlas.gameObject.layer;
//		if ( transform.parent == null ) 	transform.parent = atlas.transform;
//		transform.localPosition = new Vector3(0,0,depth);
//
//		quadMesh = new Mesh();
//		baseMesh = CreatePlaneMesh(); //
//		UpdateVerticesAndUV();
//		//UpdateMeshUV();
//		meshFilter = transform.gameObject.AddComponent<MeshFilter>();
//		if ( meshFilter == null ) throw new System.Exception("meshFilter is null");
//		meshFilter.mesh = quadMesh;
//		meshRender = transform.gameObject.AddComponent<MeshRenderer>();
//		if ( customMaterial == null ) {
//			Material material = atlas.material;
//			material.mainTexture = texture;
//			meshRender.material = material;
//		} else {
//			//Material material = atlas.materialForQuads;
//			//customMaterial.mainTexture = texture;
//			customMaterial.SetTexture( "_MainTex", texture );
//			//meshRender.sharedMaterial = customMaterial;
//			meshRender.material = customMaterial;
//		}
//		mainTexture = texture;
////		if ( atlas.pack ) material.mainTexture = atlas.atlas;
////		else material.mainTexture = atlas.GetSprite( spriteName, false, Vector2.zero ).texture;		
//		Hide ();
//	}
//
//	public UIQuadSprite SetSubSpriteRandom( float totalSubSprites ) {
//		//Texture2D texture = atlas.GetTextureFromAtlas(spriteName);
//		Texture2D texture = mainTexture;
//		Rect rect = uvRect;
//		rect.x *= texture.width;
//		rect.y *= texture.height;
//		rect.width *= texture.width;
//		rect.height *= texture.height;
//		float random = Random.Range(0,(int)totalSubSprites);
//		rect.width /= totalSubSprites;
//		rect.x += rect.width * random; 
//		Vector2 pivot = centered ? new Vector2 (0.5f, 0.5f) : Vector2.zero;
//		float pixelsToUnits = 1;
//		uint extrude = 0u;
//		SpriteMeshType meshType = SpriteMeshType.FullRect;
//		Sprite subSprite = Sprite.Create ( texture, rect, pivot, pixelsToUnits, extrude, meshType);
//		//spriteRenderer.sprite = subSprite;
//		//Debug.LogWarning("baseRect:"+uvRect+", subRect:"+rect+", random:"+random+" from "+totalSubSprites );
////		meshRender.material.mainTexture = subSprite.texture;
//		//customMaterial.SetTexture( "_MainTex", texture );
//		Material material = atlas.material;
//		material.mainTexture = subSprite.texture;
//		meshRender.material = material;
//		uvRect.width /= totalSubSprites;
//		uvRect.x += uvRect.width * random;
//		baseSize.x = rect.width;
//		baseSize.y = rect.height;
//		UpdateVerticesAndUV();
//		return this;
//	}
//
//	public void SetSecondSprite( string secondTextureName ) {
//		UISpriteAtlas atlas = UISpriteAtlas.GetSpriteAtlasForSprite( secondTextureName );
//		Texture2D secondTexture = atlas.GetTextureFromAtlas(secondTextureName);
//		meshRender.material.SetTexture( "_SecondTex", secondTexture );
//		Rect tRect = atlas.GetTextureRect( secondTextureName );
//		uvRect2 = tRect;
//		uvRect2.x /= secondTexture.width;
//		uvRect2.y /= secondTexture.height;
//		uvRect2.width /= secondTexture.width;
//		uvRect2.height /= secondTexture.height;
//		isUseSecondTexture = true;
//	}
//
//
//	new public UIUnitySprite Destroy () {
//		base.Destroy();
//		GameObject.Destroy( transform.gameObject );
//		return null;
//	}
//
//	public void Rotate( float aX, float aY, float aZ ) {
//		transform.localEulerAngles = new Vector3( aX, aY, aZ );
//	}
//
//	override public void RefreshChilds() {
//	}
//
//
//
////	public void SetColor( Color color ) {
////		this.color = color;
////		Color[] colors = (Color[])baseMesh.colors.Clone();
////		for ( int i = 0; i < colors.Length; i ++ ) {
////			colors[i] = color;
////		}
////		quadMesh.colors = colors;
////	}
////
////	public Color GetColor() {
////		return color;
////	}
//
//	public Vector2[] _uv;
//	private void UpdateMeshUV() {
//		Vector2[] uv = (Vector2[])baseMesh.uv.Clone();
//		for ( int i = 0; i < uv.Length; i ++ ) {
//			uv[i].x = uvRect.x+uvRect.width*uv[i].x;
//			uv[i].y = uvRect.y+uvRect.height*uv[i].y;
//		}
//		//Debug.LogWarning("uv.l:"+uv.Length+", v.l:"+baseMesh.vertices.Length+", qM.v.l:"+quadMesh.vertices.Length);
//		quadMesh.uv = uv;
//		this._uv = uv;
//		if ( isUseSecondTexture ) {
//			Vector2[] uv2 = (Vector2[])baseMesh.uv2.Clone();
//			for ( int i = 0; i < uv2.Length; i ++ ) {
//				uv2[i].x = uvRect2.x+uvRect2.width*uv2[i].x;
//				uv2[i].y = uvRect2.y+uvRect2.height*uv2[i].y;
//			}
//			quadMesh.uv2 = uv2;
//		}
//	}
//
//	public Vector2 center;
//	private void UpdateVerticesAndUV() {
//		Vector3[] vertices = (Vector3[])baseMesh.vertices.Clone();
//		float baseSizeX = 1, baseSizeY = 1;
//		baseSizeX = (float)baseSize.x; baseSizeY = (float)baseSize.y;
//		//float maxX = 0, maxY = 0;
//		center = Vector2.zero;
//		if ( centered ) {
//			for ( int i = 0; i < vertices.Length; i++ ) {
//				if ( vertices[i].x > center.x ) center.x = vertices[i].x;
//				if ( vertices[i].y > center.y ) center.y = vertices[i].y;
//			}
//			center /= 2f;
//		}
//		for ( int i = 0; i < vertices.Length; i++ ) {
//			vertices[i].Set( (vertices[i].x- center.x) * baseSizeX * scaleVertices.x , 
//			                (vertices[i].y- center.y) * baseSizeY * scaleVertices.y, 0 );
//		}
//		if ( vertices.Length < 3 ) Debug.LogError("vertices.Length:"+vertices.Length);
//		//quadMesh.Clear(); // make mesh uv go very wrong in big way
//	//	try {
//			quadMesh.vertices = vertices;
//		//} catch( Exception e ) { Debug.LogError(e.ToString() );
//		//}
//		quadMesh.triangles = (int[])baseMesh.triangles.Clone();
//		//Color32[] colors32s = (Color32[])baseMesh.colors32.Clone();
//		quadMesh.colors32 = baseMesh.colors32;
//		//meshFilter.mesh = quadMesh;
//		UpdateMeshUV();
//	}
//
//	Mesh CreatePlaneMesh()
//	{
//		Mesh mesh = new Mesh();
//		
//		Vector3[] vertices = new Vector3[]
//		{
//			new Vector3( 1, 1, 0),
//			new Vector3( 1, 0, 0),
//			new Vector3( 0, 1, 0),
//			new Vector3( 0, 0, 0),
//		};
//		
//		Vector2[] uv = new Vector2[]
//		{
//			new Vector2(1, 1),
//			new Vector2(1, 0),
//			new Vector2(0, 1),
//			new Vector2(0, 0),
//		};
//		
//		int[] triangles = new int[]
//		{
//			0, 1, 2,
//			2, 1, 3,
//		};		
//
//		Color[] colors = new Color[]
//		{
//			Color.white,
//			Color.white,
//			Color.white,
//			Color.white
//		};		
//		mesh.vertices = vertices;
//		mesh.uv = uv;
//		mesh.triangles = triangles;
//		mesh.colors = colors;
//		mesh.RecalculateNormals();
//		Utility.TangentSolver( mesh );
//
//		return mesh;
//	}
//
//	private void ScaleSize( float x, float y ) {
//		scaleVertices = new Vector2( x, y );
//		UpdateVerticesAndUV();
//	}
//
//	public void AddTrail( Vector2 trailScreenPosition ) {
//		trail.Add( trailScreenPosition );
//		UpdateTrail();
//	}
//
//	public List<int> trianglesList = new List<int> ();
//	public List<Vector3> verticesList = new List<Vector3> ();
//	public List<Vector2> uvList = new List<Vector2> ();    
//	public List<Color32> fourColor32 = new List<Color32>();
//	public List<Vector2> normalsList = new List<Vector2> ();
//
//	public List<float> widths;
//	public List<float> radiuses;
//	public List<float> radiuses0;
//	public List<float> radiuses1;
//
//	public void CreateSpecialMesh_ByRadiuses(List<float> radiuses, List<Color32> fourColor32, float percent) {
////		float sP0 = 0, sP1 = 0.5f, sP2 = 0.75f, sP3 = 1f;
//	//	float sP0 = 1-percent, sP1 = 1-percent*0.5f, 
//		float sP2 = 1-percent*0.25f, sP3 = 1f;
//		this.radiuses = radiuses;
//		this.fourColor32 = fourColor32;
//		List<int> trianglesList = new List<int> ();
//		List<Vector3> verticesList = new List<Vector3> ();
//		List<Vector2> uvList = new List<Vector2> ();    
//		List<Color32> color32List = new List<Color32> ();  
//		if ( fourColor32.Count == 0 ) {
//			fourColor32.Add( new Color32(0,0,0,0) );
//			fourColor32.Add( new Color32(64,64,64,0) );
//			fourColor32.Add( new Color32(255,255,255,0) );
//			fourColor32.Add( new Color32(32,32,32,0) );
//		}
//		/* start calculation */
////		Vector2[] points = coords.ToArray();
////		List<Vector3> vertexes = new List<Vector3>();
//	//	Vector2 lastPerpendicular = Vector2.zero;
////		float step = 360f / radiuses.Count;
////		float angle = 0;
//		float max =  radiuses.Count;
//		int index;
//	//	Vector2 vx1, vx2;
//		float texCount = 0;
//		float texCountAdd = 8f*1f/(max+1);
//		float xAdd = -4f*1f/(max+1), yAdd = -4f*1f/(max+1), xStart = 0, yStart = 1;
//		normalsList.Clear();
//		for (int c = 0; c < radiuses.Count+1; c++ ) {
//			Vector2 normal = new Vector2( xStart, yStart ).normalized;
//			normalsList.Add( normal );
//			xStart += xAdd; if ( xStart > 1 || xStart < -1 ) { xStart = xStart - 2*xAdd; xAdd =- xAdd; }
//			yStart += yAdd; if ( yStart > 1 || yStart < -1 ) { yStart = yStart - 2*yAdd; yAdd =- yAdd; }
//			if ( c == radiuses.Count ) index = 0;  else index = c;
//			//verticesList.Add( Vector3.zero ); // 0, 0
//			verticesList.Add( (Vector3)normal * radiuses[index] * sP2 );
//			verticesList.Add( (Vector3)normal * radiuses[index] * sP2 );
//			verticesList.Add( (Vector3)normal * radiuses[index] * sP2 );
//			verticesList.Add( (Vector3)normal * radiuses[index] * sP3 );
//
//			uvList.Add( new Vector2(texCount,0f) ); 
//			uvList.Add( new Vector2(texCount,0.3f) );
//			uvList.Add( new Vector2(texCount,0.55f) ); 
//			uvList.Add( new Vector2(texCount, 1f) );
////			uvList.Add( new Vector2(0f, texCount) ); 
////			uvList.Add( new Vector2(0.3f, texCount) );
////			uvList.Add( new Vector2(0.55f, texCount) ); 
////			uvList.Add( new Vector2(1f, texCount) );
//			texCount += texCountAdd;
//			if ( texCount > 1 || texCount < 0 ) {
//				texCount = texCount - 2*texCountAdd;
//				texCountAdd =- texCountAdd;
//			}
//			if ( c != 0 ) {	
//				// 0451
//				trianglesList.Add ( 0 +(c-1)*4 );
//				trianglesList.Add ( 4 +(c-1)*4 );
//				trianglesList.Add ( 5 +(c-1)*4 );
//				trianglesList.Add ( 0 +(c-1)*4 );
//				trianglesList.Add ( 5 +(c-1)*4 );
//				trianglesList.Add ( 1 +(c-1)*4 );
//				// 1562
//				trianglesList.Add ( 1 +(c-1)*4 );
//				trianglesList.Add ( 5 +(c-1)*4 );
//				trianglesList.Add ( 6 +(c-1)*4 );
//				trianglesList.Add ( 1 +(c-1)*4 );
//				trianglesList.Add ( 6 +(c-1)*4 );
//				trianglesList.Add ( 2 +(c-1)*4 );
//				// 2673
//				trianglesList.Add ( 2 +(c-1)*4 );
//				trianglesList.Add ( 6 +(c-1)*4 );
//				trianglesList.Add ( 7 +(c-1)*4 );
//				trianglesList.Add ( 2 +(c-1)*4 );
//				trianglesList.Add ( 7 +(c-1)*4 );
//				trianglesList.Add ( 3 +(c-1)*4 );
//			}
////			color32List.Add( new Color32(0,0,0,0) );
//			color32List.Add( fourColor32[0] );
//			color32List.Add( fourColor32[1] );
//			color32List.Add( fourColor32[2] );
//			color32List.Add( fourColor32[3] );
////			color32List.Add( new Color32(0,0,0,0) );
//			//			color32List.Add( new Color32(0,0,0,0) );
//		}
////		for (int c = 0; c < radiuses.Count+1; c++ ) {
////			Vector2 normal = new Vector2( xStart, yStart ).normalized;
////			xStart += xAdd;
////			if ( xStart > 1 || xStart < -1 ) { xStart = xStart - 2*xAdd; xAdd =- xAdd; }
////			yStart += yAdd;
////			if ( yStart > 1 || yStart < -1 ) { yStart = yStart - 2*yAdd; yAdd =- yAdd; }
////			if ( c == radiuses.Count ) {
////				vx1 = normal * ( radiuses[0] - widths[0] );
////				vx2 = normal * ( radiuses[0] + widths[0] );
////			}
////			else {
////				vx1 = normal * ( radiuses[c] - widths[c] );
////				vx2 = normal * ( radiuses[c] + widths[c] );
////			}
////			verticesList.Add( (Vector3)vx1 ); // 0, 0
////			verticesList.Add( (Vector3)vx2 ); // 1, 0
////			uvList.Add( new Vector2(0, texCount) ); uvList.Add( new Vector2(1,texCount) );
////			texCount += texCountAdd;
////			if ( texCount > 1 || texCount < 0 ) {
////				texCount = texCount - 2*texCountAdd;
////				texCountAdd =- texCountAdd;
////			}
////			if ( c != 0 ) {	
////				trianglesList.Add ( 0 +(c-1)*2 );
////				trianglesList.Add ( 2 +(c-1)*2 );
////				trianglesList.Add ( 1 +(c-1)*2 );
////				trianglesList.Add ( 1 +(c-1)*2 );
////				trianglesList.Add ( 2 +(c-1)*2 );
////				trianglesList.Add ( 3 +(c-1)*2 );
////			}
////			color32List.Add( new Color32(0,0,0,0) );
////			color32List.Add( new Color32(255,255,255,128) );
//////			color32List.Add( new Color32(0,0,0,0) );
////		}
//		/* write changes */
//		baseMesh.Clear();
//		baseMesh.vertices = verticesList.ToArray();
//		baseMesh.triangles = trianglesList.ToArray();
//		baseMesh.uv = uvList.ToArray();
//		baseMesh.colors32 = color32List.ToArray();
//		UpdateVerticesAndUV();
//		if ( quadMesh.vertexCount != color32List.Count ) Debug.LogError("vertexCount:"+quadMesh.vertexCount+"!="+color32List.Count);
//		//quadMesh.colors32 = color32List.ToArray();
//		quadMesh.RecalculateNormals();
//		//Utility.TangentSolver( quadMesh );
//		//UpdateMeshUV();
//		this.trianglesList = trianglesList;
//		this.uvList = uvList;
//		this.verticesList = verticesList;
//	}
//
//	public void UpdateByRadiusesUpdate() {
//		if ( radiuses0 == null || radiuses1 == null || radiuses0.Count != radiuses1.Count ) throw new System.Exception("radiuses0&1");
//	//	float sP0 = 1-percent, sP1 = 1-percent*0.5f, sP2 = 1-percent*0.25f, sP3 = 1f;
//		//List<Vector3> verticesList = new List<Vector3> ();
//		/* start calculation */
//		float max = radiuses0.Count, radius, delta;
//	//	int index;
//	//	float xAdd = -4f*1f/(max+1), yAdd = -4f*1f/(max+1), xStart = 0, yStart = 1;
//		Vector3[] vertices = new Vector3[(radiuses.Count+1)*4];
//		for (int c = 0, i=0, index; c < radiuses0.Count+1; c++ ) {
////			Vector2 normal = new Vector2( xStart, yStart ).normalized;
////			xStart += xAdd; if ( xStart > 1 || xStart < -1 ) { xStart = xStart - 2*xAdd; xAdd =- xAdd; }
////			yStart += yAdd; if ( yStart > 1 || yStart < -1 ) { yStart = yStart - 2*yAdd; yAdd =- yAdd; }
//			Vector2 normal = normalsList[c];
//			if ( c == radiuses0.Count ) radius = radiuses[0];  else radius = radiuses[c];
//			if ( c == radiuses0.Count ) index = 0;  else index = c;
//			//vertices[i] = Vector3.zero; i++;
//			vertices[i] = (Vector3)normal * radiuses0[index] ; i++;
//			 delta = ( radiuses1[index] - radiuses0[index] ) / 2f;
//			vertices[i] = (Vector3)normal * (radiuses0[index]+delta); i++;
//			delta = ( radiuses1[index] - radiuses0[index] ) / 4f*3f;
//			vertices[i] = (Vector3)normal * (radiuses0[index]+delta); i++;
//			vertices[i] = (Vector3)normal * radiuses1[index]; i++;
////			verticesList.Add( Vector3.zero ); // 0, 0
////			verticesList.Add( (Vector3)normal * radius / 2f );
////			verticesList.Add( (Vector3)normal * radius / 4f * 3f );
////			verticesList.Add( (Vector3)normal * radius );
//		}
//		/* write changes */
//		quadMesh.vertices = vertices; //verticesList.ToArray();
//		//verticesList.Clear();
//	}
//
//	public void CreateSpecialMesh_NegatedDamage() {
//		List<int> trianglesList = new List<int> ();
//		List<Vector3> verticesList = new List<Vector3> ();
//		List<Vector2> uvList = new List<Vector2> ();    
//		List<Color32> color32List = new List<Color32> ();  
//		// create
//		float quantity = 6;
//		float center = (quantity-1f)/ 2f;
//		for ( int i =0; i < (int)quantity; i++ ) {
//			float x = ((float)i-center) / center / 2f; // -1:+1  
//			verticesList.Add( new Vector3( x, 0, 0) ); // 0
//			verticesList.Add( new Vector3( 8f*x,1,0) ); // 1
//			float xUV = x + 0.5f;
//			uvList.Add( new Vector2( xUV, 0f ) ); // 0
//			uvList.Add( new Vector2( xUV, 1f ) ); // 1
//			if ( i > 0 ) {
//				trianglesList.Add ( 0+(i-1)*2 ); // clockwise
//				trianglesList.Add ( 1+(i-1)*2 );
//				trianglesList.Add ( 2+(i-1)*2 );
//				trianglesList.Add ( 2+(i-1)*2 );
//				trianglesList.Add ( 1+(i-1)*2 );
//				trianglesList.Add ( 3+(i-1)*2 );
//			}
//			color32List.Add( new Color32(255,255,255,255) ); // 0
//			color32List.Add( new Color32(0,0,0,32) ); // 1
//		}
//		// update base and quad
//		baseMesh.Clear();
//		baseMesh.vertices = verticesList.ToArray();
//		baseMesh.triangles = trianglesList.ToArray();
//		baseMesh.uv = uvList.ToArray();
//		baseMesh.colors32 = color32List.ToArray();
//		//baseMesh.uv2 = uvList.ToArray();
//		UpdateVerticesAndUV();
//		//quadMesh.uv2 = baseMesh.uv2;
////		if ( quadMesh.vertexCount != color32List.Count ) Debug.LogError("vertexCount:"+quadMesh.vertexCount+"!="+color32List.Count);
//		quadMesh.RecalculateNormals();
//		Utility.TangentSolver( quadMesh );
//		this.trianglesList = trianglesList;
//		this.uvList = uvList;
//		this.verticesList = verticesList;
//	}
//
//	public void UpdateByCoords() {
//		List<int> trianglesList = new List<int> ();
//		List<Vector3> verticesList = new List<Vector3> ();
//		List<Vector2> uvList = new List<Vector2> ();    
//		/* start calucaltion */
//		Vector2[] points = coords.ToArray();
//		List<Vector3> vertexes = new List<Vector3>();
//		Vector2 lastPerpendicular = Vector2.zero;
//
//		for (int i = 0; i < points.Length; i++) {
//			Vector2 p1, p2, perpendicular;
//			if ( i == points.Length -1 ) {
//				p1 = new Vector2 ( points [i].y, points [i-1].x );
//				p2 = new Vector2 ( points [i-1].y, points [i].x );
//			}
//			else {
//				p1 = new Vector2 ( points [i + 1].y, points [i].x );
//				p2 = new Vector2 ( points [i].y, points [i + 1].x );
//			}
//			perpendicular = (p1 - p2).normalized;
//			//	Vector2 v2 = points [i + 1];
//			perpendicular *= widths[i];
//			Vector2 vx1;
//			Vector2 vx2;
//			Vector2 v1 = points [i];
//			//vx1 = v1 - perpendicular;
//			//vx2 = v1 + perpendicular;
//	//		if ( lastPerpendicular != Vector2.zero ) {
//		//		vx1 = v1 - lastPerpendicular;
//		//		vx2 = v1 + lastPerpendicular;
//	//		}
//	//		else {
//				vx1 = v1 - perpendicular;
//				vx2 = v1 + perpendicular;
//	//		}
//		//	perpendicular = (p1 - p2).normalized;
//	//		perpendicular *= widths[i+1];
//		//	Vector2 vx3 = v2 + perpendicular;
//		//	Vector2 vx4 = v2 - perpendicular;
//		//	lastPerpendicular = perpendicular;
//			verticesList.Add( (Vector3)vx1 ); // 0, 0
//			verticesList.Add( (Vector3)vx2 ); // 1, 0
//		//	verticesList.Add( (Vector3)vx3 ); // 0, 1
//		//	verticesList.Add( (Vector3)vx4 ); // 0, 0 
//			uvList.Add( new Vector2(0, (float)i/(float)points.Length) );
//			uvList.Add( new Vector2(1, (float)i/(float)points.Length) );
////			uvList.Add( new Vector2(1, 1) );
////			uvList.Add( new Vector2(1, 1) );
//			if ( i != 0 ) {	
//				trianglesList.Add ( 0 +(i-1)*2 );
//				trianglesList.Add ( 1 +(i-1)*2 );
//				trianglesList.Add ( 2 +(i-1)*2 );
//				trianglesList.Add ( 2 +(i-1)*2 );
//				trianglesList.Add ( 1 +(i-1)*2 );
//				trianglesList.Add ( 3 +(i-1)*2 );
//			}
//		}
////		for (int i = 0; i < points.Length-1; i++) {
////			Vector2 p1 = new Vector2 ( points [i + 1].y, points [i].x );
////			Vector2 p2 = new Vector2 ( points [i].y, points [i + 1].x );
////			Vector2 v1 = points [i];
////			Vector2 v2 = points [i + 1];
////			Vector2 perpendicular = (p1 - p2).normalized;
////			perpendicular *= widths[i];
////			Vector2 vx1;
////			Vector2 vx2;
////			//vx1 = v1 - perpendicular;
////			//vx2 = v1 + perpendicular;
////			if ( lastPerpendicular != Vector2.zero ) {
////				vx1 = v1 - lastPerpendicular;
////				vx2 = v1 + lastPerpendicular;
////			}
////			else {
////				vx1 = v1 - perpendicular;
////				vx2 = v1 + perpendicular;
////			}
////			perpendicular = (p1 - p2).normalized;
////			perpendicular *= widths[i+1];
////			Vector2 vx3 = v2 + perpendicular;
////			Vector2 vx4 = v2 - perpendicular;
////			lastPerpendicular = perpendicular;
////			verticesList.Add( (Vector3)vx1 ); // 1, 1
////			verticesList.Add( (Vector3)vx2 ); // 1, 0
////			verticesList.Add( (Vector3)vx3 ); // 0, 1
////			verticesList.Add( (Vector3)vx4 ); // 0, 0 
////			uvList.Add( new Vector2(0, 0) );
////			uvList.Add( new Vector2(1, 0) );
////			uvList.Add( new Vector2(1, 1) );
////			uvList.Add( new Vector2(0, 1) );
////			trianglesList.Add ( 0 +i*4 );
////			trianglesList.Add ( 3 +i*4 );
////			trianglesList.Add ( 1 +i*4 );
////			trianglesList.Add ( 1 +i*4 );
////			trianglesList.Add ( 3 +i*4 );
////			trianglesList.Add ( 2 +i*4 );
////		}
//		/* write changes */
//		baseMesh.Clear();
//		baseMesh.vertices = verticesList.ToArray();
//		baseMesh.triangles = trianglesList.ToArray();
//		baseMesh.uv = uvList.ToArray();
//		UpdateVerticesAndUV();
//		//UpdateMeshUV();
//		this.trianglesList = trianglesList;
//		this.uvList = uvList;
//		this.verticesList = verticesList;
//	}
//
//	private void UpdateTrail() {
//		List<int> trianglesList = new List<int> ();
//		List<Vector3> verticesList = new List<Vector3> ();
//		List<Vector2> uvList = new List<Vector2> ();    
//
//		Vector2 numberOfTiles = new Vector2( 1, trail.Count );
//		Vector2 last = Vector2.zero;
//		numberOfTiles+= new Vector2( offset.x == 0 ? 0 : 1, offset.y == 0 ? 0 : 1 );
//		baseMesh.Clear();
//		for( int x = 0; x < numberOfTiles.x; x++ )
//		for ( int y = 0; y < numberOfTiles.y; y++ ) {
//			Mesh mesh = CreatePlaneMesh();
//			/* vertices */
//			foreach (Vector3 item  in mesh.vertices) {   
//				item.Set( item.x+x, item.y+y, 0);
//				verticesList.Add (item);
//			}
//			/* triangles */
//			foreach (int item  in mesh.triangles) {   
//				trianglesList.Add (item+4*y + x*4*(int)numberOfTiles.y );
//			}
//			/* uv-s */			
//			foreach (Vector2 item  in mesh.uv) {   
//				uvList.Add (item);
//			}
//		}
////		new Vector3( 1, 1, 0),
////		new Vector3( 1, 0, 0),
////		new Vector3( 0, 1, 0),
////		new Vector3( 0, 0, 0),
//		Vector2 delta = Vector2.zero;
//		for ( int i = 0; i < trail.Count; i++ ) {
//			if ( i > 0 ) delta = (trail[i] - trail[i-1])/1f;//new Vector2(2,2);
//			int index = 4 * i;
//			int prevIndex = 4 * (i-1);
//			Vector3 v11 = verticesList[index];
//			Vector3 v10 = verticesList[index+1];
//			Vector3 v01 = verticesList[index+2];
//			Vector3 v00 = verticesList[index+3];
//			if ( i > 0 ) {
//				v00 = verticesList[ prevIndex+2 ];
//				v10 = verticesList[ prevIndex ];
//				v11 = v10 + (Vector3)delta;
//				v01 = v00 + (Vector3)delta;
//			}
//			verticesList[index] = v11;
//			verticesList[index+1] = v10;
//			verticesList[index+2] = v01;
//			verticesList[index+3] = v00;
//		}
//
//		baseMesh.vertices = verticesList.ToArray();
//		baseMesh.triangles = trianglesList.ToArray();
//		baseMesh.uv = uvList.ToArray();
//		UpdateVerticesAndUV();
//		//UpdateMeshUV();
//	}
//
//	[SerializeField] Vector2 lastXY = Vector2.zero;
//	/// <summary>
//	/// Sets the size auto tiled. Number of tiles is y/spriteBaseHeight.
//	/// </summary>
//	/// <param name="x">The width of sprite - IGNORED.</param>
//	/// <param name="y">Needed length of sprite.</param>
//	public void SetSizeAutoTiled( float x, float y ) {
//		if ( lastXY.x == x && lastXY.y == y ) return;
//		lastXY.Set(x,y);
//		x = baseSize.x;
//		if ( y <= 0 ) throw new System.Exception("y:"+y);
//		if ( baseSize.x == 0 || baseSize.y == 0 ) throw new System.Exception("baseSize:"+baseSize);
//		Vector2 tileSize = new Vector2( Mathf.Round ( x / baseSize.x ), Mathf.Round ( y / baseSize.y ) );
//		if ( tileSize.x == 0 ) tileSize.x = 1;
//		if ( tileSize.y == 0 ) tileSize.y = 1;
//		if ( this.tileSize == Vector2.zero ) ConstructTiles( tileSize );
//		Vector2 size = new Vector2( tileSize.x * baseSize.x, tileSize.y * baseSize.y);
//		if ( size.x == 0 || size.y == 0 ) throw new System.Exception(":"+size);
//		Scale( 1, y / size.y );
//	}
//
//	private void ConstructTiles(Vector2 numberOfTiles) {
//		tileSize = numberOfTiles;
//		List<int> trianglesList = new List<int> ();
//		List<Vector3> verticesList = new List<Vector3> ();
//		List<Vector2> uvList = new List<Vector2> ();   
//		List<Color32> color32List = new List<Color32> ();  
//		baseMesh.Clear();
//		for( int x = 0; x < numberOfTiles.x; x++ )
//		for ( int y = 0; y < numberOfTiles.y; y++ ) {
//			Mesh mesh = CreatePlaneMesh();
//			/* vertices */
//			foreach (Vector3 item  in mesh.vertices) {   
//				item.Set( item.x+x, item.y+y, 0);
//				verticesList.Add (item);
//			}
//			/* triangles */
//			foreach (int item  in mesh.triangles) {   
//				trianglesList.Add (item+4*y + x*4*(int)numberOfTiles.y );
//			}
//			/* uv-s */			
//			foreach (Vector2 item  in mesh.uv) {   
//				uvList.Add (item);
//			}
//			for ( int c = 0; c< 4;c++ )
//				//color32List.Add( new Color32( 0,0,0,0 ) );
//				color32List.Add( new Color32( 255,255,255,255 ) );
//		}
//		verticesList.Add ( new Vector2( 0.5f, numberOfTiles.y+1-0.75f) );
//		trianglesList.Add (0+4*(int)(numberOfTiles.y-1)  );
//		trianglesList.Add (2+4*(int)(numberOfTiles.y-1)  );
//		trianglesList.Add (4+4*(int)(numberOfTiles.y -1) );
//		uvList.Add ( new Vector2(0.5f, 1) );
//		color32List.Add( new Color32( 0,0,0,0 ) );
//
//		baseMesh.vertices = verticesList.ToArray();
//		baseMesh.triangles = trianglesList.ToArray();
//		baseMesh.uv = uvList.ToArray();
//		baseMesh.colors32 = color32List.ToArray();
//		if (isUseSecondTexture) baseMesh.uv2 = uvList.ToArray();
//		UpdateVerticesAndUV();
//	}
//	
//	public void SetOffset( float x, float y ) {
//		float shift = 0.5f;
//		offset = new Vector2( x-(float)(int)x, y-(float)(int)y ) * shift;
//		Vector2[] uvs = baseMesh.uv;
//		for ( int i = 0; i < uvs.Length; i++ ) {
//			uvs[i].y = shift + uvs[i].y * shift - offset.y;
//		//	if ( uvs[i].y > 0.975f ) uvs[i].y = 0.975f;
//			//if ( uvs[i].y < 0.025f ) uvs[i].y = 0.025f;
//			uvs[i].x = uvRect.x+uvRect.width*uvs[i].x;
//			uvs[i].y = uvRect.y+uvRect.height*uvs[i].y;
//		}
//		quadMesh.uv = uvs;
//		if ( isUseSecondTexture ) {
//			Vector2[] uv2 = baseMesh.uv2;
//			for ( int i = 0; i < uv2.Length; i++ ) {
//				uv2[i].y = shift + uv2[i].y * shift - offset.y;
//				//	if ( uvs[i].y > 0.975f ) uvs[i].y = 0.975f;
//				//if ( uvs[i].y < 0.025f ) uvs[i].y = 0.025f;
//				uv2[i].x = uvRect2.x+uvRect2.width*uv2[i].x;
//				uv2[i].y = uvRect2.y+uvRect2.height*uv2[i].y;
//			}
//			quadMesh.uv2 = uv2;
//		}
//	}
//
//	new public void Show() {
//		if ( !isHidden ) return;
//		meshRender.enabled = true;
//		base.Show();
//	}
//	
//	new public void Hide() {
//		if ( isHidden ) return;
//		meshRender.enabled = false;
//		base.Hide();
//	}
//
////	public void ShiftUVToCenter( float shiftY ) {
////		//float shift = 0.5f;
////		//offset = new Vector2( x-(float)(int)x, y-(float)(int)y ) * shift;
////		Vector2[] uvs = baseMesh.uv;
////		for ( int i = 0; i < uvs.Length; i++ ) {
////		//	uvs[i].y = shift + uvs[i].y * shift - offset.y;
////			if ( uvs[i].y > 1f-shiftY ) uvs[i].y = 1f-shiftY;
////			if ( uvs[i].y < shiftY ) uvs[i].y = shiftY;
////			uvs[i].x = uvRect.x+uvRect.width*uvs[i].x;
////			uvs[i].y = uvRect.y+uvRect.height*uvs[i].y;
////		}
////		quadMesh.uv = uvs;
////	}
//
//
////	private void UpdateTile() {
////		List<int> trianglesList = new List<int> ();
////		List<Vector3> verticesList = new List<Vector3> ();
////		List<Vector2> uvList = new List<Vector2> ();    
////
//////		Vector2 numberOfTiles = new Vector2( Mathf.CeilToInt ( tileSize.x ), Mathf.CeilToInt ( tileSize.y ) );
////		Vector2 numberOfTiles = new Vector2( 1, Mathf.CeilToInt ( tileSize.y ) );
////		Vector2 last = -(tileSize - numberOfTiles);
////		scaleVertices = new Vector2( 1, tileSize.y / numberOfTiles.y );
////		//ScaleSize( 1, tileSize.y / numberOfTiles.y );
////		//ScaleSize( tileSize.x / numberOfTiles.x, tileSize.y / numberOfTiles.y );
////		//numberOfTiles+= new Vector2( offset.x == 0 ? 0 : 1, offset.y == 0 ? 0 : 1 );
////		baseMesh.Clear();
////		for( int x = 0; x < numberOfTiles.x; x++ )
////			for ( int y = 0; y < numberOfTiles.y; y++ ) {
////			Mesh mesh = CreatePlaneMesh();
////			/* vertices */
////			foreach (Vector3 item  in mesh.vertices) {   
////				item.Set( item.x+x, item.y+y, 0);
////				verticesList.Add (item);
////			}
////			/* triangles */
////			foreach (int item  in mesh.triangles) {   
////				//item += 4*y + x*4*(numberOfTiles.y);
////				trianglesList.Add (item+4*y + x*4*(int)numberOfTiles.y );
////			}
////			/* uv-s */			
////			foreach (Vector2 item  in mesh.uv) {   
////				//item.Set( item.x+x, item.y+y );
////				uvList.Add (item);
////			}
////		}
//////		if ( offset.x > 0 || offset.y > 0 ) {
//////			for(int i = 0; i < verticesList.Count; i++ ) {   
//////				//Vector3 v = verticesList[i];
////////				if ( offset.x > 0 ) v.x += -1+offset.x;
////////				if ( offset.y > 0 ) v.y += -1+offset.y;
//////				Vector2 uv = uvList[i];
//////			//	uv.y = 0.5f + uv.y / 2 - offset.y;
////////				if ( v.x < 0 ) { v.x = 0; uv.x = 1-offset.x; }
////////				if ( v.y < 0 ) { v.y = 0; uv.y = 1-offset.y; }
////////				if ( v.x > tileSize.x ) { v.x = tileSize.x; uv.x = 1-offset.x-last.x; }
////////				if ( v.y > tileSize.y ) { v.y = tileSize.y; uv.y = 1-offset.y-last.y; }
////////				if ( uv.x < 0 ) uv.x += 1;
////////				if ( uv.y < 0 ) uv.y += 1;
////////				if ( uv.y >= 0.9f ) uv.y = 0.9f;
////////				if ( uv.y <= 0.1f ) uv.y = 0.1f;
//////				//verticesList[i] = v;
//////				uvList[i] = uv;
//////			}
//////		}
////
//////		if ( offset.x > 0 || offset.y > 0 ) {
//////			for(int i = 0; i < verticesList.Count; i++ ) {   
//////				Vector3 v = verticesList[i];
//////				//				if ( offset.x > 0 ) v.x += -1+offset.x;
//////				//				if ( offset.y > 0 ) v.y += -1+offset.y;
//////				Vector2 uv = uvList[i];
//////				//				if ( v.x < 0 ) { v.x = 0; uv.x = 1-offset.x; }
//////				//				if ( v.y < 0 ) { v.y = 0; uv.y = 1-offset.y; }
//////				//				if ( v.x > tileSize.x ) { v.x = tileSize.x; uv.x = 1-offset.x-last.x; }
//////				//				if ( v.y > tileSize.y ) { v.y = tileSize.y; uv.y = 1-offset.y-last.y; }
//////				//				if ( uv.x < 0 ) uv.x += 1;
//////				//				if ( uv.y < 0 ) uv.y += 1;
//////				//				if ( uv.y >= 0.9f ) uv.y = 0.9f;
//////				//				if ( uv.y <= 0.1f ) uv.y = 0.1f;
//////				verticesList[i] = v;
//////				uvList[i] = uv;
//////			}
//////		}		
////		baseMesh.vertices = verticesList.ToArray();
////		baseMesh.triangles = trianglesList.ToArray();
////		baseMesh.uv = uvList.ToArray();
////		UpdateVerticesAndUV();
////		//UpdateMeshUV();
////	}
//
//	public void Scale( float scale ) { Scale ( scale, scale ); }
//	public void Scale( float scaleX, float scaleY ) {
//		Vector3 scale = transform.localScale;
//		scale.x = scaleX;
//		scale.y = scaleY;
//		transform.localScale = scale;
//	}
//
//	override public bool isPressed { 
//		set { 	pressed = value; 	} 
//		get { return pressed; } 
//	}
//	override public float unscaledWidth { get { return baseSize.x; } }
//	override public float unscaledHeight { get { return baseSize.y; } }	
//	override public float width { get { 
//			return baseSize.x * transform.localScale.x;
//		} set { Debug.LogError("set width unsupported");
//		} }
//	override public float height { get { 
//			return baseSize.y * transform.localScale.y;
//		} set { Debug.LogError("set height unsupported");
//		} }
//
//}
//
