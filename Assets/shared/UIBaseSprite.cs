﻿using UnityEngine;
using System.Collections;

public abstract class UIBaseSprite : MonoBehaviour {

//	public enum Positions { FromTop, PixelsFromBottomLeft, PixelsFromTopLeft, 
//		PositionFromBottom, PositionFromBottomRight, PositionFromBottomLeft };
//	public Positions position;
//	public Vector2 position;
	protected float depth = 0;
	public bool centered = false;
	public event System.Action<bool> onToggle;
	bool touchable = false;
	public bool pressed = false;

	void OnToggle(UITouch touch, ref bool isHit ) {
		if ( this == null ) { Debug.Log ("Object is destroyed but tryed to access! touchable="+touchable); return; }			
		if ( touchable == false ) { isHit = false; return; }
		if ( !TouchTest(touch) ) { isHit = false; return; }
		isPressed = !isPressed;
		//pressed = !pressed;
//		if ( button ) {
//			isPressed = !isPressed;
//		}
		if (onToggle != null)
			onToggle(pressed);
		//		if (onTouch != null)
		//			onTouch();
		//touch = new Touch();
		isHit = true;
	}

	abstract public bool isPressed { set; get; }

	public bool isTouchable { get { return touchable; } 
		set { 
			if ( !touchable && value ) {
				touchable = true;
				UITouchControl.OnTouchEnded( OnToggle, depth );
			}
			if ( touchable && !value ) {	
				touchable = false;
				UITouchControl.Remove( OnToggle );
			}
		}
	}
	
	public void Destroy() {
		if ( touchable ) {
//			Debug.Log("Destroyed "+name);
			UITouchControl.Remove( OnToggle );
			touchable = false;
			pressed = false;
		}
	}

	public UIBaseSprite _parent = null;
//	public Transform getParent { get { return transform.parent; } }
	public UIBaseSprite parent { set { 
			_parent = value; 
			transform.parent = value.transform; 
			transform.localPosition = new Vector3(0,0,depth);
			transform.localScale = Vector3.one;
			transform.localEulerAngles = Vector3.zero;
			//transform.position = new Vector3(0,0,depth);
		} get { return _parent; } }

//	public Transform unityParent { set { 
//			if ( value.localPosition != Vector3.zero ) throw new System.Exception("unity parent must have local position = zero");
//			transform.parent = value; 
//	} }

	public GameObject unityParent { 
		set { 
			if ( value == null ) { transform.parent = null; return; }
			if ( value.transform.localPosition != Vector3.zero ) throw new System.Exception
				("unity parent must have local position("+value.transform.localPosition+") = zero");
			transform.parent = value.transform; 
		} 
		get {
			return transform.parent.gameObject;
		}
	}
	
	abstract public float width { get; set; }
	abstract public float height { get; set; }
	abstract public float unscaledWidth { get; }
	abstract public float unscaledHeight { get; }

	virtual public Vector2 globalPosition { get { return new Vector2( transform.position.x, transform.position.y ); } }
	//virtual public float posY { get { return Screen.height+transform.localPosition.y; } }

	public bool TouchTest( UITouch touch ) { return HitTest( touch.position ); }

	public bool HitTest3D( Vector2 mouse ) {
		if ( isHidden ) return false;
		Rect normalFrame;
		//if ( parent == null ) 
		if (centered) normalFrame = new Rect( savedXY.x-width/2, savedXY.y-height/2, width, height );
		else normalFrame = new Rect( savedXY.x, savedXY.y, width, height );
		//else normalFrame = new Rect( parent.posX+posX, parent.posY+posY, width, height );
		//Debug.Log ("nousePos="+mouse+", sprite pos="+normalFrame);	
		return normalFrame.Contains( mouse );		
	}

	public bool HitTest( Vector2 mouse ) {
		if ( isHidden ) return false;
		Rect normalFrame;
		//if ( parent == null ) 
		if (centered) normalFrame = new Rect( globalPosition.x-width/2, globalPosition.y-height/2, width, height );
		else normalFrame = new Rect( globalPosition.x, globalPosition.y, width, height );
		//else normalFrame = new Rect( parent.posX+posX, parent.posY+posY, width, height );
		//Debug.Log ("nousePos="+mouse+", sprite pos="+normalFrame);	
		//Debug.Log("OnToggle: touch="+mouse+" vs "+name+" transform.position="+transform.position+" frame="+normalFrame);
		return normalFrame.Contains( mouse );		
	}

	//public void OnScreen () { RefreshPosition(); }
	//virtual public void ShowChilds() {}	
	//virtual public void HideChilds() {}	
	//virtual public void Show() {}	
	//virtual public void Hide() {}	

	public void Show() { _isHidden = false; RefreshPosition(); }
	public void Hide() { _isHidden = true; }
	[SerializeField] private bool _isHidden = false;
	public bool isHidden { get { return _isHidden; } 
		set { 
			if ( value && !_isHidden ) { Hide (); } 
			else if ( !value && _isHidden ) { Show (); } 
		}
	}

//	public bool isHidden { get { return ! transform.gameObject.activeInHierarchy; } 
//		set { if ( value ) Hide (); else Show (); }
//	}
	
//	public void Hide() {
//		isHidden = false;
//		transform.gameObject.SetActive(false);
//		//HideChilds();
//	}

//	public void SetActive( bool active ) {
//		if ( active )
//	}

	static public void Hide(UIBaseSprite sprite) { sprite.Hide (); }
	static public void Show(UIBaseSprite sprite) { sprite.Show (); }

	[SerializeField]
	Vector2 savedXY;
	public Vector2 lastPosition{ get { return savedXY; } }
	[SerializeField]
	Command savedCommand;

	enum Command { PixelsFromTopLeft, PixelsFromLeft, PixelsFromRight, PixelsFromTopRight, 
		PixelsFromBottomLeft, PixelsFromBottomRight, PixelsFromTop, PixelsFromBottom,
		PositionFromBottomRight, PositionFromBottomLeft, PositionFromTopLeft,
		PositionFromBottom, PositionFromTop, PixelsFromBottomLeft3D, //PixelsLocalPosition
	}

	public void PixelsLocalPosition(Vector3 localPosition ) {
		transform.localPosition = localPosition;
		RefreshPosition(Command.PixelsFromBottomLeft, localPosition.x, localPosition.y);
	}
	// X=0(left to right)->Screen.width  Y=(top->bottom) 0->-Screen.Height
	public void PixelsFromTopLeft(float y, float x ) {
		RefreshPosition(Command.PixelsFromTopLeft,x,y);
	}	
	public void PixelsFromLeft( float x ) {
		RefreshPosition(Command.PixelsFromLeft,x,0);
	}
	public void PixelsFromRight( float x ) {
		RefreshPosition(Command.PixelsFromRight,x,0);
	}
	// X=0(left to right)->Screen.width  Y=(top->bottom) 0->-Screen.Height
	public void PixelsFromTopRight(float y, float x ) {
		RefreshPosition(Command.PixelsFromTopRight,x,y);
	}	
	public void PixelsFromBottomLeft(float y, float x ) {
		RefreshPosition(Command.PixelsFromBottomLeft, x ,y);
	}
	public void PixelsFromBottomLeft3D(Vector2 pos ) {
		RefreshPosition(Command.PixelsFromBottomLeft3D, pos.x, pos.y);
	}
	public void PixelsFromBottomRight(float y, float x ) {
		RefreshPosition(Command.PixelsFromBottomRight, x, y);
	}
	public void PixelsFromTop(float y) {
		RefreshPosition(Command.PixelsFromTop, 0, y);
	}	
	public void PixelsFromBottom(float y) {
		RefreshPosition(Command.PixelsFromBottom, 0, y);
	}
	public void PositionFromBottomRight( float y, float x ) {
		RefreshPosition(Command.PositionFromBottomRight, x, y);
	}
	public void PositionFromBottomLeft( float y, float x ) {
		RefreshPosition(Command.PositionFromBottomLeft, x, y);
	}
	public void PixelsFromBottomLeft(Vector2 xy ) {
		RefreshPosition(Command.PixelsFromBottomLeft, xy.x ,xy.y);
	}
	public void PixelsFromBottomRight(Vector2 xy ) {
		RefreshPosition(Command.PixelsFromBottomRight, xy.x ,xy.y);
	}
	public void PositionFromTopLeft( float y, float x ) {
		RefreshPosition( Command.PositionFromTopLeft, x , y );
	}
	public void PositionFromBottom( float y ) {
		RefreshPosition(Command.PositionFromBottom, 0, y);
	}
	public void PositionFromTop( float y ) {
		RefreshPosition(Command.PositionFromTop, 0 , y);
	}
	private void RefreshPosition( Command cmd, float x, float y ) {
		savedCommand = cmd;
		savedXY = new Vector2( x, y );
		RefreshPosition();
	}

	public float pixelsFromLeft{ get {
			return transform.position.x;
		}
	}

	public float pixelsFromBottom{ get {
			return transform.position.y; // y-Screen.height
		}
	}

	public float pixelsFromTop{ get {
			return Screen.height-transform.position.y - height;
		}
	}

	public void RefreshPosition() {
		//if ( isHidden ) return;
		float x = savedXY.x;
		float y = savedXY.y;
		Vector2 vector = Vector2.zero;
		switch(	savedCommand ) {
		case Command.PixelsFromTopLeft:
			if (parent == null) vector = new Vector2( x, Screen.height-y-height); //y-Screen.height-height); // -y-height );
			else vector = new Vector2( x, -y ); //-height ) );
			break;
		case Command.PixelsFromLeft:
			vector = new Vector2( x, Screen.height/2 ); //-height/2 );
			break;
		case Command.PixelsFromRight:
			vector = new Vector2( Screen.width - x-width, Screen.height/2);//-height/2 );
			break;
		case Command.PixelsFromTopRight:
			vector = new Vector2( Screen.width - x-width, Screen.height-y-height );
			break;
		case Command.PixelsFromBottomLeft:
			if (parent == null) vector = new Vector2( x, y ); //-Screen.height );
			else vector = new Vector2( x, y ); //-parent.width ) );
			break;
		case Command.PixelsFromBottomLeft3D:
			if (parent == null) vector = new Vector2( x, -y );
			else throw new System.NotImplementedException("parent!=null");
			break;
		case Command.PixelsFromBottomRight:
			if (parent == null) vector = new Vector2( Screen.width - x-width, y );
			else vector = new Vector2( parent.width - x-width, y );
			break;
		case Command.PixelsFromTop:
			vector = new Vector2( Screen.width/2 - width/2, Screen.height-y-height );
			break;
		case Command.PixelsFromBottom:
			vector = new Vector2( Screen.width/2 - width/2, y );
			break;
		case Command.PositionFromBottomRight:
			if (parent == null) throw new System.NotImplementedException("parent==null");
			vector = new Vector2( -  unscaledWidth - (x-1) * parent.unscaledWidth, y * parent.unscaledHeight );
			break;
		case Command.PositionFromBottomLeft:
			if (parent == null) //throw new System.NotImplementedException("parent==null");
				vector = new Vector2( x * Screen.width, y * Screen.height - Screen.height );
			//Vector2 _parent = new Vector2( Screen.width, Screen.height );
			else vector = new Vector2( x * parent.unscaledWidth, y * parent.unscaledHeight );
			break;
		case Command.PositionFromTopLeft:
			if (parent == null) //throw new System.NotImplementedException("parent==null");
				vector = new Vector2( x * Screen.width, Screen.height-height - y * Screen.height );
			//Vector2 _parent = new Vector2( Screen.width, Screen.height );
			else vector = new Vector2( x * parent.unscaledWidth, -y * parent.unscaledHeight );
			break;
		case Command.PositionFromBottom:
			if (parent == null) throw new System.NotImplementedException("parent==null");
			// if parent presenet - scaling also in effect and transfer to childs
			vector = new Vector2( 0, y * parent.unscaledHeight );
			break;
		case Command.PositionFromTop:
			if (parent != null) throw new System.NotImplementedException("parent!=null");
			Vector2 _parent = new Vector2( Screen.width, Screen.height );
			vector = new Vector2(  _parent.x/2-width/2, Screen.height-height - y * _parent.y );
			break;
		}
		transform.localPosition = new Vector3( vector.x, vector.y, transform.localPosition.z );
		RefreshChilds();
	}

	void PixelsFromVector( Vector2 pos ) {
		//savedPosition = pos;
		if ( isHidden ) return;
		transform.localPosition = new Vector3( pos.x, pos.y, transform.localPosition.z );
		RefreshChilds();
	}

	virtual public void RefreshChilds() {
	}

	public float GetTotalParentsScaleY() {
		float scaleY = 1;
		Transform parent = transform.parent;
		while( true) {
			if ( parent == null ) return scaleY;
			scaleY *= parent.localScale.y;
			parent = parent.parent;
		}
	}

}
