using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;
using System.Reflection;
using UnityEngine.EventSystems;

public class UITouch {
	public const float LONGCLICKTIME = 0.5f;
	public Vector2 deltaPosition = Vector2.zero;
	public float deltaTime = 0;
	public int id = 0;
//	public TouchPhase phase;
	public UITouchControl.TouchType type;
	public Vector2 position;// = Vector2.zero;
	public Vector2 rawPosition = Vector2.zero;
	public int tapCount = 0;
	//public Vector2 lastPosition = Vector2.zero;
	public float zoom = 0;
	private static float lastTime = 0;
//	private static Vector2 lastPosition = Vector2.zero;
	public float scrollWheel = 0;



	public UITouch() {
	}

	public UITouch(float deltaMouseWheel) {
		position = new Vector2( Input.mousePosition.x, Input.mousePosition.y );
		zoom = deltaMouseWheel;
		//phase = TouchPhase.Stationary;
		type = UITouchControl.TouchType.OnStand;
	}

	public UITouch( FullScreenTouchNewUI.TouchData touchData ) {
		id = touchData.data.pointerId;
		position = touchData.data.position;
		type = touchData.type;
		deltaPosition = touchData.data.delta;
		zoom = touchData.data.scrollDelta.y; // mouse scroll on Y
		//Debug.Log ("ScrollDelta: "+touchData.data.scrollDelta);
	}

	public UITouch( Touch touch ) {
		deltaPosition = touch.deltaPosition;
		deltaTime = touch.deltaTime;
		id = touch.fingerId;
		//phase = touch.phase;
		position = touch.position;
		rawPosition = touch.rawPosition;
		tapCount = touch.tapCount;
		PhaseToType( touch.phase );
//		switch( touch.phase ) {
//		case TouchPhase.Began: type = UITouchControl.TouchType.OnDown; break;
//		case TouchPhase.Moved: type = UITouchControl.TouchType.OnMove; break;
//		case TouchPhase.Stationary: type = UITouchControl.TouchType.OnStand; break;
//		case TouchPhase.Ended: type = UITouchControl.TouchType.OnUp; break;
//		case TouchPhase.Canceled: type = UITouchControl.TouchType.OnExit; break;
//		}
	}

	public UITouch( UIMouseState mouseState, ref Vector2? lastMousePosition ) {
		var currentMousePosition = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
		
		// if we have a lastMousePosition use it to get a delta
		if (lastMousePosition.HasValue) deltaPosition = (Vector2)(currentMousePosition - lastMousePosition);
		if (mouseState == UIMouseState.DownThisFrame) { // equivalent to touchBegan
			//phase = TouchPhase.Began;
			lastMousePosition = Input.mousePosition;
			type = UITouchControl.TouchType.OnDown;
		} else if (mouseState == UIMouseState.UpThisFrame) { // equivalent to touchEnded
			//phase = TouchPhase.Ended;
			type = UITouchControl.TouchType.OnUp;
			lastMousePosition = null;
		} else { // UIMouseState.HeldDown - equivalent to touchMoved/Stationary
			//phase = TouchPhase.Moved;
			lastMousePosition = Input.mousePosition;
			type = UITouchControl.TouchType.OnMove;
		}		

		float dTime = 0;
		if( mouseState == UIMouseState.DownThisFrame || mouseState == UIMouseState.UpThisFrame) {
			dTime = Time.realtimeSinceStartup - lastTime;
			lastTime = Time.realtimeSinceStartup;
		}

		position = currentMousePosition;
		this.deltaTime = dTime;
	}

//	public Touch touch { get {
////				return UITouchMaker.createTouch ( fingerId, tapCount, position, deltaPosition, deltaTime, phase );	
//			return UITouchMaker.createTouch ( id, tapCount, position, deltaPosition, zoom, phase );	
//		}
//	}

	public bool IsEqual ( UITouch touch ) {
		if ( deltaPosition == touch.deltaPosition &&
		    deltaTime == touch.deltaTime &&
		    id == touch.id &&
		    type == touch.type &&
		    position == touch.position &&
		    rawPosition == touch.rawPosition &&
		    tapCount == touch.tapCount ) return true;
		return false;
	}

	public UITouch PhaseToType(TouchPhase phase) {
		switch( phase ) {
		case TouchPhase.Began: type = UITouchControl.TouchType.OnDown; break;
		case TouchPhase.Moved: type = UITouchControl.TouchType.OnMove; break;
		case TouchPhase.Stationary: type = UITouchControl.TouchType.OnStand; break;
		case TouchPhase.Ended: type = UITouchControl.TouchType.OnUp; break;
		case TouchPhase.Canceled: type = UITouchControl.TouchType.OnExit; break;
		default: Debug.LogError("unknown"); break;
		}
		return this;
	}
}

public class UITouchControl : MonoBehaviour {

	protected class TouchControl {
		private List<float> depths = new List<float> ();
		private List<DelegateTouch> delegates = new List<DelegateTouch> ();
		private List<int> indices = new List<int> ();
		string name;

		public TouchControl (string name) {
			this.name = name;
		}

		public void Add (DelegateTouch method, float depth) {
			depths.Add (depth);
			delegates.Add (method);
			indices.Add (0);
			Sort ();
		}
		
		public void Remove (DelegateTouch method) {
			if (!delegates.Contains (method))
				return;
			// TODO there are possibility what same method many times in delegates not only once
			depths.RemoveAt (delegates.IndexOf (method));
			delegates.Remove (method);
			indices.RemoveAt (0);
			Sort (); // todo wrong here !
			//Debug.Log("delegates["+delegates.Count+"], depths["+depths.Count+"], indices["+indices.Count+"]");
		}
		
		void Sort () {

			// depths[n] delegates[n] indices[n]
			// indices from 0 to n is target index of delegates with lower depths
			float depth;
			int index;
			int count = depths.Count;
			
			bool[] check = new bool[count]; // array with true if this index is set
			for (int i = 0; i < count; i++)
				check [i] = false;
			
			for (int i = 0; i < count; i++) {
				depth = 9999;
				index = -1;
				for (int j = 0; j < count; j++) {
					if (check [j])
						continue;
					if (depths [j] < depth) {
						depth = depths [j];
						index = j;
					}
				}
				indices [i] = index;
				check [index] = true;
			}
			// test
			int sum1 = 0, sum2 = 0;
			for (int i = 0; i < count; i++) {
				if (check [i] == false)
					throw new System.Exception ("Sort test(bool) failed");
				sum1 += i;
				sum2 += indices [i];
			}
			if (sum1 != sum2)
				throw new System.Exception ("Sort test(sum) failed");			
		}

		public override string ToString () {
			//((DelegateTouch)delegates[indices[i]]). 
			string text = "TouchControl: " + name;
			for (int i = 0; i < depths.Count; i++) 
				text += "\n" + "Depth[" + i + "]=" + depths [i] + ", Delegate[" + i + "]=" + delegates [i] + ", Indices[" + i + "]=" + indices [i];
			//Debug.Log("Depth["+i+"]="+depths[i]+", Delegate["+i+"]="+delegates[i]+", Indices["+i+"]="+indices[i]);
			return text;
		}

		public void Invoke (UITouch touch) {
			//Invoke( touch.touch );
			//Debug.Log(this);
			bool isHit;
			//bool isNull;
			for (int i = 0; i < depths.Count; i++) {
				isHit = false;
				//isNull = false;
				//Debug.Log ( ((Object)delegates[indices[i]].). );
				((DelegateTouch)delegates [indices [i]]) (touch, ref isHit);
				//bool isHit = ((DelegateTouch)delegates[indices[i]])( touch, isHit, isNull );
				//				if ( isNull ) {
				//					//string info = typeof( ( DelegateTouch ) delegates [ indices [ i ] ] ).GetProperties (  )[ 0 ].Name;
				//					string info = ( delegates [ indices [ i ] ]).Method.ToString();
				//					Debug.Log("UITouchControl(depth="+i+") isNull is true, info="+info);
				//					continue; // TODO del null ref
				//				}
				if (isHit)
					break;
			}
		}

//		public void Invoke (Touch touch) {
//			//Debug.Log(this);
//			bool isHit;
//			//bool isNull;
//			for (int i = 0; i < depths.Count; i++) {
//				isHit = false;
//				//isNull = false;
//				//Debug.Log ( ((Object)delegates[indices[i]].). );
//				((DelegateTouch)delegates [indices [i]]) (touch, ref isHit);
//				//bool isHit = ((DelegateTouch)delegates[indices[i]])( touch, isHit, isNull );
////				if ( isNull ) {
////					//string info = typeof( ( DelegateTouch ) delegates [ indices [ i ] ] ).GetProperties (  )[ 0 ].Name;
////					string info = ( delegates [ indices [ i ] ]).Method.ToString();
////					Debug.Log("UITouchControl(depth="+i+") isNull is true, info="+info);
////					continue; // TODO del null ref
////				}
//				if (isHit)
//					break;
//			}
//		}
	}

	public enum TouchType { OnClick, OnMove, OnEnter, OnExit, OnUp, OnDown, OnScroll, OnStand }
	
	static public UITouchControl instance;
	
	public event System.Action<UITouch> onLookAtTouch;
	public event System.Action<Vector2, float> onZoomChange;

	//public event System.Action<float,float> onPositionChange;
//	public event System.Action<Vector2> onCursorMove;
//	public event System.Action<Camera> onTransformChange;
	//public event System.Action<Vector2> onClick;
	public event System.Action onScreenOrientation;
	private event System.Action _onScreenSize;

	public static event System.Action onScreenSize { 
		add { instance._onScreenSize += value; } 
		remove { instance._onScreenSize -= value; }
	}

	public event System.Action onUpdate;
//	public event System.Action<Touch> onTouchBegan;
//	public event System.Action<Touch> onTouchMoved;
//	public event System.Action<Touch> onTouchEnded;
	
#if UNITY_EDITOR || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_WEBPLAYER
	private Vector2? lastMousePosition;
#endif
	int scrWidth;
	int scrHeight;
	ScreenOrientation scrOrientation;
	public int touchesAdded = 0;
	public int touchesRemoved = 0;
	public FullScreenTouchNewUI newUI;
	bool[] isIdOnExit = new bool[100];

	public delegate void DelegateTouch (UITouch touch,ref bool isHit);

	static public void OnUpdate (System.Action method) {
		UITouchControl.instance.onUpdate += method;
	}

	TouchControl onTouchZoomControl = new TouchControl ("Zoom");
	// Touch.timeDelta = zoom radius
	static public void OnTouchZoom (DelegateTouch method, float depth) {
		UITouchControl.instance.touchesAdded ++;
		UITouchControl.instance.onTouchZoomControl.Add (method, depth);
	}
	
	TouchControl onTouchBeganControl = new TouchControl ("Began");
	static public void OnTouchBegan (DelegateTouch method, float depth) {
		UITouchControl.instance.touchesAdded ++;
		UITouchControl.instance.onTouchBeganControl.Add (method, depth);
	}

	TouchControl onTouchMovedControl = new TouchControl ("Moved");
	static public void OnTouchMoved (DelegateTouch method, float depth) {
		CheckInstance();
		UITouchControl.instance.touchesAdded ++;
		UITouchControl.instance.onTouchMovedControl.Add (method, depth);
	}

	TouchControl onTouchStationaryControl = new TouchControl ("Stationary");
	static public void OnTouchStationary (DelegateTouch method, float depth) {
		UITouchControl.instance.touchesAdded ++;
		UITouchControl.instance.onTouchStationaryControl.Add (method, depth);
	}

	TouchControl onTouchEndedControl = new TouchControl ("Ended");
	static public void OnTouchEnded (DelegateTouch method, float depth) {
	//	if ( method.Method.Name == "OnTouchEnded" ) throw new Exception("OnTouchEnded: dont do it!");
		UITouchControl.instance.touchesAdded ++;
		UITouchControl.instance.onTouchEndedControl.Add (method, depth);
	}

	static public void Remove (System.Action method) {
		UITouchControl.instance.onUpdate -= method;
	}

	static public void Remove (DelegateTouch method) {
		UITouchControl.instance.touchesRemoved ++;
		UITouchControl.instance.onTouchBeganControl.Remove (method);
		UITouchControl.instance.onTouchMovedControl.Remove (method);
		UITouchControl.instance.onTouchEndedControl.Remove (method);
		UITouchControl.instance.onTouchZoomControl.Remove (method);
		UITouchControl.instance.onTouchStationaryControl.Remove (method);
	}
	
	void Awake () {
		//Debug.Log(" UITouchControl Awake");
		instance = this;
		scrWidth = Screen.width;
		scrHeight = Screen.height;
		scrOrientation = Screen.orientation;
	}

	static private void CheckInstance() {
		if ( instance == null ) { 
			GameObject go = GameObject.Find("TouchControl");
			UITouchControl tc = null;
			if ( go != null ) tc = go.transform.GetComponent<UITouchControl>();
			//Debug.LogWarning("instance("+instance+") go("+go+") tc("+tc+")"); 
			if ( tc != null ) instance = tc;
		}
	}

//	public void Start() { Debug.Log(" UITouchControl Start"); }	
//	public void OnEnable() { Debug.Log(" UITouchControl OnEnable"); }
//	public void OnDisable() { Debug.Log(" UITouchControl OnDisable"); }

	//UIMouseState lastState = new UIMouseState ();
	//private float lastStateTime = 0;
	
	void Update () {
		if (onUpdate != null)
			onUpdate ();
		
		if (Input.GetKey (KeyCode.Home))
			Debug.Log ("Platform getkey keycode.home");
		if (Input.GetKey (KeyCode.Escape))
			Debug.Log ("Platform getkey keycode.escape");
		if (Input.GetKey (KeyCode.Menu))
			Debug.Log ("Platform getkey keycode.menu");
		if (Input.GetKey (KeyCode.Escape))
			Debug.Log ("Platform getkeydown escape");
		
		// DONT check orientation because orientation change first then width and height next time
		if (scrOrientation != Screen.orientation) {
			Debug.Log (" Screen( " + scrWidth + "->" + Screen.width + ", " + scrHeight + "->" + Screen.height + ", " + scrOrientation + "->" + Screen.orientation + " )");				//Log.Add(" Screen( "+Screen.width+", "+Screen.height+", "+Screen.orientation+" )");
			if (onScreenOrientation != null)
				onScreenOrientation ();
			scrOrientation = Screen.orientation;
		}
		
		if ((scrWidth != Screen.width) || (scrHeight != Screen.height)) {
			//Debug.Log (" Screen( " + scrWidth + "->" + Screen.width + ", " + scrHeight + "->" + Screen.height + ", " + scrOrientation + "->" + Screen.orientation + " )");
			if (_onScreenSize != null) 
				_onScreenSize ();
			scrWidth = Screen.width;
			scrHeight = Screen.height;
			scrOrientation = Screen.orientation;
		}

		if ( newUI != null ) {
			FullScreenTouchNewUI.TouchData data = newUI.PopLastData();
			if ( data == null ) return;
			if ( data.type == TouchType.OnExit ) isIdOnExit[-data.data.pointerId] = true;
			if ( data.type == TouchType.OnEnter ) isIdOnExit[-data.data.pointerId] = false;
			// work on data
			if ( isIdOnExit[-data.data.pointerId] ) return;
			//Debug.Log( data );
			LookAtTouch (new UITouch(data));
			return;
		}
		
		//Debug.Log ("I.tC=" + Input.touchCount);
		if (Input.touchCount == 1) {
			//Debug.Log ("dT=" + Input.GetTouch (0).deltaTime);
			LookAtTouch (new UITouch( Input.GetTouch (0) ));
		} else if (Input.touchCount == 2) {
			// touch zoom
			Touch touch0 = Input.GetTouch (0);
			Touch touch1 = Input.GetTouch (1);
			LookAtTouch (new UITouch(touch0));
			LookAtTouch (new UITouch(touch1));
			if (touch0.phase == TouchPhase.Moved && touch0.phase == TouchPhase.Moved) {
				// both touches is moved to close or out
				//touch0.deltaPosition
				Vector2 center = (touch0.position + touch1.position) / 2;
				// pos0(10, 10) delta0(-2, 0) && pos1(150, 60) delta(10, 5) == move out
				float dNow = Vector2.Distance (touch0.position, touch1.position);
				float dBefore = Vector2.Distance (touch0.position - touch0.deltaPosition, touch1.position - touch1.deltaPosition);
				//Log.Add ( "dNow=" + dNow + ", dBefore=" + dBefore );
				float delta = (dNow - dBefore) / 1000;
				if (onZoomChange != null) {
					onZoomChange (center, delta);
				}
				//onZoomControl.Invoke ( new CustomTouch( center, dNow - dBefore ) );	
				onTouchZoomControl.Invoke ( new UITouch( delta ) );	
				//onTouchZoomControl.Invoke (UITouchMaker.createTouch (0, 0, center, new Vector2 (), delta, TouchPhase.Moved));	
			}
		}

//		// only do our touch processing if we have some touches
//		if (Input.touchCount > 0) {
//			// Examine all current touches
//			for (int i = 0; i < Input.touchCount; i++) {
//				LookAtTouch (Input.GetTouch (i));
//			}
//		}
#if UNITY_EDITOR || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_WEBPLAYER
		else
		{
			// no touches. so check the mouse input if we are in the editor
			// check for each mouse state in turn, no elses here. They can all occur on the same frame!
		//	float deltaTime = 0;
			if( Input.GetMouseButtonDown( 0 ) ) {
			//	deltaTime = Time.realtimeSinceStartup - lastStateTime;
			//	lastStateTime = Time.realtimeSinceStartup;
			//	LookAtTouch( UITouchMaker.createTouchFromInput( UIMouseState.DownThisFrame, ref lastMousePosition, deltaTime ) );
				LookAtTouch( new UITouch( UIMouseState.DownThisFrame, ref lastMousePosition) );
			}
			if( Input.GetMouseButton( 0 ) )
				LookAtTouch( new UITouch( UIMouseState.HeldDown, ref lastMousePosition) );
			//LookAtTouch( UITouchMaker.createTouchFromInput( UIMouseState.HeldDown, ref lastMousePosition ) );
			if( Input.GetMouseButtonUp( 0 ) ) {
				//deltaTime = Time.realtimeSinceStartup - lastStateTime;
			//	lastStateTime = Time.realtimeSinceStartup;
				LookAtTouch( new UITouch( UIMouseState.UpThisFrame, ref lastMousePosition) );
				//LookAtTouch( UITouchMaker.createTouchFromInput( UIMouseState.UpThisFrame, ref lastMousePosition ) );
			}
 			if (Input.GetAxis ("Mouse ScrollWheel") != 0)  {
				if (onZoomChange != null) 
					onZoomChange( Input.mousePosition, Input.GetAxis ("Mouse ScrollWheel") );
				//var mousePos = new Vector2( Input.mousePosition.x, Input.mousePosition.y );
				//onTouchZoomControl.Invoke ( UITouchMaker.createTouch(0,0,mousePos,new Vector2(),Input.GetAxis ("Mouse ScrollWheel"),TouchPhase.Moved) );	
				onTouchZoomControl.Invoke ( new UITouch( Input.GetAxis ("Mouse ScrollWheel") ) );	

			}
			
	//		if (Input.GetMouseButton (0)) 
	//			if (Input.GetAxis ("Mouse X") != 0 || Input.GetAxis ("Mouse Y") != 0) 
	//				if (onPositionChange != null)
	//					onPositionChange( Input.GetAxis ("Mouse X"), Input.GetAxis ("Mouse Y") );
				
//			if (Input.GetAxis ("Mouse X") != 0 || Input.GetAxis ("Mouse Y") != 0)
//				if (onCursorMove != null)
//					onCursorMove( new Vector2( Input.mousePosition.x, Input.mousePosition.y ) );
				
		//	if (Input.GetMouseButtonUp (0)) 
		//		onClick( new Vector2( Input.mousePosition.x, Input.mousePosition.y ) );		
		}
#endif
	}

	private void LookAtTouch (UITouch touch) {
		#if UNITY_EDITOR || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_WEBPLAYER
		if (Input.GetAxis ("Mouse ScrollWheel") != 0) 
			touch.scrollWheel = Input.GetAxis ("Mouse ScrollWheel");
		#endif
		// Began - A finger touched the screen.
		// Moved - A finger moved on the screen.
		// Stationary - A finger is touching the screen but hasn't moved.
		// Ended - A finger was lifted from the screen. This is the final phase of a touch.
		// Canceled - The system cancelled tracking for the touch, as when (for example)
		if (onLookAtTouch != null) onLookAtTouch ( touch );
		if (touch.type == TouchType.OnDown) onTouchBeganControl.Invoke (touch);				
		if (touch.type == TouchType.OnMove) onTouchMovedControl.Invoke (touch);				
		if (touch.type == TouchType.OnUp) onTouchEndedControl.Invoke (touch);				
		if (touch.type == TouchType.OnClick) {} //onTouchEndedControl.Invoke (touch);				
		if (touch.type == TouchType.OnEnter) {} //onTouchEndedControl.Invoke (touch);				
		if (touch.type == TouchType.OnExit) {} //onTouchEndedControl.Invoke (touch);				
		if (touch.type == TouchType.OnStand) {} //onTouchEndedControl.Invoke (touch);				
		if (touch.type == TouchType.OnScroll) {} onTouchZoomControl.Invoke (touch);				
		// stationary not implemented
		//if (touch.phase == TouchPhase.Stationary) onTouchEndedControl.Invoke (touch);				
	}

//	private void LookAtTouch (Touch touch) {
//		if (onLookAtTouch != null)
//			onLookAtTouch ( new UITouch(touch) );
//		// Began - A finger touched the screen.
//		// Moved - A finger moved on the screen.
//		// Stationary - A finger is touching the screen but hasn't moved.
//		// Ended - A finger was lifted from the screen. This is the final phase of a touch.
//		// Canceled - The system cancelled tracking for the touch, as when (for example)
//		//			  the user puts the device to her face or more than five touches happened simultaneously. 
//		//			  This is the final phase of a touch.
//		//	if (touch.phase == TouchPhase.Began) if ( onTouchBegan != null ) onTouchBegan( touch );	
//		//if (touch.phase == TouchPhase.Moved) if ( onTouchMoved != null ) onTouchMoved( touch );	
////		if (touch.phase == TouchPhase.Ended) if ( onTouchEnded != null ) onTouchEnded( touch );				
//		//if (touch.phase == TouchPhase.Ended) StartEvent( onTouchEnded, touch );				
//		if (touch.phase == TouchPhase.Began)
//			onTouchBeganControl.Invoke (touch);				
//		if (touch.phase == TouchPhase.Moved)
//			onTouchMovedControl.Invoke (touch);				
//		if (touch.phase == TouchPhase.Ended)
//			onTouchEndedControl.Invoke (touch);				
//	}
	
//	void StartEvent( System.Action<Touch> someEvent, Touch touch ) {
//		if ( someEvent != null ) {
//			System.Delegate[] eventArray = someEvent.GetInvocationList();
//			foreach( System.Action<Touch> cEvent in eventArray ) 
//				cEvent.Invoke( touch );
//		}		
//	}
}

//#if UNITY_EDITOR || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_WEBPLAYER CAYSE
public enum UIMouseState { UpThisFrame, DownThisFrame, HeldDown };

/// <summary>
/// this class now exists only to allow standalones/web players to create Touch objects
/// </summary>
//public struct UITouchMaker {
////	public static Touch createTouch (int id, int tapCount, Vector2 position, Vector2 deltaPos, float timeDelta, TouchPhase phase) {
////		var self = new Touch ();
////		ValueType valueSelf = self;
////		var type = typeof(Touch);
////		
////		type.GetField ("m_FingerId", BindingFlags.Instance | BindingFlags.NonPublic).SetValue (valueSelf, id);
////		type.GetField ("m_TapCount", BindingFlags.Instance | BindingFlags.NonPublic).SetValue (valueSelf, tapCount);
////		type.GetField ("m_Position", BindingFlags.Instance | BindingFlags.NonPublic).SetValue (valueSelf, position);
////		type.GetField ("m_PositionDelta", BindingFlags.Instance | BindingFlags.NonPublic).SetValue (valueSelf, deltaPos);
////		type.GetField ("m_TimeDelta", BindingFlags.Instance | BindingFlags.NonPublic).SetValue (valueSelf, timeDelta);
////		type.GetField ("m_Phase", BindingFlags.Instance | BindingFlags.NonPublic).SetValue (valueSelf, phase);
////		
////		return ((Touch)valueSelf);
////	}
//	
////	public static Touch createTouchFromInput (UIMouseState mouseState, ref Vector2? lastMousePosition, float deltaTime) {
////		var self = new Touch ();
////		ValueType valueSelf = self;
////		var type = typeof(Touch);
////		
////		var currentMousePosition = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
////		
////		// if we have a lastMousePosition use it to get a delta
////		if (lastMousePosition.HasValue)
////			type.GetField ("m_PositionDelta", BindingFlags.Instance | BindingFlags.NonPublic).SetValue (valueSelf, currentMousePosition - lastMousePosition);
////		
////		if (mouseState == UIMouseState.DownThisFrame) { // equivalent to touchBegan
////			type.GetField ("m_Phase", BindingFlags.Instance | BindingFlags.NonPublic).SetValue (valueSelf, TouchPhase.Began);
////			lastMousePosition = Input.mousePosition;
////		} else if (mouseState == UIMouseState.UpThisFrame) { // equivalent to touchEnded
////			type.GetField ("m_Phase", BindingFlags.Instance | BindingFlags.NonPublic).SetValue (valueSelf, TouchPhase.Ended);
////			lastMousePosition = null;
////		} else { // UIMouseState.HeldDown - equivalent to touchMoved/Stationary
////			type.GetField ("m_Phase", BindingFlags.Instance | BindingFlags.NonPublic).SetValue (valueSelf, TouchPhase.Moved);
////			lastMousePosition = Input.mousePosition;
////		}
////		
////		type.GetField ("m_Position", BindingFlags.Instance | BindingFlags.NonPublic).SetValue (valueSelf, currentMousePosition);
////		type.GetField ("m_TimeDelta", BindingFlags.Instance | BindingFlags.NonPublic).SetValue (valueSelf, deltaTime);
////
////		return (Touch)valueSelf;
////	}
//}
//#endif CAYSE