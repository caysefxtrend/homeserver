﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpeedTests : MonoBehaviour {
	class Cell { public int a; public Cell(int b){a=b;} }
	public int count = 10000;
	public bool start = false;
	public bool indexArrayTest = false;
	public bool dictionaryTest = false;
	public bool listTest = false;
	public bool vectorsTest = false;
	public bool gameobjectsTest = false;
	public bool nullCheckTest = false;
	// Use this for initialization
	public Array<Transform> array;
	public GameObject testGO;

	void Start () {
		Debug.Log("Start");
		//start = false;	
		if ( gameobjectsTest ) TestGameObjectsCreate();

	}
	
	// Update is called once per frame
	void Update () {
		if ( start && !gameobjectsTest ) {
			start = false;
			if ( indexArrayTest ) TestIndexArray();
			if ( dictionaryTest ) TestDictionary();
			if ( listTest ) TestList();
			if ( vectorsTest ) TestVectorsEqual();
			if ( nullCheckTest ) TestNullCheck();
		}	
	}

	void FixedUpdate() {
		if ( start && gameobjectsTest && testGO == null  ) TestGameObjectsUpdate();
	}

	public class NullCheck { public bool a = true; }
	float TestNullCheckSlow() {
		int a = 0;
		NullCheck instanceNull = null;
//		NullCheck instanceNotNull = new NullCheck();
		float time = Time.realtimeSinceStartup;
		for( int i = 0; i < count; i++ ) {
			if ( instanceNull == null ) { a++; } 
		}
		return Time.realtimeSinceStartup - time;
	}
	float TestNullCheckFast() {
		int a = 0;
		//NullCheck instanceNull = null;
		NullCheck instanceNotNull = new NullCheck();
		float time = Time.realtimeSinceStartup;
		for( int i = 0; i < count; i++ ) {
			if ( instanceNotNull == null ) { a++; } 
		}
		return Time.realtimeSinceStartup - time;
	}
	float TestNullCheckSlowObj() {
		//int a = 0;
		//NullCheck instanceNull = null;
		//NullCheck instanceNotNull = new NullCheck();
		float time = Time.realtimeSinceStartup;
		//for( int i = 0; i < count; i++ ) {
		//	if ( (object)instanceNull == null ) { a++; } 
		//}
		return Time.realtimeSinceStartup - time;
	}
	float TestNullCheckFastObj() {
		//int a = 0;
		//NullCheck instanceNull = null;
		///NullCheck instanceNotNull = new NullCheck();
		float time = Time.realtimeSinceStartup;
	//	bool b = false;
	//	for( int i = 0; i < count; i++ ) {
	//		b = !b;
	//		if (b) { a++; } 
//}
		return Time.realtimeSinceStartup - time;
	}
	void TestNullCheck() {
		NullCheck instanceNull = null;
		if ( !(instanceNull == null) ) throw new UnityException();
		if ( !((object)instanceNull == null) ) throw new UnityException();
		NullCheck instanceNotNull = new NullCheck();
		if ( instanceNotNull == null ) throw new UnityException();
		if ( (object)instanceNotNull == null ) throw new UnityException();

		Debug.LogWarning("TestNullCheck.Slow="+TestNullCheckSlow());
		Debug.LogWarning("TestNullCheck.Fast="+TestNullCheckFast());
		Debug.LogWarning("TestNullCheck.ObjSlow="+TestNullCheckSlowObj());
		Debug.LogWarning("TestNullCheck.ObjFast="+TestNullCheckFastObj());
	}

	void TestGameObjectsCreate() {
		array = new Array<Transform>(count);
		GameObject go;
		for( int i = 0; i < count; i++ ) {
			if ( testGO == null ) go = new GameObject();
			else go = (GameObject)Instantiate(testGO);
			go.transform.parent = transform;
			go.name = "go "+i;
			go.AddComponent<SpriteRenderer>();
			array.Add( go.transform );
		}
	}
	void TestGameObjectsUpdate() {
		for( int i = 0; i < count; i++ ) {
			Transform t = array.Get(i);
			SpriteRenderer sr = t.GetComponent<SpriteRenderer>();
			sr.color = new Color( 1,Random.value,1,1);
			//t.localPosition = Vector3.zero;
		}
	}

	int TestVector2iEqual() {
		Vector2i first = new Vector2i(10,10);
		Vector2i second = new Vector2i(-17,-17);
		int a = 0;
		for ( int i = 0; i < count; i++ ) if ( first.IsEqual(second) ) a++;
		return a;
	}

	int TestVector2Equal() {
		Vector2 first = new Vector2(10,10);
		Vector2 second = new Vector2(-17,-17);
		int a = 0;
		for ( int i = 0; i < count; i++ ) if ( first == second ) a++;
		return a;
	}

	int TestVector2iEqualSplit() {
		Vector2i first = new Vector2i(10,10);
		Vector2i second = new Vector2i(-17,-17);
		int a = 0;
		for ( int i = 0; i < count; i++ ) if ( first.x == second.x && first.y == second.y ) a++;
		return a;
	}

	int TestVector2EqualSplit() {
		Vector2 first = new Vector2(10,10);
		Vector2 second = new Vector2(-17,-17);
		int a = 0;
		for ( int i = 0; i < count; i++ ) if ( first.x == second.x && first.y == second.y ) a++;
		return a;
	}

	int TestVectorDirectEqual() {
		int x1 = 10, y1 = 10;
		int x2 = -17, y2 = -17;
		int a = 0;
		for ( int i = 0; i < count; i++ ) if ( x1 == x2 && y1 == y2 ) a++;
		return a;
	}

	int TestVector2iIsZero() {
		Vector2i first = new Vector2i(10,10);
//		Vector2i second = new Vector2i(-17,-17);
		int a = 0;
		for ( int i = 0; i < count; i++ ) if ( first.IsZero() ) a++;
		return a;
	}

	int TestVector2iEqualBool() {
		Vector2i first = new Vector2i(10,10);
		Vector2i second = new Vector2i(-17,-17);
		int a = 0;
		for ( int i = 0; i < count; i++ ) if ( first.IsEqualBool(second) ) a++;
		return a;
	}

	public struct Vector2iStruct {
		public int x, y;
		public Vector2iStruct (int x, int y) 	{ this.x = x; this.y = y; } 
		public static bool operator == (Vector2iStruct a, Vector2iStruct b)
		{	return a.x == b.x && a.y == b.y;	}
		public static bool operator != (Vector2iStruct a, Vector2iStruct b)
		{	return !(a.x == b.x && a.y == b.y);	}
		public override bool Equals (object t)
		{ return ((Vector2iStruct)t).x == x && ((Vector2iStruct)t).y == y; }
		public override int GetHashCode() { return new Vector2(x,y).GetHashCode(); }
	}

	int TestVector2iStructEqual() {
		Vector2iStruct first = new Vector2iStruct(10,10);
		Vector2iStruct second = new Vector2iStruct(-17,-17);
		int a = 0;
		for ( int i = 0; i < count; i++ ) if ( first == second ) a++;
		return a;
	}

	int TestVectorsEqual() {
		int a = 0;
		a += TestVector2iEqual();
		a += TestVector2Equal();
		a += TestVector2iEqualSplit();
		a += TestVector2EqualSplit();
		a += TestVectorDirectEqual();
		a += TestVector2iIsZero();
		a += TestVector2iEqualBool();
		a += TestVector2iStructEqual();
		return a;
	}

	int TestList() {
		List<Cell> array = new List<Cell>();
		int a = 0;
		a += ListSet(ref array);
		a += ListGet(ref array);
		return a;
	}

	private int ListSet(ref List<Cell> array) { 
		for ( int i = 0; i < count; i++ ) array.Add(new Cell(i)); return 1; }
	private int ListGet(ref List<Cell> array) {
		int a=0; for ( int i = 0; i < count; i++ ) a+=array[i].a; return a; }

	int TestIndexArray() {
		Cell[] array = new Cell[count];
		int a = 0;
		a += IndexArraySet(ref array);
		a += IndexArrayGet(ref array);
		a += IndexArraySizeGrow(ref array);
		return a;
	}

	private int IndexArraySet(ref Cell[] array) { 
		for ( int i = 0; i < count; i++ ) array[i] = new Cell(i); return 1; }
	private int IndexArrayGet(ref Cell[] array) {
		int a=0; for ( int i = 0; i < count; i++ ) a+=array[i].a; return a; }
	private int IndexArraySizeGrow(ref Cell[] array) {
		int a=0; array = new Cell[0]; 
		for ( int i = 1; i < count; i++ ) {
			Cell[] arrayT = new Cell[i];
			System.Array.Copy(array,arrayT,array.Length);
			//array.CopyTo(arrayT,0);
			array = arrayT;
			//a+=array[i]; 
		}
		return a; }

	int TestDictionary() {
		Dictionary<int, int> map = new Dictionary<int, int>(count); 
		int a = 0;
		a += DictionarySet(ref map);
		a += DictionaryGet(ref map);
		return a;
	}

	private int DictionarySet(ref Dictionary<int, int> map) { 
		for ( int i = 0; i < count; i++ ) map.Add(i,i); return 1; }
	private int DictionaryGet(ref Dictionary<int, int> map) { 
		int a=0; for ( int i = 0; i < count; i++ ) a+=map[i]; return a; }

}
