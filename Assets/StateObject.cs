﻿// State object for reading client data asynchronously
using System.Collections.Generic;
using System.Net.Sockets;
using UnityEngine;

[System.Serializable]
public class StateObject
{
    public StateObject(int index) { this.index = index; messageId = 0; }
    public int messages = 0;
    // status
    public string status = "";
    // index in list
    [System.NonSerialized] public int index;
    // Client main socket.
    public Socket listenerSocket = null;
    // Client  socket.
    public Socket workSocket = null;
    // Size of receive buffer.
    public const int BufferSize = 1024; //1024;
                                        // Receive buffer.
    [System.NonSerialized] public byte[] buffer = new byte[BufferSize];
    // Received data string.
    //	[System.NonSerialized] public string sb = "";  
    // remote ip address = "192.168.172.253";
    public string remoteIP = "";
    // message id
    public System.UInt16 messageId;
    public bool isConnected = false;
    public bool onSend = false;
    public Device device = null;
    [System.NonSerialized] public string sendSaved = "";
    [System.NonSerialized] public bool isNew = true;
    [System.NonSerialized] public bool isMustClosed = false;

    public void Close()
    {
        device = null;
        Debug.Log(this + " is closed");
        if (workSocket != null)
        {
            try
            {
                workSocket.Close();
                workSocket = null;
            }
            catch (System.Exception e) { Debug.LogError(this + ".Close: " + e.ToString()); }
        }
        messageId = 0;
        remoteIP = "";
        status = "Closed";
    }

    public override string ToString()
    {
        return "State[" + index + "=" + (remoteIP.Length == 0 ? "not connected" : remoteIP) + ", msgId=" + messageId + ", status=" + status + "]";
    }

    public bool IsConnected()
    {
        if (workSocket != null && workSocket.Connected) isConnected = true;
        else isConnected = false;
        if (workSocket != null && !workSocket.Connected && remoteIP.Length > 0) return false;
        return true;
    }
}
