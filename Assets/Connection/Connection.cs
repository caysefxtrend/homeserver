﻿
[System.Serializable]
public class Connection
{
	public int Id;
	public string Device;
	public string Component;

    public override string ToString()
    {
        return "id="+Id+" device="+Device+" comp="+Component;
    }
}