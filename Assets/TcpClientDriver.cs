﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Text;

public class TcpClientDriver : MonoBehaviour, IDriver
{

//	[System.Serializable]

	//public static TcpClientDriver instance = null;

	TcpClient mySocket;
	NetworkStream theStream;
	//	StreamWriter theWriter;
	//	StreamReader theReader;
	//public string Host = "192.168.19.128";
	public Device device;
	public string host = "";
	public int port = 0;

//	public string localhost = "10.0.1.12";
//	public string mac = "00-0E-C6-8F-BC-00";

	//byte[] data = new byte[1024];
	public Boolean isSocketReady = false;
	public bool isDisabled = false;
	public bool isInited = false;
	public bool isInConnect = false;
	public string state = "";
	public bool isShowLog = false;

	public List<string> commands = new List<string>();

	public float setConnectTimeout = 3f;
	public float setReadFrameTimeout = 1f;
	//	public bool isStatusSet = false;
//	public bool setReadWriteLog = true;
	public UInt16 transactionId = 0;
	public string lastWrite = "";
	public string lastRead = "";
	public int strangeZeroMessageBytes = 0;
	private bool isSelfDestroy = false;

	byte[] dataBuffer = new byte[1024];
//	public List<BeckhoffCommand> commands = new List<BeckhoffCommand>();

	//private List<byte> modbusPrefix = new List<byte>() { 0,0,0,0,0 }; 
	// Use this for initialization

	public override string ToString ()
	{
		return state;// string.Format ("[TcpClientDriver]");
	}


	public static TcpClientDriver Create(Device device,string ip, int port)
	{
		GameObject go = new GameObject();
		go.name = "TcpClientDriver "+ip+":"+port;
		go.transform.SetParent( Master.instance.gameObject.transform, false );
		TcpClientDriver driver = go.AddComponent<TcpClientDriver>();
		driver.Init( device, ip, port );
		return driver;
	}

	public void Init(Device device, string ip, int port)
	{
		this.device = device; this.host = ip; this.port = port;
		isInited = true;
		state = "Init";
	}

	public void Destroy()
	{
		//CloseSocket ();
		isSelfDestroy = true;
   		//GameObject.Destroy(gameObject);
	}

	public void SendCommand(string cmd, Device device)
	{
		if ( isInited && isSocketReady && !isInConnect ) commands.Add(cmd);
	}

//	void Awake() { TcpClientDriver.instance = this; }

	void Start () { }

	void OnEnable()
	{
		isDisabled = false;
		//StartCoroutine(OpenSocketCoroutine());
	//	StartCoroutine(ReadFrameCoroutine());
	}

	void OnDisable()
	{		
		//StopCoroutine(OpenSocketCoroutine());
		//	StopCoroutine(ReadFrameCoroutine());
		CloseSocket ();
		isDisabled = true;
	}

//	IEnumerator OpenSocketCoroutine ()
//	{
//		if ( isDisabled ) yield break;
//		if ( !isSocketReady ) OpenSocket();
//	//	if ( isSocketReady && !isInited ) WriteSocket (MakeCmd_ReadRegisters());
//		yield return new WaitForSeconds(setConnectTimeout);
//		StartCoroutine(OpenSocketCoroutine());
//		yield break;
//	}

//	IEnumerator ReadFrameCoroutine ()
//	{
//		if ( isDisabled ) yield break;
//		if ( isInited ) WriteSocket (MakeCmd_ReadFrame());
//		yield return new WaitForSeconds(setReadFrameTimeout);
//		StartCoroutine(ReadFrameCoroutine());
//		yield break;
//	}

	private float everySecond = 0;
	private float oneTenSecond = 0;
	// Update is called once per frame
	void Update ()
	{
		if ( isSelfDestroy ) { CloseSocket (); GameObject.Destroy(gameObject); }
		if ( Time.time - everySecond > 1f ) // every second
		{
			if ( isInited && !isSocketReady && !isInConnect ) OpenSocket();
			everySecond = Time.time;
		}
		if ( Time.time - oneTenSecond > 0.1f ) // 1/10 second
		{
			if ( isInited && isSocketReady && !isInConnect && commands.Count > 0 ) 
			{
				WriteSocket( commands[0] );
				if ( isShowLog )
					Debug.Log("Device "+device.GetValueByName("Название")+"."+device.GetValueByName("Комната")+" - driver connected to "+host+":"+port+", Write: "+commands[0]);
				if ( commands.Count > 0 ) commands.RemoveAt(0); // socket can be close and commands cleared
			}				
			oneTenSecond = Time.time;
		}


		string msg = ReadSocketString ();
		if ( msg.Length > 0 )
		{
			if ( isShowLog )
				Debug.Log("Device "+device.GetValueByName("Название")+"."+device.GetValueByName("Комната")+" - driver connected to "+host+":"+port+", Receive: "+msg);
				//Debug.Log(Time.time.ToString()+" : Receive: "+msg);
			DEFAULT.OnDriverReceiveString( device, msg );
			//ParseDataFromModbus( bytesReaded );
		}
	}

	public void OpenSocket ()
	{
		try {
			device.SetValueByName("0","подключено");
			if ( mySocket == null )
				mySocket = new System.Net.Sockets.TcpClient(AddressFamily.InterNetwork);
			transactionId ++;
			isInConnect = true;
			state = "in connect";
			mySocket.BeginConnect(host, port, 
				new AsyncCallback(ConnectCallback), mySocket);
		} catch (Exception e) {	Debug.LogError ("OpenSocket: "+host+":"+port+" error: " + e); }
	}

	public void ConnectCallback(IAsyncResult ar)
	{		
		TcpClient socket = (TcpClient) ar.AsyncState;
		isInConnect = false;
		state = "connect fail";
		socket.EndConnect(ar); // thread break if connection fail !
		theStream = socket.GetStream ();
		transactionId = 0;
		isSocketReady = true;	
		state = "connect success";
		device.SetValueByName("1","подключено");
		Debug.Log("Device "+device.GetValueByName("Название")+"."+device.GetValueByName("Комната")+" - driver connected to "+host+":"+port+", подключено="+device.GetValueByName("подключено"));
		commands.Clear();
		DEFAULT.OnDriverConnect( device );
	}

	public void CloseSocket ()
	{
		state = "socket closed";
		commands.Clear();
		//lastWrite = "TvDriver.CloseSocket: ["+transactionId+"] "+host+":"+port;
		//if (setReadWriteLog) Debug.Log(lastWrite);
		if (!isSocketReady) return;
		//theWriter.Close ();
		//theReader.Close ();
		if ( mySocket != null ) { mySocket.Close (); mySocket = null; }
		isSocketReady = false;
		transactionId = 0;
		device.SetValueByName("0","подключено");
		//DEFAULT.OnDriverDisconnect( device );
		//isInited = false;
	}

	public void WriteSocket (string cmd)
	{		
		if (!isSocketReady) return;
		if (!mySocket.Connected ) { Debug.Log("Socket is disconnected"); return; }
		try {	
			state = "send: "+cmd;
		//	Debug.Log(Time.time.ToString()+" : "+state);
			byte[] byteBuffer = Encoding.UTF8.GetBytes (cmd);
			theStream.Write(byteBuffer,0,byteBuffer.Length);
			theStream.Flush();
		} catch (Exception e) 
		{	
			Debug.Log ("Socket error(device="+device.GetValueByName("Название")+"): " + e); 
			CloseSocket();
		}
	}

//	public void WriteSocket (List<byte> cmd)
//	{		
//		if (!isSocketReady) return;
//		if (!mySocket.Connected ) { Debug.Log("Socket is disconnected"); return; }
//		if (cmd.Count == 0) { Debug.LogWarning("writeSocket: empty cmd"); return; }
//		try {	
//			byte[] byteBuffer = cmd.ToArray();
//			theStream.Write(byteBuffer,0,byteBuffer.Length);
//			string msg = "";
//			for(int i = 0; i < byteBuffer.Length; i++)
//				msg += byteBuffer[i].ToString()+", ";
//		//	if (setReadWriteLog) Debug.Log("writeSocket: '"+msg+"'");
//			lastWrite = msg;
//			theStream.Flush();
//		} catch (Exception e) 
//		{	
//			Debug.Log ("Socket error: " + e); 
//			CloseSocket();
//		}
//	}

	public string ReadSocketString ()
	{
		if (!isSocketReady || !mySocket.Connected) return "";
		if(theStream.CanRead && theStream.DataAvailable)
		{
		//	int numberOfBytesRead = 0;
		//	string receiveMsg = "";
			//byte[] data = new byte[1024];
			//return theStream.ReadByte().ToString();
			int numberOfBytesRead = theStream.Read(dataBuffer, 0, dataBuffer.Length);  
			if ( numberOfBytesRead == 0 ) { Debug.LogWarning("zero bytes received"); return ""; }
			return System.Text.Encoding.UTF8.GetString(dataBuffer, 0, numberOfBytesRead);
			//if ( numberOfBytesRead == 0 ) socketReady = false;
			//for(int i = 0; i < numberOfBytesRead; i++)
			//	receiveMsg += dataBuffer[i].ToString()+", ";
			//		Debug.Log("ReadSocket: ["+numberOfBytesRead+"] '" + receiveMsg+"', data.available="+theStream.DataAvailable.ToString());
			//lastRead = receiveMsg;
			//return numberOfBytesRead;
		}
		return "";
	}


}

