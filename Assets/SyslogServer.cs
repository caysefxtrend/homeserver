﻿using UnityEngine;
using System.Collections;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

// State object for reading client data asynchronously
using System.Collections.Generic;

[System.Serializable]
public class SLState {
	public SLState(int index) { this.index = index; messageId = 0; }
	// index in list
	public int index;
	// Client main socket.
	public UdpClient listenerSocket = null;
	// Client  socket.
	public Socket workSocket = null;
	// Size of receive buffer.
	public const int BufferSize = 1024; //1024;
	// Receive buffer.
	[System.NonSerialized] public byte[] buffer = new byte[BufferSize];
	// Received data string.
	[System.NonSerialized] public string sb = "";  
	// remote ip address = "192.168.172.253";
	public string remoteIP = "";
	// message id
	public UInt16 messageId;
	public bool isConnected = false;
	public bool onSend = false;
	public bool onReceive = false;
	// message queue
	//	public List<string> messages = new List<string>();
	public Timer closeTimer = null;
	public Device device = null;

	public void Close()
	{
		Debug.Log(this+".Close:");
		if ( workSocket != null )
		{
			try
			{
				//workSocket.Shutdown(SocketShutdown.Both);
				workSocket.Close();
			} catch (Exception e) {	Debug.LogError(this+".Close: "+e.ToString());}
		}
		messageId = 0;
		workSocket = null;
		remoteIP = "";
		sb = "";
	}

	public override string ToString ()
	{
		return string.Format ("State["+index+"="+(remoteIP.Length==0?"not connected":remoteIP)+", msgId="+messageId+"]");
	}

	public bool IsConnected()
	{
		if (workSocket!=null && workSocket.Connected ) isConnected = true;
		else isConnected = false;
		if (workSocket!=null && !workSocket.Connected && remoteIP.Length > 0 ) return false;
		return true;
	}
}

public class SyslogServer : MonoBehaviour
{
	public static SyslogServer instance;
	UdpClient listener;
	static bool isDisabled = false;
	public int listenPort = 514;
	//public ProtocolType protocolType = ProtocolType.Udp;
	public int maximumConnections = 1;
	public List<SLState> connections = new List<SLState>();
	private int stateIdCounter = 0;
	public bool setShowLog = false;
	public Encoding encoding = Encoding.UTF8;
	//public bool skipFrameAfterSend = true;
	public IPEndPoint ipEndPoint;
	//static Socket listener2;
	// Use this for initialization
	void Awake()
	{
		SyslogServer.instance = this;	
	}

	void Start ()
	{

		//string text = "";
		//text.
		//Debug.Log( encoding.GetString(state.buffer,0,bytesRead) );
	}

	// Update is called once per frame
	void Update ()
	{
		if ( connections.Count < maximumConnections && !isDisabled && listener != null)
			CreateNewConnection();
		// check connection
		for(int i = connections.Count-1; i>=0; i--)
			if ( connections[i].IsConnected() == false ) CloseConnetion(connections[i]);
	}

//	void FixedUpdate()
//	{
//		foreach(StateObject state in ClientServer.instance.connections)
//		{
//			if ( state.workSocket == null ) continue;
//			if ( state.workSocket.Connected == false ) continue;
//			if ( state.device == null ) continue;
//			if ( state.device.messages == null ) continue;
//			int count = state.device.messages.Count;
//			if ( count > 0 )
//			{
//				if ( skipFrameAfterSend ) count = 1;
//				for(int i = 0; i < count; i++)
//				{
//					if ( state.onSend ) break;
//					Send(state, "{"+state.messageId.ToString()+":"+state.device.messages[0]+"}");
//					//Send(state, state.device.messages[0]);
//					state.device.messages.RemoveAt(0);
//					state.messageId ++;
//				}
//			}
//		}
//	}

	void OnEnable ()
	{
		isDisabled = false;
		StartListening();
	}
	void OnDisable ()
	{
		isDisabled = true;
		//listener.DisconnectAsync((.Disconnect(true);
		//listener.Disconnect(false);
		//listener.Listen(0);
		//listener.Shutdown(SocketShutdown.Both);
		listener.Close();
		foreach(SLState state in connections)
			state.Close();
		connections.Clear();
		//		Debug.Log("OnDisable");
	}

	// Thread signal.
	//public static ManualResetEvent allDone = new ManualResetEvent(false);

	public static void CloseConnetion(SLState state)
	{
		SyslogServer.instance.connections.Remove(state);
		state.Close();
	}

	public void StartListening() {
		// Data buffer for incoming data.
		//byte[] bytes = new Byte[1024];
		try {
			// Establish the local endpoint for the socket.
//			IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
			// The DNS name of the computer
			// running the listener is "host.contoso.com".
			//IPHostEntry ipHostInfo = Dns.Resolve(Dns.GetHostName());
//			IPAddress ipAddress = ipHostInfo.AddressList[0];
//			string total = "";
//			foreach(IPAddress ip in ipHostInfo.AddressList) total+= ip+", ";
//			Debug.Log("ipHostInfo"+ipHostInfo+", ipAddress["+ipHostInfo.AddressList.Length+"]=["+total+"], IPAddress.Loopback="+IPAddress.Loopback+", IPAddress.Broadcast="+IPAddress.Broadcast);
		} catch (Exception ex) { Debug.LogWarning("Possibly not connected to network err="+ex); }
		try {
			ipEndPoint = new IPEndPoint(IPAddress.Any, listenPort);
		// Create a TCP/IP socket.
			listener = new UdpClient(ipEndPoint); // Socket(AddressFamily.InterNetwork, SocketType.Dgram, protocolType );

		// Bind the socket to the local endpoint and listen for incoming connections.

		//	listener.Bind(localEndPoint);
		//	listener.Listen(maximumConnections);
		} catch (Exception e) { Debug.LogError(e.ToString()); }
	}

	private void CreateNewConnection()
	{
		SLState state = new SLState(stateIdCounter); stateIdCounter++;
		state.listenerSocket = listener;
		connections.Add( state );
		try
		{
			//while (true) 
			// Set the event to nonsignaled state.
			//allDone.Reset();
			// Start an asynchronous socket to listen for connections.
			//	Debug.Log(state+" Waiting for a connection...");
			state.onReceive = true;
			listener.BeginReceive( new AsyncCallback(ReceiveCallback), state );
			// Wait until a connection is made before continuing.
			//allDone.WaitOne();
			//}
		} catch (Exception e) {	Debug.LogError(e.ToString());} // TODO проблема
	}

//	public static void AcceptCallback(IAsyncResult ar) {
//		// Signal the main thread to continue.
//		//	allDone.Set();
//		// Get the socket that handles the client request.
//		StateObject state = (StateObject) ar.AsyncState;
//		//Socket listener = state(Socket) ar.AsyncState;
//		state.workSocket = state.listenerSocket.EndAccept(ar);
//		IPEndPoint remoteEndPoint = (IPEndPoint)state.workSocket.RemoteEndPoint;
//		state.remoteIP = remoteEndPoint.Address.ToString();
//		state.device = Master.GetOrCreateClientByIP(state.remoteIP);
//		//remoteEndPoint.Address.ToString();
//		//		Debug.Log(state+".AcceptCallback: BeginReceive.");
//		state.onReceive = true;
//
//		state.workSocket.BeginReceive( state.buffer, 0, StateObject.BufferSize, 0,
//			new AsyncCallback(ReceiveCallback), state);
//	}

	public static void ReceiveCallback(IAsyncResult ar) {
		//string content = "";
		// Retrieve the state object and the handler socket from the asynchronous state object.
		SLState state = (SLState) ar.AsyncState;
		UdpClient listener = state.listenerSocket;
		//Socket handler = state.workSocket;
		string message = "";
		// Read data from the client socket. 
		//IPEndPoint endPoint = listener.Client.RemoteEndPoint;
		byte[] bytes = listener.EndReceive(ar, ref SyslogServer.instance.ipEndPoint);
		state.onReceive = false;
		if (bytes.Length <= 0) 
		{ 
			// thats mean connection is somehow broken
			Debug.Log(state+"SyslogServer.ReceiveCallback: bytesRead="+bytes.Length+", CloseConnection of state="+state.ToString());
			CloseConnetion(state);
		}
		else {
			//Debug.Log("[3]="+state.buffer[3].ToString());
			message = SyslogServer.instance.encoding.GetString(bytes,0,bytes.Length);
			// There  might be more data, so store the data received so far.
			state.sb += message;
			// Check for end-of-file tag. If it is not there, read 
			// more data.
			//content = state.sb;
			//	string text = "";
			//	for( int i = 0; i < bytesRead; i++ ) text+=state.buffer[i].ToString()+", ";
			//	int endIndex = message.IndexOf(';');
			string end = SyslogServer.instance.ipEndPoint.ToString();
			if (SyslogServer.instance.setShowLog)
				Debug.Log("ReceiveCallback: ("+end+") msg["+bytes.Length+"]: "+message);
			//int endIndex = message.IndexOf(';') > 0 )
			//	if ( endIndex >= 0 )
			//	{
			//	int index = state.sb.IndexOf(';');
			//	string command = state.sb.Substring(0, index+1-1); // -1 coz skip ';'
//			ClientControl.IncomeCommand(state.device, state.remoteIP, state.sb);
			//Debug.Log("Execute command["+state.remoteIP+"]='"+command+"'");
			//		state.sb = state.sb.Substring(index+1, state.sb.Length-index-1);
			state.sb = "";
			//	}						
			// continue receiving
			state.onReceive = true;
			listener.BeginReceive( new AsyncCallback(ReceiveCallback), state );
//			listener.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
//				new AsyncCallback(ReceiveCallback), state);

		}
	}

	//	public static void Send(string remoteIP, string message)
	//	{
	//		foreach(StateObject state in ClientServer.instance.connections)
	//		{
	//			if ( state.remoteIP == remoteIP )
	//			{
	//				//Send(state, state.messageId.ToString()+":"+message+";");
	//				state.messages.Add("{"+state.messageId.ToString()+":"+message+"}");
	//
	//				//Send(state, "#"+state.messageId.ToString()+":"+message+"");
	//				state.messageId ++;
	//				return;
	//			}
	//		}
	////		Debug.LogWarning("ClientServer.Send: cant find connection for remoteIP="+remoteIP+", message="+message);
	//	}

//	private static void OnTimer(object obj)
//	{
//		StateObject state = (StateObject)obj;
//		Debug.LogWarning("On send timeout(1sec) - state="+state);
//		ClientServer.CloseConnetion( state );
//	}

//	private static void Send(StateObject state, String data) {
//		state.onSend = true;
//		state.closeTimer = new Timer(ClientServer.OnTimer,state,1000,Timeout.Infinite);
//		//state.closeTimer.
//		//	if ( state.workSocket.Connected == false ) return;
//		if (ClientServer.instance.setShowLog)
//			Debug.Log(state+".Send: data="+data);
//		// Convert the string data to byte data using encoding.
//		byte[] byteData = ClientServer.instance.encoding.GetBytes(data);
//
//		//		string test2 = ClientServer.instance.encoding.GetString(byteData,0,byteData.Length);
//		//	char[] test1 = test2.ToCharArray();
//		//	for(int i = 0; i < test1.Length; i++)
//		//		if ( test1[i] < 32 ) Debug.LogWarning("Send wrong state="+state+", msg="+data);
//		//Timer timer = new Timer();
//		// Begin sending the data to the remote device.
//		state.workSocket.BeginSend(byteData, 0, byteData.Length, 0,
//			new AsyncCallback(SendCallback), state);
//	}
//
//	private static void SendCallback(IAsyncResult ar) {
//		try {
//			StateObject state = (StateObject) ar.AsyncState;
//			state.closeTimer.Dispose();
//			Socket handler = state.workSocket;
//			if ( handler == null ) {
//				ClientServer.CloseConnetion( state );
//				return;
//			}
//			// Complete sending the data to the remote device.
//			int bytesSent = handler.EndSend(ar);
//			if (ClientServer.instance.setShowLog)
//				Debug.Log(state+".Sent "+bytesSent+" bytes to client." );
//			state.onSend = false;
//		} catch (Exception e) {
//			Debug.LogException(e);
//			StateObject state = (StateObject) ar.AsyncState;
//			ClientServer.CloseConnetion( state );
//		}
//	}

	public static bool IsClientIpOnline(string remoteIP)
	{
		foreach(SLState state in SyslogServer.instance.connections)
		{
			if ( state.isConnected && state.remoteIP == remoteIP ) return true;
		}
		return false;
	}

}