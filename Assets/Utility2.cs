﻿using UnityEngine;
using System.Collections;

public class Utility2 
{
	public static void SelfTest()
	{
		string rbgBase = "175.1.255";
		int rgbInt = RgbStringToInt(rbgBase);
		string rgbString = IntToRgbString(rgbInt);
		if ( rgbString != "168.0.248" ) Debug.LogError("FAIL");
//		Debug.Log("Utility.SelfTest: rbgBase="+rbgBase+", rgbInt="+rgbInt+", rgbString="+rgbString);
	}

	public static string IntToRgbString(int value)
	{
		int red = ((value>>10) & 0x1f)<<3;
		int green = ((value>>5) & 0x1f)<<3;
		int blue = ((value) & 0x1f)<<3;
		return red.ToString()+"."+green.ToString()+"."+blue.ToString();
	}

	public static int RgbStringToInt(string color)
	{
		string[] colors = color.Split('.');
		int red = int.Parse( colors[0] );
		int green = int.Parse( colors[1] );
		int blue = int.Parse( colors[2] );
		red = red >> 3;
		green = green >> 3;
		blue = blue >> 3;
		var value = (red<<10)+(green<<5)+(blue);
		return value;
	}


}

