﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Ping server.
/// 0.03 секунд занимает проход от пингсервера до реальной отправки
/// </summary>

public class PingServer : MonoBehaviour {
	public static PingServer instance;

	public class PingBody
	{
		public Device device;
		public float every = 0;
		public float timer = 0;
		public int index = 0;
		public bool isActive = false;
		public List<string> returns = new List<string>();

		public PingBody(Device device) { this.device = device; Init(); }
		public void Init()
		{
			//timers[index] = System.DateTime.Now.Millisecond;
			timer = 0;
			every = 0.001f * float.Parse( device.GetValueByName("Пингов в мс") );
			isActive = device.GetValueByName("Пинг") == "1";
			if ( device.GetValueByName("Пингов в мс") == "0" ) isActive = false;
			//every = int.Parse( device.GetValueByName("Пингов в мс") );
		}
		public void PingReturn( string id )
		{
			returns.Add(id);
		}
	}

	[SerializeField] List<PingBody> pings = new List<PingBody>();
	public bool logSend = true;
	public bool logReturn = true;

//	[SerializeField] ConcurrentList<Device> devices = new ConcurrentList<Device>();
////	[SerializeField] ConcurrentList<float> everyMs = new ConcurrentList<float>();
////	[SerializeField] ConcurrentList<float> timers = new ConcurrentList<float>();
//	[SerializeField] ConcurrentList<int> everyMs = new ConcurrentList<int>();
//	[SerializeField] ConcurrentList<int> timers = new ConcurrentList<int>();

	void Awake() { PingServer.instance = this; }
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		int max = pings.Count;
		PingBody body = null;
		for( int i = 0; i < max && i < pings.Count; i++)
		{
			try
			{
				body = pings[i];
				if ( !body.isActive ) continue;
				//if ( body.every == 0 ) continue;
				//Device device = devices[i];
				//float every = everyMs[i];
				//float timer = timers[i];
				//Check(devices[i], everyMs[i], timers[i], i);
				float time = Time.time;
				if ( time-body.timer > body.every   ) 
				{
					//SendPing(device, time, timer, every);
					if ( logSend )
					Debug.Log("SendPing: device="+body.device.GetValueByName("Название")+", time="+time+", timer="+body.timer+", every="+body.every);
					ClientControl.AddCmd(body.device, null, null, Device.Command.Ping, time.ToString());

					body.timer = time;
				}
				if ( body.returns.Count > 0 )
				{
					int returnsMax = body.returns.Count;
					for(int j = 0; j < returnsMax; j++ )
					{
						float delta = time - float.Parse( body.returns[0] );
						if ( logReturn )
						Debug.Log("PingReturn: device="+body.device.GetValueByName("Название")+", id="+body.returns[0]+", time="+time+", delta="+delta );
						body.returns.RemoveAt(0);
					}
				}
			}
			catch(System.Exception exc)
			{
				Debug.LogError("Ping exc="+exc);
				break;
			}				
		}	
	}

//	void Check(Device device, int every, int timer, int index)
//	{
//		//float time = Time.time;
//		int time = System.DateTime.Now.Millisecond;
//		if ( every != 0 && time-timer > every   ) 
//		{
//			SendPing(device, time, timer, every);
//			timers[index] = time;
//		}
//	}

//	private void SendPing(Device device, int time, int timer, int every)
//	{
//		Debug.Log("SendPing: device="+device.GetValueByName("Название")+", time="+time+", timer="+timer+", every="+every);
//		ClientControl.AddCmd(device, null, null, Device.Command.Ping, time.ToString());
//	}

	public static void PingReturn(Device device, string id)
	{
		//float time = Time.time;
		PingBody body = null;
		foreach(PingBody iterator in PingServer.instance.pings) if ( iterator.device == device ) { body = iterator;break; }
		if ( body == null ) { device.SetValueByName("0","Пинг"); return; }
		body.PingReturn( id );
		//Debug.Log("PingReturn: device="+device.GetValueByName("Название")+", id="+id+", time="+ );
	}

	public static void OnChangePingOfDevice(Device device)
	{
		PingServer.instance.LocalOnChangePingOfDevice(device);
	}

	void LocalOnChangePingOfDevice(Device device)
	{
		string ping = device.GetValueByName("Пинг");
		Debug.Log("PingServer: client="+device.GetValueByName("Название")+", Пинг="+ping);
//		int index = 0;
		PingBody body = null;
		foreach(PingBody iterator in pings) if ( iterator.device == device ) { body = iterator;break; }
//		int index = devices.IndexOf( device );
		if ( ping == "0" && body != null )
		{
			body.isActive = false;
		}
		else if ( ping == "1" )
		{
			if ( body == null )
			{
				pings.Add( new PingBody( device ) );
				//devices.Add( device );
				//timers.Add( 0.001f * (float)System.DateTime.Now.Millisecond );
				//timers.Add( System.DateTime.Now.Millisecond );
				//everyMs.Add( 0.001f * float.Parse( device.GetValueByName("Пингов в мс") ) );
				//everyMs.Add( int.Parse( device.GetValueByName("Пингов в мс") ) );
			}
			else
			{   
				body.Init();
				//timers[index] = System.DateTime.Now.Millisecond;
//				timers[index] = Time.time;
				//everyMs[index] = 0.001f * float.Parse( device.GetValueByName("Пингов в мс") );
				//everyMs[index] = int.Parse( device.GetValueByName("Пингов в мс") );
			}				
		}
	}
}
