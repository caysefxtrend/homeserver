﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Net.Sockets;
using System.Collections.Generic;

public enum BeckhoffItems 
{
	NONE=0,
	DIMMER=1,
	TRIGGER=2,
	UBIQ=3,
	MS=4,
	SB=5,
	CURTAINS=6,
	ZONE=7,
	DMX=8,
	DENON_AVR=9,
	WINTEK=10,
	HVAC=11,
	LOCK=12,
	BTRIGGER=13,
	LG_TV=14,
	amount=15
};

public class ModbusClient : MonoBehaviour
{
	enum ModbusTransportCommands
	{
		mtcGet=0,  mtcSet=1
	};
	enum ModbusFunctions 
	{
		readCoils= +1,
		readInputCoils= +2,
		readRegisters= +3,
		readInputRegisters= +4,
		writeCoil= +5,
		writeRegister= +6,
		writeRegisters= 0x10
	};

	[System.Serializable]
	public struct BeckhoffInit
	{
		public int transportAmount;
		public int sizeOfElement;
		public int inputOffset;
		public int outputOffset;
		public int dataBufferSize;
		public int dataBufferOffset;
	}

	[System.Serializable]
	public struct BeckhoffFrame
	{
		public int id;
		// main
		public int status;
		// input
		public int inputStatus;
		public int inputCommand;
		public int inputAddressType;
		public int inputAddressIndex;
		public int inputValue;
		// output
		public int outputStatus;
		public int outputCommand;
		public int outputAddressType;
		public int outputAddressIndex;
		public int outputValue;
	}

	[System.Serializable]
	public struct BeckhoffCommand
	{
		public int inputCommand;
		public int inputAddressType;
		public int inputAddressIndex;
		public int inputValue;
	}

	public static ModbusClient instance = null;

	TcpClient mySocket;
	NetworkStream theStream;
//	StreamWriter theWriter;
//	StreamReader theReader;
	public string Host = "192.168.19.128";
	//public string Host = "10.0.1.20";
	public Int16 Port = 502;
	//byte[] data = new byte[1024];
	public Boolean isSocketReady = false;
	public bool isDisabled = false;
	public bool isInited = false;
	public float setConnectTimeout = 3f;
	public float setReadFrameTimeout = 0.1f;
//	public bool isStatusSet = false;
	public bool setReadWriteLog = false;

	public bool setTestFlood = false; 
	//public void ToggleFlood() { setTestFlood = !setTestFlood; Debug.LogWarning("ToggleFlood="+setTestFlood); }
	public bool SetTestFlood
	{ 
		get { 
			Debug.LogWarning("SetTestFlood get");
			if ( ModbusClient.instance == null ) return false; 
			else return ModbusClient.instance.setTestFlood; 
		} 
		set {
			Debug.LogWarning("SetTestFlood set="+value);
			if ( ModbusClient.instance != null ) ModbusClient.instance.setTestFlood = value; 
		}
	}
	public UInt16 transactionId = 0;
	public string lastWrite = "";
	public string lastRead = "";
	public int strangeZeroMessageBytes = 0;
	public BeckhoffInit beckhoffInit;
	public BeckhoffFrame frame = new BeckhoffFrame();
	byte[] dataBuffer = new byte[1024];
	public static List<BeckhoffCommand> commands = new List<BeckhoffCommand>();

	private List<byte> modbusPrefix = new List<byte>() { 0,0,0,0,0 }; 
	// Use this for initialization
	void Awake () 
	{
		ModbusClient.instance = this;
	}

	void Start ()	{	}

	void OnEnable()
	{
		isDisabled = false;
		StartCoroutine(OpenSocketCoroutine());
		StartCoroutine(ReadFrameCoroutine());
	}

	void OnDisable()
	{		
	//	StopCoroutine(OpenSocketCoroutine());
	//	StopCoroutine(ReadFrameCoroutine());
		CloseSocket ();
		isDisabled = true;
	}

	IEnumerator OpenSocketCoroutine ()
	{
		if ( isDisabled ) yield break;
		if ( !isSocketReady ) OpenSocket();
		if ( isSocketReady && !isInited ) WriteSocket (MakeCmd_ReadRegisters());
		yield return new WaitForSeconds(setConnectTimeout);
		StartCoroutine(OpenSocketCoroutine());
		yield break;
	}

	IEnumerator ReadFrameCoroutine ()
	{
		if ( isDisabled ) yield break;
		if ( isInited ) WriteSocket (MakeCmd_ReadFrame());
		yield return new WaitForSeconds(setReadFrameTimeout);
		StartCoroutine(ReadFrameCoroutine());
		yield break;
	}

	
	// Update is called once per frame
	void Update ()
	{
		
		int bytesReaded = ReadSocket ();
		if ( bytesReaded > 0 )
		{
			ParseDataFromModbus( bytesReaded );
		}
		else 
			if ( setTestFlood )
				DEFAULT.OnReceiveMessageFromModbus(8, 3, UnityEngine.Random.Range(0,65535));
			
	}

	public void OpenSocket ()
	{
		try {
			if ( mySocket != null ) { mySocket.Close (); mySocket = null; }
			if ( mySocket == null )
				mySocket = new TcpClient(AddressFamily.InterNetwork);
			transactionId ++;
			//string my = mySocket.Client.LocalEndPoint.ToString();
			lastWrite = "BeginConnect["+transactionId+"] "+Host+":"+Port;
			if (setReadWriteLog) Debug.Log(lastWrite);
			mySocket.BeginConnect(Host, Port, 
				new AsyncCallback(ConnectCallback), mySocket);
			//mySocket = new TcpClient (Host, Port);
			//mySocket.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);
		//	theStream = mySocket.GetStream ();
			//theWriter = new StreamWriter (theStream);
			//theReader = new StreamReader (theStream);
		//	isSocketReady = true;
		} catch (Exception e) {	Debug.Log ("Modbus.OpenSocket: "+Host+":"+Port+" error: " + e); }
	//	transactionId = 0;
	}

	public void ConnectCallback(IAsyncResult ar)
	{
		lastWrite = "EndConnect["+transactionId+"] "+Host+":"+Port;
		if (setReadWriteLog) Debug.Log(lastWrite);
		TcpClient socket = (TcpClient) ar.AsyncState;
		transactionId = 0;
		if (socket.Client == null)
		{
			isSocketReady = false;	
			return;
		}
			
		socket.EndConnect(ar);
	//	Debug.LogWarning("Mdobus.ConnectCallback.after endconnect");
		theStream = socket.GetStream ();
		if ( !mySocket.Connected ) Debug.LogError("ModbusClient.ConnectCallback: mySocket.Connected="+mySocket.Connected);
		//Debug.LogWarning("Mdobus.ConnectCallback.after endconnect 2");
		// There possibility about we connected but connection is fake
		// only answer from beckhoff tell us what is real
//		Master.SetPar("Сервер","Бекоф подключён", "да" );	
//		Master.SetPar("Сервер","Сообщений от бекофа", "0" );
//		Master.InitializeDevices();
		//Debug.LogWarning("Mdobus.ConnectCallback.after endconnect 3");
	}

//	public static void ConnectCallback(IAsyncResult ar) {
//		ModbusClient.instance.lastWrite = "Modbus.ConnectCallback: EndConnect["+ModbusClient.instance.transactionId+"] "+ModbusClient.instance.Host+":"+ModbusClient.instance.Port;
//		if (ModbusClient.instance.setReadWriteLog) Debug.Log(ModbusClient.instance.lastWrite);
//		TcpClient socket = (TcpClient) ar.AsyncState;
//		socket.EndConnect(ar);
//		ModbusClient.instance.theStream = socket.GetStream ();
//		ModbusClient.instance.transactionId = 0;
//		ModbusClient.instance.isSocketReady = true;
//		ModbusClient.instance.isSocketReady = true;
//		//Debug.Log("Modbus.ConnectCallback:");
//	}
//
	public void CloseSocket ()
	{
		lastWrite = "Modbus.CloseSocket: ["+transactionId+"] "+Host+":"+Port;
		if (setReadWriteLog) Debug.Log(lastWrite);
		if (!isSocketReady) return;
		//theWriter.Close ();
		//theReader.Close ();
		mySocket.Close ();
		mySocket = null;
		isSocketReady = false;
		transactionId = 0;
		isInited = false;
		Master.SetBeckhoffStatus(false);
		//Master.SetPar("Сервер","Бекоф подключён", "0" );			
	}

	public void WriteSocket (List<byte> cmd)
	{		
		if (!isSocketReady) return;
		if (!mySocket.Connected ) {
			Debug.Log("Socket is disconnected"); 
			CloseSocket();
			return; 
		}
		if (cmd.Count == 0) { Debug.LogWarning("writeSocket: empty cmd"); return; }
		try {	
		//String foo = theLine + "\r\n";
		//	Debug.Log("write '"+foo+"'");
		//theWriter.Write (foo);

		//	char[] buffer = new char[]{(char)0,(char)0,(char)0,(char)0,(char)0,(char)6,(char)0,(char)4,
		//		(char)128,(char)0,(char)0,(char)6};
			byte[] byteBuffer = cmd.ToArray();
			theStream.Write(byteBuffer,0,byteBuffer.Length);
			//theWriter.Write (buffer);
			string msg = "";
			for(int i = 0; i < byteBuffer.Length; i++)
				msg += byteBuffer[i].ToString()+", ";
			if (setReadWriteLog) Debug.Log("writeSocket: '"+msg+"'");
			lastWrite = msg;
			theStream.Flush();
		//theWriter.Flush ();
		} catch (Exception e) 
		{	
			HandleLog.LogBeckhoff ("UnityServer: Close Socket coz: " + e); 
			CloseSocket();
		}
	}

	public int ReadSocket ()
	{
		if (!isSocketReady || !mySocket.Connected) return 0;
		if(theStream.CanRead && theStream.DataAvailable)
		{
			int numberOfBytesRead = 0;
			string receiveMsg = "";
			//byte[] data = new byte[1024];
			//return theStream.ReadByte().ToString();
			numberOfBytesRead = theStream.Read(dataBuffer, 0, dataBuffer.Length);  
			if ( numberOfBytesRead == 0 ) { Debug.LogWarning("zero bytes received"); return 0; }
			 //= System.Text.Encoding.ASCII.GetString(data, 0, numberOfBytesRead);
			//if ( numberOfBytesRead == 0 ) socketReady = false;
			for(int i = 0; i < numberOfBytesRead; i++)
				receiveMsg += dataBuffer[i].ToString()+", ";
	//		Debug.Log("ReadSocket: ["+numberOfBytesRead+"] '" + receiveMsg+"', data.available="+theStream.DataAvailable.ToString());
			lastRead = receiveMsg;
			return numberOfBytesRead;
		}
		return 0;
	}

	private void ParseDataFromModbus( int bytesReaded )
	{
		byte[] buffer = dataBuffer;
		int tId = buffer[0]*256+buffer[1]; // Transaction identifier 	2 	For synchronization between messages of server & client
		int pId = buffer[2]*256+buffer[3]; // Protocol identifier 	2 	Zero for Modbus/TCP
		int len = buffer[4]*256+buffer[5]; // Length field 	2 	Number of remaining bytes in this frame
		int uId = buffer[6]; // Unit identifier 	1 	Slave address (255 if not used)
		int fn = buffer[7]; // Function code 	1 	Function codes as in other variants
		if ( fn >= 128 ) { Debug.LogError("Modbus fn="+(fn-128)+" ,error="+buffer[8]); return; }
		int bytesInData = buffer[8];
		string info = "tId="+tId+", pId="+pId+", len="+len+", uId="+uId+", fn="+fn+", bytes="+bytesInData;
		info +=", data";
		for(int i = 9; i < buffer.Length && i-9 < bytesInData; i++ )
			info +=", "+buffer[i];
		
		// Data bytes 	n 	Data as response or commands
		if ( fn == (int)ModbusFunctions.readInputRegisters )
		{
			if ( len != bytesInData + 3 ) Debug.LogWarning("wrong message from beckhoff");
			if ( bytesInData != 12 ) Debug.LogWarning("wrong message from beckhoff");
			int transportAmount = buffer[9]*256+buffer[10];
			int sizeOfElement = buffer[11]*256+buffer[12];
			int inputOffset = buffer[13]*256+buffer[14];
			int outputOffset = buffer[15]*256+buffer[16];
			int dataBufferSize = buffer[17]*256+buffer[18];
			int dataBufferOffset = buffer[19]*256+buffer[20];
			//Debug.Log("Pars="+tId+", pId="+pId+", len="+len+", uId="+uId+", fn="+fn+", buffer.len="+buffer.Length);
			beckhoffInit.transportAmount = transportAmount;
			beckhoffInit.sizeOfElement = sizeOfElement;
			beckhoffInit.inputOffset = inputOffset;
			beckhoffInit.outputOffset = outputOffset;
			beckhoffInit.dataBufferSize = dataBufferSize;
			beckhoffInit.dataBufferOffset = dataBufferOffset;
			if ( beckhoffInit.transportAmount > 0 ) 
			{ 
				info ="Init success! " +info; 
				isInited = true; 
				Master.SetBeckhoffStatus(true);
				//Debug.Log("ModbusClient: Beckhoff подключён к unity-server!");
				//Master.SetPar("Сервер","Бекоф подключён", "1" );	
				//Master.SetPar("Сервер","Сообщений от бекофа", "0" );
				//Master.InitializeDevices();
			}
			else info ="Init unsuccess! "+info;
		}
		else if ( fn == (int)ModbusFunctions.readRegisters )
		{
			if ( len != bytesInData + 3 ) Debug.LogWarning("wrong message from beckhoff");
			if ( bytesInData != 238 ) Debug.LogWarning("wrong message from beckhoff: bytesInData="+bytesInData+"!=238");
			int frameStatus = buffer[9]*256+buffer[10];
			if ( frameStatus != 0xFFFF ) info="Frame unready";
			else 
			{
			// read frame
			frame.id = tId;
			frame.status = buffer[9]*256+buffer[10];
			frame.inputStatus = buffer[11]*256+buffer[12];
			frame.inputCommand = buffer[13]*256+buffer[14];
			frame.inputAddressIndex = buffer[15]*256+buffer[16]; // dword=right word then left word
			frame.inputAddressType = buffer[17]*256+buffer[18];
			frame.inputValue = buffer[19]*256+buffer[20];
			int output = 9+beckhoffInit.outputOffset;
			frame.outputStatus = buffer[output]*256+buffer[output+1];
			frame.outputCommand = buffer[output+2]*256+buffer[output+3];
			frame.outputAddressIndex = buffer[output+4]*256+buffer[output+5];
			frame.outputAddressType = buffer[output+6]*256+buffer[output+7];
			frame.outputValue = buffer[output+8]*256+buffer[output+9];
			info ="Read frame success! "+info;
			}
		}
		else if ( fn == (int)ModbusFunctions.writeRegisters )
		{
			info ="Write frame success! "+info;
		}
		else if ( fn == 0 && tId == 0 && len==0 )
		{
			strangeZeroMessageBytes += bytesReaded; return;
		}
		else 
		{
			info ="Unknown function! "+info;
			Debug.LogWarning("Parse["+bytesReaded+" bytes]: "+info);
		}
		if (setReadWriteLog) Debug.Log("Parse["+bytesReaded+"]: "+info);
		lastRead = info;
		if ( fn == (int)ModbusFunctions.readRegisters )
		{
			ProcessInputOutput();
		}
	}

	private void ProcessInputOutput()
	{
		bool sendInput = false;
		bool sendOutput = false;
		if ( frame.inputStatus == 1 )
		{
			// out last message was not processed - need wait more
			return;
		}
		if ( frame.status != 0xffff )
		{
			// beckhoff unready to work with us
			return;
		//	frame.status = 0xffff;
		//	sendInput = true;
		}
		if ( frame.inputStatus == 0 )
		{
			// we can send another message
			if ( commands.Count > 0 )
			{
				BeckhoffCommand cmd = commands[0];
				commands.RemoveAt(0);
				frame.inputStatus = 1;
				frame.inputCommand = cmd.inputCommand;
				frame.inputAddressIndex = cmd.inputAddressIndex;
				frame.inputAddressType = cmd.inputAddressType;
				frame.inputValue = cmd.inputValue;
				sendInput = true;
			}
		}
		if ( frame.outputStatus == 1 )
		{
			// new message received
			sendOutput = true;
			DEFAULT.OnReceiveMessageFromModbus(frame.outputAddressType, frame.outputAddressIndex, frame.outputValue);
			frame.outputStatus = 0;
			frame.outputCommand = 0;
			frame.outputAddressType = 0;
			frame.outputAddressIndex = 0;
			frame.outputValue = 0;
		}
		if ( sendInput || sendOutput )
		{
			WriteSocket (MakeCmd_WriteFrame());
		}
	}

	private List<byte> MakeCmd_ReadRegisters()
	{
		List<byte> cmd = new List<byte>( modbusPrefix );
		cmd.Add(6); // Number of remaining bytes in this frame
		cmd.Add(255); // Slave address (255 if not used)
		cmd.Add((byte)ModbusFunctions.readInputRegisters); // 4 Function codes as in other variants
		cmd.Add(128); // Address of first register to read (16-bit) = 0x8000 beckhoff
		cmd.Add(0);
		cmd.Add(0); // Number of registers to read (16-bit)
		cmd.Add(6);
		return cmd;
	}

	private List<byte> MakeCmd_WriteFrame()
	{
		//List<byte> cmd = new List<byte>( modbusPrefix );
		List<byte> cmd = new List<byte>();// modbusPrefix );
		cmd.AddWord( transactionId ); // Transaction identifier 	2 	For synchronization between messages of server & client
		transactionId ++;
		cmd.AddWord( 0 ); // Protocol identifier 	2 	Zero for Modbus/TCP
		cmd.AddWord( 6 ); // Length field 	2 	Number of remaining bytes in this frame
		cmd.AddByte( 255 ); // Slave address (255 if not used)
		cmd.AddByte((int)ModbusFunctions.writeRegisters); // 3 Function codes as in other variants
		int address = 0x3000;//+beckhoffInit.inputOffset/2;
		cmd.AddWord( address ); // Address of first register to read (16-bit) = 0x3000 beckhoff
		int size = 1+5+beckhoffInit.dataBufferSize/2+5;
		cmd.AddWord( size ); // Number of holding registers to preset/write (16-bit)
		cmd.AddByte( size*2 ); // Number of bytes of register values to follow (8-bit)
		cmd.AddWord( frame.status );
		cmd.AddWord( frame.inputStatus );
		cmd.AddWord( frame.inputCommand );
		cmd.AddWord( frame.inputAddressIndex );
		cmd.AddWord( frame.inputAddressType );
		cmd.AddWord( frame.inputValue );
		for(int i = 0; i < beckhoffInit.dataBufferSize; i++ )
			cmd.AddByte( 0 );
		cmd.AddWord( frame.outputStatus );
		cmd.AddWord( frame.outputCommand );
		cmd.AddWord( frame.outputAddressIndex );
		cmd.AddWord( frame.outputAddressType );
		cmd.AddWord( frame.outputValue );

		cmd[5]= (byte)(cmd.Count-6);

		return cmd;
	}

	private List<byte> MakeCmd_ReadFrame()
	{
		//List<byte> cmd = new List<byte>( modbusPrefix );
		List<byte> cmd = new List<byte>();// modbusPrefix );
		cmd.AddWord( transactionId ); // Transaction identifier 	2 	For synchronization between messages of server & client
		transactionId ++;
		cmd.AddWord( 0 ); // Protocol identifier 	2 	Zero for Modbus/TCP
		cmd.AddWord( 6 ); // Number of remaining bytes in this frame
		cmd.Add( 255 ); // Slave address (255 if not used)
		cmd.Add((byte)ModbusFunctions.readRegisters); // Function codes as in other variants
		int address = 0x3000;//+beckhoffInit.inputOffset/2;
		cmd.AddWord( address ); // Address of first holding register to preset/write (16-bit) = 0x3000 beckhoff
		int size = beckhoffInit.sizeOfElement / 2;
		cmd.AddWord( size ); // Number of registers to read (16-bit)
		return cmd;
	}

	public static void SendGet(BeckhoffItems type, int position)
	{
		if ( !ModbusClient.instance.isInited ) return;
//		ModbusClient modbus = ModbusClient.instance;
		//if ( ! modbus.isInited ) return;
		//List<byte> cmd = new List<byte>( modbusPrefix );
		BeckhoffCommand cmd = new BeckhoffCommand();
		cmd.inputCommand = (int)ModbusTransportCommands.mtcGet;
		cmd.inputAddressType = (int)type;
		cmd.inputAddressIndex = position;
		HandleLog.LogBeckhoff( "Modbus.SendGet: cmd="+cmd.inputCommand+", type="+cmd.inputAddressType+
			", index="+cmd.inputAddressIndex);
		ModbusClient.commands.Add( cmd );
	}

	public static void SendSet(BeckhoffItems type, int position, int value)
	{
//		ModbusClient modbus = ModbusClient.instance;
		//if ( ! modbus.isInited ) return;
		//List<byte> cmd = new List<byte>( modbusPrefix );
		BeckhoffCommand cmd = new BeckhoffCommand();
		cmd.inputCommand = (int)ModbusTransportCommands.mtcSet;
		cmd.inputAddressType = (int)type;
		cmd.inputAddressIndex = position;
		cmd.inputValue = value;
		HandleLog.LogBeckhoff( "Modbus.SendSet: cmd="+cmd.inputCommand+", type="+cmd.inputAddressType+
			", index="+cmd.inputAddressIndex+", value="+cmd.inputValue);
		ModbusClient.commands.Add( cmd );
	}

}

