﻿using UnityEngine;
using System.Collections;

public interface IDriver
{
	string ToString();
	void SendCommand(string cmd,Device device);
	void Destroy();
	//IDriver Create(string ip);
}

