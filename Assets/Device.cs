﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using System.Collections.Concurrent;
//using System.Collections.Concurrent;
//using System.Collections.Concurrent;

[System.Serializable]
public class Device 
{
	public enum Command 
	{
		AddDevice = 0,
		Message = 1,
		SetParameter = 2,
		YourIP = 3,
		BeginLoadDB = 4,
		EndLoadDB = 5,
		RemoveDevice = 6,
		ConfigsList = 7,
		Ping = 8,
		PingReturn = 9
	}

	[System.Serializable]
	public class Message
    {
        public Message(Device device, Device.Parameter par, string cmd)
        { this.device = device; this.par = par; this.cmd = cmd; }
        public Message(List<byte> simpleCmd)
        { this.simpleCmd = simpleCmd; }
        [System.NonSerialized] public Device            device;
		[System.NonSerialized] public Device.Parameter  par;
                               public string            cmd;
                               public List<byte>        simpleCmd = new List<byte>(); // cmd used with Simple
        public override string ToString()
        {
            if (simpleCmd.Count >= 3)
            {
                string s = "cmd="+simpleCmd[0] + " id="+((simpleCmd[1]<<8)+simpleCmd[2])+" value=";
                for (var index = 3; index < simpleCmd.Count; index++)
                {
                    var i = simpleCmd[index];
                    s += i + ", ";
                }
                return s;
            }
            return cmd;
        }
    }

    [System.Serializable]
	public class Parameter
	{
		public string name;
		public string type;
		public string value;
		public Parameter(string name, string type, string value)
		{
			this.name = name; this.type = type; this.value = value;
		}
		public override string ToString ()
		{			
			return name+ (name==type?"":"("+type+")") +"="+value;
		}
	}

    public int id;
    public List<Parameter> parameters = new List<Parameter>();
	public IDriver driver = null;
    public ConcurrentList<Message> messages = null;
    public ConcurrentList<Message> messagesSimple = null;
    public bool isClientStopSetMessages = false;
	//public List<Message> messages = null;

	public void Clear()
	{
		if ( GetValueByName("Тип")== "CLIENT" ) 
			ClientServer.RemoveConnectionByIP( GetValueByName("IP") );
		if ( parameters != null ) parameters.Clear();
        if (messages != null) messages.Clear();
        if (messagesSimple != null) messagesSimple.Clear();
        if ( driver != null ) driver.Destroy();
		driver = null;
	}

	public void AddParameter(string name, string type, string value)
	{
		parameters.Add(new Parameter(name,type,value));		
	}

	public override string ToString ()
	{
		string result = "Id=" + id + "";
        foreach (Parameter par in parameters)
		{
			if ( result.Length > 0 ) result +=",";
			result+=par.ToString();
		}
		return result;
	}

	public string ToStringEx ()
	{
		string result = "Id="+id+"";
		foreach(Parameter par in parameters)
		{
			if ( result.Length > 0 ) result +=",";
			result+=par.ToString();
		}
		if ( driver != null ) result += ",driver="+driver.ToString();
		if ( messages != null ) result += ",messages count="+messages.Count;
		if ( GetValueByName("Тип") == "SERVER" )
		{
			result += ClientServer.GetConnectionInfo();
		}
		//else result += ", messages is null";
		return result;
	}

	public string GetValueByName (string name)
	{
		foreach(Parameter par in parameters)
			if ( par.name == name ) return par.value;
		//Debug.LogWarning("Device.GetValueByName: Name="+name+", "+this.ToString());
		return "";
	}

	public string GetValueByType (string type)
	{
		foreach(Parameter par in parameters)
			if ( par.type == type ) return par.value;
		return "";
	}
	
	
	public List<Parameter> GetParametersByType (string type)
	{
		List<Parameter> pars = new List<Parameter>();
		foreach(Parameter par in parameters)
			if ( par.type == type ) pars.Add(par);
		return pars;
	}

	public Parameter SetValueByName( string value, string name )
	{
		foreach(Parameter par in parameters)
			if ( par.name == name ) { par.value = value; return par; }
		return null;
	}

	public Parameter GetParameterByName (string name)
	{
		foreach(Parameter par in parameters)
			if ( par.name == name ) return par;
		return null;
	}

	public bool Parse( string deviceString )
	{
		//List<Parameter> parameters = new List<Parameter>();
		string[] parStrings = deviceString.Split(',');
		if ( parStrings.Length == 0 )
		{
			Debug.LogError("Device.Parse: wrong deviceString="+deviceString);
			return false;
		}
		foreach( string parString in parStrings )
		{
			Parameter par = NewParameterFromString( parString );
			if ( par == null ) {
				Debug.LogError("Device.Parse: wrong deviceString="+deviceString+", par="+parString);
				return false; }
		
			parameters.Add(par);	
		}
		return true;
	}

	public Parameter NewParameterFromString( string par )
	{
		int typeDescIndex = par.IndexOf('(');
		int valueIndex = par.IndexOf('=');
		if ( valueIndex < 0 )
		{
			Debug.LogError("Device.Parse: wrong parString="+par+", par="+par);
			return null;
		}
		string name, type, value;
		if ( typeDescIndex < 0 ) {
			name = par.Substring(0, valueIndex);
			type = name;
			int spaceIndex = type.IndexOf(' ');
			if ( spaceIndex >= 0 ) type = type.Substring(0, spaceIndex);
		}
		else
		{
			name = par.Substring(0, typeDescIndex);
			if ( valueIndex-typeDescIndex-2 < 0 ) throw new UnityException("Device.NewParameterFromString: device \""+this.ToString()+"\" par \""+par+"\"");
			type = par.Substring(typeDescIndex+1, valueIndex-typeDescIndex-2);
		}
		value = par.Substring(valueIndex+1, par.Length-valueIndex-1);
		//Debug.Log("par="+par+", name="+name+", desc="+desc+", value="+value);
		return new Parameter(name,type,value);
	}

	public Parameter SetParameterByParameterString( string parString )
	{
		Parameter par = NewParameterFromString( parString );
		if ( par == null ) {
			Debug.LogError("Device.SetParameterByParameterString: wrong parString="+parString);
			return null; }
		foreach(Parameter parSearch in parameters)
		{
			if ( parSearch.name != par.name ) continue;
			//if ( parSearch.type != par.type ) continue;
			parSearch.value = par.value;
			return par;
		}
		return null;		
	}
}