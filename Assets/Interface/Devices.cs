﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Devices : MonoBehaviour {
	public TypeListPopulate populate;

	private void OnEnable()
	{
		if (populate == null ) return;
		populate.OnDeviceButton();
	}
}
