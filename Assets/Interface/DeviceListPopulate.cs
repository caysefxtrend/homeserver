﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DeviceListPopulate : MonoBehaviour {
	public GameObject element;
	public List<GameObject> elements = new List<GameObject>();
	public string filter = "";
	public string cmd = "";
	// Use this for initialization
	void Start () {	}

	private float fixedLastTime = 0;
	// Update is called once per frame
	void Update () {
		if ( Time.fixedTime - fixedLastTime > 1f ) // every second
		{
			if ( Master.IsChanged )
			{
				RefreshByCommand(cmd);
				Master.IsChanged = false;
			}
			fixedLastTime = Time.fixedTime;
		}
	}

	void OnEnable() {
		OnDisable();
		RefreshByCommand(cmd);
//		List<Device> devices = Master.Devices;
//		if ( devices.Count == 0 ) return;
//		foreach( Device device in devices )
//		{
//			GameObject el = GameObject.Instantiate( element );
//			RectTransform rectt = ((RectTransform)el.transform);
//			rectt.SetParent( element.transform.parent, false );
//			el.SetActive( true );
//			elements.Add( el );
//		}
//		RefreshText();
	}
	void OnDisable() {
		foreach(GameObject go in elements)
			Destroy( go );
		elements.Clear();
	}

	void ReCreate(int number)
	{
		if ( number == elements.Count ) return;
		OnDisable();		
		for( int i = 0; i < number; i++ )
		{
			GameObject el = GameObject.Instantiate( element );
			RectTransform rectt = ((RectTransform)el.transform);
			rectt.SetParent( element.transform.parent, false );
			el.SetActive( true );
			elements.Add( el );
		}
	}

//	public void RefreshText()
//	{
//		List<Device> devices = new List<Device>( Master.Devices );
//		if ( filter != "" )
//		{
//			for( int i = devices.Count-1; i >= 0; i-- )
//			{
//				if ( devices[i].GetValueByName("Тип") != filter )
//					devices.RemoveAt(i);
//			}
//		}
//		ReCreate( devices.Count );
//		for( int i = 0; i < devices.Count; i++ )
//		{
//			UnityEngine.UI.Text text = elements[i].GetComponent<UnityEngine.UI.Text>();
//			text.text = devices[i].ToString();
//		}
//	}

	public void RefreshByCommand(string cmd)
	{
		if ( cmd == "" ) { OnDisable(); return; }
		this.cmd = cmd;
		if ( cmd == "OnDeviceButton" )
		{
			List<Device> devices = Master.GetDevicesByType(filter);	
			ReCreate( devices.Count );
			for( int i = 0; i < devices.Count; i++ )
			{
				UnityEngine.UI.Text text = elements[i].GetComponent<UnityEngine.UI.Text>();
				text.text = devices[i].ToStringEx();
			}
		}
		else if ( cmd == "OnLogButton" )
		{
			if ( filter == "Сервер" )
			{
				//Debug.LogWarning("DeviceListPopulate.RefreshByCommand: Сервер");
				List<string> log = HandleLog.GetStringLog();
				int max = log.Count; // count can change in runtime
				ReCreate( max );
				for( int i = max-1; i >= 0; i-- )
				{
					UnityEngine.UI.Text text = elements[max-1-i].GetComponent<UnityEngine.UI.Text>();
					text.text = "["+i+"]"+log[i];
				}
			}
			else if ( filter == "Beckhoff" )
			{
				List<string> log = HandleLog.GetBeckhoffLog();
			//	if ( log.Count > 100 ) Debug.LogWarning("blc="+log.Count);
				int max = log.Count; // count can change in runtime
				ReCreate( max );
				for( int i = max-1; i >= 0; i-- )
				{
					UnityEngine.UI.Text text = elements[max-1-i].GetComponent<UnityEngine.UI.Text>();
					text.text = "["+i+"]"+log[i];
				}
			}			
			else 
			{
				//Debug.LogWarning("DeviceListPopulate.RefreshByCommand: "+filter);
			}
		}
		else Debug.LogWarning("RefreshByCommand: unknown cmd="+cmd);
	}
}
