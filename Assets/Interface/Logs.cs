﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Logs : MonoBehaviour
{
    public Button OnBeckhoff;
    public Button OnLocal;
    public Transform ItemsParent;
    public Transform defaultItem;
    private string _lastCommand = "";
    private List<Text> items = new List<Text>();

    private void Start()
    {
        OnBeckhoff.onClick.AddListener(() => { RefreshByCommand("Beckhoff"); });
        OnLocal.onClick.AddListener(() => { RefreshByCommand("Local");  });
    }

    private void OnEnable()
    {
    }

    private void OnDisable()
    {
    }

    private float fixedLastTime = 0;
    // Update is called once per frame
    private void Update()
    {
        if (Time.fixedTime - fixedLastTime > 1f) // every second
        {
            if (Master.IsChanged)
            {
                RefreshByCommand(_lastCommand);
                Master.IsChanged = false;
            }
            fixedLastTime = Time.fixedTime;
        }
    }

    private void RefreshByCommand(string cmd)
    {
        _lastCommand = cmd;
        if (string.IsNullOrEmpty(_lastCommand)) return;

        List<string> log;
        if (_lastCommand == "Local")
        {
            log = HandleLog.GetStringLog();
        }
        else if (_lastCommand == "Beckhoff")
        {
            log = HandleLog.GetBeckhoffLog();
        }
        else return;
        int max = log.Count; // count can change in runtime
        ReCreate(max);
        for (int i = 0; i < items.Count; i++)
        {
            if (i < max) items[i].text = "[" + i + "]" + log[max-i-1];
            else items[i].text = "";
        }
    }

    void ReCreate(int number)
    {
        if (number <= items.Count) return;
        int need = number - items.Count;
        for (int i = 0; i < need; i++)
        {
            GameObject el = GameObject.Instantiate(defaultItem.gameObject);
            RectTransform rectt = ((RectTransform)el.transform);
            rectt.SetParent(ItemsParent, false);
            el.SetActive(true);
            items.Add(el.GetComponent<Text>());
        }
    }
}
