﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TopMenu : MonoBehaviour {
	public Button Connections;
	public Button Devices;
	public Button Logs;
	public Button Stats;
	public Button Commands;
	public Button Options;

	// Use this for initialization
	void Start () {
		var screens = GameObject.Find("Screens").GetComponent<Screens>();
		Connections.onClick.AddListener( () => { screens.Show(Screens.Types.Connections); } );
		Devices.onClick.AddListener( () => { screens.Show(Screens.Types.Devices); } );
		Logs.onClick.AddListener( () => { screens.Show(Screens.Types.Logs); } );
		Stats.onClick.AddListener( () => { screens.Show(Screens.Types.Stats); } );
		Commands.onClick.AddListener( () => { screens.Show(Screens.Types.Commands); } );
		Options.onClick.AddListener( () => { screens.Show(Screens.Types.Options); } );
	}
}
