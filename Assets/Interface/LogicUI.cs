﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogicUI : MonoBehaviour 
{
	public Transform ItemsParent;
	public LogicItemUI defaultLogicItem;
	public Button OnPlus;
	public Button OnMinus;
	private List<LogicItemUI> items = new List<LogicItemUI>();

	// Use this for initialization
	void Start () {
		Clear();
		//foreach(var item in Master.Connections)
		//	AddItem(item);		
		//OnPlus.OnPointerUp.
	}

	private void Clear()
	{
		defaultLogicItem.gameObject.SetActive(false);
		foreach(var ui in items)
			Destroy(ui.gameObject);
		items.Clear();
	}

	private void AddItem(Connection connection)
	{
		var go = Instantiate(defaultLogicItem);
		go.transform.SetParent(ItemsParent);
		go.transform.localScale = Vector3.one;
		items.Add( go );
        var ui = go.GetComponent<LogicItemUI>();
        ui.Set(connection);
		go.gameObject.SetActive(true);
        ui.OnId.onClick.AddListener(() => { OnId(ui, connection); });
        ui.OnDevice.onClick.AddListener(() => { OnDevice(ui, connection); });
        ui.OnComponent.onClick.AddListener(() => { OnComponent(ui, connection); });
    }

    private void OnId(LogicItemUI ui, Connection connection)
    {
        Debug.LogWarning("OnId: "+connection);
    }

    private void OnDevice(LogicItemUI ui, Connection connection)
    {
        Debug.LogWarning("OnDevice: " + connection);
    }

    private void OnComponent(LogicItemUI ui, Connection connection)
    {
        Debug.LogWarning("OnComponent: " + connection);
    }
}
