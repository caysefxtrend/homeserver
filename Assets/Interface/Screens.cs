﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Screens : MonoBehaviour {

	public enum Types { Connections, Devices, Logs, Stats, Commands, Options }

	public GameObject Connections;
	public GameObject Devices;
	public GameObject Logs;
	public GameObject Stats;
	public GameObject Commands;
	public GameObject Options;

	private void Start()
	{
		HideAll();
	}

	public void Show(Types type)
	{
		HideAll();
		switch(type)
		{
		case Types.Connections:
			Connections.SetActive(true);
			break;
		case Types.Devices:
			Devices.SetActive(true);
			break;
		case Types.Logs:
			Logs.SetActive(true);
			break;
		case Types.Stats:
			Stats.SetActive(true);
			break;
		case Types.Commands:
			Commands.SetActive(true);
			break;
		case Types.Options:
			Options.SetActive(true);
			break;
		}
	}

	private void HideAll()
	{
		foreach(Transform child in transform)
			child.gameObject.SetActive(false);
	}

}
