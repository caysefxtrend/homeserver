﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TypeListPopulate : MonoBehaviour
{
	public DeviceListPopulate devListPopulate;
	public GameObject element;
	public List<GameObject> elements = new List<GameObject>();
	public string cmd = "";

	void OnEnable() {
		OnDisable();
	}
	void OnDisable() {
		foreach(GameObject go in elements)
			Destroy( go );
		elements.Clear();
		cmd = "";
	}

	void OnClick (GameObject el)
	{
		string text = el.GetComponentInChildren<UnityEngine.UI.Text>().text;
		devListPopulate.filter = text;
		devListPopulate.RefreshByCommand(cmd);
	}

	void OnClick (string text)
	{
		devListPopulate.filter = text;
		devListPopulate.RefreshByCommand(cmd);
	}

	public void OnDeviceButton()
	{		
		OnDisable();
		cmd = "OnDeviceButton";
		List<string> types = new List<string>(Master.deviceTypes.Split(','));
		if ( types.Count == 0 ) return;
		foreach( string item in types )
		{
			CreateGO(item);
		}
	}

	private void CreateGO(string textValue)
	{
		GameObject el = GameObject.Instantiate( element );
		RectTransform rectt = ((RectTransform)el.transform);
		rectt.SetParent( element.transform.parent, false );
		UnityEngine.UI.Text text = el.GetComponentInChildren<UnityEngine.UI.Text>();
		text.text = textValue;
		UnityEngine.UI.Button button = el.GetComponent<UnityEngine.UI.Button>();
		button.onClick.AddListener(  () => {OnClick(textValue);} );
		el.SetActive( true );
		elements.Add( el );
	}

	public void OnLogButton()
	{
		OnDisable();
		cmd = "OnLogButton";
		CreateGO("Beckhoff");
		CreateGO("Сервер");	
	}
}

