﻿using UnityEngine;
using UnityEngine.UI;

public class LogicItemUI : MonoBehaviour {

	[SerializeField] private Connection _connection;
	[SerializeField] private Text Id = null;
    [SerializeField] private Text DeviceRoom = null;
    [SerializeField] private Text DeviceType = null;
    [SerializeField] private Text DeviceName = null;
    [SerializeField] private Text Component = null;

	[SerializeField] public Button OnId;
	[SerializeField] public Button OnDevice;
	[SerializeField] public Button OnComponent;


	// Use this for initialization
	public void Set (Connection connection) 
	{
		_connection = connection;	
		Id.text = _connection.Id.ToString();
        Component.text = _connection.Component;
        var device = Master.GetDeviceByName(_connection.Device);
        if (device == null) DeviceName.text = "not found device " + _connection.Device;
        else
        {
            DeviceRoom.text = device.GetValueByName("Комната");
            DeviceType.text = device.GetValueByName("Тип").ToLower();
            DeviceName.text = _connection.Device;
        }
	}

}
