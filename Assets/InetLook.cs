﻿using UnityEngine;
using System.Collections;
using System.Net;
using UnityEngine.UI;

public class InetLook : MonoBehaviour {

	public static InetLook instance = null;

	public enum Status { Disconnected, Unpaid, Ok };

	public Text textUI;

	public Status status = Status.Ok;
	public float statusMinutes = 0;
	private float changeStatusTime = 0;

	public string megafonSite = "megafon.ru";
	public string otherSite1 = "ya.ru";
	public string otherSite2 = "google.com";
	public float look_interval = 60;
	private float lastLookTime = 0;
	private Ping megafonPing = null;
	private Ping other1Ping = null;
	private Ping other2Ping = null;
	[SerializeField] string log = "";

	public static float DisconnectedTime{ get 
		{
			if ( InetLook.instance == null ) return 0;
			if ( InetLook.instance.status != Status.Disconnected ) return 0;
			return InetLook.instance.statusMinutes;
		}
	}

	void Awake() { InetLook.instance = this; }

	void Start () {
		SetStatus(Status.Ok);
	}
	
	// Update is called once per frame
	void Update () {
		statusMinutes = (Time.time - changeStatusTime)/60;
		if ( Time.time - lastLookTime > look_interval ) // every second
		{
			if ( megafonPing == null && other1Ping == null && other2Ping == null )
			{
				//status = Status.Ok;
				StartLooking();
				if ( megafonPing == null && other1Ping == null && other2Ping == null )
					SetStatus( Status.Disconnected );
				lastLookTime = Time.time;
			}
			else 
			{
				int megafonTime = -1, other1Time = -1, other2Time = -1;
				if ( megafonPing != null ) 
					{ megafonTime = megafonPing.time; megafonPing.DestroyPing(); }
				if ( other1Ping != null ) 
					{ other1Time = other1Ping.time; other1Ping.DestroyPing(); }
				if ( other2Ping != null ) 
					{ other2Time = other2Ping.time; other2Ping.DestroyPing(); }
				if ( megafonTime == -1 && other1Time == -1 && other2Time == -1 )
					SetStatus( Status.Disconnected );
				else if ( megafonTime != -1 && other1Time == -1 && other2Time == -1 )
					SetStatus( Status.Unpaid );
				else SetStatus( Status.Ok );
				megafonPing = null; other1Ping = null; other2Ping = null;
				log = megafonTime+"+"+other1Time+"+"+other2Time;
			}
		}
	}

	public string today 
	{ 
		get 
		{
			string now = "";
			System.DateTime time = System.DateTime.Now;
			now += time.Year+"."+time.Month+"."+time.Day;
			now += " "+time.Hour+":"+time.Minute+":"+time.Second;
			return now;
		}
	}

	private void SetStatus( Status newStatus )
	{
		if ( status != newStatus ) 
		{ 
			status = newStatus; 
			changeStatusTime = Time.time; 
			statusMinutes = 0; 
			Debug.LogWarning( "статус интернета: "+status.ToString() );
			//Master.SetPar("Сервер","Бекоф подключён", status?"1":"0" );	
			Master.SetInternetStatus(newStatus==Status.Disconnected?0:newStatus==Status.Ok?1:2);
		}
		if ( textUI != null ) 
		{
			if ( status == Status.Ok ) textUI.text = "Интернет в порядке уже "+(int)statusMinutes+" минут";
			else if ( status == Status.Unpaid ) textUI.text = "Интернет не оплачен уже "+(int)statusMinutes+" минут";
			else if ( status == Status.Disconnected ) textUI.text = "Интернет отключён уже "+(int)statusMinutes+" минут";
		}
	}

	public void StartLooking()
	{
		string megafon_ip = SiteToIp( megafonSite );
		if ( megafon_ip.Length != 0 ) megafonPing = new Ping(megafon_ip);
		string other1_ip = SiteToIp( otherSite1 );
		if ( other1_ip.Length != 0 ) other1Ping = new Ping(other1_ip);
		string other2_ip = SiteToIp( otherSite2 );
		if ( other2_ip.Length != 0 ) other2Ping = new Ping(other2_ip);
		//Debug.Log(megafon_entry.AddressList[0]+"+"+megafon_entry.AddressList[1]);		
	}

	private string SiteToIp( string site )
	{		
		IPHostEntry entry;
		try {
			entry = Dns.GetHostEntry(site);
		} catch { return ""; }

		if ( entry.AddressList.Length == 0 ) return "";
		if ( entry.AddressList.Length == 1 ) return entry.AddressList[0].ToString();
		foreach(IPAddress addr in entry.AddressList)
		{
			if ( addr.ToString().Contains(".") ) 
				return addr.ToString();
		}
		return entry.AddressList[0].ToString();
	}
}
